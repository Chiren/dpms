<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Employer Jobs Controller.
 * Author 	: Xeme IT Solutions
 * Version 	: 1.0
 */

class Emp_jobs extends MY_Controller {
	public function __construct()
	{
		// Check user group and redirect on load of accreditation Controller 

		parent::__construct();
		$this->load->library('session');
		$user_info=$this->session->userdata();
		if($user_info['user_group'][0]->group_id!=5)
		{
			redirect('');
		}
		$this->load->helper('accreditation');
		$y=$this->session->userdata();
		$request_status=check_request($y['user_id']);
		if($request_status[0]->status=='approved')
		{
			//redirect('account');
		}
		elseif($request_status[0]->status=='pending')
		{
			redirect('account');
		}
	}
	public function index()
	{
		echo "<h1>Test</h1>";
	}
	public function add_jobs()
	{
		$this->render('employer/add_new_job');
	}
	public function get_all_categories()
	{
		$this->load->model('emp_jobs_m');
		$data=$this->emp_jobs_m->get_all_cat_m();
		foreach($data as $d)
		{
			print_r("<option value=".$d->cat_id.">".$d->category_name."</option>");
		}
	}
	public function get_skills($id)
	{
		$this->load->model('emp_jobs_m');
		$data=$this->emp_jobs_m->get_skills_m($id);
		foreach($data as $d)
		{
			print_r("<label><input type='checkbox' value=".$d->skill_id." name='skills[]'>'".$d->skill_name."'</label>");
		}
	
	}
	public function add_job_process()
	{
		$this->load->helper(array('form', 'url'));
		$config['upload_path'] = 'uploads';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '100';
		$config['max_width'] = '1024';
		$config['max_height'] = '768';

		$this->load->library('upload', $config);
		if($this->upload->do_upload())
		{
			$finfo=$this->upload->data();
			//print_r($finfo);
			$data=$_POST;
			$file=array('file_name'=>$finfo['file_name']);
			$result=array_merge($data,$file);
			//print_r($result);
			$title=$this->input->post('title');
			$string = str_replace(' ', '-', $title); // Replaces all spaces with hyphens.
			$slug=preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
			$this->load->model('emp_jobs_m');
			$response=$this->emp_jobs_m->add_job_m(json_encode($result),strtolower($slug));
			if($response==1)
			{
				$this->session->set_flashdata('job_post_success', "Successfully you have Posted Job");
				redirect('account');
			}
			else
			{
				$this->session->set_flashdata('error', "Your Data Can't  saved");
				$this->render('employer/add_new_job');
			}
		}
		else 
		{
			$file_error=$this->upload->display_errors();
			$this->session->set_flashdata('file_error', $file_error);
			$this->render('employer/add_new_job');
		}
	}
	public function applications()
	{ 
		$this->load->model('emp_jobs_m');
		$user_id=$this->session->userdata();
		$user=$user_id['user_id'];
		$data=$this->emp_jobs_m->get_applications($user);
		//print_r($data);
		$this->mViewData['result'] = $data;
		$this->render('employer/all_applications');
	}
	public function employer_applications($slug)
	{
		$this->load->model('emp_jobs_m');
		$data=$this->emp_jobs_m->emp_applications($slug);

		if(!empty($data))
		{
			$this->mViewData['result'] = $data;
			$this->render('employer/emp_applications');
		}
		else
		{
			redirect('account');
		}
	}
	public function approve_application($id)
	{
	
		$this->load->model('emp_jobs_m');
		$data=$this->emp_jobs_m->approve_applications_m($id);
		if($data==1)
		{
			$this->session->set_flashdata('approve', ' Approved Application');
			echo "<script>history.go(-1);</script>";
		}
		else
		{
			$this->session->set_flashdata('error', ' Can not approve');
			echo "<script>history.go(-1);</script>";
		}
	}
	public function ignore_application($id)
	{
	
		$this->load->model('emp_jobs_m');
		$data=$this->emp_jobs_m->ignore_applications_m($id);
		if($data==1)
		{
			$this->session->set_flashdata('approve', ' Ignored Application');
			echo "<script>history.go(-1);</script>";
		}
		else
		{
			$this->session->set_flashdata('error', ' Can not ignore');
			echo "<script>history.go(-1);</script>";
		}
	}
}