<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Jobs Public Controller.
 * Author 	: Xeme IT Solutions
 * Version 	: 1.0
 */

class Jobs extends MY_Controller {
	
	public function index()
	{
		$this->load->model('jobs_m');
		$data=$this->jobs_m->all_jobs();
		$this->mViewData['result'] = $data;
		$this->render('jobs/all_jobs');
		
	}
	public function single($slug)
	{
		$this->load->model('jobs_m');
		$data=$this->load->jobs_m->single_jobs_m($slug);
		if(!empty($data))
		{
			$this->mViewData['result'] = $data;
		}
		else 
		{
			redirect('404');
		}
		$this->mTitle=str_replace('-',' ',ucwords($slug));
		$this->render('jobs/single_jobs');
	}
	public function apply($slug)
	{
		
		if(!$this->ion_auth->logged_in())
		{
			redirect('login');
		}
		else
		{
			$group=$this->session->userdata('user_group');
			if($group[0]->group_id == 6)
			{
				$this->load->model('jobs_m');
				$data=$this->jobs_m->apply_m($slug);
				//print_r($data);
				$this->session->set_flashdata('job_post_success','You have successfully applied for job');
				redirect('account');
				
			}
			else
			{
				redirect('login');
			}
		}
	}
}
?>