<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * How It Works Controller
 * Author	: Xeme IT Solutions
 * Version	: 1.0
 */

class How_it_works extends MY_Controller {

	public function for_professionals()
	{
		$this->render('howitworks/professionals');  /* Calling View for professionals page */
	}
	public function for_organizations()				
	{
		$this->render('howitworks/organizations');	/* Calling View for organizations page */
	}
	public function for_enterprises()				
	{
		$this->render('howitworks/enterprises');	/* Calling View for enterprises page */
	}
}
