<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Accreditation Controller.
 * Author 	: Xeme IT Solutions
 * Version 	: 1.0
 */

class Accreditation extends MY_Controller {
	public function __construct()
	{
		/* Check user group and redirect on load of accreditation Controller */
		parent::__construct();
		$this->load->library('session');
		$user_info=$this->session->userdata();
		if($user_info['user_group'][0]->group_id!=5)
		{
			redirect('');
		}
		/* End of checking user group */
		
		/* Request status check */
		$this->load->helper('accreditation');
		$y=$this->session->userdata();
		$request_status=check_request($y['user_id']);
		if($request_status[0]->status=='approved')
		{
			redirect('account');
		}
		elseif($request_status[0]->status=='pending')
		{
			redirect('account');
		}
		/*End Request status check */
		
	}
	
	public function index()
	{
		/* Accreditation will redirect to section-1 */
			redirect('accreditation/section-1');
	}
	public function section_1()
	{
		
		$this->load->library('session');
		$user_info=$this->session->userdata();
		if($user_info['user_group'][0]->group_id!=5) /* Check user group */
		{
			redirect('');
		}
		else
		{
			$this->load->model('accreditation_m');
			$res=$this->accreditation_m->section_1(1);  /* Calling model function */
			$data['result']=$res;						/* Setting result in array for sending that in view */
			$this->mViewData['result'] = $data; 		/* Send result array to view */
			$this->render('accreditation/section1');	/* Load view of section 1 */
		}
			
	}
	public function section_2()
	{
		$this->load->library('session');
		$user_info=$this->session->userdata();
		if($user_info['user_group'][0]->group_id!=5)	/* Check user group */
		{
			redirect('');
		}
		else
		{
			$this->load->model('accreditation_m');
			$res=$this->accreditation_m->section_1(2);	/* Calling model function */
			$data['result']=$res;						/* Setting result in array for sending that in view */
			$this->mViewData['result'] = $data;			/* Send result array to view */
			$this->render('accreditation/section1');	/* Load view of section 2*/
	
		}
	}
	public function section_3()
	{
		$this->load->library('session');
		$user_info=$this->session->userdata();
		if($user_info['user_group'][0]->group_id!=5)	/* Check user group */
		{
			redirect('');
		}
		else
		{
	
			$this->load->model('accreditation_m');		
			$res=$this->accreditation_m->section_1(3);	/* Calling model function */
			$data['result']=$res;						/* Setting result in array for sending that in view */
			$this->mViewData['result'] = $data;			/* Send result array to view */
			$this->render('accreditation/section1');	/* Load view of section 3*/
		}
	}
	/* Un used function */
	public function section_4()
	{
		$this->load->library('session');
		$user_info=$this->session->userdata();
		if($user_info['user_group'][0]->group_id!=5)
		{
			redirect('');
		}
		else
		{
	
			$this->render('accreditation/section4');
		}
	}
	/* End un used function */
	public function save_section_1()
	{
		$ans=$this->input->post();		/* Get all form inputs values as array */
		$this->load->library('session'); 
		$this->session->set_userdata('accreditation_ans_1',$ans);  /* set input values array in session */
		redirect('accreditation/section-2'); /* Redirect to section 2 */
	}
	public function save_section_2()
	{
		$ans=$this->input->post();		/* Get all form inputs values as array */
		$this->load->library('session');
		$this->session->set_userdata('accreditation_ans_2',$ans);	/* set input values array in session */
		redirect('accreditation/section-3');	 /* Redirect to section 2 */
	}
	public function save_section_3()
	{
		$ans=$this->input->post();		/* Get all form inputs values as array */
		$this->load->library('session');
		$this->session->set_userdata('accreditation_ans_3',$ans);	/* set input values array in session */
		redirect('accreditation/final_accreditation');	 /* Redirect to section 2 */
	}
	public function final_accreditation()
	{
		$this->load->library('session');
		/* Get data which is saved in session */
		$user_info=$this->session->userdata();
		$ans=array(
			'section1'=>$this->session->userdata('accreditation_ans_1'),
			'section2'=>$this->session->userdata('accreditation_ans_2'),
			'section3'=>$this->session->userdata('accreditation_ans_3'),
			);
		/* End Get data which is saved in session */
		
		$data=array(				
				'array'=>serialize($ans),
				'user_id'=>$user_info['user_id'],
				);												/* set Data in array with serialize form*/
		$this->load->model('accreditation_m');
		$this->accreditation_m->save_test($data);
		$this->session->unset_userdata('accreditation_ans_1');		/* Unset Data from session */
		$this->session->unset_userdata('accreditation_ans_2');		/* Unset Data from session */
		$this->session->unset_userdata('accreditation_ans_3');		/* Unset Data from session */
		redirect('account');										/* Redirect to user account */
	}
	
}
