<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Notifications Controller.
 * Author 	: Xeme IT Solutions
 * Version 	: 1.0
 */

class Notifications extends MY_Controller {
	
	public function index()
	{
		$this->load->model('notifications_m');
		$data=$this->notifications_m->get_all_notifications();
		$this->mViewData['result'] = $data;
		$this->render('notifications/all_notifications');
	}
	public function header_notifications($user_id)
	{
		$this->load->model('notifications_m');
		$data=$this->notifications_m->header_notification($user_id);
		foreach ($data as $d)
		{
			$temp=json_decode($d->data);
			echo "<li style='border-bottom: 1px solid rgb(204, 204, 204);'> <a href='notifications' style='color: #000 !important'>$temp->data</a></li><br>";
		}
	}
}