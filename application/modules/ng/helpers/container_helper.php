<?php 


function get_job_name_by_con($id)
{
	$ci=&get_instance();
	$sql="SELECT * FROM job_master join container_job ON  container_job.job_id=job_master.job_id WHERE container_job.container_id=".$id." order by container_job.container_job_id ASC";
	$query=$ci->db->query($sql);
	return $query->result();
}

function get_job_status($id)
{
	$ci=&get_instance();
	$sql="SELECT * FROM jobscreen_master join container_job ON  container_job.container_job_id=jobscreen_master.container_job_id WHERE container_job.container_id=".$id." order by container_job.container_job_id ASC";
	$query=$ci->db->query($sql);
	return $query->result();
}

function get_completed_date($id)
{
	$ci=&get_instance();
	$sql="SELECT * FROM jobscreen_master join container_job ON container_job.container_job_id=jobscreen_master.container_job_id WHERE container_job.container_id=".$id." ORDER BY jobscreen_master.wh_updated DESC LIMIT 1";
	$query=$ci->db->query($sql);
	return $query->result();
}

function completed_container()
{
	$ci=&get_instance();
	$sql="SELECT container_master.* FROM container_master join container_job on container_job.container_id=container_master.container_id where container_master.con_status='4' AND container_job.ng='1' AND MONTH(container_master.completed_date) = MONTH(NOW()) AND YEAR(container_master.completed_date) = YEAR(NOW()) GROUP BY container_id";
	$query=$ci->db->query($sql);
	return $query->result();
}

?>