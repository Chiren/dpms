<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}
	public function index()
	{
		$this->mTitle = "Account Settings";
		$form1 = $this->form_builder->create_form('ng/account/update_info');
		$this->mViewData['form1'] = $form1;
		$form2 = $this->form_builder->create_form('ng/account/change_password');
		$this->mViewData['form2'] = $form2;
		$this->render('account');
	}
	public function update_info()
	{
		$data = $this->input->post();
		if ($this->ion_auth->update($this->mUser->id, $data))
		{
			$messages = $this->ion_auth->messages();
			$this->system_message->set_success($messages);
		}
		else
		{
			$errors = $this->ion_auth->errors();
			$this->system_message->set_error($errors);
		}
		redirect('ng/account');
	}

	public function change_password()
	{
		$data = array('password' => $this->input->post('new_password'));
		if ($this->ion_auth->update($this->mUser->id, $data))
		{
			$messages = $this->ion_auth->messages();
			$this->system_message->set_success($messages);
		}
		else
		{
			$errors = $this->ion_auth->errors();
			$this->system_message->set_error($errors);
		}
		redirect('ng/account');
	}
	
	public function photo()
	{
		$this->load->helper(array('form', 'url')); 
		$this->load->helper('file');
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'png|jpg|jpeg';
		
		$config['file_name'] = date('YmdHisu').rand(0,100);
	 	$this->load->library('upload',$config);
		
	    $this->upload->do_upload('image');
		$image=$this->upload->data();
		$img = $image['file_name'];
		
		$ci = &get_instance();
		$ci -> load -> library('session');
		$x = $ci -> session -> userdata('user_group');
		$y = $ci -> session -> userdata();
		$users = $y['user_id'];
		$this->load->model('message_m');
		
		$data = $this->message_m->update_image($users,$img);
		
		redirect('ng/account');	
	}
	
	public function logout()
	{
		$this->ion_auth->logout();
		redirect('admin/login');
	}
}