<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}
	public function tpd()
	{
		$crud = $this->crud->generate_crud('tpd_master');
		$this->mTitle = 'TPD';
		$this->mViewData['crud_data'] = $this->crud->render();
		$this->render('crud');
		
	}
	public function container_type()
	{	
		$crud = $this->crud->generate_crud('container_type_master');
		$this->mTitle = 'Container Type';
		$this->mViewData['crud_data'] = $this->crud->render();
		$this->render('crud');
	}
	
	public function defects()
	{
		$crud = $this->crud->generate_crud('defects_master');
		$this->mTitle = 'Defects Code';
		$this->mViewData['crud_data'] = $this->crud->render();
		$this->render('crud');
	}
	
	
	public function import()
	{
		
		include 'PHPExcel/IOFactory.php';
		$inputFileName = FCPATH."Defects Code.xls"; 
		
		try 
		{
			$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
		} 
		catch(Exception $e) 
		{
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}

		$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
		$arrayCount = count($allDataInSheet);
		
		for($i=3;$i<=78;$i++) 
		{
			$code = trim($allDataInSheet[$i]["H"]);
			$name = trim($allDataInSheet[$i]["I"]);
			
			$this->load->model('container_model');
			$inserdata=$this->container_model->insert_deficts($code,$name);	
					
		}	
	}

	public function get_defacts()
	{
		$chk = $this->input->post('keyword');
		
		$this->load->model('General_model');
		$data = $this->General_model->get_defacts($chk);
		
		if(!empty($data))
		{
			print_r("<ul id='country-list'>");
			foreach($data as $d)
			{
				$dd=$d->defects;
				?>
				<li onClick="selectCountry('<?php echo $dd; ?>');"><?php echo $dd; ?></li>
				<?php
				
			}	
			print_r("</ul>");
			
		}
		
		
	}
	
	public function get_defacts1()
    {
        $chk = $_GET['term'];
        $data1=array();
        $this->load->model('General_model');
        $data = $this->General_model->get_defacts($chk);
        foreach($data as $row)
        {
        	$data1[] = $row->defects;
    	}
   		echo json_encode($data1); 
    }
	
	
	
}
