<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">	
	<base href="<?php echo $base_url; ?>" />
	
	<?php // Meta data ?>
	<title><?=$this->e($title)?></title>
	<meta name='author' content='Michael Chan (https://github.com/waifung0207)'>
	<meta name='description' content='CI Bootstrap 3'>
	<?=$this->section('meta')?>

	<?php // Scripts at page start ?>
	<script src='<?php echo dist_url('adminlte.min.js'); ?>'></script>
	<script src='<?php echo dist_url('admin.min.js'); ?>'></script>
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<?=$this->section('scripts_head')?>

	<?php // Scripts and Stylesheets for Grocery CRUD ?>
	<?=$this->section('scripts_crud')?>

	<?php // Stylesheets ?>
	<link href='<?php echo dist_url('adminlte.min.css'); ?>' rel='stylesheet'>
	<link href='<?php echo dist_url('admin.min.css'); ?>' rel='stylesheet'>
	<?=$this->section('styles')?>
	<style>
			.input-xs {
			  height: 25px;
			  padding: 2px 5px;
			  font-size: 15px;
			  line-height: 1.5; /* If Placeholder of the input is moved up, rem/modify this. */
			}
			.sidebar-mini.sidebar-collapse .main-sidebar
			{
				position: fixed;
			}
	</style>
</head>	
<body class="<?php echo $body_class; ?> sidebar-collapse sidebar-mini">
	<script>
     		var popup = setInterval(function()
     		{
          		$('#popup').load('<?php echo base_url(); ?>tpd95qa/general/popup');
     		}, 4000); 
		</script>
	<div id="popup">
	</div>
	<script>
		function mypopup() { 
  			$('#popup').load('<?php echo base_url(); ?>tpd95qa/general/popup');
		}
		var refresh = setInterval("mypopup()", 30000);
	</script>
	
	<?php // Main content (from inner view, or nested layout) ?>
	<?=$this->section('content')?>

	<?php // Scripts at page end ?>
	<?=$this->section('scripts_foot')?>
	
</body>
</html>