<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jobs extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}

	public function index()
	{
		echo "Hello Screen";
		die();
	}
	public function add()
	{
		$this->load->model('jobs_model');
		$data = $this->jobs_model->get_all();
		$this->mViewData['result']=$data;
		$this->render('jobs/add');
	}
	public function save()
	{
		$data['job_name'] = $_REQUEST['name'];
		$data['date'] = $_REQUEST['date'];
		$data['fg_code'] = $_REQUEST['fg_code'];
		$data['amount_value'] = $_REQUEST['amount'];
		$this->load->model('jobs_model');
		$id=$this->jobs_model->save($data);
		redirect('management/jobs/add');
	}
	public function view($id)
	{
		echo $id;
	}
	public function edit($id)
	{
		$this->load->model('jobs_model');
		$data = $this->jobs_model->get($id);
		$this->mViewData['result']=$data;
		$this->render('jobs/edit');	
	}
	public function update()
	{
		$_REQUEST['id'];
		$data['job_name'] = $_REQUEST['name'];
		$data['date'] = $_REQUEST['date'];
		$data['fg_code'] = $_REQUEST['fg_code'];
		$data['amount_value'] = $_REQUEST['amount'];
		$this->load->model('jobs_model');
		$id=$this->jobs_model->update($_REQUEST['id'], $data);
		redirect('management/jobs/add');
	}
	public function delete($id)
	{
		$this->load->model('jobs_model');
		$this->jobs_model->delete($id);
		redirect('management/jobs/add');
	}
	
	public function all()
	{
		$this->load->model('jobs_model');
		$data = $this->jobs_model->get_all_jobs();
		$this->mViewData['result']=$data;
		$this->render('jobs/all');
	}
}
