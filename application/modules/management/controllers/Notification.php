<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends Admin_Controller {

	public function index()
	{
		$this->render('notification/index');
	}
	
	public function insert()
	{
		date_default_timezone_set('Asia/Calcutta');
		$date = date('Y-m-d H:i:s');
		
		$data['receiver'] = $this->input->post('user');
		$data['msg'] = $this->input->post('msg');
		$data['date'] = $date;
		$this->load->model('notification_model');
		$id=$this->notification_model->save($data);
		redirect('management/notification');
	}
	
}