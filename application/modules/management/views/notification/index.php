<?php $this->layout('layouts::default') ?>
<?php
$ci=&get_instance();
$ci->load->helper('notification');
?>
<script src="<?php echo base_url(); ?>assets/pci/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/pci/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/pci/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/pci/responsive.bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable({
    	"ordering": false
    });
} );
</script>
<style>
.dataTables_paginate.paging_simple_numbers
{
	text-align: right;
}
.pagination
{
	margin: 0px;
}
.dataTables_filter
{
	text-align: right;
}
</style>
<div class="box" id="auto_load_div">
	<div class="box-body no-padding">
		<br />
		<form action="<?php echo base_url(); ?>management/notification/insert" method="post" enctype="multipart/form-data">
			<div class="col-md-4 form-group">
				<label>User:-</label>
				<select name="user" class="form-control">
					<option value="">Select</option>
					<?php $users = get_user_by_notification(); ?>
					<?php foreach($users as $u) { ?>
					<option value="<?php echo $u->user_id; ?>"><?php echo $u->first_name; ?></option>
					<?php } ?>
				</select>
			</div>
			<div class="col-md-7 form-group">
				<label>Message:-</label>
				<textarea name="msg" class="form-control"></textarea>
			</div>
			<div class="col-md-1 form-group">
				<label>&nbsp;</label><br />
				<button type="submit" class="btn btn-primary">Send</button>
			</div>	
		</form>
		
		<div class="col-md-12">
			<h3>History</h3>
			<hr />
			<table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        		<thead>
					<tr>    
				    	<th>Users</th>
				        <th>Message</th>
				        <th>Status</th>
				        <th>Date of Send</th>
				        <th>Date of Read</th>
				 	</tr>
        		</thead>
				<tfoot>
					<tr>
				    	<th>Users</th>
				        <th>Message</th>
				        <th>Status</th>
				        <th>Date of Send</th>
				        <th>Date of Read</th>
				 	</tr>
				</tfoot>
	        	<tbody>
	        		<?php $popup = get_popup(); ?>
	        		<?php foreach($popup as $p) { ?>
        			<tr>
        				<td><?php $usd = get_user_by_notifica($p->receiver);
									foreach($usd as $us)
									{
										echo $us->first_name;
									} 
        					?>
        				</td>
        				<td><?php echo $p->msg; ?></td>
        				<td><?php if($p->status == '0') { echo "Not Show"; } else { echo "Show"; } ?></td>
        				<td><?php echo $p->date; ?></td>
        				<td><?php if($p->accepted_date == '0000-00-00 00:00:00'){ echo ""; } else { echo $p->accepted_date; } ?></td>			
        			</tr> 
        			<?php } ?>
      			</tbody>
        	</table>	
		</div>
	</div>	
</div>