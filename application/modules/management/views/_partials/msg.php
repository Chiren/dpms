<?php
$ci=&get_instance();
$ci->load->helper('message');
?>			

<a href="#" class="dropdown-toggle" data-toggle="dropdown">
	<i class="fa fa-envelope-o"></i>
    <span class="label label-success" id="countmsg">
    	<?php $num = get_message_num(); echo $num; ?>
    </span>
</a>

<ul class="dropdown-menu">
	<li class="header">You have <?php echo $num; ?> messages</li>
    <li>
    	<ul class="menu">
        	<?php $msg = get_message();
        			foreach($msg as $mm) {
					$mmm = get_innermsg($mm->mid);
					foreach($mmm as $m)
					{
			?>
        	<li>
        		<a href="<?php echo base_url(); ?>management/message/chat/<?php echo $m->sender ?>">
                	<div class="pull-left">
                    	<img src="<?php echo base_url(); ?>uploads/user2-160x160.jpg" class="img-circle" alt="User Image">
                    </div>
                   	<h4>
                    	<?php $usm = get_user_message($m->sender); 
                    			foreach($usm as $um)
								{
									echo $um->first_name;
								}
                    	?>
                   	</h4>
                    <p><?php echo $m->msg; ?></p>
            	</a>
          	</li>
          	<?php }  } ?>
     	</ul>
   	</li>
    <li class="footer"><a href="<?php echo base_url(); ?>management/message">See All Messages</a></li>
</ul>