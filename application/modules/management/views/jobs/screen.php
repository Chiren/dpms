<?php $this->layout('layouts::default') ?>
<?php
$ci=&get_instance();
$ci->load->helper('common');
?>
<div class="text-right">
	<select id="tpdselect">
		<option value="all">All</option>
		<option value="60">Tpd-60</option>
		<option value="95">Tpd-95</option>
		<option value="100">Tpd-100</option>
		<option value="NG">Tpd-NG</option>
	</select>
</div>
<script>
	$(function(){
		$("#tpdselect").change(function(){
			var tpd = $("#tpdselect").val();
			$.ajax({
				'url':'<?php echo base_url();?>management/jobscreen/tpd100get/'+tpd,
				'type':'POST',
				'success':function(message)
				{	
					$("#maindiv").html(message);	
				}
				});
			});
	});
</script> 
<div class="box" id="maindiv">
	<?php include('inner.php'); ?>	
</div>