<?php $this->layout('layouts::default') ?>
<style>
.input-xs {
  height: 25px;
  padding: 2px 5px;
  font-size: 15px;
  line-height: 1.5;
}
</style>

<div class="row">
<div class="col-lg-5">
	<div class="box box-primary">
    	<form action="jobs/update">
        	<div class="box-header with-border">
            	<h3 class="box-title">Edit Jobs</h3>
            </div>
            <div class="box-body">
            	<?php foreach($result as $r){ ?>
            	<input type="hidden" name="id" value="<?php echo $r->job_id; ?>" />	
            	<div class="form-group">
              		<label>Name</label>
              		<input type="text" name="name" class="form-control input-xs" value="<?php echo $r->job_name; ?>" placeholder="Name" />
              	</div>
              	<div class="form-group">
              		<label>FG Code</label>
              		<input type="text" name="fg_code" class="form-control input-xs" value="<?php echo $r->fg_code; ?>" placeholder="Name" />
              	</div>
              	<div class="form-group">
              		<label>Date</label>
              		<input type="text" name="date" class="form-control input-xs" id="datepicker" value="<?php echo $r->date; ?>" placeholder="Date" />
              	</div>
              	<div class="form-group">
              		<label>Amount Value</label>
              		<input type="text" name="amount" class="form-control input-xs" placeholder="Amount Value" value="<?php echo $r->amount_value; ?>" />
              	</div>          
              	<?php } ?>
            </div>
            <div class="box-footer text-center ">
              	<button class="pull-right btn btn-primary" type="submit">Edit</button>
            </div>
        </form>
	</div>
</div>
</div>

<link rel="stylesheet" href="../assets/plugins/datepicker/datepicker3.css" />
<script src="../assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script>
$(function () {
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
    $(".timepicker").timepicker({
      showInputs: false,
     
    });
  });
</script>
