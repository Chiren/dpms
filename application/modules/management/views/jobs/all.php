<?php $this->layout('layouts::default') ?>
<style>
	.dataTables_filter
	{
		text-align: right;
	}
	.dataTables_paginate.paging_simple_numbers
	{
		text-align: right;
	}
</style>
<div class="row">
	<div class="col-lg-12">
		<div class="box box-primary">
	    	<div class="box-header with-border">
	        	<h3 class="box-title">Jobs List</h3>
	           	<div class="box-tools pull-right">
	               	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
	               	<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
	           	</div>
	       	</div>
        
        	<div class="box-body">
        		<table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
            		<thead>
              			<tr>
	              			<th>Sr</th>
	              			<th>Name</th>
	              			<th>Stock</th>
	              			<th>Date</th>	
	              			<th>Status</th>
	              		</tr>
	              	</thead>
	              	<tbody>
              			<?php $i = 1; foreach ($result as $r) { ?>
						<tr>
	              			<td><?php echo $i; $i++; ?></td>
	              			<td><?php print_r($r->job_name); ?></td>
	              			<td><?php print_r($r->in_stock_qty); ?></td>
	              			<td><?php print_r($r->date); ?></td>
	              			<td><?php 
	              						if($r->status == 0)
										{
											echo "Working";
										} 
										else 
										{
											echo "Completed";
										}
	              				?>
	              			</td>
	              		</tr>	  
					 	<?php } ?>
              		</tbody>
				</table>
			</div>
		</div>
	</div>
</div>	
<script src="<?php echo base_url(); ?>assets/pci/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/pci/dataTables.bootstrap.min.js "></script>
<script>
	$(document).ready(function() {
    $('#example').DataTable();
} );
</script>