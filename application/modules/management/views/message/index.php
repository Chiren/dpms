<?php $this->layout('layouts::default') ?>
<?php
$ci=&get_instance();
$ci->load->helper('message');
?>
<style>
.tab-content{
border-style:none;

}
.nav-tabs{
border-style:none;

}
.btn-primary
{
background-color: #00AFD5;
}
.divwidth {
  width: 350px;
  height: 170px;
  margin: 24px 10px;
  padding: 16px;
  float: left;
}

.people-list {
  width: 260px;
  float: left;
  background-color: #444753;
  color: #fff;
  height: 500px;
  overflow: hidden;
}
.people-list .search {
  padding: 20px;
}
.people-list input {
  border-radius: 3px;
  border: none;
  padding: 14px;
  color: white;
  background: #6A6C75;
  width: 90%;
  font-size: 14px;
}
.people-list .fa-search {
  position: relative;
  left: -25px;
}
.people-list ul {
  padding: 0px 20px;
  height: 499px;
  overflow-y: scroll;
}
.people-list ul li {
  padding-bottom: 20px;
}
.people-list img {
  float: left;
}
.people-list .about {
  float: left;
  margin-top: 8px;
}
.people-list .about {
  padding-left: 8px;
}
.people-list .status {
  color: #92959E;
}

.chat {
  width: 800px;
  float: left;
  background: #F2F5F8;
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
  color: #434651;
}
.chat .chat-header {
  padding: 20px;
  border-bottom: 2px solid white;
}
.chat .chat-header img {
  float: left;
}
.chat .chat-header .chat-about {
  float: left;
  padding-left: 10px;
  margin-top: 6px;
}
.chat .chat-header .chat-with {
  font-weight: bold;
  font-size: 16px;
}
.chat .chat-header .chat-num-messages {
  color: #92959E;
}
.chat .chat-header .fa-star {
  float: right;
  color: #D8DADF;
  font-size: 20px;
  margin-top: 12px;
}
.chat .chat-history {
  padding: 30px 30px 20px;
  border-bottom: 2px solid white;
  overflow-y: auto;
  height: 300px;
 
}
.chat .chat-history .message-data {
  margin-bottom: 15px;
}
.chat .chat-history .message-data-time {
  color: #a8aab1;
  padding-left: 6px;
}
.chat .chat-history .message {
  color: white;
  padding: 18px 20px;
  line-height: 26px;
  font-size: 16px;
  border-radius: 7px;
  margin-bottom: 30px;
  width: 90%;
  position: relative;
}
.chat .chat-history .message:after {
  bottom: 100%;
  left: 7%;
  border: solid transparent;
  content: " ";
  height: 0;
  width: 0;
  position: absolute;
  pointer-events: none;
  border-bottom-color: #86BB71;
  border-width: 10px;
  margin-left: -10px;
}
.chat .chat-history .my-message {
  background: #86BB71;
}
.chat .chat-history .other-message {
  background: #94C2ED;
}
.chat .chat-history .other-message:after {
  border-bottom-color: #94C2ED;
  left: 93%;
}
.chat .chat-message {
  padding: 30px;
}
.chat .chat-message textarea {
  width: 100%;
  border: none;
  padding: 10px 20px;
  font: 14px/22px "Lato", Arial, sans-serif;
  margin-bottom: 10px;
  border-radius: 5px;
  resize: none;
}
.chat .chat-message .fa-file-o, .chat .chat-message .fa-file-image-o {
  font-size: 16px;
  color: gray;
  cursor: pointer;
}
.chat .chat-message button {
  float: right;
  color: #94C2ED;
  font-size: 16px;
  text-transform: uppercase;
  border: none;
  cursor: pointer;
  font-weight: bold;
  background: #F2F5F8;
}
.chat .chat-message button:hover {
  color: #75b1e8;
}

.online, .offline, .me {
  margin-right: 3px;
  font-size: 10px;
}

.online {
  color: #86BB71;
}

.offline {
  color: #E38968;
}

.me {
  color: #94C2ED;
}

.align-left {
  text-align: left;
}

.align-right {
  text-align: right;
}

.float-right {
  float: right;
}

.clearfix:after {
  visibility: hidden;
  display: block;
  font-size: 0;
  content: " ";
  clear: both;
  height: 0;
}
.clearfix
{
	list-style: outside none none;
}
.changecss
{
	color: #fff;
}
.active > .changecss
{
	color: #337ab7;
}
.active div > .changecss
{
	color: #337ab7;
}
.dropdown-menu.inner.selectpicker
{
    height: 250px !important;
}
</style>

<div class="box" id="auto_load_div">
	<div class="box-body">

	<div class="people-list" id="people-list">
		<div class="col-md-6">
        <h3>Chat List</h3>
        </div>
        <div class="col-md-6">
        <button class="btn btn-danger" style="margin-top: 20px;" data-toggle="modal" data-target="#mulchat">Multiple Chat</button>
        <div class="example-modal">
            <div class="modal modal-primary" id="mulchat">
                  <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Multiple Chat</h4>
                              </div>
                              <div class="modal-body">
                               <form role="form" name="container-action" method="post">
                                  <div class="box-body">
                                      <label>Select Users</label>
                                    <div class="form-group">
                                      <script src="<?php echo base_url(); ?>assets/pci/bootstrap-select.min.js"></script>
                                      <script>
                                          $('.selectpicker').selectpicker({
                                     
                                        });
                                      </script>
                                      
                                    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/pci/bootstrap-select.min.css" />
   
                                    <select class="selectpicker" id="msgmuluid" name="msgmuluid" multiple>
                                     <?php $usersm = get_message_user(); ?>
                                            <?php foreach($usersm as $mchat) { ?>
                                             <option value="<?php echo $mchat->id; ?>"><?php echo $mchat->first_name; ?></option>
                                      <?php } ?>
                                    </select>
                                    </div>
                                     <label>Message</label>
                                    <div class="form-group">
                                        <textarea class="form-control" id="mulmsg" name="mulmsg" placeholder="Enter Message"></textarea>
                                    </div>
                                </div>
                            </form>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                                <button type="button" id="mulsend" class="btn btn-outline btnsave" data-dismiss="modal" >Send</button>
                              </div>
                        </div>
                    </div>
                  </div>
            </div>
           <script>
        $(document).ready(function(){       
       
            $("#mulsend").click(function(){                   
                           
                var msg;               
                msg = $("#mulmsg").val();
                var chatuserid=$("#msgmuluid").val();
                if(msg != '')
                {
                     $.ajax({
                       type: "POST",
                       url:BASE_URL + "management/message/mulmsg_save",
                       data: {cmt:msg,chatuser:chatuserid},                 
                       success: function(response){                               
                                                                    
                       },
                       error:function(error)
                       {
                         
                       }
                    });               
                }
                else
                {
                    alert('please enter message');
                }                           
               
            });       
           
        });
    </script>
   
        </div>
    	<ul class="list col-md-12" id="myTab">
    		<?php $musers = get_message_user(); ?>
    		<?php foreach($musers as $mu) { ?>
   			<li class="clearfix">
   				<a data-toggle="tab" class="changecss active<?php echo $mu->id; ?>" onclick="chat(<?php echo $mu->id; ?>)" href="#<?php echo $mu->id; ?>" aria-expanded="false">
          			<img src="<?php if($mu->image == null) { echo base_url().'uploads/2017011210572600000030.png';  } else { echo base_url(); ?>uploads/<?php echo $mu->image; } ?>" alt="avatar" style="border-radius:50px;" width="30" height="30"/>
          			<div class="about">
            			<div class="name">
            				<?php echo $mu->first_name; ?>
            			</div>
           			</div>
        		</a>
        		<div class="col-md-1" style="margin-top: 7px;float: right;">
        			<a data-toggle="tab" class="changecss" onclick="chat(<?php echo $mu->id; ?>)" href="#<?php echo $mu->id; ?>" aria-expanded="false"><i class="fa fa-refresh" aria-hidden="true"></i></a>
        		</div>
        	</li>
        	<?php } ?>
      	</ul>
    </div>
    
    <div class="tab-content">
    	<script>
var BASE_URL = '<?php echo base_url(); ?>';
</script>
<script>
		function chat(id) {
			$.ajax({
					   type: "POST",
					   url: BASE_URL + "management/message/chathistory",
					   data: {id:id},								 
					   success: function(response){					   
							$(".chat-history").html(response);					   				  
					   },
					   error:function(error)
					   {
						  
					   }
					});	
		}
	</script>	
    	<?php if($ii !=0) { ?>
    		<script type="text/javascript">
			$(document).ready(function(){ 
    			$("#myTab li a.active<?php echo $ii; ?>").tab('show');
			});
		</script>
    	<?php } else { ?>	
    	<script type="text/javascript">
			$(document).ready(function(){ 
    			$("#myTab li:eq(0) a").tab('show');
			});
		</script>
		<?php } ?>

   		<?php $muser = get_message_user(); ?>
   		<?php foreach($muser as $mus) { ?>
   		<div class="chat tab-pane fade" id="<?php echo $mus->id; ?>" >
    		<div class="chat-header clearfix">
        		<img src="<?php if($mus->image == null) { echo base_url().'uploads/2017011210572600000030.png';  } else { echo base_url(); ?>uploads/<?php echo $mus->image; } ?>" alt="avatar" style="border-radius:50px;" width="30" height="30"/>
        		<div class="chat-about">
		        	<div class="chat-with">
					<?php echo $mus->first_name; ?>
					</div>
		       	</div>       
      		</div> 		
      		
      		<div class="chat-history">
      			<div class="col-md-12" style="margin-top: 7px;font-family: bold" align="center">Tab To Refresh&nbsp;&nbsp;&nbsp;<a data-toggle="tab" onclick="chat(<?php echo $mus->id; ?>)" href="#<?php echo $mus->id; ?>" aria-expanded="false"><i class="fa fa-refresh" aria-hidden="true"></i></a></div>
			</div>
      		
      		<div class="chat-message clearfix">
      			<textarea name="cmt_data" class="cmt_data<?php echo $mus->id; ?>" id="cmt_data" placeholder="Enter Message" style="width: 90%"></textarea>
      			<input type="hidden" name="cmt_user" id="cmt_user" class="cmt_user<?php echo $mus->id; ?>" value="<?php echo $mus->id; ?>"/>
      			<div style="float: right;margin-top: 20px">
      				<input type="submit" name="btn_cmt" id="btn_cmt" class="btn btn-success btn_cmt<?php echo $mus->id; ?>" value="send"/>
      			</div>	
			</div>
		</div>
		<script>
		$(document).ready(function(){		
		
			$(".btn_cmt<?php echo $mus->id; ?>").click(function(){					
							
				var msg;				
				msg = $(".cmt_data<?php echo $mus->id; ?>").val();
				var chatuserid=$(".cmt_user<?php echo $mus->id; ?>").val();
				if(msg != '')
				{
					 $.ajax({
					   type: "POST",
					   url:BASE_URL + "management/message/msg_save",
					   data: {cmt:msg,chatuser:chatuserid},				  
					   success: function(response){								
							$(".chat-history").append(response);
							$(".cmt_data<?php echo $mus->id; ?>").val('');					   				  
					   },
					   error:function(error)
					   {
						  
					   }
					});				
				}
				else
				{
					alert('please enter message');
				}							
				
			});		
			
		});
	</script>
	
	<?php } ?>
		
    </div>   

   </div>
</div>
			