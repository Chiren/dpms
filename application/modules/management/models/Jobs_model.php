<?php 
class Jobs_model  extends CI_Model  {
	
	function save($data)
	{
		$msg = $this->db->insert('job_master', $data);
		
	}
	function get_all(){
		$this->db->where('status', '0');
		return $this->db->get('job_master')->result();
	}
	function get($id){
		$this->db->where('job_id', $id);
		return $this->db->get('job_master')->result();
	}
	function update($id, $data){
		$this->db->where('job_id',$id);
		return $this->db->update('job_master',$data);
	}
	function delete($id){
		$this->db->where('job_id',$id);
		$this->db->delete('job_master');
	}
	
	function get_all_jobs(){
		return $this->db->get('job_master')->result();
	}
	
}
?>

