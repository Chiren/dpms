<?php
$ci=&get_instance();
$ci->load->helper('common');
?>
<header class="main-header">
	<!--a href="" class="logo"><b><?php echo "KM" ; ?></b>Kennel Master</a-->
	 <a href="<?php echo "#"; ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>PD</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Piramal</b>DPMS</span>
    </a>
	<nav class="navbar navbar-static-top" role="navigation">
		<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</a>
		<h4 style="color:#fff;float:left;font-family:fontAwesome;margin-top: 15px;">Dispatch Planning and Monitoring System (DPMS)</h4>
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<!-- Messages: style can be found in dropdown.less-->
          
           <script>
     		var msg = setInterval(function()
     		{
          		$('#msgid').load('<?php echo base_url(); ?>tpd60qa/general/message');
     		}, 49000); 
			</script>	
          
          <li class="dropdown messages-menu" id="msgid">
            	<?php include('msg.php'); ?>
          </li>
          
       		<script>
			$(function(){
				$("#msgid").click(function(){
					$("#countmsg").hide();
					clearInterval(msg);
					myFunctionmsg();
					$("#countmsg").hide();
					$.ajax({
							'url':"<?php echo base_url(); ?>tpd60qa/general/readmsg/<?php echo $user->id; ?>",
							'success':function(data){
										},
					});
					$("#countmsg").hide();
				});
			});
				function myFunctionmsg() {
				  $('#msgid').load('<?php echo base_url(); ?>tpd60qa/general/message');
				}
				var msgdata = setInterval("myFunctionmsg()", 119000);
			</script>
          
          
          <script>
     		var auto = setInterval(function()
     		{
          		$('#nofication').load('<?php echo base_url(); ?>tpd60qa/general/notification');
     		}, 50000); 
		</script>
          
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu" id="nofication">
            <?php include("notifications.php"); ?>
          </li>
          <script>
$(function(){
	$("#nofication").click(function(){
		$("#count").hide();
		clearInterval(auto);
		myFunction();
		
		$("#count").hide();
		$.ajax({
			'url':"<?php echo base_url(); ?>tpd60qa/general/rednotifications/<?php echo $user->id; ?>",
			'success':function(data){
				},
		});
		$("#count").hide();
	});
});
function myFunction() {
  $('#nofication').load('<?php echo base_url(); ?>tpd60qa/general/notification');
}
var refreshId = setInterval("myFunction()", 120000);
</script>
          <!-- Tasks: style can be found in dropdown.less -->
          <!--li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">3</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 3 Urgent Alerts</li>
              <li>
                
                <ul class="menu">
                  <li>
                    <a href="#">
                      <h3>
                        Get Warehouse Green
                        <small class="pull-right">90</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-red" style="width: 90%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">90% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li-->
         
				<li class="dropdown user user-menu">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<?php $image1 = get_userimage(); foreach ($image1 as $img1) { if($img1->image == null){ ?>	
							<img src="<?php echo base_url(); ?>uploads/2017011207134400000096.png" class="img-circle" alt="User Image" style="height: 25px; width: 25px; margin-right: 10px; margin-top: -2px;">
						<?php } else { ?>
							<img src="<?php echo base_url(); ?>uploads/<?php echo $img1->image; ?>" class="img-circle" alt="User Image" style="height: 25px; width: 25px; margin-right: 10px; margin-top: -2px;">
						<?php } } ?>
						<span class="hidden-xs"><?php echo $user->first_name; ?></span>
					</a>
					<ul class="dropdown-menu">
						<li class="user-header">
							<?php $image1 = get_userimage(); foreach ($image1 as $img1) { if($img1->image == null){ ?>	
						 		<img class="img-circle" src="<?php echo base_url(); ?>uploads/2017011207134400000096.png" alt="User Image">
							<?php } else { ?>
								<img src="<?php echo base_url(); ?>/uploads/<?php echo $img1->image; ?>" class="img-circle" alt="User Image">
							<?php } } ?>
							<p><?php echo $user->first_name; ?></p>
						</li>
						<li class="user-footer">
							<div class="pull-left">
								<a href="account" class="btn btn-default btn-flat">Account</a>
							</div>
							<div class="pull-right">
								<a href="account/logout" class="btn btn-default btn-flat">Sign out</a>
							</div>
						</li>
					</ul>
					<li class="dropdown user user-menu">
					<img src="<?php echo base_url(); ?>uploads/logo/2017-05-182.png" />
				</li>
				</li>
			</ul>
		</div>
	</nav>
</header>