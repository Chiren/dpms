<?php 

function get_containers()
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_master where con_status!='4' ORDER BY createdOn DESC";
	$query=$ci->db->query($sql);
	return $query->result();
}

function get_con_part($id)
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_job join jobscreen_master on jobscreen_master.container_job_id=container_job.container_job_id where container_id=$id";
	$query=$ci->db->query($sql);
	return $query->result();
}

function get_activity()
{
	$ci=&get_instance();
	$sql="SELECT activity.* FROM activity join tpd_master on tpd_master.id=activity.tpd where tpd_master.name ='60' AND MONTH(activity.date) = MONTH(NOW()) AND YEAR(activity.date) = YEAR(NOW()) ORDER BY a_id DESC";
	$query=$ci->db->query($sql);
	return $query->result();
}

function get_user($id)
{
	$ci=&get_instance();
	$sql="SELECT first_name FROM users where id=$id";
	$query=$ci->db->query($sql);
	return $query->result();
}

function get_jobs($id)
{
	$ci=&get_instance();
	$sql="SELECT job_name FROM job_master where job_id=$id";
	$query=$ci->db->query($sql);
	return $query->result();
}

function get_containe($id)
{
	$ci=&get_instance();
	$sql="SELECT container_type FROM container_master where container_id=$id";
	$query=$ci->db->query($sql);
	return $query->result();
}

function get_activitycount()
{
	$ci=&get_instance();
	$sql="SELECT * FROM activity where DATE(date)=DATE(NOW())";
	$query=$ci->db->query($sql);
	return $query->num_rows();
}

function get_tpd60_ansa()
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_master join container_job on container_job.container_id=container_master.container_id join tpd_master on tpd_master.id=container_job.tpd where MONTH(container_master.container_date)=MONTH(NOW()) AND YEAR(container_master.container_date)=YEAR(NOW()) AND container_master.container_name='ANSA' AND tpd_master.name='60'";
	$query=$ci->db->query($sql);
	return $query->result();
}

function get_tpd60_all()
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_master join container_job on container_job.container_id=container_master.container_id join tpd_master on tpd_master.id=container_job.tpd where MONTH(container_master.container_date)=MONTH(NOW()) AND YEAR(container_master.container_date)=YEAR(NOW()) AND tpd_master.name='60'";
	$query=$ci->db->query($sql);
	return $query->result();
}

function get_tpd100ansa($id)
{
	$ci=&get_instance();
	$sql="SELECT * FROM jobscreen_master join container_job on container_job.container_job_id=jobscreen_master.container_job_id where container_job.container_id=$id";
	$query=$ci->db->query($sql);
	return $query->result();
}

function get_total_container()
{
	$ci=&get_instance();
	$sql="SELECT container_id FROM container_master where MONTH(container_date)=MONTH(NOW()) AND YEAR(container_date)=YEAR(NOW())";
	$query=$ci->db->query($sql);
	return $query->result();
}

function get_total_container100($id)
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_job join tpd_master on tpd_master.id=container_job.tpd where tpd_master.name='60' AND container_job.container_id=$id GROUP BY container_job.container_id";
	$query=$ci->db->query($sql);
	return $query->result();
}


function get_total_completedcontainer()
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_master where MONTH(container_date)=MONTH(NOW()) AND YEAR(container_date)=YEAR(NOW()) AND con_status='4'";
	$query=$ci->db->query($sql);
	return $query->result();
}

function get_total_completedcontainer100($id)
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_job join tpd_master on tpd_master.id=container_job.tpd where tpd_master.name='60' AND container_job.container_id=$id GROUP BY container_job.container_id";
	$query=$ci->db->query($sql);
	return $query->result();
}

function get_total_detainingcontainer()
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_master where MONTH(container_date)=MONTH(NOW()) AND YEAR(container_date)=YEAR(NOW()) AND con_status='2'";
	$query=$ci->db->query($sql);
	return $query->result();	
}

function get_total_detainingcontainer100($id)
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_job join tpd_master on tpd_master.id=container_job.tpd where tpd_master.name='60' AND container_job.container_id=$id GROUP BY container_job.container_id";
	$query=$ci->db->query($sql);
	return $query->result();
}
function get_tpd60_ansanew()
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_master join container_job on container_job.container_id=container_master.container_id join tpd_master on tpd_master.id=container_job.tpd join jobscreen_master on jobscreen_master.container_job_id=container_job.container_job_id where MONTH(container_master.container_date)=MONTH(NOW()) AND YEAR(container_master.container_date)=YEAR(NOW()) AND container_job.decoration='1' AND tpd_master.name='60' AND container_job.qty!=jobscreen_master.ansa_completed AND container_master.con_status!='4'";
	$query=$ci->db->query($sql);
	return $query->num_rows();
}

function get_tpd60_allnewqc()
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_master join container_job on container_job.container_id=container_master.container_id join tpd_master on tpd_master.id=container_job.tpd join jobscreen_master on jobscreen_master.container_job_id=container_job.container_job_id where MONTH(container_master.container_date)=MONTH(NOW()) AND YEAR(container_master.container_date)=YEAR(NOW()) AND tpd_master.name='60' AND container_job.qty!=jobscreen_master.qc_completed AND container_job.decoration='0' AND container_master.con_status!='4'";
	$query=$ci->db->query($sql);
	return $query->num_rows();
}
function get_tpd60_allnewqc1()
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_master join container_job on container_job.container_id=container_master.container_id join tpd_master on tpd_master.id=container_job.tpd join jobscreen_master on jobscreen_master.container_job_id=container_job.container_job_id where MONTH(container_master.container_date)=MONTH(NOW()) AND YEAR(container_master.container_date)=YEAR(NOW()) AND container_job.decoration='1' AND tpd_master.name='60' AND container_job.qty!=jobscreen_master.qc_completed AND jobscreen_master.ansa_completed!='0' AND (jobscreen_master.ansa_completed * 100)/container_job.qty > 25 AND jobscreen_master.ansa_completed != jobscreen_master.qc_completed AND container_master.con_status!='4'";
	$query=$ci->db->query($sql);
	return $query->num_rows();
}

function get_tpd60_allnewqa()
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_master join container_job on container_job.container_id=container_master.container_id join tpd_master on tpd_master.id=container_job.tpd join jobscreen_master on jobscreen_master.container_job_id=container_job.container_job_id where MONTH(container_master.container_date)=MONTH(NOW()) AND YEAR(container_master.container_date)=YEAR(NOW()) AND tpd_master.name='60' AND container_job.qty!=jobscreen_master.qa_completed AND jobscreen_master.qa!='0' AND jobscreen_master.qa_completed!=jobscreen_master.qc_completed AND container_master.con_status!='4'";
	$query=$ci->db->query($sql);
	return $query->num_rows();
}
function get_tpd60_allnewwh()
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_master join container_job on container_job.container_id=container_master.container_id join tpd_master on tpd_master.id=container_job.tpd join jobscreen_master on jobscreen_master.container_job_id=container_job.container_job_id where MONTH(container_master.container_date)=MONTH(NOW()) AND YEAR(container_master.container_date)=YEAR(NOW()) AND tpd_master.name='60' AND container_job.qty!=jobscreen_master.wh_completed AND jobscreen_master.wh!='0' AND jobscreen_master.wh_completed!=jobscreen_master.qa_completed AND container_master.con_status!='4'";
	$query=$ci->db->query($sql);
	return $query->num_rows();
}
function get_total_container60()
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_master join container_job on container_master.container_id=container_job.container_id join tpd_master on container_job.tpd=tpd_master.id where MONTH(container_master.container_date)=MONTH(NOW()) AND YEAR(container_master.container_date)=YEAR(NOW()) AND tpd_master.name='60'";
	$query=$ci->db->query($sql);
	return $query->num_rows();
}
function get_total_detainingcontainer60()
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_master join container_job on container_master.container_id=container_job.container_id join tpd_master on container_job.tpd=tpd_master.id where MONTH(container_master.container_date)=MONTH(NOW()) AND YEAR(container_master.container_date)=YEAR(NOW()) AND (container_master.con_status='2' OR container_master.con_status='1') AND tpd_master.name='60'";
	$query=$ci->db->query($sql);
	return $query->num_rows();	
}
function get_total_completedcontainer60()
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_master join container_job on container_master.container_id=container_job.container_id join tpd_master on container_job.tpd=tpd_master.id where MONTH(container_master.container_date)=MONTH(NOW()) AND YEAR(container_master.container_date)=YEAR(NOW()) AND container_master.con_status='4' AND tpd_master.name='60'";
	$query=$ci->db->query($sql);
	return $query->num_rows();
}

function get_total_container60p()
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_master join container_job on container_master.container_id=container_job.container_id join tpd_master on container_job.tpd=tpd_master.id where MONTH(container_master.container_date)=MONTH(NOW()) AND YEAR(container_master.container_date)=YEAR(NOW()) AND tpd_master.name='60' group by container_job.container_id";
	$query=$ci->db->query($sql);
	return $query->num_rows();
}
function get_total_detainingcontainer60d()
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_master join container_job on container_master.container_id=container_job.container_id join tpd_master on container_job.tpd=tpd_master.id where MONTH(container_master.container_date)=MONTH(NOW()) AND YEAR(container_master.container_date)=YEAR(NOW()) AND (container_master.con_status='2' OR container_master.con_status='1') AND tpd_master.name='60' group by container_job.container_id";
	$query=$ci->db->query($sql);
	return $query->num_rows();	
}
function get_total_completedcontainer60c()
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_master join container_job on container_master.container_id=container_job.container_id join tpd_master on container_job.tpd=tpd_master.id where MONTH(container_master.container_date)=MONTH(NOW()) AND YEAR(container_master.container_date)=YEAR(NOW()) AND container_master.con_status='4' AND tpd_master.name='60' group by container_job.container_id";
	$query=$ci->db->query($sql);
	return $query->num_rows();
}


?>