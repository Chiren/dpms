<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Containerscreen extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}
	public function index()
	{
		$this->load->model('containerscreen_model');
		$data = $this->containerscreen_model->get_all();
		$this->mViewData['result']=$data;
		$this->render('container/screen');
	}
	
	public function con()
	{
		$this->load->model('containerscreen_model');
		$data = $this->containerscreen_model->get_all();
		$this->mViewData['result']=$data;
		$this->render('container/container');
	}
	
	public function completed()
	{
		$this->render('container/completed');
	}
	
	
}
