<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$config['site'] = array(

	'name' => 'Piramal CPMS',
	'title' => '',
	'multilingual' => array(),
	'authorized_groups' => array(
		'tpd100wh'		=> array('adminlte_skin' => 'skin-purple')
	),

	'menu' => array(
		'home' => array(
			'groups'	=> array('admin', 'manager', 'staff'),
			'name'		=> 'Home',
			'url'		=> '',
			'icon'		=> 'fa fa-home',
		),
		'container_screen' => array(
			'groups'	=> array('admin'),
			'name'		=> 'Container Screen',
			'url'		=> 'containerscreen/',
			'icon'		=> 'fa fa-truck',
		),
		'job_screen' => array(
			'groups'	=> array('admin'),
			'name'		=> 'Job Screen',
			'url'		=> 'jobscreen',
			'icon'		=> 'fa fa-tachometer',
		),
		
		'Message' => array(
			'groups'	=> array('admin'),
			'name'		=> 'Message', 
			'url'		=> 'message',
			'icon'		=> 'fa fa-commenting-o',				
		),
		
		'logout' => array(
			'groups'	=> array('admin', 'manager', 'staff'),
			'name'		=> 'Sign Out',
			'url'		=> 'account/logout',
			'icon'		=> 'fa fa-sign-out',
		)
	),

	'useful_links' => array(
		
	),

	'debug' => array(
		'view_data'		=> FALSE,	// whether to display MY_Controller's mViewData at page end
		'profiler'		=> FALSE,	// whether to display CodeIgniter's profiler at page end
	),
);