<?php
$ci=&get_instance();
$ci->load->helper('home');
?>
<?php $containers = get_containers(); ?>	
            		<?php foreach($containers as $con) { ?>
            			<tr>
			                <td><?php echo $con->container_type; ?></td>
			                <td><?php echo date ("j M,y",strtotime($con->container_date)); ?></td>
			                <td style="text-align: center">
			                	<?php $today = date("Y-m-d"); ?>
			                	<?php $c_date = date ("Y-m-d",strtotime($con->container_date)); ?>
			                		<?php if($con->con_status == 0) { ?>
			                	    	<a class="label label-warning">Booked</a>
			                	    	<?php } elseif ($con->con_status == 1) { ?>	
			                			
			                				<span class="label label-success">Reported</span>
			                			
											
										<?php } elseif ($con->con_status == 2) { ?>
											<?php 	$date1 = new DateTime($today);
													$date2 = new DateTime($con->chk_date);
													$diff = $date1->diff($date2);
											?>
			                				<span class="label label-danger">Detaining</span> <small>Since <?php printf('%d', $diff->d); ?> Days</small>
			                			<?php } elseif ($con->con_status == 3) { ?>
			                				<span class="label label-success">Canceled</span>	
			                			<?php } elseif ($con->con_status == 4) { ?>
			                				<span class="label label-success">Completed</span>
			                			<?php }?>	 	
			                </td>
			            </tr>
			        <?php } ?>