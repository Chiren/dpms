<?php 
class Jobscreen_model  extends CI_Model  {
	
	function get_all(){
		$this->db->select('container_job.*,container_master.*, job_master.*,jobscreen_master.*');
		$this->db->from('container_job');
		$this->db->join('container_master', 'container_job.container_id = container_master.container_id');
		$this->db->join('job_master', 'job_master.job_id = container_job.job_id'); 
		$this->db->join('jobscreen_master', 'jobscreen_master.container_job_id = container_job.container_job_id'); 
		$this->db->join('tpd_master', 'container_job.tpd = tpd_master.id');
		$this->db->where("tpd_master.name","100"); 
		$this->db->where("job_status","0");
		//$this->db->where("con_status='0' OR con_status='1' OR con_status='2'");
		$this->db->order_by("con_status", "DESC");
		$this->db->order_by("container_date", "asc");
		return $this->db->get()->result();
	}
	
	function qcadd($id,$data,$activity,$sound)
	{
		$this->db->where("id",$id);
		$this->db->update('jobscreen_master',$data);
		$this->db->insert('activity', $activity);
		$this->db->insert('activity_master', $sound);
	}
	
	function qaadd($id,$data,$activity,$sound)
	{
		$this->db->where("id",$id);
		$this->db->update('jobscreen_master',$data);
		$this->db->insert('activity', $activity);
		$this->db->insert('activity_master', $sound);
	}
	
	function whadd($id,$data,$activity,$sound)
	{
		$this->db->where("id",$id);
		$this->db->update('jobscreen_master',$data);
		$this->db->insert('activity', $activity);
		$this->db->insert('activity_master', $sound);
	}
	
	function ansaadd($id,$data,$activity,$sound)
	{
		$this->db->where("id",$id);
		$this->db->update('jobscreen_master',$data);
		$this->db->insert('activity', $activity);
		$this->db->insert('activity_master', $sound);
	}

	function get_job_name($jobs)
	{
		$sql = "select job_name from job_master where job_id=$jobs";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function get_container_name($id)
	{
		$sql = "select container_type from container_master where container_id=$id";
		$query = $this->db->query($sql);
		return $query->result();
	}
	function tpdchange($id,$tpd)
	{
		$sql = "update container_job set tpd='".$tpd."' where container_job_id='".$id."'";
		$query = $this->db->query($sql);	
	}
	
	function get_job_container($id)
	{
		$sql = "select container_job.container_job_id,container_job.qty,container_job.container_id,container_job.job_id,container_job.tpd,jobscreen_master.qa_per,jobscreen_master.wh_completed,jobscreen_master.ansa_completed from jobscreen_master join container_job on container_job.container_job_id=jobscreen_master.container_job_id where id=$id";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function qc_startdate($date,$container_job_id)
	{
		$sql = "select container_job_id from qc_start where container_job_id='".$container_job_id."'";
		$query = $this->db->query($sql);
		$chk = $query->num_rows();
		
		if($chk == 0)
		{
			$sql1 = "insert into qc_start (container_job_id,qc_start_datetime) value ('".$container_job_id."','".$date."')";
			$query1 = $this->db->query($sql1);
		}
	}
	
	function qc_log($date,$container_job_id,$status)
	{
		$sql = "select container_job_id from qc_log where container_job_id='".$container_job_id."' AND status='0'";
		$query = $this->db->query($sql);
		$chk = $query->num_rows();
		
		if($chk == 0)
		{
			$sql1 = "insert into qc_log (container_job_id,end_time,status) value ('".$container_job_id."','".$date."','".$status."')";
			$query1 = $this->db->query($sql1);
		}
	}
	
	function qc_logtwo($date,$container_job_id,$status)
	{
		$sql = "select container_job_id from qc_log where container_job_id='".$container_job_id."' AND status='1'";
		$query = $this->db->query($sql);
		$chk = $query->num_rows();
		
		if($chk == 0)
		{
			$sql1 = "insert into qc_log (container_job_id,end_time,status) value ('".$container_job_id."','".$date."','".$status."')";
			$query1 = $this->db->query($sql1);
		}
	}
	
	
	function qa_startdate($date,$container_job_id)
	{
		$sql = "select container_job_id from qa_start where container_job_id='".$container_job_id."'";
		$query = $this->db->query($sql);
		$chk = $query->num_rows();
		
		if($chk == 0)
		{
			$sql1 = "insert into qa_start (container_job_id,qa_start_datetime) value ('".$container_job_id."','".$date."')";
			$query1 = $this->db->query($sql1);
		}
	}
	
	function qa_log($date,$container_job_id,$status)
	{
		$sql = "select container_job_id from qa_log where container_job_id='".$container_job_id."' AND status='0'";
		$query = $this->db->query($sql);
		$chk = $query->num_rows();
		
		if($chk == 0)
		{
			$sql1 = "insert into qa_log (container_job_id,end_time,status) value ('".$container_job_id."','".$date."','".$status."')";
			$query1 = $this->db->query($sql1);
		}
	}
	
	function qa_logtwo($date,$container_job_id,$status)
	{
		$sql = "select container_job_id from qa_log where container_job_id='".$container_job_id."' AND status='1'";
		$query = $this->db->query($sql);
		$chk = $query->num_rows();
		
		if($chk == 0)
		{
			$sql1 = "insert into qa_log (container_job_id,end_time,status) value ('".$container_job_id."','".$date."','".$status."')";
			$query1 = $this->db->query($sql1);
		}
	}
	
	
	function wh_startdate($date,$container_job_id)
	{
		$sql = "select container_job_id from wh_start where container_job_id='".$container_job_id."'";
		$query = $this->db->query($sql);
		$chk = $query->num_rows();
		
		if($chk == 0)
		{
			$sql1 = "insert into wh_start (container_job_id,wh_start_datetime) value ('".$container_job_id."','".$date."')";
			$query1 = $this->db->query($sql1);
		}
	}
	
	function wh_log($date,$container_job_id,$status)
	{
		$sql = "select container_job_id from wh_log where container_job_id='".$container_job_id."' AND status='0'";
		$query = $this->db->query($sql);
		$chk = $query->num_rows();
		
		if($chk == 0)
		{
			$sql1 = "insert into wh_log (container_job_id,end_time,status) value ('".$container_job_id."','".$date."','".$status."')";
			$query1 = $this->db->query($sql1);
		}
	}
	
	function wh_logtwo($date,$container_job_id,$status)
	{
		$sql = "select container_job_id from wh_log where container_job_id='".$container_job_id."' AND status='1'";
		$query = $this->db->query($sql);
		$chk = $query->num_rows();
		
		if($chk == 0)
		{
			$sql1 = "insert into wh_log (container_job_id,end_time,status) value ('".$container_job_id."','".$date."','".$status."')";
			$query1 = $this->db->query($sql1);
		}
	}
	
	
	function ansa_startdate($date,$container_job_id)
	{
		$sql = "select container_job_id from ansa_start where container_job_id='".$container_job_id."'";
		$query = $this->db->query($sql);
		$chk = $query->num_rows();
		
		if($chk == 0)
		{
			$sql1 = "insert into ansa_start (container_job_id,ansa_start_datetime) value ('".$container_job_id."','".$date."')";
			$query1 = $this->db->query($sql1);
		}
	}
	
	function ansa_log($date,$container_job_id,$status)
	{
		$sql = "select container_job_id from ansa_log where container_job_id='".$container_job_id."' AND status='0'";
		$query = $this->db->query($sql);
		$chk = $query->num_rows();
		
		if($chk == 0)
		{
			$sql1 = "insert into ansa_log (container_job_id,end_time,status) value ('".$container_job_id."','".$date."','".$status."')";
			$query1 = $this->db->query($sql1);
		}
	}
	
	function ansa_logtwo($date,$container_job_id,$status)
	{
		$sql = "select container_job_id from ansa_log where container_job_id='".$container_job_id."' AND status='1'";
		$query = $this->db->query($sql);
		$chk = $query->num_rows();
		
		if($chk == 0)
		{
			$sql1 = "insert into ansa_log (container_job_id,end_time,status) value ('".$container_job_id."','".$date."','".$status."')";
			$query1 = $this->db->query($sql1);
		}
	}

	function get_qa_logsdata($id)
	{
		$sql="select jobscreen_master.qa_before,jobscreen_master.qa_same,jobscreen_master.qa_same,qa_late,container_master.chk_date from jobscreen_master join container_job on container_job.container_job_id=jobscreen_master.container_job_id join container_master on container_master.container_id=container_job.container_id where jobscreen_master.id=$id";
		$query=$this->db->query($sql);
		return $query->result();	
	}
}
?>

