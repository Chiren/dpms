<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General extends Admin_Controller {
	
	

	public function notification() 
	{
		$this->render('_partials/notifications');	
	}
	public function rednotifications($id)
	{
		$this->load->model('General_model');
		$data = $this->General_model->rednoti($id);
	}
	
	public function readnotifi($id)
	{
		date_default_timezone_set('Asia/Calcutta');
		$date = date('Y-m-d H:i:s');
		
		$data['accepted_date'] = $date;
		$data['status'] = '1';
		$this->load->model('General_model');
		$id = $this->General_model->readnotifi($id,$data);
	}
	
	public function popup() 
	{
		$this->render('_layouts/popup');	
	}
	
	public function sound()
	{
		$this->render('jobs/sound');
	}
	
	public function soundcom($id)
	{
		$this->load->model('General_model');
		$data = $this->General_model->soundcom($id);
	}
	
	function message()
	{
		$this->render('_partials/msg');
	}
	
	public function readmsg($id)
	{
		$this->load->model('General_model');
		$data = $this->General_model->readmsg($id);
	}
	
	
}