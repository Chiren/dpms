<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jobscreen extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}

	public function index()
	{
		
		$this->load->model('jobscreen_model');
		$data = $this->jobscreen_model->get_all();
		$this->mViewData['result']=$data;
		$this->render('jobs/screen');
	}
	
	public function reloaded()
	{
		$this->load->model('jobscreen_model');
		$data = $this->jobscreen_model->get_all();
		$this->mViewData['result']=$data;
		$this->render('jobs/inner');
	}
	
	
	public function qcadd()
	{
		$abc = $_POST;
		$id = $this -> input -> post('qid');
		$con1 = $this -> input -> post('qcon');
		$con2 = $this -> input -> post('qcon2');
		date_default_timezone_set('Asia/Calcutta');
		$date = date('Y-m-d H:i:s');
		$this->load->model('jobscreen_model');
		
		$ci = &get_instance();
		$ci -> load -> library('session');
		$x = $ci -> session -> userdata('user_group');
		$y = $ci -> session -> userdata();
		$users = $y['user_id'];
		$time = date('H:i:s');
		$dates = date('Y-m-d');
		$cons = $this->jobscreen_model->get_job_container($id);
		
		foreach($cons as $c)
		{
			$container = $c->container_id;
			$jobs = $c->job_id;
			$tpd = $c->tpd;
			$qa_per = $c->qa_per;
			$container_job_id = $c->container_job_id;
			$totalqty = $c->qty;
		}
		
		 
		$dateinsert = $this->jobscreen_model->qc_startdate($date,$container_job_id);
		$ckp = $con1+$con2;
		$qcl = (($ckp*100)/$totalqty);
		
		if($qcl > 25 && $qcl != 100)
		{
			$status = 0;
			$log1 = $this->jobscreen_model->qc_log($date,$container_job_id,$status);	
		}
		elseif($qcl == 100)
		{
			$status = 1;
			$log2 = $this->jobscreen_model->qc_logtwo($date,$container_job_id,$status);	
		} 
		 
		$qa_per_data = $qa_per+$con1;
		
		$activity['user'] = $users;
		$activity['jobs'] = $jobs;
		$activity['container'] = $container;
		$activity['tpd'] = $tpd;
		$activity['time'] = $time;
		$activity['note'] = $con1." QTY -Pass "; 
		$activity['date'] = $dates;
		
		$sounds = $this->jobscreen_model->get_job_name($jobs);
		foreach($sounds as $s)
		{
			$jobs_name = $s->job_name;
		}
		$soundscon = $this->jobscreen_model->get_container_name($container);
		foreach($soundscon as $scon)
		{
			$con_name = $scon->container_type;
		}
		
		$sound['tpd'] = $tpd;
		$sound['contents'] = "Container ".$con_name." Jobs".$jobs_name." Products ".$con1." Ready";
		
		
		$data['qc'] =  $con1+$con2 ;
		$data['qc_completed'] = $con1+$con2;
		$data['qa'] =  $con1+$con2 ;
		$data['qa_per'] = $qa_per_data;
		$data['qa_rem'] = null;
		$data['qc_updated'] = $date;
		$data['qc_comment'] = null;
		
		$id = $this->jobscreen_model->qcadd($id,$data,$activity,$sound);
		
	}
	
	public function qaadd()
	{
		$abc = $_POST;
		$id = $this -> input -> post('qaid');
		$con = $this -> input -> post('qacon1');
		$qa = $this -> input -> post('qa');
		$qa_per = $this -> input -> post('qaper');
		$qa_com = $this -> input -> post('qacom');
		$comments = $this -> input -> post('qacon2');
		$chk = $this->input->post('chk');
		
		if($chk == $qa)
		{
			$qa_completed = $con+$qa_com;
			
			
			if($con=='0')
			{
				$qctotal = $qa-$qa_per;
			}
			else
			{
				$abc = $qa_per-$con;
				$qctotal = $qa-$abc;
			}
			$this->load->model('jobscreen_model');
			date_default_timezone_set('Asia/Calcutta');
			if($con < $chk)
			{
				$chkdate = date('Y-m-d');
				$qa_logdata = $this->jobscreen_model->get_qa_logsdata($id);
				foreach($qa_logdata as $qa_l)
				{
					if($qa_l->chk_date == $chkdate)
					{
						$data['qa_same'] = $qa_l->qa_same+1;
					}
					if($qa_l->chk_date < $chkdate)
					{
						$data['qa_before'] = $qa_l->qa_before+1;
					}
					if($qa_l->chk_date > $chkdate)
					{
						$data['qa_late'] = $qa_l->qa_late+1;
					}
				}
			}
		
		
			date_default_timezone_set('Asia/Calcutta');
			$date = date('Y-m-d H:i:s');
		
			
		
			$ci = &get_instance();
			$ci -> load -> library('session');
			$x = $ci -> session -> userdata('user_group');
			$y = $ci -> session -> userdata();
			$users = $y['user_id'];
			$time = date('H:i:s');
			$dates = date('Y-m-d');
			$cons = $this->jobscreen_model->get_job_container($id);
		
			foreach($cons as $c)
			{
				$container = $c->container_id;
				$jobs = $c->job_id;
				$tpd = $c->tpd;
				$container_job_id = $c->container_job_id;
				$totalqty = $c->qty;
			}
			
			$dateinsert = $this->jobscreen_model->qa_startdate($date,$container_job_id);
			$qal = (($qa_completed*100)/$totalqty);
		
			if($qal > 25 && $qal != 100)
			{
				$status = 0;
				$log1 = $this->jobscreen_model->qa_log($date,$container_job_id,$status);	
			}
			elseif($qal == 100)
			{
				$status = 1;
				$log2 = $this->jobscreen_model->qa_logtwo($date,$container_job_id,$status);	
			} 
			
			
			$rem = $chk-$con;
			$activity['user'] = $users;
			$activity['jobs'] = $jobs;
			$activity['container'] = $container;
			$activity['tpd'] = $tpd;
			$activity['time'] = $time; 
			$activity['note'] =$qa_completed." QTY - Pass, ".$rem." QTY - Fail";
			$activity['date'] = $dates;
		
		
			$sounds = $this->jobscreen_model->get_job_name($jobs);
			foreach($sounds as $s)
			{
				$jobs_name = $s->job_name;
			}
			$soundscon = $this->jobscreen_model->get_container_name($container);
			foreach($soundscon as $scon)
			{
				$con_name = $scon->container_type;
			}
		
			$sound['tpd'] = $tpd;
			$sound['contents'] = "Container ".$con_name." Jobs".$jobs_name." Products ".$con1." Ready";
		
			$rem = $chk-$con;
			$data['qa_completed'] = $qa_completed;
			$data['wh'] =  $qa_completed;
			$data['qa_rem'] =  $con;
			$data['qc'] =  $qctotal;
			$data['qc_completed'] =  $qctotal;
			$data['qa'] =  $qctotal;
			$data['qa_updated'] = $date;
			$data['qc_comment'] = $qa_completed." QTY - Pass, ".$rem." QTY - Fail <br>".$comments;
			$data['wh_per'] = $con;
			$data['qa_per'] = 0;
		
			$id = $this->jobscreen_model->qaadd($id,$data,$activity,$sound);
			
		}
		else
		{
			$qa_completed = $con+$qa_com;
			$qa_per_data = $qa_per-$chk;
			
			if($con=='0')
			{
				$qctotal = $qa-$chk;
			}
			else
			{
				$abc = $chk-$con;
				$qctotal = $qa-$abc;
			}
			
			$this->load->model('jobscreen_model');
			date_default_timezone_set('Asia/Calcutta');
			
			if($con < $chk)
			{
				$chkdate = date('Y-m-d');
				$qa_logdata = $this->jobscreen_model->get_qa_logsdata($id);
				foreach($qa_logdata as $qa_l)
				{
					if($qa_l->chk_date == $chkdate)
					{
						$data['qa_same'] = $qa_l->qa_same+1;
					}
					if($qa_l->chk_date < $chkdate)
					{
						$data['qa_before'] = $qa_l->qa_before+1;
					}
					if($qa_l->chk_date > $chkdate)
					{
						$data['qa_late'] = $qa_l->qa_late+1;
					}
				}
			}
			
			$date = date('Y-m-d H:i:s');
		
			$this->load->model('jobscreen_model');
		
			$ci = &get_instance();
			$ci -> load -> library('session');
			$x = $ci -> session -> userdata('user_group');
			$y = $ci -> session -> userdata();
			$users = $y['user_id'];
			$time = date('H:i:s');
			$dates = date('Y-m-d');
			$cons = $this->jobscreen_model->get_job_container($id);
		
			foreach($cons as $c)
			{
				$container = $c->container_id;
				$jobs = $c->job_id;
				$tpd = $c->tpd;
				$container_job_id = $c->container_job_id;
				$totalqty = $c->qty;
			}
			
			$dateinsert = $this->jobscreen_model->qa_startdate($date,$container_job_id);
			$qal = (($qa_completed*100)/$totalqty);
		
			if($qal > 25 && $qal != 100)
			{
				$status = 0;
				$log1 = $this->jobscreen_model->qa_log($date,$container_job_id,$status);	
			}
			elseif($qal == 100)
			{
				$status = 1;
				$log2 = $this->jobscreen_model->qa_logtwo($date,$container_job_id,$status);	
			} 
			$rem = $chk-$con;
			$activity['user'] = $users;
			$activity['jobs'] = $jobs;
			$activity['container'] = $container;
			$activity['tpd'] = $tpd;
			$activity['time'] = $time;
			$activity['note'] = $qa_completed." QTY - Pass, ".$rem." QTY - Fail";
			$activity['date'] = $dates;
		
		
			$sounds = $this->jobscreen_model->get_job_name($jobs);
			foreach($sounds as $s)
			{
				$jobs_name = $s->job_name;
			}
			$soundscon = $this->jobscreen_model->get_container_name($container);
			foreach($soundscon as $scon)
			{
				$con_name = $scon->container_type;
			}
		
			$sound['tpd'] = $tpd;
			$sound['contents'] = "Container ".$con_name." Jobs".$jobs_name." Products ".$con1." Ready";
		
			$rem = $chk-$con;
			$data['qa_completed'] = $qa_completed;
			$data['wh'] =  $qa_completed;
			$data['qa_rem'] =  $con;
			$data['qc'] =  $qctotal;
			$data['qc_completed'] =  $qctotal;
			$data['qa'] =  $qctotal;
			$data['qa_updated'] = $date;
			//$data['qc_comment'] = $comments."Completed Products ".$qa_completed." Rejected ".$rem;
			$data['qc_comment'] = $qa_completed." QTY - Pass, ".$rem." QTY - Fail <br>".$comments;
			$data['wh_per'] = $con;
			$data['qa_per'] = $qa_per_data;
		
			$id = $this->jobscreen_model->qaadd($id,$data,$activity,$sound);
			
		}	
	}
	
	public function whadd()
	{
		$abc = $_POST;
		$id = $this -> input -> post('qid');
		$qty = $this -> input -> post('qt');
		 		
		date_default_timezone_set('Asia/Calcutta');
		$date = date('Y-m-d H:i:s');
		
		$this->load->model('jobscreen_model');
		
		
		$ci = &get_instance();
		$ci -> load -> library('session');
		$x = $ci -> session -> userdata('user_group');
		$y = $ci -> session -> userdata();
		$users = $y['user_id'];
		$time = date('H:i:s');
		$dates = date('Y-m-d');
		$cons = $this->jobscreen_model->get_job_container($id);
		
		foreach($cons as $c)
		{
			$container = $c->container_id;
			$jobs = $c->job_id;
			$tpd = $c->tpd;
			$wh = $c->wh_completed;
			$container_job_id = $c->container_job_id;
			$totalqty = $c->qty;
		}
		$final = $qty+$wh;
		
		$dateinsert = $this->jobscreen_model->wh_startdate($date,$container_job_id);
		$whl = (($final*100)/$totalqty);
		
		if($whl > 25 && $whl != 100)
		{
			$status = 0;
			$log1 = $this->jobscreen_model->wh_log($date,$container_job_id,$status);	
		}
		elseif($whl == 100)
		{
			$status = 1;
			$log2 = $this->jobscreen_model->wh_logtwo($date,$container_job_id,$status);	
		}
		
		
		$activity['user'] = $users;
		$activity['jobs'] = $jobs;
		$activity['container'] = $container;
		$activity['tpd'] = $tpd;
		$activity['time'] = $time;
		$activity['note'] =   $qty." QTY - Pass "; 
		$activity['date'] = $dates;
		
		$sounds = $this->jobscreen_model->get_job_name($jobs);
		foreach($sounds as $s)
		{
			$jobs_name = $s->job_name;
		}
		$soundscon = $this->jobscreen_model->get_container_name($container);
		foreach($soundscon as $scon)
		{
			$con_name = $scon->container_type;
		}
		
		$sound['tpd'] = $tpd;
		$sound['contents'] = "Container ".$con_name." Jobs".$jobs_name." Products ".$con1." Ready";
		
		$data['wh_completed'] = $final;
		$data['wh_updated'] = $date;
		$ids = $this->jobscreen_model->whadd($id,$data,$activity,$sound);
	}
	
	
	public function ansaadd1()
	{
		$abc = $_POST;
		$id = $this -> input -> post('id');
		$ansa1 = $this -> input -> post('ansa1');
		$ansa2 = $this -> input -> post('ansa2');
		$ansacom = $ansa1+$ansa2;
		
		date_default_timezone_set('Asia/Calcutta');
		
		$date = date('Y-m-d H:i:s');
		
		$this->load->model('jobscreen_model');
		
		
		$ci = &get_instance();
		$ci -> load -> library('session');
		$x = $ci -> session -> userdata('user_group');
		$y = $ci -> session -> userdata();
		$users = $y['user_id'];
		$time = date('H:i:s');
		$dates = date('Y-m-d');
		
		$cons = $this->jobscreen_model->get_job_container($id);
		
		foreach($cons as $c)
		{
			$container = $c->container_id;
			$jobs = $c->job_id;
			$tpd = $c->tpd;
			$container_job_id = $c->container_job_id;
			$totalqty = $c->qty;
		}
		
		$dateinsert = $this->jobscreen_model->ansa_startdate($date,$container_job_id);
		$ansal = (($ansacom*100)/$totalqty);
		
		if($ansal > 25 && $ansal != 100)
		{
			$status = 0;
			$log1 = $this->jobscreen_model->ansa_log($date,$container_job_id,$status);	
		}
		elseif($ansal == 100)
		{
			$status = 1;
			$log2 = $this->jobscreen_model->ansa_logtwo($date,$container_job_id,$status);	
		}
		
		$activity['user'] = $users;
		$activity['jobs'] = $jobs;
		$activity['container'] = $container;
		$activity['tpd'] = $tpd;
		$activity['time'] = $time;
		$activity['note'] = $ansacom." QTY - Pass ";
		$activity['date'] = $dates;
		
		$sounds = $this->jobscreen_model->get_job_name($jobs);
		foreach($sounds as $s)
		{
			$jobs_name = $s->job_name;
		}
		$soundscon = $this->jobscreen_model->get_container_name($container);
		foreach($soundscon as $scon)
		{
			$con_name = $scon->container_type;
		}
		
		$sound['tpd'] = $tpd;
		$sound['contents'] = "Container ".$con_name." Jobs".$jobs_name." Products ".$con1." Ready";
		
		
		$data['ansa'] = $ansacom;
		$data['ansa_completed'] = $ansacom;
		$data['ansa_updated'] = $date;
		
		$ids = $this->jobscreen_model->ansaadd($id,$data,$activity,$sound);
	}

	public function qcadd2()
	{
		$abc = $_POST;
		$id = $this -> input -> post('id');
		$con1 = $this -> input -> post('ansaqc1');
		$con2 = $this -> input -> post('ansaqc2');
		
		$ansa = $this -> input -> post('ansaqc3');
		
		date_default_timezone_set('Asia/Calcutta');
		$date = date('Y-m-d H:i:s');
		$set = $con1+$con2;
		$total = $ansa-($con1+$con2);
		$final = 0;
		if($total == 0)
		{
			$final = $ansa;
		}
		else
		{
			$final = $total;
		}
		
		$this->load->model('jobscreen_model');
		
		
		$ci = &get_instance();
		$ci -> load -> library('session');
		$x = $ci -> session -> userdata('user_group');
		$y = $ci -> session -> userdata();
		$users = $y['user_id'];
		$time = date('H:i:s');
		$dates = date('Y-m-d');
		$cons = $this->jobscreen_model->get_job_container($id);
		
		foreach($cons as $c)
		{
			$container = $c->container_id;
			$jobs = $c->job_id;
			$tpd = $c->tpd;
			$qa_per = $c->qa_per;
			$container_job_id = $c->container_job_id;
			$totalqty = $c->qty;
		}
		$qa_per_data = $qa_per+$con2;
		
		$dateinsert = $this->jobscreen_model->qc_startdate($date,$container_job_id);
		$ckp = $con1+$con2;
		$qcl = (($ckp*100)/$totalqty);
		
		if($qcl > 25 && $qcl != 100)
		{
			$status = 0;
			$log1 = $this->jobscreen_model->qc_log($date,$container_job_id,$status);	
		}
		elseif($qcl == 100)
		{
			$status = 1;
			$log2 = $this->jobscreen_model->qc_logtwo($date,$container_job_id,$status);	
		} 
		 
		
		$activity['user'] = $users;
		$activity['jobs'] = $jobs;
		$activity['container'] = $container;
		$activity['tpd'] = $tpd;
		$activity['time'] = $time;
		$activity['note'] = $set." QTY - Pass ";
		
		$activity['date'] = $dates;
		
		$sounds = $this->jobscreen_model->get_job_name($jobs);
		foreach($sounds as $s)
		{
			$jobs_name = $s->job_name;
		}
		$soundscon = $this->jobscreen_model->get_container_name($container);
		foreach($soundscon as $scon)
		{
			$con_name = $scon->container_type;
		}
		
		$sound['tpd'] = $tpd;
		$sound['contents'] = "Container ".$con_name." Jobs".$jobs_name." Products ".$con1." Ready";
		
		
		
		$data['qc'] =  $con1+$con2 ;
		$data['qc_completed'] =  $con1+$con2 ;
		$data['qa'] =  $con1+$con2;
		$data['qa_per'] = $qa_per_data;
		$data['qa_rem'] = null;
		$data['qc_updated'] = $date;
		$data['qc_comment'] = null;
		//$data['ansa'] = $con1+$con2;;
		//$data['ansa_completed'] = $con1+$con2;
		$id = $this->jobscreen_model->qcadd($id,$data,$activity,$sound);
	}


	public function qaadd2()
	{
		$abc = $_POST;
		$id = $this -> input -> post('qaid');
		$con = $this -> input -> post('qacon1');
		$qa = $this -> input -> post('qa');
		$qa_per = $this -> input -> post('qaper');
		$qa_com = $this -> input -> post('qacom');
		$comments = $this -> input -> post('qacon2');
		$chk = $this->input->post('chk');
		
		if($chk == $qa)
		{
		
			$qa_completed = $con+$qa_com;
			if($con=='0')
			{
				$qctotal = $qa-$qa_per;
			}
			else
			{
				$abc = $qa_per-$con;
				$qctotal = $qa-$abc;
			}
			date_default_timezone_set('Asia/Calcutta');
			$this->load->model('jobscreen_model');
			
			if($con < $chk)
			{
				$chkdate = date('Y-m-d');
				$qa_logdata = $this->jobscreen_model->get_qa_logsdata($id);
				foreach($qa_logdata as $qa_l)
				{
					if($qa_l->chk_date == $chkdate)
					{
						$data['qa_same'] = $qa_l->qa_same+1;
					}
					if($qa_l->chk_date < $chkdate)
					{
						$data['qa_before'] = $qa_l->qa_before+1;
					}
					if($qa_l->chk_date > $chkdate )
					{
						$data['qa_late'] = $qa_l->qa_late+1;
					}
				}
			}
			
			
			
			$date = date('Y-m-d H:i:s');
			$ci = &get_instance();
			$ci -> load -> library('session');
			$x = $ci -> session -> userdata('user_group');
			$y = $ci -> session -> userdata();
			$users = $y['user_id'];
			$time = date('H:i:s');
			$dates = date('Y-m-d');
			$cons = $this->jobscreen_model->get_job_container($id);
		
			foreach($cons as $c)
			{
				$container = $c->container_id;
				$jobs = $c->job_id;
				$tpd = $c->tpd;
				$container_job_id = $c->container_job_id;
				$totalqty = $c->qty;
			}
			
			$dateinsert = $this->jobscreen_model->qa_startdate($date,$container_job_id);
			$qal = (($qa_completed*100)/$totalqty);
		
			if($qal > 25 && $qal != 100)
			{
				$status = 0;
				$log1 = $this->jobscreen_model->qa_log($date,$container_job_id,$status);	
			}
			elseif($qal == 100)
			{
				$status = 1;
				$log2 = $this->jobscreen_model->qa_logtwo($date,$container_job_id,$status);	
			} 
			
			$rem = $chk-$con;
			$activity['user'] = $users;
			$activity['jobs'] = $jobs;
			$activity['container'] = $container;
			$activity['tpd'] = $tpd;
			$activity['time'] = $time;
			$activity['note'] = $qa_completed." QTY - Pass, ".$rem." QTY - Fail";
			$activity['date'] = $dates;
		
			$sounds = $this->jobscreen_model->get_job_name($jobs);
			foreach($sounds as $s)
			{
				$jobs_name = $s->job_name;
			}
			$soundscon = $this->jobscreen_model->get_container_name($container);
			foreach($soundscon as $scon)
			{
				$con_name = $scon->container_type;
			}
		
			$sound['tpd'] = $tpd;
			$sound['contents'] = "Container ".$con_name." Jobs".$jobs_name." Products ".$con1." Ready";
		
		
			$data['qa_completed'] = $qa_completed;
			$data['wh'] =  $qa_completed;
			$data['qa_rem'] =  $con;
			$data['qc'] =  $qctotal;
			$data['qc_completed'] =  $qctotal;
			$data['qa'] =  $qctotal;
			$data['qa_updated'] = $date;
			$data['wh_updated'] = $date;
			//$data['qc_comment'] = $comments."Completed Products ".$qa_completed." Rejected ".$con;
			$data['qc_comment'] = $qa_completed." QTY - Pass, ".$rem." QTY - Fail <br>".$comments;
			//$data['ansa'] = $qctotal;
			//$data['ansa_completed'] = $qctotal;
			$data['qa_per'] = 0;
		
			$id = $this->jobscreen_model->qaadd($id,$data,$activity,$sound);
		
		}
		else
		{
			$qa_completed = $con+$qa_com;
			$qa_per_data = $qa_per-$chk;
			
			if($con=='0')
			{
				$qctotal = $qa-$chk;
			}
			else
			{
				$abc = $chk-$con;
				$qctotal = $qa-$abc;
			}
			
			date_default_timezone_set('Asia/Calcutta');
			$this->load->model('jobscreen_model');
			
			if($con < $chk)
			{
				$chkdate = date('Y-m-d');
				$qa_logdata = $this->jobscreen_model->get_qa_logsdata($id);
				foreach($qa_logdata as $qa_l)
				{
					if($qa_l->chk_date == $chkdate)
					{
						$data['qa_same'] = $qa_l->qa_same+1;
					}
					if($qa_l->chk_date < $chkdate)
					{
						$data['qa_before'] = $qa_l->qa_before+1;
					}
					if($qa_l->chk_date > $chkdate)
					{
						$data['qa_late'] = $qa_l->qa_late+1;
					}
				}
			}
			
			$date = date('Y-m-d H:i:s');
		
			$ci = &get_instance();
			$ci -> load -> library('session');
			$x = $ci -> session -> userdata('user_group');
			$y = $ci -> session -> userdata();
			$users = $y['user_id'];
			$time = date('H:i:s');
			$dates = date('Y-m-d');
			$cons = $this->jobscreen_model->get_job_container($id);
		
			foreach($cons as $c)
			{
				$container = $c->container_id;
				$jobs = $c->job_id;
				$tpd = $c->tpd;
				$container_job_id = $c->container_job_id;
				$totalqty = $c->qty;
			}
			
			$dateinsert = $this->jobscreen_model->qa_startdate($date,$container_job_id);
			$qal = (($qa_completed*100)/$totalqty);
		
			if($qal > 25 && $qal != 100)
			{
				$status = 0;
				$log1 = $this->jobscreen_model->qa_log($date,$container_job_id,$status);	
			}
			elseif($qal == 100)
			{
				$status = 1;
				$log2 = $this->jobscreen_model->qa_logtwo($date,$container_job_id,$status);	
			} 
			
			$rem = $chk-$con;
			$activity['user'] = $users;
			$activity['jobs'] = $jobs;
			$activity['container'] = $container;
			$activity['tpd'] = $tpd;
			$activity['time'] = $time;
			$activity['note'] = $qa_completed." QTY - Pass, ".$rem." QTY - Fail";
			$activity['date'] = $dates;
		
			$sounds = $this->jobscreen_model->get_job_name($jobs);
			foreach($sounds as $s)
			{
				$jobs_name = $s->job_name;
			}
			$soundscon = $this->jobscreen_model->get_container_name($container);
			foreach($soundscon as $scon)
			{
				$con_name = $scon->container_type;
			}
		
			$sound['tpd'] = $tpd;
			$sound['contents'] = "Container ".$con_name." Jobs".$jobs_name." Products ".$con1." Ready";
		
			
			$data['qa_completed'] = $qa_completed;
			$data['wh'] =  $qa_completed;
			$data['qa_rem'] =  $con;
			$data['qc'] =  $qctotal;
			$data['qc_completed'] =  $qctotal;
			$data['qa'] =  $qctotal;
			$data['qa_updated'] = $date;
			$data['wh_updated'] = $date;
			$data['qc_comment'] = $qa_completed." QTY - Pass, ".$rem." QTY - Fail <br>".$comments;
			//$data['qc_comment'] = $comments."Completed Products ".$qa_completed." Rejected ".$con;
			//$data['ansa'] = $qctotal;
			//$data['ansa_completed'] = $qctotal;
			$data['qa_per'] = $qa_per_data;
		
			$id = $this->jobscreen_model->qaadd($id,$data,$activity,$sound);
		}	
		
	}
	public function tpdchange()
	{
		$id = $this->input->post('ids');
		$tpd = $this->input->post('tpdname');
		
		$this->load->model('jobscreen_model');
		$id = $this->jobscreen_model->tpdchange($id,$tpd);
	}
	
	public function ansawhadd()
	{
		$abc = $_POST;
		$id = $this -> input -> post('qid');
		$qty = $this -> input -> post('qt');
		 		
		date_default_timezone_set('Asia/Calcutta');
		$date = date('Y-m-d H:i:s');
		
		$this->load->model('jobscreen_model');
		
		
		$ci = &get_instance();
		$ci -> load -> library('session');
		$x = $ci -> session -> userdata('user_group');
		$y = $ci -> session -> userdata();
		$users = $y['user_id'];
		$time = date('H:i:s');
		$dates = date('Y-m-d');
		$cons = $this->jobscreen_model->get_job_container($id);
		
		foreach($cons as $c)
		{
			$container = $c->container_id;
			$jobs = $c->job_id;
			$tpd = $c->tpd;
			$wh = $c->wh_completed;
			$container_job_id = $c->container_job_id;
			$totalqty = $c->qty;
		}
		
		$final = $qty+$wh;
		
		$dateinsert = $this->jobscreen_model->wh_startdate($date,$container_job_id);
		$whl = (($final*100)/$totalqty);
		
		if($whl > 25 && $whl != 100)
		{
			$status = 0;
			$log1 = $this->jobscreen_model->wh_log($date,$container_job_id,$status);	
		}
		elseif($whl == 100)
		{
			$status = 1;
			$log2 = $this->jobscreen_model->wh_logtwo($date,$container_job_id,$status);	
		}
		
		
		$activity['user'] = $users;
		$activity['jobs'] = $jobs;
		$activity['container'] = $container;
		$activity['tpd'] = $tpd;
		$activity['time'] = $time;
		$activity['note'] = $qty." QTY - Pass ";
		$activity['date'] = $dates;
		
		$sounds = $this->jobscreen_model->get_job_name($jobs);
		foreach($sounds as $s)
		{
			$jobs_name = $s->job_name;
		}
		$soundscon = $this->jobscreen_model->get_container_name($container);
		foreach($soundscon as $scon)
		{
			$con_name = $scon->container_type;
		}
		
		$sound['tpd'] = $tpd;
		$sound['contents'] = "Container ".$con_name." Jobs".$jobs_name." Products ".$con1." Ready";
		
		$data['wh_completed'] = $final;
		$data['wh_updated'] = $date;
		$data['ansa'] = $final;
		$ids = $this->jobscreen_model->whadd($id,$data,$activity,$sound);
	}

	public function ansadata()
	{
		$abc = $_POST;
		$id = $this -> input -> post('qid');
		$qty = $this -> input -> post('qt');
		 		
		date_default_timezone_set('Asia/Calcutta');
		$date = date('Y-m-d H:i:s');
		
		$this->load->model('jobscreen_model');
		
		
		$ci = &get_instance();
		$ci -> load -> library('session');
		$x = $ci -> session -> userdata('user_group');
		$y = $ci -> session -> userdata();
		$users = $y['user_id'];
		$time = date('H:i:s'); 
		$dates = date('Y-m-d');
		$cons = $this->jobscreen_model->get_job_container($id);
		
		foreach($cons as $c)
		{
			$container = $c->container_id;
			$jobs = $c->job_id;
			$tpd = $c->tpd;
			$ansa = $c->ansa_completed;
			$container_job_id = $c->container_job_id;
			$totalqty = $c->qty;
		}
		
		$final = $qty+$ansa;
		
		$dateinsert = $this->jobscreen_model->ansa_startdate($date,$container_job_id);
		$ansal = (($final*100)/$totalqty);
		
		if($ansal > 25 && $ansal != 100)
		{
			$status = 0;
			$log1 = $this->jobscreen_model->ansa_log($date,$container_job_id,$status);	
		}
		elseif($ansal == 100)
		{
			$status = 1;
			$log2 = $this->jobscreen_model->ansa_logtwo($date,$container_job_id,$status);	
		}
		
		$activity['user'] = $users;
		$activity['jobs'] = $jobs;
		$activity['container'] = $container;
		$activity['tpd'] = $tpd;
		$activity['time'] = $time; 
		$activity['note'] = $qty." QTY -Pass ";
		$activity['date'] = $dates;
		
		$sounds = $this->jobscreen_model->get_job_name($jobs);
		foreach($sounds as $s)
		{
			$jobs_name = $s->job_name;
		}
		$soundscon = $this->jobscreen_model->get_container_name($container);
		foreach($soundscon as $scon)
		{
			$con_name = $scon->container_type; 
		} 
		
		$sound['tpd'] = $tpd;
		$sound['contents'] = "Container ".$con_name." Jobs".$jobs_name." Products ".$con1." Ready";
		
		$data['ansa_completed'] = $final;
		$data['ansa_updated'] = $date;
		$ids = $this->jobscreen_model->whadd($id,$data,$activity,$sound);
	}
	
	  
	
}
 