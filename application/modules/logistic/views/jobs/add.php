<?php $this->layout('layouts::default') ?>
<style>
	tbody
	{
		height: 500px !important;
		overflow: scroll !important;
	}
</style>
<style>
.input-xs {
  height: 25px;
  padding: 2px 5px;
  font-size: 15px;
  line-height: 1.5;
}
</style>

<div class="row">
<div class="col-lg-12">
	<div class="box box-primary">
    	<form action="jobs/save">
        	<div class="box-header with-border">
            	<h3 class="box-title">Add New Jobs</h3>
            </div>
            <div class="box-body">
            	<div class="form-group col-md-6">
              		<label>Name</label>
              		<input type="text" name="name" class="form-control input-xs" placeholder="Name" />
              	</div>
              	
              	<div class="form-group col-md-6">
              		<label>FG Code</label>
              		<input type="text" name="fg_code" class="form-control input-xs" placeholder="FG Code" />
              	</div>
              	
              	<div class="form-group col-md-6">
              		<label>Date</label>
              		<input type="text" name="date" class="form-control input-xs" id="datepicker" placeholder="Date" />
              	</div>
              	<div class="form-group col-md-6">
              		<label>Amount Value</label>
              		<input type="text" name="amount" class="form-control input-xs" placeholder="Amount Value" />
              	</div>            
            </div>
            <div class="box-footer text-center ">
              	<button class="pull-right btn btn-primary" type="submit">ADD</button>
            </div>
        </form>
	</div>
</div>

<div class="col-lg-12">
	<div class="box box-primary">
    	<div class="box-header with-border">
        	<h3 class="box-title">All Jobs</h3>
           	<div class="box-tools pull-right">
               	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
          	</div>
       	</div>
        
        <div class="box-body">
        	<table id="exampleabc" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
            	<thead>
              		<tr>
              			<th>Sr</th>
              			<th>Name</th>
              			<th>FG Code</th>
              			<th>Date</th>
              			<th>Amount Value</th>	
              			<th>Option</th>
              		</tr>
              	</thead>
              	<tfoot>
					<tr>
			    		<th>Sr</th>
              			<th>Name</th>
              			<th>FG Code</th>
              			<th>Date</th>
              			<th>Amount Value</th>	
              			<th>Option</th>
			 		</tr>
				</tfoot>
              	<tbody>
              			<?php $i = 1; foreach ($result as $r) { ?>
						<tr>
	              			<td><?php echo $i; $i++; ?></td>
	              			<td><?php print_r($r->job_name); ?></td>
	              			<td><?php print_r($r->fg_code); ?></td>
	              			<td><?php print_r($r->date); ?></td>
	              			<td><?php print_r($r->amount_value); ?></td>
	              			<td>
	              				<a href="jobs/edit/<?php print_r($r->job_id); ?>" class="btn btn-box-tool input-sm" style="background-color: #605CA8; color:white" ><i class="fa fa-pencil"></i></a>
	              				<a href="jobs/delete/<?php print_r($r->job_id); ?>" class="btn btn-box-tool input-sm" style="background-color: red; color:white" ><i class="fa fa-trash"></i></a> 
	              			</td>
	              		</tr>	  
					 	<?php } ?>
              	</tbody>
			</table>
		</div>
	</div>
</div>
</div>

<link rel="stylesheet" href="../assets/plugins/datepicker/datepicker3.css" />
<script src="../assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script>
$(function () {
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy/mm/dd'
    });
    $(".timepicker").timepicker({
      showInputs: false,
     
    });
  });
</script>