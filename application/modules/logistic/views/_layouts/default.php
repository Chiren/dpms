<?php $this->layout('layouts::base') ?>
<?php
$ci=&get_instance();
$ci->load->helper('common');
?>
<style>
	.user-panel > .info {
    left: 55px;
    line-height: 1;
    padding: 5px 5px 5px 15px;
    position: absolute;
}
</style>
<div class="wrapper">

	<?php $this->insert('partials::navbar'); ?>

	<?php // Left side column. contains the logo and sidebar ?>
	<aside class="main-sidebar">
		<section class="sidebar">
			<div class="user-panel" style="height:65px">
				<div class="pull-left image">
          			<?php $image1 = get_userimage(); foreach ($image1 as $img1) { if($img1->image == null){ ?>	
						<img src="<?php echo base_url(); ?>uploads/2017011207134400000096.png" class="img-circle" alt="User Image">
					<?php } else { ?>
						<img src="<?php echo base_url(); ?>uploads/<?php echo $img1->image; ?>" class="img-circle" alt="User Image">
					<?php } } ?>
        		</div>
				<div class="pull-left info">
					<p><?php echo $user->first_name; ?></p>
					<a href="account"><i class="fa fa-circle text-success"></i> Online</a>
				</div>
			</div>
			<?php // (Optional) Add Search box here ?>
			<?php //$this->insert('partials::sidemenu_search'); ?>
			<?php $this->insert('partials::sidemenu'); ?>
		</section>
	</aside>

	<?php // Right side column. Contains the navbar and content of the page ?>
	<div class="content-wrapper">
		<section class="content-header">
			<h1><?php echo $title; ?></h1>
			<?php $this->insert('partials::breadcrumb'); ?>
		</section>
		
		<section class="content">
			<?=$this->section('content')?>
			<?php $this->insert('partials::back_btn'); ?>
		</section>
	</div>

	<?php // Footer ?>
	<footer class="main-footer">
		<?php if (ENVIRONMENT=='development'): ?>
			<div class="pull-right hidden-xs">
				
			</div>
		<?php endif; ?>
		<strong>&copy; 2016-2017 <a href="http://www.xemesolutions.com"><?php echo "Xeme IT Solutions"; ?></a></strong> All rights reserved.
	</footer>

</div> 