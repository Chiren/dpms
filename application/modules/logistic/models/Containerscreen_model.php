<?php 
class Containerscreen_model  extends CI_Model  {
	
	function save($data, $job)
	{
		$msg = $this->db->insert('container_master', $data);
		$cid = $this->db->insert_id();
		
		$l = count($job['jobs']);
		//echo $l;
		for($i = 0; $i < $l; $i++){
			$data = array(
				'container_id' => $cid,
				'job_id' => $job['jobs'][$i],
				'qty' => $job['qty'][$i]
			);
			//echo "<pre>"; 
			//print_r($data);	 
			$this->db->insert('container_job', $data);
		}
		
		
	}
	function get_all(){
		//$sql = "select * from container_master join container_job on container_master.container_id = container_job.container_id join jobscreen_master on jobscreen_master.container_job_id = container_job.container_job_id GROUP BY container_job.container_id";
		//$query = $this->db->query($sql);
		//return $query->result();
		$this->db->order_by("con_status", "DESC");
		$this->db->where("con_status='0' OR con_status='1' OR con_status='2'");
		$this->db->order_by("container_date", "asc");
		return $this->db->get('container_master')->result();
	}
	function get($id){
		$this->db->where('id', $id);
		return $this->db->get('container_master')->result();
	}
	function update($id, $data){
		$this->db->where('id',$id);
		return $this->db->update('container_master',$data);
	}
	function delete($id){
		$this->db->where('id',$id);
		$this->db->delete('container_master');
	}
}
?>

