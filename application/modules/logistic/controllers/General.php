<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General extends Admin_Controller {
	
	

	public function notification() 
	{
		$this->render('_partials/notifications');	
	}
	public function rednotifications($id)
	{
		$this->load->model('General_model');
		$data = $this->General_model->rednoti($id);
	}
	
	function message()
	{
		$this->render('_partials/msg');
	}
	
	public function readmsg($id)
	{
		$this->load->model('General_model');
		$data = $this->General_model->readmsg($id);
	}
	
}