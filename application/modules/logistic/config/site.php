<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$config['site'] = array(

	'name' => 'Piramal CPMS',
	'title' => '',
	'multilingual' => array(),
	'authorized_groups' => array(
		'logistic'		=> array('adminlte_skin' => 'skin-purple')
	),

	'menu' => array(
		'home' => array(
			'groups'	=> array('admin', 'manager', 'staff'),
			'name'		=> 'Home',
			'url'		=> '',
			'icon'		=> 'fa fa-home',
		),
		
		'container_screen' => array(
			'groups'	=> array('admin'),
			'name'		=> 'Container Screen',
			'url'		=> 'containerscreen/',
			'icon'		=> 'fa fa-truck',		
		),
			
		'job_screen' => array(
			'groups'	=> array('admin'),
			'name'		=> 'Job Screen',
			'url'		=> 'jobscreen',
			'icon'		=> 'fa fa-tachometer',
		),
		
		'container_managent' => array(
			'groups'	=> array('admin'),
			'name'		=> 'Container Management',
			'url'		=> '#',
			'icon'		=> 'fa fa-calendar-minus-o',
			'children'  => array(
				'Add New Container'	=> 'container/add', 
				),
		),
		
		'job_management' => array(
			'groups'	=> array('admin'),
			'name'		=> 'Job Management',
			'url'		=> '#',
			'icon'		=> 'fa fa-folder-open-o',
			'children'  => array(
				'Add New Jobs'	=> 'jobs/add',
				'Current Jobs'	=> 'jobs/current'
			),
		),
			
		'settings' => array(
			'groups'	=> array('admin'),
			'name'		=> 'Settings',
			'url'		=> '#',
			'icon'		=> 'fa fa-cogs',
			'children'  => array(
				'TPD'	=> 'settings/tpd',
				'Container Type'	=> 'settings/container_type'
			),
		),	
			
		'Notification' => array(
			'groups'	=> array('admin'),
			'name'		=> 'Notification',
			'url'		=> 'notification',
			'icon'		=> 'fa fa-bell-o',
		),
			
		'Message' => array(
			'groups'	=> array('admin'),
			'name'		=> 'Message',
			'url'		=> 'message',
			'icon'		=> 'fa fa-commenting-o',
		),
			
		'logout' => array(
			'groups'	=> array('admin', 'manager', 'staff'),
			'name'		=> 'Sign Out',
			'url'		=> 'account/logout',
			'icon'		=> 'fa fa-sign-out',
		)
	),

	// Useful links to display at bottom of sidemenu (e.g. to pages outside Admin Panel)
	'useful_links' => array(
	
	),

	// For debug purpose (available only when ENVIRONMENT = 'development')
	'debug' => array(
		'view_data'		=> FALSE,	// whether to display MY_Controller's mViewData at page end
		'profiler'		=> FALSE,	// whether to display CodeIgniter's profiler at page end
	),
);