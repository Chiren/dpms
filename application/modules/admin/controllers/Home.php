<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Admin_Controller {
	
	

	public function index() 
	{	
		$this->render('home');	
	}
	
	public function booked($id)
	{
		$this->load->model('General_model');
		$data = $this->General_model->booked($id);
		redirect("admin/home");
	}
	
	public function canceled($id)
	{
		$this->load->model('General_model');
		$data = $this->General_model->canceled($id);
		redirect("admin/home");
	}
	
	public function completed($id)
	{
		$this->load->model('General_model');
		$data = $this->General_model->completed($id);
		redirect("admin/home");
	}
	
	public function detaining($id)
	{
		$this->load->model('General_model');
		$data = $this->General_model->detaining($id);
	}
	
	public function reported($id)
	{
		$this->load->model('General_model');
		$data = $this->General_model->reported($id);
	}
	
	public function detain($id)
	{
		$this->load->model('General_model');
		$data = $this->General_model->detain($id);
	}
	
	public function reloaded()
	{
		$this->render('planned_containers');
	}
	
	
	
}
