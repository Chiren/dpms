<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Uploadcsv extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}
	public function index()
	{
		$this->render('excelimport');
	}
	public function import()
	{
		//$size=$_FILES['file']['size'];
		//echo $size;
		//exit;
		ini_set('memory_limit', '-1');
		$this->load->helper(array('form', 'url')); 
		$this->load->helper('file');
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'xlsx|csv|xls';
		
		$config['file_name'] = date('YmdHisu').rand(0,100);
	 	$this->load->library('upload',$config);
		
	    $this->upload->do_upload('file');
		$filexcel=$this->upload->data();
		$exxel = $filexcel['file_name'];
		
		$fullpath = base_url();
		$all = $fullpath."uploads/".$exxel;

		include 'PHPExcel/IOFactory.php';
		$inputFileName = FCPATH."uploads/".$exxel; 
		
		try 
		{
			$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
		} 
		catch(Exception $e) 
		{
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}

		$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
		$arrayCount = count($allDataInSheet);
		
		$fggroup=array();
		$allfggroup=array();
		
		for($i=1;$i<=$arrayCount;$i++) 
		{
			$container_type = trim($allDataInSheet[$i]["A"]);
			$container_date = trim($allDataInSheet[$i]["B"]);
			$customer_name = trim($allDataInSheet[$i]["F"]);
			$fg_code = trim($allDataInSheet[$i]["H"]);
			$jobs = trim($allDataInSheet[$i]["I"]);
			$plant = trim($allDataInSheet[$i]["J"]);
			$tpd = trim($allDataInSheet[$i]["M"]);
			$status = trim($allDataInSheet[$i]["N"]);
			$qty = trim($allDataInSheet[$i]["Q"]);
			$amount_value = trim($allDataInSheet[$i]["S"]);
		
			$fggroup['fgcode']=$fg_code;
			
			
			if($container_type == null)
			{
				
			}
			else
			{
				$fggroup['ctype']=$container_type;
				$ctype = $container_type;
				$cdate = $container_date;
			}
			$allfggroup[]=$fggroup;	
			$chkcon = (explode("-",$container_type));
			if($container_type == null)
			{
				if($jobs == null)
				{
					
				}
				else
				{
					$this->load->model('container_model');
					$inserdata=$this->container_model->insert_con_excelsheet2($qty,$jobs,$tpd,$plant,$fg_code,$amount_value,$ctype,$cdate,$status,$amount_value);	
				}	
				
			}
			if($chkcon[0] == 'MAT' || $chkcon[0] == 'SEL' || $chkcon[0] == 'LCL' || $chkcon[0] == 'ANSA')
			{
				$container_name = $chkcon[0];
				$this->load->model('container_model');
				
				$inserdata=$this->container_model->insert_con_excelsheet1($customer_name,$container_type,$container_name,$qty,$jobs,$tpd,$container_date,$fg_code,$amount_value,$plant,$status,$amount_value);		
			}
					
		}
		$this->load->model('container_model');
		$deletedata=$this->container_model->delete_con($allfggroup);
		
		redirect('admin/containerscreen');
	}


	public function jobadd()
	{
		
		include 'PHPExcel/IOFactory.php';
		$inputFileName = FCPATH."KSB.XLSX"; 
		
		try 
		{
			$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
		} 
		catch(Exception $e) 
		{
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}

		$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
		$arrayCount = count($allDataInSheet);
		
		for($i=2;$i<=$arrayCount;$i++) 
		{
			
			$fg = trim($allDataInSheet[$i]["A"]);
			$name = trim($allDataInSheet[$i]["B"]);
			$this->load->model('container_model');
			date_default_timezone_set('Asia/Calcutta');
			$dates = date('Y-m-d');
			$data['job_name'] =$name;
			$data['fg_code'] =$fg;
			$data['date'] =$dates;
			
			$inserdata=$this->container_model->insertjob($data);
			
		}
			
	}
 

}