<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jobs extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}

	public function index()
	{
		echo "Hello Screen";
		die();
	}
	public function add()
	{
		$this->load->model('jobs_model');
		$data = $this->jobs_model->get_all();
		$this->mViewData['result']=$data;
		$this->render('jobs/add');
	}
	public function save()
	{
		$data['job_name'] = $_REQUEST['name'];
		$data['date'] = $_REQUEST['date'];
		$data['fg_code'] = $_REQUEST['fg_code'];
		$data['amount_value'] = $_REQUEST['amount'];
		$this->load->model('jobs_model');
		$id=$this->jobs_model->save($data);
		redirect('admin/jobs/add');
	}
	public function view($id)
	{
		echo $id;
	}
	public function edit($id)
	{
		$this->load->model('jobs_model');
		$data = $this->jobs_model->get($id);
		$this->mViewData['result']=$data;
		$this->render('jobs/edit');	
	}
	public function update()
	{
		$_REQUEST['id'];
		$data['job_name'] = $_REQUEST['name'];
		$data['date'] = $_REQUEST['date'];
		$data['fg_code'] = $_REQUEST['fg_code'];
		$data['amount_value'] = $_REQUEST['amount'];
		$this->load->model('jobs_model');
		$id=$this->jobs_model->update($_REQUEST['id'], $data);
		redirect('admin/jobs/add');
	}
	public function delete($id)
	{
		$this->load->model('jobs_model');
		$this->jobs_model->delete($id);
		redirect('admin/jobs/add');
	}
	
	public function all()
	{
		$this->load->model('jobs_model');
		$data = $this->jobs_model->get_all_jobs();
		$this->mViewData['result']=$data;
		$this->render('jobs/all');
	}
	
	public function current()
	{
		$this->load->model('jobs_model');
		$data = $this->jobs_model->get_current_jobs();
		$this->mViewData['result']=$data;
		$this->render('jobs/current');
	}
	
	public function replace()
	{
		$all = $this->input->post('job');
		$container =$this->input->post('container');
		
		$data = explode("-", $all);
		$con_id = $data[0];
		$con_job_id = $data[1];
		$qty = $data[2];
		
		$this->load->model('jobs_model');
		$data2 = $this->jobs_model->get_replace_jobs($container,$con_id,$con_job_id,$qty);
		
		redirect('admin/jobs/current'); 
	}
	
}
