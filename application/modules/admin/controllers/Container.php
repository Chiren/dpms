<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Container extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}

	/**
	 * Account Settings
	 */
	public function index()
	{
		$this->load->model('peak_periode_model');
		$data = $this->peak_periode_model->get_all();
		$this->mViewData['result']=$data;
		$this->render('peak_periode/peak_periode');
	}
	public function add()
	{
		
		$this->load->model('jobs_model');
		$jobs_data = $this->jobs_model->get_all();
		
		$this->load->model('Container_model'); 
		$data = $this->Container_model->get_all_incompletd();
		$this->mViewData['result']=array(
			'jobs'				=> $jobs_data,
			'tabledata'		=> $data
		);
		$this->render('container/add');
	}
	public function save()
	{	
		$str = $_REQUEST['container_type']."-". $_REQUEST['container_number'];
		$data['customer_name'] = $_REQUEST['customer_name'];
		$data['container_type'] = $str;
		$data['container_name'] = $_REQUEST['container_type'];
		$data['container_date'] = $_REQUEST['date'];
		$data['updatedOn'] = $_REQUEST['date'];
		//$data['total_quility'] = $_REQUEST['total_qty'];
		
		$job['jobs'] = $_REQUEST['jobs'];
		$job['qty'] = $_REQUEST['qty'];
		$job['tpd'] = $_REQUEST['tpd'];
		$job['plain'] = $_REQUEST['plain'];
		$job['job_price'] = $_REQUEST['price'];
		
		$this->load->model('container_model');
		$id=$this->container_model->save($data, $job);
		redirect('admin/container/add');
	}
	public function view($id)
	{
		$this->load->model('container_model');
		$data = $this->container_model->get($id);
		$datatwo = $this->container_model->get_job($id);
		$this->mViewData['result']=$data;
		$this->mViewData['job']=$datatwo;
		$this->render('container/view');
	}
	public function edit($id) 
	{
		$this->load->model('jobs_model');
		$jobs_data = $this->jobs_model->get_all();
			
		$this->load->model('container_model');
		$data = $this->container_model->get($id);
		
		$datatwo = $this->container_model->get_job($id);
		
		$this->mViewData['result']=$data;
		$this->mViewData['job']=$datatwo;
		$this->mViewData['resultjob']=$jobs_data;
		
		$this->render('container/edit');	
	}
	
	public function update()
	{
		$str = $_REQUEST['container_type']."-". $_REQUEST['container_number'];
		$data['customer_name'] = $_REQUEST['customer_name'];
		$data['container_type'] = $str;
		$data['container_name'] = $_REQUEST['container_type'];
		$data['container_date'] = $_REQUEST['date'];
		$data['updatedOn'] = $_REQUEST['date'];
		//$data['total_quility'] = $_REQUEST['total_qty'];
		
		
		$job['jobs'] = $_REQUEST['container_job_id'];
		$job['qty'] = $_REQUEST['qty'];
		$job['jobsid'] = $_REQUEST['jobs'];
		$job['tpd'] = $_REQUEST['tpd'];
		$job['plain'] = $_REQUEST['plain'];
		$job['job_price'] = $_REQUEST['price'];
		
		$this->load->model('container_model');
		$id=$this->container_model->update($_REQUEST['id'], $data,$job);
		redirect('admin/container/add');
	}
	
	public function delete($id)
	{
		$this->load->model('container_model');
		$this->container_model->delete($id);
		redirect('admin/container/add');
	}
	public function deletejobs() {
		$id = $this -> input -> post('id');
		
		
		$this -> load -> model('container_model');
		$data=$this -> container_model -> deletejobs($id);
	}
	
	public function demo()
	{
		$this->render('container/demo');
	}
	
	public function multipledelete()
	{
		$mid = $this->input->post('deletem');
		$this->load->model('container_model');
		
		foreach($mid as $m)
		{
			$id = $m;
			$this->container_model->delete($id);
		}
		
		redirect('admin/container/add');
		
	}


	public function chkcon($name)
	{
		$this->load->model('container_model');
		$data = $this->container_model->chkname($name);
		
		echo $data;
	}

	
	 

}
