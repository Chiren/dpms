<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends Admin_Controller {
	
	public function leadtime()
	{
		$this->render('reports/leadtime');	
	}
	
	// lead time
	
	public function skuleadtime()
	{
		$this->render('reports/skuleadtime');	
	}
	
	public function skudisplay()
	{
		$dept = $this->input->post('dept');
		$date = $this->input->post('my');
		
		$dateexplod = explode('-', $date);
		$year = $dateexplod['0'];
		$month = $dateexplod['1'];
		$this->load->model('reports_model');
		?>	
			<script>
				var table = $('#exampletwo').DataTable( {
        			lengthChange: false,
        			buttons: [
        					{
            					extend: 'excel',
            					text: 'Excel',
            					footer: true,
                   	 			header: true
                				
            				}
        			],
        		});
 				table.buttons().container().appendTo( '#exampletwo_wrapper .col-sm-6:eq(0)' );
    		
			</script>	
					
		<?php
		if($dept == 1)
		{
			$data = $this->reports_model->get_qc($month,$year);
			$sumqcry = 0;
			$sumqcyg = 0;
		
		
		print_r('<table id="exampletwo" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
	            <tr>
		        	<th>Bottle Name</th>
		        	<th>QTY</th>
		            <th>Red to Yellow</th>
		            <th>Yellow to Green</th>
		       	</tr>
	       	</thead>
			   
		    <tbody>');
	       	foreach($data as $d){ 
	       		print_r("<tr>");	
	       			print_r("<td>"); echo $d->job_name; print_r("</td>");
	       			print_r("<td>"); echo $d->qty; print_r("</td>");
	       			print_r("<td>"); 
	       					if($d->decoration == 0)
	       					{
	       						$qcrys = $this->reports_model->get_qc_redyellowend($d->container_job_id);
								foreach($qcrys as $qcs)
								{
										$aa = $d->start_date;
										$datetime = new DateTime($aa);
										$la_time = new DateTimeZone('Asia/Calcutta');
										$datetime->setTimezone($la_time);
										$date_1 = $datetime->format('Y-m-d H:i:s');
										$date_2 = $qcs->end_time;
										$differenceFormat1 = '%i';
										$differenceFormat2 = '%h'; 
										$differenceFormat3 = '%d';
										$datetime1 = date_create($date_1);
										$datetime2 = date_create($date_2);
										$interval = date_diff($datetime1, $datetime2);
										$minut = $interval->format($differenceFormat1);
										$hos = $interval->format($differenceFormat2);
										$days = $interval->format($differenceFormat3);
										$his = $hos*60;
										$dis = $days*24*60;
										$tol = (($minut+$his+$dis)/60)/24;
										echo number_format($tol, 2, '.', ',');
										$sumqcry+=number_format($tol, 2, '.', ',');
								}
								
	       					} else {
	       						$qcrysa = $this->reports_model->get_qc_redyellowansastart($d->container_job_id);
	       						foreach($qcrysa as $qcsa)
								{
										$date_1 = $qcsa->ansa_start_datetime;
										$qcrycom = $this->reports_model->get_qc_redyellowansaend($qcsa->container_job_id);
										$date_2 = null;
										foreach($qcrycom as $qcryco)
										{
											$date_2 = $qcryco->end_time;
										}
										$differenceFormat1 = '%i';
										$differenceFormat2 = '%h';
										$differenceFormat3 = '%d';
										$datetime1 = date_create($date_1);
										$datetime2 = date_create($date_2);
										$interval = date_diff($datetime1, $datetime2);
										$minut = $interval->format($differenceFormat1);
										$hos = $interval->format($differenceFormat2);
										$days = $interval->format($differenceFormat3);
										$his = $hos*60;
										$dis = $days*24*60;
										$tol = (($minut+$his+$dis)/60)/24;
										echo number_format($tol, 2, '.', ',');
										$sumqcry+=number_format($tol, 2, '.', ',');
								}
							
							} 
	       			
	       			print_r("</td>");
	       			print_r("<td>"); 
	       					$qcygs = $this->reports_model->get_qc_yellowgreenstart($d->container_job_id);
	       					foreach($qcygs as $qcyg)
	       					{
	       						$date_1 = $qcyg->end_time;
								$qcyge = $this->reports_model->get_qc_yellowgreenend($qcyg->container_job_id);	
	       						$date_2 = null;
	       						foreach($qcyge as $qctgee)
								{
									$date_2 = $qctgee->end_time;	
								}
								$differenceFormat1 = '%i';
								$differenceFormat2 = '%h';
								$differenceFormat3 = '%d';
								$datetime1 = date_create($date_1);
								$datetime2 = date_create($date_2);
								$interval = date_diff($datetime1, $datetime2);
								$minut = $interval->format($differenceFormat1);
								$hos = $interval->format($differenceFormat2);
								$days = $interval->format($differenceFormat3);
								$his = $hos*60;
								$dis = $days*24*60;
								$tol = (($minut+$his+$dis)/60)/24;
								echo number_format($tol, 2, '.', ',');
								$sumqcyg+=number_format($tol, 2, '.', ',');
							}
	       			print_r("</td>");
	       		print_r("</tr>");
	       		 } 	
	       	print_r("</tbody>");
			print_r("<tfoot>
					<th>"); print_r("</th>");
					print_r("<th>"); print_r("</th>");
                	print_r("<th>"); echo $sumqcry; print_r("</th>");
                	print_r("<th>"); echo $sumqcyg; print_r("</th>");
           	print_r("</tfoot>");
			print_r("</table>");
		
			
		} 
		
		if($dept == 2)
		{
			$data = $this->reports_model->get_qc($month,$year);
			$sumqary = 0;
			$sumqayg = 0;
		?>
			
			<table id="example2" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
	            <tr>
		        	<th>Bottle Name</th>
		        	<th>QTY</th>
		            <th>Red to Yellow</th>
		            <th>Yellow to Green</th>
		       	</tr>
	       	</thead>
	       	   
	   
	       	<tbody>
	       		<?php foreach($data as $d){ ?>
	       		<tr>	
	       			<td><?php echo $d->job_name; ?></td>
	       			<td><?php echo $d->qty; ?></td>
	       			<td>
	       				<?php $qarystart = $this->reports_model->get_qa_redyellowstart($d->container_job_id); 
	       						
								foreach($qarystart as $qarys)
								{
									$date_1 = $qarys->qc_start_datetime;
									$qaryend = $this->reports_model->get_qa_redyellowend($qarys->container_job_id);
									$date_2 = null;
									foreach($qaryend as $qarye)
									{
										$date_2 = $qarye->end_time;	
									}
									$differenceFormat1 = '%i';
									$differenceFormat2 = '%h';
									$differenceFormat3 = '%d';
									$datetime1 = date_create($date_1);
									$datetime2 = date_create($date_2);
									$interval = date_diff($datetime1, $datetime2);
									$minut = $interval->format($differenceFormat1);
									$hos = $interval->format($differenceFormat2);
									$days = $interval->format($differenceFormat3);
									$his = $hos*60;
									$dis = $days*24*60;
									$tol = (($minut+$his+$dis)/60)/24;
									echo number_format($tol, 2, '.', ',');
									$sumqary+=number_format($tol, 2, '.', ',');
								}
	       				?>	
	       			</td>
	       			<td>
	       				<?php $qaygstart = $this->reports_model->get_qa_redyellowend($d->container_job_id); 
	       						
								foreach($qaygstart as $qaygs)
								{
									$date_1 = $qaygs->end_time;
									$qaygend = $this->reports_model->get_qa_yellowgreenend($qaygs->container_job_id);
									$date_2 = null;
									foreach($qaygend as $qayge)
									{
										$date_2 = $qayge->end_time;	
									}
									$differenceFormat1 = '%i';
									$differenceFormat2 = '%h';
									$differenceFormat3 = '%d';
									$datetime1 = date_create($date_1);
									$datetime2 = date_create($date_2);
									$interval = date_diff($datetime1, $datetime2);
									$minut = $interval->format($differenceFormat1);
									$hos = $interval->format($differenceFormat2);
									$days = $interval->format($differenceFormat3);
									$his = $hos*60;
									$dis = $days*24*60;
									$tol = (($minut+$his+$dis)/60)/24;
									echo number_format($tol, 2, '.', ',');
									$sumqayg+=number_format($tol, 2, '.', ',');
								}
	       				?>
	       			</td>
	       		</tr>
	       		<?php } ?>	
	       	</tbody>
	       	<tfoot>
                	<th></th>
		        	<th></th>
		            <th><?php echo $sumqary; ?></th>
		            <th><?php echo $sumqayg; ?></th>
           	</tfoot>
			</table>
			<script>
				var tabletwo = $('#example2').DataTable( {
        			lengthChange: false,
        			buttons: [
        					{
            					extend: 'excel',
            					text: 'Excel',
            					footer: true,
                   	 			header: true
                				
            				}
        			],
            	});
 				tabletwo.buttons().container().appendTo( '#example2_wrapper .col-sm-6:eq(0)' );
    		
			</script>
			
			
		<?php 
		}
		
		if($dept == 3)
		{
			$data = $this->reports_model->get_qc($month,$year);
			$sumwhry = 0;
			$sumwhyg = 0;
		?>	
			<table id="example3" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
	            <tr>
		        	<th>Bottle Name</th>
		        	<th>QTY</th>
		            <th>Red to Yellow</th>
		            <th>Yellow to Green</th>
		       	</tr>
	       	</thead>
	       	
	   
	       	<tbody>
	       		<?php foreach($data as $d){ ?>
	       		<tr>	
	       			<td><?php echo $d->job_name; ?></td>
	       			<td><?php echo $d->qty; ?></td>
	       			<td>
	       				<?php $whrystart = $this->reports_model->get_wh_redyellowstart($d->container_job_id); 
	       						
								foreach($whrystart as $whrys)
								{
									$date_1 = $whrys->qa_start_datetime;
									$whryend = $this->reports_model->get_qa_redyellowend($whrys->container_job_id);
									$date_2 = null;
									foreach($whryend as $whrye)
									{
										$date_2 = $whrye->end_time;	
									}
									$differenceFormat1 = '%i';
									$differenceFormat2 = '%h';
									$differenceFormat3 = '%d';
									$datetime1 = date_create($date_1);
									$datetime2 = date_create($date_2);
									$interval = date_diff($datetime1, $datetime2);
									$minut = $interval->format($differenceFormat1);
									$hos = $interval->format($differenceFormat2);
									$days = $interval->format($differenceFormat3);
									$his = $hos*60;
									$dis = $days*24*60;
									$tol = (($minut+$his+$dis)/60)/24;
									echo number_format($tol, 2, '.', ',');
									$sumwhry+= number_format($tol, 2, '.', ',');
								}
	       				?>	
	       			</td>
	       			<td>
	       				<?php $whygstart = $this->reports_model->get_wh_redyellowend($d->container_job_id); 
	       						
								foreach($whygstart as $whygs)
								{
									$date_1 = $whygs->end_time;
									$whygend = $this->reports_model->get_wh_yellowgreenend($whygs->container_job_id);
									$date_2 = null;
									foreach($whygend as $whyge)
									{
										$date_2 = $whyge->end_time;	
									}
									$differenceFormat1 = '%i';
									$differenceFormat2 = '%h';
									$differenceFormat3 = '%d';
									$datetime1 = date_create($date_1);
									$datetime2 = date_create($date_2);
									$interval = date_diff($datetime1, $datetime2);
									$minut = $interval->format($differenceFormat1);
									$hos = $interval->format($differenceFormat2);
									$days = $interval->format($differenceFormat3);
									$his = $hos*60;
									$dis = $days*24*60;
									$tol = (($minut+$his+$dis)/60)/24;
									echo number_format($tol, 2, '.', ',');
									$sumwhyg+= number_format($tol, 2, '.', ',');
								}
	       				?>
	       			</td>
	       		</tr>
	       		<?php } ?>	
	       	</tbody>
	       	<tfoot>
	       			<th></th>
	       			<th></th>
	       			<th><?php echo $sumwhry; ?></th>
	       			<th><?php echo $sumwhyg; ?></th>
	       	</tfoot>
			</table>	
			<script>
				var table = $('#example3').DataTable( {
        			lengthChange: false,
        			buttons: [
        					{
            					extend: 'excel',
            					text: 'Excel',
            					footer: true,
                   	 			header: true
                				
            				}
        			],
        		});
 				table.buttons().container().appendTo( '#example3_wrapper .col-sm-6:eq(0)' );
    		
			</script>
			
		<?php
		}
		
		if($dept == 4)
		{
			$data = $this->reports_model->get_ansa($month,$year);
			$sumansary = 0;
			$sumansayg = 0;
		?>	
			<table id="example4" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
	            <tr>
		        	<th>Bottle Name</th>
		        	<th>QTY</th>
		            <th>Red to Yellow</th>
		            <th>Yellow to Green</th>
		       	</tr>
	       	</thead>
	       	
	   
	       	<tbody>
	       		<?php foreach($data as $d){ ?>
	       		<tr>	
	       			<td><?php echo $d->job_name; ?></td>
	       			<td><?php echo $d->qty; ?></td>
	       			<td>
	       				<?php 
									$date_1 = $d->start_date;
									$ansaryend = $this->reports_model->get_ansa_redyellowend($d->container_job_id);
									$date_2 = null;
									foreach($ansaryend as $ansarye)
									{
										$date_2 = $ansarye->end_time;	
									}
									$differenceFormat1 = '%i';
									$differenceFormat2 = '%h';
									$differenceFormat3 = '%d';
									$datetime1 = date_create($date_1);
									$datetime2 = date_create($date_2);
									$interval = date_diff($datetime1, $datetime2);
									$minut = $interval->format($differenceFormat1);
									$hos = $interval->format($differenceFormat2);
									$days = $interval->format($differenceFormat3);
									$his = $hos*60;
									$dis = $days*24*60;
									$tol = (($minut+$his+$dis)/60)/24;
									echo number_format($tol, 2, '.', ',');
									$sumansary+=number_format($tol, 2, '.', ',');
								
	       				?>	
	       			</td>
	       			<td>
	       				<?php $ansaygstart = $this->reports_model->get_ansa_redyellowend($d->container_job_id); 
	       						
								foreach($ansaygstart as $ansaygs)
								{
									$date_1 = $ansaygs->end_time;
									$ansaygend = $this->reports_model->get_ansa_yellowgreenend($ansaygs->container_job_id);
									$date_2 = null;
									foreach($ansaygend as $ansayge)
									{
										$date_2 = $ansayge->end_time;	
									}
									$differenceFormat1 = '%i';
									$differenceFormat2 = '%h';
									$differenceFormat3 = '%d';
									$datetime1 = date_create($date_1);
									$datetime2 = date_create($date_2);
									$interval = date_diff($datetime1, $datetime2);
									$minut = $interval->format($differenceFormat1);
									$hos = $interval->format($differenceFormat2);
									$days = $interval->format($differenceFormat3);
									$his = $hos*60;
									$dis = $days*24*60;
									$tol = (($minut+$his+$dis)/60)/24;
									echo number_format($tol, 2, '.', ',');
									$sumansayg+=number_format($tol, 2, '.', ',');
								}
	       				?>
	       			</td>
	       		</tr>
	       		<?php } ?>	
	       	</tbody>
	       	<tfoot>
	       			<th></th>
	       			<th></th>
	       			<th><?php echo $sumansary; ?></th>
	       			<th><?php echo $sumansayg; ?></th>
	       	</tfoot>
			</table>	
			<script>
				var table = $('#example4').DataTable( {
        			lengthChange: false,
        			buttons: [
        					{
            					extend: 'excel',
            					text: 'Excel',
            					footer: true,
                   	 			header: true
                				
            				}
        			],
        			
        			
            	});
 				table.buttons().container().appendTo( '#example4_wrapper .col-sm-6:eq(0)' );
    		
			</script>
			
		<?php	
			
		}
		
		
	}


		// sku lead time
		

	public function ratio()
	{
		$this->render('reports/ratio');	
	}
	
	public function rationset()
	{
		$dept = $this->input->post('dept');
		$date = $this->input->post('my');
		
		$dateexplod = explode('-', $date);
		$year = $dateexplod['0'];
		$month = $dateexplod['1'];
		$this->load->model('reports_model');
		
	if($dept == '100')
	{
		$tpd100qcd = 0;
		$tpd100qcb = 0;
		$tpd100qcs = 0;
		$tpd100qad = 0;
		$tpd100qab = 0;
		$tpd100qas = 0;
		$tpd100whd = 0;
		$tpd100whb = 0;
		$tpd100whs = 0;
		$tpd100ansad = 0;
		$tpd100ansab = 0;
		$tpd100ansas = 0;
			
			$tpd100 = $this->reports_model->get_tpd100ratio($month,$year);	
			foreach($tpd100 as $t100)
			{
				if($t100->chk_date != '0000-00-00')
				{
					$tpd100qcend = $this->reports_model->get_tpd100ratioqcend($t100->container_job_id);
					foreach($tpd100qcend as $tpd100qce)
					{
						$dateqc = date("Y-m-d",strtotime($tpd100qce->end_time));
						if($t100->chk_date > $dateqc)
						{	
							$tpd100qcb++;
						}
						if($t100->chk_date < $dateqc)
						{
							$tpd100qcd++;
						}
						if($t100->chk_date == $dateqc)
						{
							$tpd100qcs++;
						}	
					}
					//Qc end
					
					$tpd100qaend = $this->reports_model->get_tpd100ratioqaend($t100->container_job_id);
					foreach($tpd100qaend as $tpd100qae)
					{
						$dateqa = date("Y-m-d",strtotime($tpd100qae->end_time));
						if($t100->chk_date > $dateqa)
						{	
							$tpd100qab++;
						}
						if($t100->chk_date < $dateqa)
						{
							$tpd100qad++;
						}
						if($t100->chk_date == $dateqa)
						{
							$tpd100qas++;
						}	
					}
					//Qa end
					
					$tpd100whend = $this->reports_model->get_tpd100ratiowhend($t100->container_job_id);
					foreach($tpd100whend as $tpd100whe)
					{
						$datewh = date("Y-m-d",strtotime($tpd100whe->end_time));
						if($t100->chk_date > $datewh)
						{	
							$tpd100whb++;
						}
						if($t100->chk_date < $datewh)
						{
							$tpd100whd++;
						}
						if($t100->chk_date == $datewh)
						{
							$tpd100whs++;
						}	
					}
					//Wh end
					
					$tpd100ansaend = $this->reports_model->get_tpd100ratioansaend($t100->container_job_id);
					foreach($tpd100ansaend as $tpd100ansae)
					{
						$dateansa = date("Y-m-d",strtotime($tpd100ansae->end_time));
						if($t100->chk_date > $dateansa)
						{	
							$tpd100ansab++;
						}
						if($t100->chk_date < $dateansa)
						{
							$tpd100ansad++;
						}
						if($t100->chk_date == $dateansa)
						{
							$tpd100ansas++;
						}	
					}
					//Ansa end
					
					
				} 
			}
			if($tpd100ansab != '0' || $tpd100ansas != '0' || $tpd100ansad != '0' || $tpd100qcb != '0' || $tpd100qcs != '0' || $tpd100qcd != '0' || $tpd100qab != '0' || $tpd100qas != '0' || $tpd100qad != '0' || $tpd100whb != '0' || $tpd100whs != '0' || $tpd100whd != '0') {
			?>
			<div id="chartdiv1" class="col-md-6" style="margin-top: 15px; height:300px"></div>
			<div id="chartdiv2" class="col-md-6" style="margin-top: 15px; height:300px"></div>
			<div id="chartdiv3" class="col-md-6" style="margin-top: 15px; height:300px"></div>
			<div id="chartdiv4" class="col-md-6" style="margin-top: 15px; height:300px"></div>
			<script>
			var chart = AmCharts.makeChart( "chartdiv1", {
		  		"type": "pie",
		  		"theme": "light",
		  		"titles": [ {
		    	"text": "ANSA",
		    	"color":"#333333",
		    	"size": 16
		    
		  		}],
		  			"dataProvider": [ {
		    		"bottle": "Before",
		    		"bottle_no": <?php echo $tpd100ansab; ?>
		    	}, {
		    		"bottle": "On same Day",
		    		"bottle_no": <?php echo $tpd100ansas; ?>
		  		}, {
		    		"bottle": "Detained",
		    		"bottle_no": <?php echo $tpd100ansad; ?>
		  		},],
		  			"valueField": "bottle_no",
		  			"titleField": "bottle",
		  			"startEffect": "elastic",
		  			"startDuration": 2,
		  			"labelRadius": 15,
		  			"innerRadius": "50%",
		  			"depth3D": 10,
		  			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
		  			"angle": 15,
		  			"labelText":"[[title]]:[[value]]",
		  			"export": {
		    		"enabled": false
		  		}
				} );
				</script>
				
				<script>
			var chart = AmCharts.makeChart( "chartdiv2", {
		  		"type": "pie",
		  		"theme": "light",
		  		"titles": [ {
		    	"text": "QC",
		    	"color":"#333333",
		    	"size": 16
		    
		  		}],
		  			"dataProvider": [ {
		    		"bottle": "Before",
		    		"bottle_no": <?php echo $tpd100qcb; ?>
		    	}, {
		    		"bottle": "On same Day",
		    		"bottle_no": <?php echo $tpd100qcs; ?>
		  		}, {
		    		"bottle": "Detained",
		    		"bottle_no": <?php echo $tpd100qcd; ?>
		  		},],
		  			"valueField": "bottle_no",
		  			"titleField": "bottle",
		  			"startEffect": "elastic",
		  			"startDuration": 2,
		  			"labelRadius": 15,
		  			"innerRadius": "50%",
		  			"depth3D": 10,
		  			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
		  			"angle": 15,
		  			"labelText":"[[title]]:[[value]]",
		  			"export": {
		    		"enabled": false
		  		}
				} );
				</script>
				
				<script>
			var chart = AmCharts.makeChart( "chartdiv3", {
		  		"type": "pie",
		  		"theme": "light",
		  		"titles": [ {
		    	"text": "QA",
		    	"color":"#333333",
		    	"size": 16
		    
		  		}],
		  			"dataProvider": [ {
		    		"bottle": "Before",
		    		"bottle_no": <?php echo $tpd100qab; ?>
		    	}, {
		    		"bottle": "On same Day",
		    		"bottle_no": <?php echo $tpd100qas; ?>
		  		}, {
		    		"bottle": "Detained",
		    		"bottle_no": <?php echo $tpd100qad; ?>
		  		},],
		  			"valueField": "bottle_no",
		  			"titleField": "bottle",
		  			"startEffect": "elastic",
		  			"startDuration": 2,
		  			"labelRadius": 15,
		  			"innerRadius": "50%",
		  			"depth3D": 10,
		  			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
		  			"angle": 15,
		  			"labelText":"[[title]]:[[value]]",
		  			"export": {
		    		"enabled": false
		  		}
				} );
				</script>
				
				<script>
			var chart = AmCharts.makeChart( "chartdiv4", {
		  		"type": "pie",
		  		"theme": "light",
		  		"titles": [ {
		    	"text": "WH",
		    	"color":"#333333",
		    	"size": 16
		    
		  		}],
		  			"dataProvider": [ {
		    		"bottle": "Before",
		    		"bottle_no": <?php echo $tpd100whb; ?>
		    	}, {
		    		"bottle": "On same Day",
		    		"bottle_no": <?php echo $tpd100whs; ?>
		  		}, {
		    		"bottle": "Detained",
		    		"bottle_no": <?php echo $tpd100whd; ?>
		  		},],
		  			"valueField": "bottle_no",
		  			"titleField": "bottle",
		  			"startEffect": "elastic",
		  			"startDuration": 2,
		  			"labelRadius": 15,
		  			"innerRadius": "50%",
		  			"depth3D": 10,
		  			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
		  			"angle": 15,
		  			"labelText":"[[title]]:[[value]]",
		  			"export": {
		    		"enabled": false
		  		}
				} );
				</script>
		<?php
			}
			else
			{
				print_r("<div class='col-md-12 text-center'> <h1>No Records</h1></div>");
			}			
			
			}
			if($dept == '60')
			{
				$tpd60qcd = 0;
				$tpd60qcb = 0;
				$tpd60qcs = 0;
				$tpd60qad = 0;
				$tpd60qab = 0;
				$tpd60qas = 0;
				$tpd60whd = 0;
				$tpd60whb = 0;
				$tpd60whs = 0;
				$tpd60ansad = 0;
				$tpd60ansab = 0;
				$tpd60ansas = 0;
			
			$tpd60 = $this->reports_model->get_tpd60ratio($month,$year);	
			foreach($tpd60 as $t60)
			{
				if($t60->chk_date != '0000-00-00')
				{
					$tpd60qcend = $this->reports_model->get_tpd60ratioqcend($t60->container_job_id);
					foreach($tpd60qcend as $tpd60qce)
					{
						$date60qc = date("Y-m-d",strtotime($tpd60qce->end_time));
						if($t60->chk_date > $date60qc)
						{	
							$tpd60qcb++;
						}
						if($t60->chk_date < $date60qc)
						{
							$tpd60qcd++;
						}
						if($t60->chk_date == $date60qc)
						{
							$tpd60qcs++;
						}	
					}
					//Qc end
					
					$tpd60qaend = $this->reports_model->get_tpd60ratioqaend($t60->container_job_id);
					foreach($tpd60qaend as $tpd60qae)
					{
						$date60qa = date("Y-m-d",strtotime($tpd60qae->end_time));
						if($t60->chk_date > $date60qa)
						{	
							$tpd60qab++;
						}
						if($t60->chk_date < $date60qa)
						{
							$tpd60qad++;
						}
						if($t60->chk_date == $date60qa)
						{
							$tpd60qas++;
						}	
					}
					//Qa end
					
					$tpd60whend = $this->reports_model->get_tpd60ratiowhend($t60->container_job_id);
					foreach($tpd60whend as $tpd60whe)
					{
						$date60wh = date("Y-m-d",strtotime($tpd60whe->end_time));
						if($t60->chk_date > $date60wh)
						{	
							$tpd60whb++;
						}
						if($t60->chk_date < $date60wh)
						{
							$tpd60whd++;
						}
						if($t60->chk_date == $date60wh)
						{
							$tpd60whs++;
						}	
					}
					//Wh end
					
					$tpd60ansaend = $this->reports_model->get_tpd60ratioansaend($t60->container_job_id);
					foreach($tpd60ansaend as $tpd60ansae)
					{
						$date60ansa = date("Y-m-d",strtotime($tpd60ansae->end_time));
						if($t60->chk_date > $date60ansa)
						{	
							$tpd60ansab++;
						}
						if($t60->chk_date < $date60ansa)
						{
							$tpd60ansad++;
						}
						if($t60->chk_date == $date60ansa)
						{
							$tpd60ansas++;
						}	
					}
					//Ansa end
					
					
				} 
			}
			if($tpd60ansab != '0' || $tpd60ansas != '0' || $tpd60ansad != '0' || $tpd60qcb != '0' || $tpd60qcs != '0' || $tpd60qcd != '0' || $tpd60qab != '0' || $tpd60qas != '0' || $tpd60qad != '0' || $tpd60whb != '0' || $tpd60whs != '0' || $tpd60whd != '0') {		
			?>
			<div id="chartdiv1" class="col-md-6" style="margin-top: 15px; height:300px"></div>
			<div id="chartdiv2" class="col-md-6" style="margin-top: 15px; height:300px"></div>
			<div id="chartdiv3" class="col-md-6" style="margin-top: 15px; height:300px"></div>
			<div id="chartdiv4" class="col-md-6" style="margin-top: 15px; height:300px"></div>
			<script>
			var chart = AmCharts.makeChart( "chartdiv1", {
		  		"type": "pie",
		  		"theme": "light",
		  		"titles": [ {
		    	"text": "ANSA",
		    	"color":"#333333",
		    	"size": 16
		    
		  		}],
		  			"dataProvider": [ {
		    		"bottle": "Before",
		    		"bottle_no": <?php echo $tpd60ansab; ?>
		    	}, {
		    		"bottle": "On same Day",
		    		"bottle_no": <?php echo $tpd60ansas; ?>
		  		}, {
		    		"bottle": "Detained",
		    		"bottle_no": <?php echo $tpd60ansad; ?>
		  		},],
		  			"valueField": "bottle_no",
		  			"titleField": "bottle",
		  			"startEffect": "elastic",
		  			"startDuration": 2,
		  			"labelRadius": 15,
		  			"innerRadius": "50%",
		  			"depth3D": 10,
		  			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
		  			"angle": 15,
		  			"labelText":"[[title]]:[[value]]",
		  			"export": {
		    		"enabled": false
		  		}
				} );
				</script>
				
				<script>
			var chart = AmCharts.makeChart( "chartdiv2", {
		  		"type": "pie",
		  		"theme": "light",
		  		"titles": [ {
		    	"text": "QC",
		    	"color":"#333333",
		    	"size": 16
		    
		  		}],
		  			"dataProvider": [ {
		    		"bottle": "Before",
		    		"bottle_no": <?php echo $tpd60qcb; ?>
		    	}, {
		    		"bottle": "On same Day",
		    		"bottle_no": <?php echo $tpd60qcs; ?>
		  		}, {
		    		"bottle": "Detained",
		    		"bottle_no": <?php echo $tpd60qcd; ?>
		  		},],
		  			"valueField": "bottle_no",
		  			"titleField": "bottle",
		  			"startEffect": "elastic",
		  			"startDuration": 2,
		  			"labelRadius": 15,
		  			"innerRadius": "50%",
		  			"depth3D": 10,
		  			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
		  			"angle": 15,
		  			"labelText":"[[title]]:[[value]]",
		  			"export": {
		    		"enabled": false
		  		}
				} );
				</script>
				
				<script>
			var chart = AmCharts.makeChart( "chartdiv3", {
		  		"type": "pie",
		  		"theme": "light",
		  		"titles": [ {
		    	"text": "QA",
		    	"color":"#333333",
		    	"size": 16
		    
		  		}],
		  			"dataProvider": [ {
		    		"bottle": "Before",
		    		"bottle_no": <?php echo $tpd60qab; ?>
		    	}, {
		    		"bottle": "On same Day",
		    		"bottle_no": <?php echo $tpd60qas; ?>
		  		}, {
		    		"bottle": "Detained",
		    		"bottle_no": <?php echo $tpd60qad; ?>
		  		},],
		  			"valueField": "bottle_no",
		  			"titleField": "bottle",
		  			"startEffect": "elastic",
		  			"startDuration": 2,
		  			"labelRadius": 15,
		  			"innerRadius": "50%",
		  			"depth3D": 10,
		  			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
		  			"angle": 15,
		  			"labelText":"[[title]]:[[value]]",
		  			"export": {
		    		"enabled": false
		  		}
				} );
				</script>
				
				<script>
			var chart = AmCharts.makeChart( "chartdiv4", {
		  		"type": "pie",
		  		"theme": "light",
		  		"titles": [ {
		    	"text": "WH",
		    	"color":"#333333",
		    	"size": 16
		    
		  		}],
		  			"dataProvider": [ {
		    		"bottle": "Before",
		    		"bottle_no": <?php echo $tpd60whb; ?>
		    	}, {
		    		"bottle": "On same Day",
		    		"bottle_no": <?php echo $tpd60whs; ?>
		  		}, {
		    		"bottle": "Detained",
		    		"bottle_no": <?php echo $tpd60whd; ?>
		  		},],
		  			"valueField": "bottle_no",
		  			"titleField": "bottle",
		  			"startEffect": "elastic",
		  			"startDuration": 2,
		  			"labelRadius": 15,
		  			"innerRadius": "50%",
		  			"depth3D": 10,
		  			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
		  			"angle": 15,
		  			"labelText":"[[title]]:[[value]]",
		  			"export": {
		    		"enabled": false
		  		}
				} );
				</script>
		<?php	
			}
			else
			{
				print_r("<div class='col-md-12 text-center'> <h1>No Records</h1></div>");
			}			
			}	
			if($dept == '95')
			{
				$tpd95qcd = 0;
				$tpd95qcb = 0;
				$tpd95qcs = 0;
				$tpd95qad = 0;
				$tpd95qab = 0;
				$tpd95qas = 0;
				$tpd95whd = 0;
				$tpd95whb = 0;
				$tpd95whs = 0;
				$tpd95ansad = 0;
				$tpd95ansab = 0;
				$tpd95ansas = 0;
			
			$tpd95 = $this->reports_model->get_tpd95ratio($month,$year);	
			foreach($tpd95 as $t95)
			{
				if($t95->chk_date != '0000-00-00')
				{
					$tpd95qcend = $this->reports_model->get_tpd95ratioqcend($t95->container_job_id);
					foreach($tpd95qcend as $tpd95qce)
					{
						$date95qc = date("Y-m-d",strtotime($tpd95qce->end_time));
						if($t95->chk_date > $date95qc)
						{	
							$tpd95qcb++;
						}
						if($t95->chk_date < $date95qc)
						{
							$tpd95qcd++;
						}
						if($t95->chk_date == $date95qc)
						{
							$tpd95qcs++;
						}	
					}
					//Qc end
					
					$tpd95qaend = $this->reports_model->get_tpd95ratioqaend($t95->container_job_id);
					foreach($tpd95qaend as $tpd95qae)
					{
						$date95qa = date("Y-m-d",strtotime($tpd95qae->end_time));
						if($t95->chk_date > $date95qa)
						{	
							$tpd95qab++;
						}
						if($t95->chk_date < $date95qa)
						{
							$tpd95qad++;
						}
						if($t95->chk_date == $date95qa)
						{
							$tpd95qas++;
						}	
					}
					//Qa end
					
					$tpd95whend = $this->reports_model->get_tpd95ratiowhend($t95->container_job_id);
					foreach($tpd95whend as $tpd95whe)
					{
						$date95wh = date("Y-m-d",strtotime($tpd95whe->end_time));
						if($t95->chk_date > $date95wh)
						{	
							$tpd95whb++;
						}
						if($t95->chk_date < $date95wh)
						{
							$tpd95whd++;
						}
						if($t95->chk_date == $date95wh)
						{
							$tpd95whs++;
						}	
					}
					//Wh end
					
					$tpd95ansaend = $this->reports_model->get_tpd60ratioansaend($t95->container_job_id);
					foreach($tpd95ansaend as $tpd95ansae)
					{
						$date95ansa = date("Y-m-d",strtotime($tpd95ansae->end_time));
						if($t95->chk_date > $date95ansa)
						{	
							$tpd95ansab++;
						}
						if($t95->chk_date < $date95ansa)
						{
							$tpd95ansad++;
						}
						if($t95->chk_date == $date95ansa)
						{
							$tpd95ansas++;
						}	
					}
					//Ansa end
					
					
				}
				}
			if($tpd95ansab != '0' || $tpd95ansas != '0' || $tpd95ansad != '0' || $tpd95qcb != '0' || $tpd95qcs != '0' || $tpd95qcd != '0' || $tpd95qab != '0' || $tpd95qas != '0' || $tpd95qad != '0' || $tpd95whb != '0' || $tpd95whs != '0' || $tpd95whd != '0') {		
			?>
			<div id="chartdiv1" class="col-md-6" style="margin-top: 15px; height:300px"></div>
			<div id="chartdiv2" class="col-md-6" style="margin-top: 15px; height:300px"></div>
			<div id="chartdiv3" class="col-md-6" style="margin-top: 15px; height:300px"></div>
			<div id="chartdiv4" class="col-md-6" style="margin-top: 15px; height:300px"></div>
			<script>
			var chart = AmCharts.makeChart( "chartdiv1", {
		  		"type": "pie",
		  		"theme": "light",
		  		"titles": [ {
		    	"text": "ANSA",
		    	"color":"#333333",
		    	"size": 16
		    
		  		}],
		  			"dataProvider": [ {
		    		"bottle": "Before",
		    		"bottle_no": <?php echo $tpd95ansab; ?>
		    	}, {
		    		"bottle": "On same Day",
		    		"bottle_no": <?php echo $tpd95ansas; ?>
		  		}, {
		    		"bottle": "Detained",
		    		"bottle_no": <?php echo $tpd95ansad; ?>
		  		},],
		  			"valueField": "bottle_no",
		  			"titleField": "bottle",
		  			"startEffect": "elastic",
		  			"startDuration": 2,
		  			"labelRadius": 15,
		  			"innerRadius": "50%",
		  			"depth3D": 10,
		  			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
		  			"angle": 15,
		  			"labelText":"[[title]]:[[value]]",
		  			"export": {
		    		"enabled": false
		  		}
				} );
				</script>
				
				<script>
			var chart = AmCharts.makeChart( "chartdiv2", {
		  		"type": "pie",
		  		"theme": "light",
		  		"titles": [ {
		    	"text": "QC",
		    	"color":"#333333",
		    	"size": 16
		    
		  		}],
		  			"dataProvider": [ {
		    		"bottle": "Before",
		    		"bottle_no": <?php echo $tpd95qcb; ?>
		    	}, {
		    		"bottle": "On same Day",
		    		"bottle_no": <?php echo $tpd95qcs; ?>
		  		}, {
		    		"bottle": "Detained",
		    		"bottle_no": <?php echo $tpd95qcd; ?>
		  		},],
		  			"valueField": "bottle_no",
		  			"titleField": "bottle",
		  			"startEffect": "elastic",
		  			"startDuration": 2,
		  			"labelRadius": 15,
		  			"innerRadius": "50%",
		  			"depth3D": 10,
		  			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
		  			"angle": 15,
		  			"labelText":"[[title]]:[[value]]",
		  			"export": {
		    		"enabled": false
		  		}
				} );
				</script>
				
				<script>
			var chart = AmCharts.makeChart( "chartdiv3", {
		  		"type": "pie",
		  		"theme": "light",
		  		"titles": [ {
		    	"text": "QA",
		    	"color":"#333333",
		    	"size": 16
		    
		  		}],
		  			"dataProvider": [ {
		    		"bottle": "Before",
		    		"bottle_no": <?php echo $tpd95qab; ?>
		    	}, {
		    		"bottle": "On same Day",
		    		"bottle_no": <?php echo $tpd95qas; ?>
		  		}, {
		    		"bottle": "Detained",
		    		"bottle_no": <?php echo $tpd95qad; ?>
		  		},],
		  			"valueField": "bottle_no",
		  			"titleField": "bottle",
		  			"startEffect": "elastic",
		  			"startDuration": 2,
		  			"labelRadius": 15,
		  			"innerRadius": "50%",
		  			"depth3D": 10,
		  			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
		  			"angle": 15,
		  			"labelText":"[[title]]:[[value]]",
		  			"export": {
		    		"enabled": false
		  		}
				} );
				</script>
				
				<script>
			var chart = AmCharts.makeChart( "chartdiv4", {
		  		"type": "pie",
		  		"theme": "light",
		  		"titles": [ {
		    	"text": "WH",
		    	"color":"#333333",
		    	"size": 16
		    
		  		}],
		  			"dataProvider": [ {
		    		"bottle": "Before",
		    		"bottle_no": <?php echo $tpd95whb; ?>
		    	}, {
		    		"bottle": "On same Day",
		    		"bottle_no": <?php echo $tpd95whs; ?>
		  		}, {
		    		"bottle": "Detained",
		    		"bottle_no": <?php echo $tpd95whd; ?>
		  		},],
		  			"valueField": "bottle_no",
		  			"titleField": "bottle",
		  			"startEffect": "elastic",
		  			"startDuration": 2,
		  			"labelRadius": 15,
		  			"innerRadius": "50%",
		  			"depth3D": 10,
		  			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
		  			"angle": 15,
		  			"labelText":"[[title]]:[[value]]",
		  			"export": {
		    		"enabled": false
		  		}
				} );
				</script>
		<?php
			}
			else
			{
				print_r("<div class='col-md-12 text-center'> <h1>No Records</h1></div>");
			}		
			}
			if($dept == 'NG')
			{
				
				$tpdngqcd = 0;
				$tpdngqcb = 0;
				$tpdngqcs = 0;
				$tpdngqad = 0;
				$tpdngqab = 0;
				$tpdngqas = 0;
				$tpdngwhd = 0;
				$tpdngwhb = 0;
				$tpdngwhs = 0;
				$tpdngansad = 0;
				$tpdngansab = 0;
				$tpdngansas = 0;
			
			$tpdng = $this->reports_model->get_tpdngratio($month,$year);	
			foreach($tpdng as $tng)
			{
				if($tng->chk_date != '0000-00-00')
				{
					$tpdngqcend = $this->reports_model->get_tpdngratioqcend($tng->container_job_id);
					foreach($tpdngqcend as $tpdngqce)
					{
						$datengqc = date("Y-m-d",strtotime($tpdngqce->end_time));
						if($tng->chk_date > $datengqc)
						{	
							$tpdngqcb++;
						}
						if($tng->chk_date < $datengqc)
						{
							$tpdngqcd++;
						}
						if($tng->chk_date == $datengqc)
						{
							$tpdngqcs++;
						}	
					}
					//Qc end
					
					$tpdngqaend = $this->reports_model->get_tpdngratioqaend($tng->container_job_id);
					foreach($tpdngqaend as $tpdngqae)
					{
						$datengqa = date("Y-m-d",strtotime($tpdngqae->end_time));
						if($tng->chk_date > $datengqa)
						{	
							$tpdngqab++;
						}
						if($tng->chk_date < $datengqa)
						{
							$tpdngqad++;
						}
						if($tng->chk_date == $datengqa)
						{
							$tpdngqas++;
						}	
					}
					//Qa end
					
					$tpdngwhend = $this->reports_model->get_tpdngratiowhend($tng->container_job_id);
					foreach($tpdngwhend as $tpdngwhe)
					{
						$datengwh = date("Y-m-d",strtotime($tpdngwhe->end_time));
						if($tng->chk_date > $datengwh)
						{	
							$tpdngwhb++;
						}
						if($tng->chk_date < $datengwh)
						{
							$tpdngwhd++;
						}
						if($tng->chk_date == $datengwh)
						{
							$tpdngwhs++;
						}	
					}
					//Wh end
					
					$tpdngansaend = $this->reports_model->get_tpd60ratioansaend($tng->container_job_id);
					foreach($tpdngansaend as $tpdngansae)
					{
						$datengansa = date("Y-m-d",strtotime($tpdngansae->end_time));
						if($tng->chk_date > $datengansa)
						{	
							$tpdngansab++;
						}
						if($tng->chk_date < $datengansa)
						{
							$tpdngansad++;
						}
						if($tng->chk_date == $datengansa)
						{
							$tpdngansas++;
						}	
					}
					//Ansa end
					
					
				}
				}	
			 	
			 	if($tpdngansab != '0' || $tpdngansas != '0' || $tpdngansad != '0' || $tpdngqcb != '0' || $tpdngqcs != '0' || $tpdngqcd != '0' || $tpdngqab != '0' || $tpdngqas != '0' || $tpdngqad != '0' || $tpdngwhb != '0' || $tpdngwhs != '0' || $tpdngwhd != '0') { 
				
				?>
			
			<div id="chartdiv1" class="col-md-6" style="margin-top: 15px; height:300px"></div>
			<div id="chartdiv2" class="col-md-6" style="margin-top: 15px; height:300px"></div>
			<div id="chartdiv3" class="col-md-6" style="margin-top: 15px; height:300px"></div>
			<div id="chartdiv4" class="col-md-6" style="margin-top: 15px; height:300px"></div>
			<script>
			var chart = AmCharts.makeChart( "chartdiv1", {
		  		"type": "pie",
		  		"theme": "light",
		  		"titles": [ {
		    	"text": "ANSA",
		    	"color":"#333333",
		    	"size": 16
		    
		  		}],
		  			"dataProvider": [ {
		    		"bottle": "Before",
		    		"bottle_no": <?php echo $tpdngansab; ?>
		    	}, {
		    		"bottle": "On same Day",
		    		"bottle_no": <?php echo $tpdngansas; ?>
		  		}, {
		    		"bottle": "Detained",
		    		"bottle_no": <?php echo $tpdngansad; ?>
		  		},],
		  			"valueField": "bottle_no",
		  			"titleField": "bottle",
		  			"startEffect": "elastic",
		  			"startDuration": 2,
		  			"labelRadius": 15,
		  			"innerRadius": "50%",
		  			"depth3D": 10,
		  			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
		  			"angle": 15,
		  			"labelText":"[[title]]:[[value]]",
		  			"export": {
		    		"enabled": false
		  		}
				} );
				</script>
				
				<script>
			var chart = AmCharts.makeChart( "chartdiv2", {
		  		"type": "pie",
		  		"theme": "light",
		  		"titles": [ {
		    	"text": "QC",
		    	"color":"#333333",
		    	"size": 16
		    
		  		}],
		  			"dataProvider": [ {
		    		"bottle": "Before",
		    		"bottle_no": <?php echo $tpdngqcb; ?>
		    	}, {
		    		"bottle": "On same Day",
		    		"bottle_no": <?php echo $tpdngqcs; ?>
		  		}, {
		    		"bottle": "Detained",
		    		"bottle_no": <?php echo $tpdngqcd; ?>
		  		},],
		  			"valueField": "bottle_no",
		  			"titleField": "bottle",
		  			"startEffect": "elastic",
		  			"startDuration": 2,
		  			"labelRadius": 15,
		  			"innerRadius": "50%",
		  			"depth3D": 10,
		  			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
		  			"angle": 15,
		  			"labelText":"[[title]]:[[value]]",
		  			"export": {
		    		"enabled": false
		  		}
				} );
				</script>
				
				<script>
			var chart = AmCharts.makeChart( "chartdiv3", {
		  		"type": "pie",
		  		"theme": "light",
		  		"titles": [ {
		    	"text": "QA",
		    	"color":"#333333",
		    	"size": 16
		    
		  		}],
		  			"dataProvider": [ {
		    		"bottle": "Before",
		    		"bottle_no": <?php echo $tpdngqab; ?>
		    	}, {
		    		"bottle": "On same Day",
		    		"bottle_no": <?php echo $tpdngqas; ?>
		  		}, {
		    		"bottle": "Detained",
		    		"bottle_no": <?php echo $tpdngqad; ?>
		  		},],
		  			"valueField": "bottle_no",
		  			"titleField": "bottle",
		  			"startEffect": "elastic",
		  			"startDuration": 2,
		  			"labelRadius": 15,
		  			"innerRadius": "50%",
		  			"depth3D": 10,
		  			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
		  			"angle": 15,
		  			"labelText":"[[title]]:[[value]]",
		  			"export": {
		    		"enabled": false
		  		}
				} );
				</script>
				
				<script>
			var chart = AmCharts.makeChart( "chartdiv4", {
		  		"type": "pie",
		  		"theme": "light",
		  		"titles": [ {
		    	"text": "WH",
		    	"color":"#333333",
		    	"size": 16
		    
		  		}],
		  			"dataProvider": [ {
		    		"bottle": "Before",
		    		"bottle_no": <?php echo $tpdngwhb; ?>
		    	}, {
		    		"bottle": "On same Day",
		    		"bottle_no": <?php echo $tpdngwhs; ?>
		  		}, {
		    		"bottle": "Detained",
		    		"bottle_no": <?php echo $tpdngwhd; ?>
		  		},],
		  			"valueField": "bottle_no",
		  			"titleField": "bottle",
		  			"startEffect": "elastic",
		  			"startDuration": 2,
		  			"labelRadius": 15,
		  			"innerRadius": "50%",
		  			"depth3D": 10,
		  			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
		  			"angle": 15,
		  			"labelText":"[[title]]:[[value]]",
		  			"export": {
		    		"enabled": false
		  		}
				} );
				</script>
		<?php	
				}
				else
				{
					print_r("<div class='col-md-12 text-center'> <h1>No Records</h1></div>");
				}		
			}
					
	
			}

			// ratio 
			
			
		public function qaperformance()
		{
			$this->render('reports/qaperformance');
		}
		
		public function qaperset()
		{
			$date = $this->input->post('my');
		
			$dateexplod = explode('-', $date);
			$year = $dateexplod['0'];
			$month = $dateexplod['1'];
			$this->load->model('reports_model');
			
			$qa_before = 0;
			$qa_same = 0;
			$qa_late = 0;
			
			$qa_ready = 0;
			
			$qaper = $this->reports_model->get_qaperformance($month,$year);
			foreach($qaper as $qap)
			{
				$qa_before+= $qap->qa_before;
				$qa_same +=$qap->qa_same;
				$qa_late +=$qap->qa_late;
				if($qap->qa_ready == 1)
				{
					$qa_ready++;
				}
			}
			
			$resorting = $qa_before+$qa_late+$qa_same;
			$qatable = $this->reports_model->get_qaperformancetabel($month,$year);
			?>
			<div id="chartdiv" class="col-md-12" style="margin-top: 15px; height:300px"></div>
			<script>
			var chart = AmCharts.makeChart( "chartdiv", {
		  		"type": "pie",
		  		"theme": "light",
		  		"titles": [ {
		    	"text": "No of Incidents OK material failed",
		    	"color":"#333333",
		    	"size": 16
		    
		  		}],
		  			"dataProvider": [ {
		    		"bottle": "Before",
		    		"bottle_no": <?php echo $qa_before; ?>
		    	}, {
		    		"bottle": "On same Day",
		    		"bottle_no": <?php echo $qa_same; ?>
		  		}, {
		    		"bottle": "Detained",
		    		"bottle_no": <?php echo $qa_late; ?>
		  		},],
		  			"valueField": "bottle_no",
		  			"titleField": "bottle",
		  			"startEffect": "elastic",
		  			"startDuration": 2,
		  			"labelRadius": 15,
		  			"innerRadius": "50%",
		  			"depth3D": 10,
		  			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
		  			"angle": 15,
		  			"labelText":"[[title]]:[[value]]",
		  			"export": {
		    		"enabled": false
		  		}
				} );
				</script>
				<div class="col-md-12">
		<table id="example100" class="table table-striped table-bordered" cellspacing="0" width="100%">
           	 	<thead>
	            	<tr>
		        		<th>Jobs</th>
		        		<th>Status</th>
		        		<th>Reported Date</th>
		       		</tr>
	       		</thead>
	       		<tbody>
	       		<?php 	
					foreach($qatable as $qatbl2)
					{
						if($qatbl2->qa_before== 1)
						{
							print_r('<tr>');
							$jobs = $this->reports_model->get_jobs($qatbl2->job_id);	
							foreach($jobs as $j)
							{
								echo '<td>'.$j->job_name.'</td>';
							}
							print_r('<td>Before</td>');
							$repoted_date = $this->reports_model->get_repoted($qatbl2->container_id);
							foreach($repoted_date as $rd)
							{
								if($rd->chk_date != '0000-00-00')
								{
									echo '<td>'.date('d-m-Y', strtotime($rd->chk_date)).'</td>';
								}
								else
								{
									echo '<td>Not Reported</td>';
								}
							}
							print_r('</tr>');
							
						}
						if($qatbl2->qa_same== 1 )
						{
							print_r('<tr>');
							$jobs = $this->reports_model->get_jobs($qatbl2->job_id);	
							foreach($jobs as $j)
							{
								echo '<td>'.$j->job_name.'</td>';
							}
							print_r('<td>On same Day</td>');
							$repoted_date = $this->reports_model->get_repoted($qatbl2->container_id);
							foreach($repoted_date as $rd)
							{
								if($rd->chk_date != '0000-00-00')
								{
									echo '<td>'.date('d-m-Y', strtotime($rd->chk_date)).'</td>';
								}
								else
								{
									echo '<td>Not Reported</td>';
								}
							}
							print_r('</tr>');
						}
						if($qatbl2->qa_late== 1)
						{
							print_r('<tr>');
							$jobs = $this->reports_model->get_jobs($qatbl2->job_id);	
							foreach($jobs as $j)
							{
								echo '<td>'.$j->job_name.'</td>';
							}
							print_r('<td>Detained</td>');
							$repoted_date = $this->reports_model->get_repoted($qatbl2->container_id);
							foreach($repoted_date as $rd)
							{
								if($rd->chk_date != '0000-00-00')
								{
									echo '<td>'.date('d-m-Y', strtotime($rd->chk_date)).'</td>';
								}
								else
								{
									echo '<td>Not Reported</td>';
								}
							}
							print_r('</tr>');
						}
					}
				?>
	       		</tbody>
	       		<tfoot>
                	<th>Jobs</th>
		        	<th>Status</th>
		        	<th>Reported Date</th>
           		</tfoot>
			</table>
			<script>
				var tabletwo = $('#example100').DataTable( {
        			lengthChange: false,
        			buttons: [
        					{
            					extend: 'excel',
            					text: 'Excel',
            					header: true
                				
            				}
        			],
            	});
 				tabletwo.buttons().container().appendTo( '#example100_wrapper .col-sm-6:eq(0)' );
    		
			</script>
			
			</div>
			<div class="col-md-12"><hr /></div>	
			
			<div id="chartdiv1" class="col-md-12" style="margin-top: 15px; height:300px"></div>
			
			
				
				<script>
			var chart = AmCharts.makeChart( "chartdiv1", {
		  		"type": "pie",
		  		"theme": "light",
		  		"titles": [ {
		    	"text": "No of Incidents OK material failed",
		    	"color":"#333333",
		    	"size": 16
		    
		  		}],
		  			"dataProvider": [ {
		    		"bottle": "Was originally OK while planned",
		    		"bottle_no": <?php echo $qa_ready; ?>
		    	}, {
		    		"bottle": "Failed after resorting",
		    		"bottle_no": <?php echo $resorting; ?>
		  		}, {
		    		"bottle": "Failed at last moment",
		    		"bottle_no": <?php echo $qa_same; ?>
		  		},],
		  			"valueField": "bottle_no",
		  			"titleField": "bottle",
		  			"startEffect": "elastic",
		  			"startDuration": 2,
		  			"labelRadius": 15,
		  			"innerRadius": "50%",
		  			"depth3D": 10,
		  			"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
		  			"angle": 15,
		  			"labelText":"[[title]]:[[value]]",
		  			"export": {
		    		"enabled": false
		  		}
				} );
				</script>
				
			<div class="col-md-12">
			
			<table id="example101" class="table table-striped table-bordered" cellspacing="0" width="100%">
           	 	<thead>
	            	<tr>
		        		<th>Jobs</th>
		        		<th>Status</th>
		        		<th>Reported Date</th>
		       		</tr>
	       		</thead>
	       		<tbody>
	       		<?php 	
					foreach($qatable as $qatbl)
					{
						if($qatbl->qa_ready== 1)
						{
							print_r('<tr>');
							$jobs = $this->reports_model->get_jobs($qatbl->job_id);	
							foreach($jobs as $j)
							{
								echo '<td>'.$j->job_name.'</td>';
							}
							print_r('<td>Was originally OK while planned</td>');
							$repoted_date = $this->reports_model->get_repoted($qatbl->container_id);
							foreach($repoted_date as $rd)
							{
								if($rd->chk_date != '0000-00-00')
								{
									echo '<td>'.date('d-m-Y', strtotime($rd->chk_date)).'</td>';
								}
								else
								{
									echo '<td>Not Reported</td>';
								}
							}
							print_r('</tr>');
							
						}
						if($qatbl->qa_same== 1 )
						{
							print_r('<tr>');
							$jobs = $this->reports_model->get_jobs($qatbl->job_id);	
							foreach($jobs as $j)
							{
								echo '<td>'.$j->job_name.'</td>';
							}
							print_r('<td>Failed at last moment</td>');
							$repoted_date = $this->reports_model->get_repoted($qatbl->container_id);
							foreach($repoted_date as $rd)
							{
								if($rd->chk_date != '0000-00-00')
								{
									echo '<td>'.date('d-m-Y', strtotime($rd->chk_date)).'</td>';
								}
								else
								{
									echo '<td>Not Reported</td>';
								}
							}
							print_r('</tr>');
							
							print_r('<tr>');
							$job = $this->reports_model->get_jobs($qatbl->job_id);	
							foreach($job as $jj)
							{
								echo '<td>'.$jj->job_name.'</td>';
							}
							print_r('<td>Failed after resorting</td>');
							$repoted_dates = $this->reports_model->get_repoted($qatbl->container_id);
							foreach($repoted_dates as $rds)
							{
								if($rds->chk_date != '0000-00-00')
								{
									echo '<td>'.date('d-m-Y', strtotime($rds->chk_date)).'</td>';
								}
								else
								{
									echo '<td>Not Reported</td>';
								}
							}
							print_r('</tr>');
						}
						
						if($qatbl->qa_late== 1)
						{
							print_r('<tr>');
							$jobs = $this->reports_model->get_jobs($qatbl->job_id);	
							foreach($jobs as $j)
							{
								echo '<td>'.$j->job_name.'</td>';
							}
							print_r('<td>Failed after resorting</td>');
							$repoted_date = $this->reports_model->get_repoted($qatbl->container_id);
							foreach($repoted_date as $rd)
							{
								if($rd->chk_date != '0000-00-00')
								{
									echo '<td>'.date('d-m-Y', strtotime($rd->chk_date)).'</td>';
								}
								else
								{
									echo '<td>Not Reported</td>';
								}
							}
							print_r('</tr>');
						}

						if($qatbl->qa_before== 1)
						{
							print_r('<tr>');
							$jobs = $this->reports_model->get_jobs($qatbl->job_id);	
							foreach($jobs as $j)
							{
								echo '<td>'.$j->job_name.'</td>';
							}
							print_r('<td>Failed after resorting</td>');
							$repoted_date = $this->reports_model->get_repoted($qatbl->container_id);
							foreach($repoted_date as $rd)
							{
								if($rd->chk_date != '0000-00-00')
								{
									echo '<td>'.date('d-m-Y', strtotime($rd->chk_date)).'</td>';
								}
								else
								{
									echo '<td>Not Reported</td>';
								}	
							}
							print_r('</tr>');
							
						}
					}
				?>
	       			</tbody>
	       			<tfoot>
                		<th>Jobs</th>
		        		<th>Status</th>
		        		<th>Reported Date</th>
           			</tfoot>
				</table>
				<script>
					var tabletwo = $('#example101').DataTable( {
        			lengthChange: false,
        			buttons: [
        					{
            					extend: 'excel',
            					text: 'Excel',
            					header: true
                				
            				}
        				],
            		});
 					tabletwo.buttons().container().appendTo( '#example101_wrapper .col-sm-6:eq(0)' );
				</script>
			
			</div>	
		
		<?php
				
		}
			



}


