<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Message extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}
	public function index()
	{
		$id=0;
		$this->mViewData['ii']=$id;	
		$this->render('message/index');
	}
	public function chat($id)
	{
		$this->mViewData['ii']=$id;
		$this->render('message/index');
	}
	
	
	public function chathistory()
	{
			$user_id = $this->session->user_id;
			$chat_id = $this->input->post('id');
			$this->load->model('message_m');
			$select = $this->message_m->messagesall($user_id,$chat_id);
			
			foreach($select as $key=>$row)
			{				

				if($row->sender == $user_id)
				{
					echo "<div style='width:700px; text-align:right'>";
					echo "<font size='2'>".date("d/m/Y h:i:s",strtotime($row->datetime))."</font><br/>";
					echo "<font style='font-family:arial'>".$row->msg."</font><br/>";	  
					echo '</div><br/>';
				}
				else
				{
					echo "<div style='width:700px; text-align:left'>";
					echo "<font size='2'>".date("d/m/Y h:i:s",strtotime($row->datetime))."</font><br/>";
					echo "<font style='font-family:arial'>".$row->msg."</font><br/>";	  
					echo '</div><br/>';
				}
			}		
			
			    
	}
	
	
	public function msg_save()
		{					
	
			$data['sender'] = $this->session->user_id;
			$data['msg'] = $this->input->post('cmt');
			$data['receiver'] = $this->input->post('chatuser');
			date_default_timezone_set('Asia/Calcutta');
			$data['datetime'] = date("Y-m-d h:i:s");
			
			$this->load->model('message_m');             							
			$inquery = $this->message_m->insert_comment($data);	
			
			if($inquery)
			{
				 		
					echo "<div style='width:700px; text-align:right'>";
					echo "<font size='2'>".date('d/m/Y h:i:s')."</font><br/>";
					echo "<font style='font-family:arial'>".$data['msg']."</font><br/>";
					echo '</div><br/>';	 
			}
			else
			{
				echo "Data not inserted";
			}
		}
	
	public function mulmsg_save()
		{					
	
			$data['sender'] = $this->session->user_id;
			$data['msg'] = $this->input->post('cmt');
			$alluser = $this->input->post('chatuser');
			
			
			date_default_timezone_set('Asia/Calcutta');
			$data['datetime'] = date("Y-m-d h:i:s");
			foreach ($alluser as $user) {
				$data['receiver']=$user;
				$this->load->model('message_m');             							
			$inquery = $this->message_m->insert_commentmul($data);	
			}
				
			
			
		}
		
	
	
}
?>	