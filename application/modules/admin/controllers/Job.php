<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// NOTE: this controller inherits from MY_Controller,
// instead of Admin_Controller since no authentication is required
class Job extends MY_Controller {
		
	public function tpd100()
	{	
		$this->load->model('job_model');
		$data = $this->job_model->get_all_100();
		$this->mViewData['result']=$data;
		$this->render('all/tpd100');
	}
	public function tpd100inner()
	{
		$this->load->model('job_model');
		$data = $this->job_model->get_all_100();
		$this->mViewData['result']=$data;
		$this->render('all/tpd100inner');
	}
	public function tpd60()
	{	
		$this->load->model('job_model');
		$data = $this->job_model->get_all_60();
		$this->mViewData['result']=$data;
		$this->render('all/tpd60');
	}
	public function tpd60inner()
	{	
		$this->load->model('job_model');
		$data = $this->job_model->get_all_60();
		$this->mViewData['result']=$data;
		$this->render('all/tpd60inner');
	}
	public function tpd95()
	{	
		$this->load->model('job_model');
		$data = $this->job_model->get_all_95();
		$this->mViewData['result']=$data;
		$this->render('all/tpd95');
	}
	public function tpd95inner()
	{	
		$this->load->model('job_model');
		$data = $this->job_model->get_all_95();
		$this->mViewData['result']=$data;
		$this->render('all/tpd95inner');
	}
	public function ng()
	{	
		$this->load->model('job_model');
		$data = $this->job_model->get_all_ng();
		$this->mViewData['result']=$data;
		$this->render('all/ng');
	}
	public function nginner()
	{	
		$this->load->model('job_model');
		$data = $this->job_model->get_all_ng();
		$this->mViewData['result']=$data;
		$this->render('all/nginner');
	}
	public function ansa()
	{	
		$this->load->model('job_model');
		$data = $this->job_model->get_all_ansa();
		$this->mViewData['result']=$data;
		$this->render('all/ansa');
	}
	public function ansainner()
	{	
		$this->load->model('job_model');
		$data = $this->job_model->get_all_ansa();
		$this->mViewData['result']=$data;
		$this->render('all/ansainner');
	}
	
	
	
	public function sound()
	{
		$this->render('all/sound');
	}
	
	public function soundcom($id)
	{
		$this->load->model('General_model');
		$data = $this->General_model->soundcom($id);
	}	
		
}

	