<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// NOTE: this controller inherits from MY_Controller,
// instead of Admin_Controller since no authentication is required
class Login extends MY_Controller {

	/**
	 * Login page and submission
	 */
	public function index()
	{
		$this->load->library('form_builder');
		$form = $this->form_builder->create_form();
		if($this->ion_auth->logged_in())
	{
		$role = $_SESSION["role"];
		if($role == 1){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('admin');
				}else if($role == 2){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('management');
				}else if($role == 3){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('ng');
				}else if($role == 4){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					//die("abcd"); 
					redirect('executive');
				}else if($role == 7){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('tpd60qc');
				}else if($role == 9){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('tpd60qa'); 
				}else if($role == 10){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('tpd60wh');
				}else if($role == 11){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('tpd95qa');
				}else if($role == 12){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('tpd95qc');
				}else if($role == 13){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('tpd95wh');
				}else if($role == 14){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('tpd100qa');
				}else if($role == 15){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('tpd100qc');
				}else if($role == 16){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('tpd100wh');
				}else if($role == 17){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('pdi');
				}else if($role == 18){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('logistic');
				}else if($role == 19){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('ansa');
				}
	}
	else
	{
		if ($form->validate())
		{
			// passed validation
			$identity = $this->input->post('username');
			$password = $this->input->post('password');
			$remember = ($this->input->post('remember')=='on');
			
			if ($this->ion_auth->login($identity, $password, $remember))
			{
				// login succeed
				$role = $_SESSION["role"];
				//print_r($role); 
				//die();
				/*$messages = $this->ion_auth->messages();
				$this->system_message->set_success($messages);
				redirect('admin');*/
				if($role == 1){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('admin');
				}else if($role == 2){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('management');
				}else if($role == 3){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('ng');
				}else if($role == 4){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					//die("abcd"); 
					redirect('executive');
				}else if($role == 7){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('tpd60qc');
				}else if($role == 9){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('tpd60qa'); 
				}else if($role == 10){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('tpd60wh');
				}else if($role == 11){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('tpd95qa');
				}else if($role == 12){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('tpd95qc');
				}else if($role == 13){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('tpd95wh');
				}else if($role == 14){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('tpd100qa');
				}else if($role == 15){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('tpd100qc');
				}else if($role == 16){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('tpd100wh');
				}else if($role == 17){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('pdi');
				}else if($role == 18){
					$messages = $this->ion_auth->messages();
					$this->system_message->set_success($messages);
					redirect('logistic');
				}
				
			}
			else
			{
				// login failed
				$errors = $this->ion_auth->errors();
				$this->system_message->set_error($errors);
				refresh();
			}
		}
		}
		// display form when no POST data, or validation failed
		$this->mViewData['body_class'] = 'login-page';
		$this->mViewData['form'] = $form;
		$this->mBodyClass = 'login-page';
		$this->render('login', 'empty');
	}
}
