<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Month_change extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}

	public function index()
	{
		$this->render('month_change');
	}
	
	public function deletemonth()
	{
		$this->load->model('general_model');
		$this->general_model->monthdelete();
		redirect('home');
	}
	
}