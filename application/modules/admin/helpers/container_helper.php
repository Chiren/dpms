<?php 


function get_job_name_by_con($id)
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_job join job_master ON container_job.job_id=job_master.job_id WHERE container_job.container_id=".$id." order by container_job.container_job_id ASC";
	$query=$ci->db->query($sql);
	return $query->result();
}

function get_job_status($id)
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_job join jobscreen_master ON container_job.container_job_id=jobscreen_master.container_job_id WHERE container_job.container_id=".$id." order by container_job.container_job_id ASC";
	$query=$ci->db->query($sql);
	return $query->result();
}

function get_completed_date($id)
{
	$ci=&get_instance();
	$sql="SELECT * FROM jobscreen_master join container_job ON container_job.container_job_id=jobscreen_master.container_job_id WHERE container_job.container_id=".$id." ORDER BY jobscreen_master.wh_updated DESC LIMIT 1";
	$query=$ci->db->query($sql);
	return $query->result();
}

function completed_container()
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_master where con_status='4' AND MONTH(completed_date) = MONTH(NOW()) AND YEAR(completed_date) = YEAR(NOW())";
	$query=$ci->db->query($sql);
	return $query->result();
}

?>