<?php $this->layout('layouts::default') ?>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<div class="container">
<section class="col-lg-11 connectedSortable ui-sortable">
                 <!-- quick email widget -->
          <div class="box box-info">
            <div class="box-header ui-sortable-handle" style="cursor: move;">
              <i class="fa fa-user"></i>

              <h3 class="box-title">Edit User</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                
                
              </div>
              <!-- /. tools -->
            </div>
            <div class="box-body">
             </div>
            <div class="box-footer clearfix">
             <form name="frm_update" action="auth/updateuser" method="post">
             	<?php foreach ($result as $user) {  ?>
<div class="col-md-12">
			
			<div class="col-md-5">First Name</div>
			<div class="col-md-5">Last Name</div>
				
        	
	
</div>

<div class="col-md-12">
	<div class="col-md-5">
		<input type="text" name="fname" class="form-control" value="<?php echo $user->first_name; ?>"/>
	</div>
	<div class="col-md-5">
		<input type="text" name="lname" id="lname" class="form-control" value="<?php echo $user->last_name; ?>"/>
		
	</div>
</div>
<div class="col-md-12">&nbsp;</div>
<div class="col-md-12">
	<div class="col-md-5">Username</div>
	<div class="col-md-5">Password</div>
</div>
<div class="col-md-12">
	<div class="col-md-5">
		<input type="text" name="username" class="form-control" value="<?php echo $user->username; ?>"/>
	</div>
	<div class="col-md-5">
		<input type="text" name="upassword" class="form-control" value=""/>
		<input type="hidden" name="uid" class="form-control" value="<?php echo $user->uid; ?>"/>
	</div>
</div>
<div class="col-md-12">&nbsp;</div>
<div class="col-md-12">
	<div class="col-md-5">Username</div>
	
</div>
<div class="col-md-12">
	<div class="col-md-5">
		
		<?php
			   	$ci=&get_instance();
			   	$ci->load->helper('common');
				$allgroups=get_groups();
		?>
		<select name="select_group" class="form-control">
			<option value="">-- Select Group --</option>
			<?php foreach ($allgroups as $group) { ?>
				<option value="<?php echo $group->id; ?>" <?php if($group->id==$user->group_id) echo 'selected="selected"' ?> ><?php echo $group->description; ?></option>
			<?php } ?>
		</select>
	</div>
</div>
<div class="col-md-12">&nbsp;</div>
<div class="col-md-12">
	<div class="col-md-6">
	 <button type="submit" class="pull-left btn btn-default" id="save">Save
                <i class="fa fa-arrow-circle-right"></i></button>
    </div>
</div>
<?php } ?>
</form>
            </div>
            
          </div>

        </section>
</div>