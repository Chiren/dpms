<?php $this->layout('layouts::default') ?>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<script src="<?php echo base_url(); ?>assets/pci/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/pci/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/pci/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/pci/responsive.bootstrap.min.js"></script>
<style>
.dataTables_paginate.paging_simple_numbers
{
	text-align: right;
}
.pagination
{
	margin: 0px;
}
.dataTables_filter
{
	text-align: right;
}
</style>
<div class="container">
<section class="col-lg-12 connectedSortable ui-sortable">
                 <!-- quick email widget -->
          <div class="box box-info">
            <div class="box-header ui-sortable-handle" style="cursor: move;">
              <i class="fa fa-user"></i>

              <h3 class="box-title">Users</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <a href="user/add"><button>Add New User</button></a>
                
              </div>
              <!-- /. tools -->
            </div>
            <div class="box-body">
             </div>
            <div class="box-footer clearfix">
              <?php
				$ci=&get_instance();
				$ci->load->helper('common');
				$allusers_data=get_users();
				
						  	
		 ?>
		<table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0">
        <thead>
        	
            <tr>
                <th>Username</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>User Type</th>
                <th>Action</th>
                
                
            </tr>
        </thead>
        <tbody>
        	<?php
        	foreach ($allusers_data as $user) {
					
			?>	
        	<tr>
                <td><?php echo $user->username; ?></td>
                <td><?php echo $user->first_name; ?></td>
                <td><?php echo $user->last_name; ?></td>
                <td><?php echo $user->description; ?></td>
                <td>
                	<a href="user/edituser/<?php echo $user->uid; ?>" title="Editar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                	
                	<a href="user/duser/<?php echo $user->uid; ?>" title="Excluir" onclick="return confirm('Are You Sure To Detele?')"><i class="fa fa-trash-o" aria-hidden="true"></i>
</a>
                </td>
            </tr>
           <?php } ?>
        </tbody>
    </table>
            </div>
          </div>

        </section>
</div>