<?php
$ci=&get_instance();
$ci->load->helper('common');
date_default_timezone_set('Asia/Calcutta');
?>
<script>
$("#tpdselect option").each(function(){
  if ($(this).val() == "<?php echo $tpds; ?>")
    $(this).attr("selected","selected");
});
</script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/pci/jquery-ui.css">
<script src="<?php echo base_url(); ?>assets/pci/jquery-ui.js"></script> 
<style>
	.info-box-content
	{
		margin: 0px !important;
	}
	.progress-description
	{
		white-space: normal;
		font-size: 11px;
	}
	#country-list {
    float: left;
    list-style: outside none none;
    margin: 0;
    padding: 0;
	}
	#country-list > li {
    cursor: pointer;
    padding: 2px 0;
	}
	.ui-menu.ui-widget.ui-widget-content.ui-autocomplete.ui-front
    {
        z-index: 999999!important;
        
    }
    h5{
    	margin-top:5px!important;
    	margin-bottom:5px!important;
    }
</style>
<script>
			$(document).ready(function () {
  
  				$(".number").keypress(function (e) {
    
   	  			if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
   	     			return false;
   				}

   				});
			});
		</script>        
        <div class="box-body no-padding">
        		 
              <table class="table table-striped">
              	<thead>
              		<tr>
              			
              			<th>
              				<center><h3 style="font-weight: bold">Job Name</h3></center>	
              			</th>
              			<th>
              				<div class="col-md-3"><center><h3 style="font-weight: bold">ANSA</h3></center></div>
              				<div class="col-md-3"><center><h3 style="font-weight: bold">QC</h3></center></div>
              				<div class="col-md-3"><center><h3 style="font-weight: bold">QA</h3></center></div>
              				<div class="col-md-3"><center><h3 style="font-weight: bold">WH</h3></center></div>	
              			</th>
              			<th>
              				<center><h3 style="font-weight: bold">Status</h3></center>
              			</th>
              			
              		</tr>
              	</thead>
                <tbody>
                	<?php foreach($result as $r){ ?>
                	
                	<?php if($r->container_name != 'ANSA') { ?>
                	             	
                	<!-- if check the ANSA or Not  -->	
                	<?php if($r->decoration == 1) { ?>  <!-- ANSA Yes -->	
                	
                	<tr>
                  		<td class="col-md-2">
                  			<!--<h4 class="bg-green" style="width:100%; text-align:center;"><?php echo $r->container_type; ?></h4>-->
                  			<h4 class="bg-blue" style="width:100%"><?php echo $r->job_name; ?></h4> 
                  			<div class="col-md-8"><h5><a style="color: #000;" data-toggle="modal" data-target="#TPD<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">TPD: <?php $tpd = get_tpd_by_id($r->tpd); foreach($tpd as $t) { echo $t->name; } ?> | QTY: <?php echo $r->qty; ?></a></h5></div>
                  			<div class="col-md-4"><h5><?php echo $r->container_type; ?></h5></div>
                  			<div class="col-md-8"><h5><?php echo $r->customer_name; ?></h5></div>
                  			<div class="col-md-4"><h5><?php echo date ("j M,y",strtotime($r->container_date)); ?></h5></div>
                  			<!-- TPD PopUp Srart -->
                  			<div class="example-modal">
        							<div class="modal modal-primary" id="TPD<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Change TPD. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
              											<label>Select TPD</label>
                										<input type="hidden" id="tpdid<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->container_job_id; ?>" />
                										<div class="form-group">
                											<?php $all_tpd=get_tpd_by_job(); ?>
                											<select class="form-control" id="tpdname<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
                													<?php foreach ($all_tpd as $ntpd) { ?>
                														<option value="<?php echo $ntpd->id; ?>" <?php if($ntpd->id==$r->tpd) echo 'selected="selected"' ?>><?php echo $ntpd->name; ?></option>
                													<?php } ?>
                											</select>
                										</div>
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="tpdbtn<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
        						
        						<script>
               						$(function(){
												$("#tpdbtn<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id=$("#tpdid<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var tpd=$("#tpdname<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {ids:id,tpdname:tpd}
												$.ajax({
														'url':'<?php echo base_url();?>admin/jobscreen/tpdchange',
														'type':'POST',
														'data':data,
														'success':function(message)
														{	
															$( "#maindiv" ).load( "jobscreen/reloaded" );
					
														}
														});
														});
								   						});
               					</script>
        						
        						<!-- TPD PopUp End -->
                  		</td> 
                  
                 	 	<td class="col-md-9">
                 	 		<?php $qcl = (($r->qc*100)/$r->qty); ?>	
                     		<?php $qal = (($r->qa_completed*100)/$r->qty); ?>
                     		<?php $whl = (($r->wh_completed*100)/$r->qty); ?>
                     		<?php $ansal = (($r->ansa*100)/$r->qty); ?>
                  			<div class="row"> 
                  			
                  			
                  			<!--  ANSA is less then 25%   -->
                  			
                  			<?php if($ansal < 25) { ?>	
                  				
                  				<div class="col-md-3">
                       				<div class="info-box bg-red" data-toggle="modal" data-target="#ANSA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qty-$r->ansa_completed; ?></span>
                              				<span> <?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																{ echo "-"; }
																else {
                                    							$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																}
                                    							?></span>   
                              				                              
                							<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $ansal; ?>%"></div>
                              					</div>
                                 		 		
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="ANSA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - ANSA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
              											<label>Pending Products :- <?php echo $r->qty-$r->ansa_completed; ?></label>
                										<input type="hidden" id="ansa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<div class="form-group">
                											<input type="hidden" id="ansa2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->ansa; ?>" />
                  											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="ansa3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control number" />
                										</div>
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="ansa4<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
        						<script>  
                                    $('#ansa3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->qty-$r->ansa_completed; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                             
                                    }
                                    });
                                </script>
                     			<script>
               						$(function(){
												$("#ansa4<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id1=$("#ansa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var ansa1=$("#ansa2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var ansa2=$("#ansa3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {id:id1,ansa1:ansa1,ansa2:ansa2}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/ansaadd1',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               					</script>
                     			
                    			<div class="col-md-3">
                       				<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->ansa_completed-$r->qc_completed; ?></span> 
                              				<span>
                                    			<?php  if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    									$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																		$t1 = strtotime($today)*1000;
																		$t2 = strtotime($adate)*1000;
																		$tt = $t1-$t2;
																		$elapsed = (($tt / 1000)/60)/60;
                        												$a = $elapsed;
                        												$f = sprintf ("%.2f", $a);
                        												$nn=$f;
                        												$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																		echo $nnn." Hrs Ago"; 
																		}
                                    							?>
                                  					</span>                             
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qcl; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            			</div>
                        			</div>
                     			</div>
                     	
                     			<!-- QA is Black  -->
                     			<!-- QA Condition -->
								<?php if($r->qa > $r->qa_completed) { ?>  
									<!-- QA is Purple  -->
									<?php if($r->qa_rem !=0) { ?>
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<span>
                                    			<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    									$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																		$t1 = strtotime($today)*1000;
																		$t2 = strtotime($adate)*1000;
																		$tt = $t1-$t2;
																		$elapsed = (($tt / 1000)/60)/60;
                        												$a = $elapsed;
                        												$f = sprintf ("%.2f", $a);
                        												$nn=$f;
                        												$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																		echo $nnn." Hrs Ago";
																		}
															?>
                                  					</span>
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
              											<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label>
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                										</div> 
										            </div>
            										</form>
              										</div>
              										<div class="modal-footer">
                										<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                										<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              										</div>
            										</div>
            									</div>
          									</div>
        								</div>
        								<script>
                                    		
                                    
                                   	 		$('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->qa_per; ?>;
                                      	if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                                <script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product1=$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
                                     
                                      if ($(this).val() > product1){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
        								<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
		                     			<script>
		                     			$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
										});
		                     			
		               					$(function(){
											$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
							  					var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con3=$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:con3}
										$.ajax({
											'url':'<?php echo base_url();?>admin/jobscreen/qaadd2',
											'type':'POST',
											'data':data,
											'success':function(message)
											{	
												$( "#maindiv" ).load( "jobscreen/reloaded" );
		
											}
											});
											});
					   						});
		               					</script>
									<?php } elseif($r->qa_rem == '0') { ?>
										<!-- QA is Blue  -->
									<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              					<span>
                                    					 <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?>
                                  				</span>                             
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
              											<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label>
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                										</div> 
										            </div>
            										</form>
              										</div>
              										<div class="modal-footer">
                										<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                										<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              										</div>
            										</div>
            									</div>
          									</div>
        								</div>
        								<script>
                                    
                                    
                                    $('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->qa_per; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                                <script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
        								<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
		                     			<script>
		                     			$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
										});
		                     			
		               					$(function(){
											$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
							  					var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con3=$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:con3}
										$.ajax({
											'url':'<?php echo base_url();?>admin/jobscreen/qaadd2',
											'type':'POST',
											'data':data,
											'success':function(message)
											{	
												$( "#maindiv" ).load( "jobscreen/reloaded" );
		
											}
											});
											});
					   						});
		               					</script>	
									<?php } else { ?>		                   	
                     				<!-- QA is RED  -->
                     				<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              					<span>
                                    					<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?>
                                  				</span>                             
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
              											<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label>
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                										</div> 
										            </div>
            										</form>
              										</div>
              										<div class="modal-footer">
                										<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                										<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              										</div>
            										</div>
            									</div>
          									</div>
        								</div>
        								<script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                                <script>
                                    
                                    
                                    $('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                     var product=<?php echo $r->qa_per; ?>; 
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
        								<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
		                     			<script>
		                     			$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
										});
		                     			
		               					$(function(){
											$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
							  					var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con3=$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:con3}
										$.ajax({
											'url':'<?php echo base_url();?>admin/jobscreen/qaadd2',
											'type':'POST',
											'data':data,
											'success':function(message)
											{	
												$( "#maindiv" ).load( "jobscreen/reloaded" );
		
											}
											});
											});
					   						});
		               					</script>
               						<?php } ?>		
               						
               					<!-- QA is Green -->	   
                     			 <?php } elseif($r->qa == $r->qa_completed && $r->qa_completed != 0) { ?>  
                     			   
                     			<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              				<span>
                                    			<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?>
                                  					</span>
                              				                          
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              						</div>
                                 		 			
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			
                     			<?php } elseif($r->qa_completed == 0) { ?>
                     			   
                     			   <div class="col-md-3">
                       				<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              				<span>
                                    			<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?>
                                  					</span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              						</div>
                                 		 			
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			   
                     			<?php } ?>   
                     			
                     			<?php if($r->wh < $r->wh_completed) { ?>   
                    			    		
                        		<?php if($whl < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span>
                                    			<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?>
                                  				</span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($whl == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span>
                                    			<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?>
                                  				</span>                                   
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span>
                                    			<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?>
                                  				</span>                                   
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh ==  $r->wh_completed) { ?>
                     				
                     			<?php if($whl == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span>
                                    			<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?>
                                  				</span>                                    
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span>
                                    			<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?>
                                  				</span>                                  
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
        						<?php } elseif($r->wh > $r->wh_completed) { ?>
        							
        						<?php if($whl < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal" data-target="#WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span>
                                    			<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?>
                                  				</span>                                   
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - WH Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<input type="hidden" id="wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
										            	<label>Requested Products :- <?php echo $r->wh-$r->wh_completed; ?></label>
										            	<input type="text" id="wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control" />
										            </div>
            										</form>
              									</div>
              									<div class="modal-footer"> 
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Completed</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
                     			<script>
                                    
                                    
                                    $('#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->wh-$r->wh_completed; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                     			 <script>
               						$(function(){
												$("#wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var qty=$("#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
					  							var id=$("#wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {qid:id,qt:qty}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/whadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>  
                				                				
                				<?php } elseif($whl == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>   
                             	 			<span>
                                    			<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?>
                                  				</span>                                 
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" data-target="#WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span>
                                    			<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?>
                                  				</span>                                  
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - WH Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<input type="hidden" id="wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
										            	<label>Requested Products :- <?php echo $r->wh-$r->wh_completed; ?></label>
										            	<input type="text" id="wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control" />
										            </div>
            										</form>
              									</div>
              									<div class="modal-footer"> 
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Completed</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
                     			<script>
                                    
                                    
                                    $('#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->wh-$r->wh_completed; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                     			 <script>
               						$(function(){
												$("#wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var qty=$("#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
					  							var id=$("#wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {qid:id,qt:qty}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/whadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
                     			<?php } ?>
        							
        						<?php } ?>	
                				
                	
                	
                	<!--  ANSA is 100% Completed  -->
                	
                	<?php } elseif($ansal == 100) { ?>
                		 		
                		<div class="col-md-3">
                       		<div class="info-box bg-green" id="qc-mat-81" >
                            	<div class="info-box-content">
                              		<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qty-$r->ansa_completed; ?></span> 
                              		<span>
                                    	<?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																{ echo "-"; }
																else {
                                    							$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																}
                                    				      ?>
                                  		</span>                             
                					<div class="progress1">
                              			<div class="progress">
                                			<div class="progress-bar" style="width: <?php echo $ansal; ?>%"></div>
                              			</div>
                                 		
                                 	 </div>
                            	</div>
                        	</div>
                     	</div>
                     	
                     	<!-- Qc is less then 25% -->
                     			
                 		<?php if($r->qc_completed < $r->ansa_completed) { ?>
                     		
                     		<!-- QC  -->
                     		<div class="col-md-3">
                       			<div class="info-box bg-red" data-toggle="modal" data-target="#QC<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            		<div class="info-box-content">
                              			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->ansa_completed-$r->qc_completed; ?></span> 
                              			<span>
                                    	<?php  if($r->qc_updated == '0000-00-00 00:00:00')
															{ echo "-"; }
															else {
                                    							$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
															}
                                    						?>
                                  		</span>                             
                						<div class="progress1">
                              				<div class="progress">
                                				<div class="progress-bar" style="width: <?php echo $qcl; ?>%"></div>
                              				</div>
                                 		 	<span class="progress-description">
                                    		
                                    						<?php echo $r->qc_comment; ?>
                                  			</span>
                                 	 	</div>
                            		</div>
                        		</div>
                     		</div>
                     		
                     		<div class="example-modal">
        						<div class="modal modal-primary" id="QC<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          							<div class="modal-dialog">
								    	<div class="modal-content">
								        	<div class="modal-header">
                								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  								<span aria-hidden="true">&times;</span></button>
                								<h4 class="modal-title">Container Settings - QC Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              								</div>
              								<div class="modal-body">
               									<form role="form" name="container-action" method="post">
              									<div class="box-body">
              										<label>Requested Products :- <?php echo $r->ansa_completed-$r->qc; ?></label>
                									<input type="hidden" id="ansaqcid<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                									<div class="form-group">
                									<input type="hidden" id="ansaqccon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qc; ?>" />
                									<input type="hidden" id="ansaqccon2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->ansa_completed; ?>" />
                  									<label for="examplecomment">Number of pcs Pass</label>
                  									<input type="text" id="ansaqccon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control number" />
                									</div>
										      	</div>
            									</form>
              								</div>
              								<div class="modal-footer">
                								<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                								<button type="button" id="ansaqcbtn<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Save Changes</button>
              								</div>
            							</div>
            						</div>
          						</div>
        					</div>
                     		<script>
                                    
                                    
                                    $('#ansaqccon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->ansa_completed-$r->qc; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>	
                     		<script>
               					$(function(){
									$("#ansaqcbtn<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  				var id=$("#ansaqcid<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  					var con=$("#ansaqccon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  					var con2=$("#ansaqccon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  					var con3=$("#ansaqccon2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  					var data= {id:id,ansaqc1:con,ansaqc2:con2,ansaqc3:con3}
									
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/qcadd2',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               				</script>	
               				
                     		<!-- QA  -->
                     		
                     		<?php if($r->qa > $r->qa_completed) { ?>  
									
									<!-- QA is purple  -->
									<?php if($r->qa_rem !=0) { ?>
										
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              					<span>
                                    					<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?>
                                  				</span>                             
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
              											<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label>
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                										</div> 
										            </div>
            										</form>
              										</div>
              										<div class="modal-footer">
                										<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                										<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              										</div>
            										</div>
            									</div>
          									</div>
        								</div>
        								<script>
                                    
                                    
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>	
                                <script>
                                    
                                    
                                    $('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->qa_per; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>	
        								<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
		                     			<script>
		                     			$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
										});
		                     			
		               					$(function(){
											$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
							  					var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con3=$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:con3}
										$.ajax({
											'url':'<?php echo base_url();?>admin/jobscreen/qaadd2',
											'type':'POST',
											'data':data,
											'success':function(message)
											{	
												$( "#maindiv" ).load( "jobscreen/reloaded" );
		
											}
											});
											});
					   						});
		               					</script>
		                     			
									<!-- QA is blue  -->
									<?php } elseif($r->qa_rem == '0') { ?>
										
										<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              					<span>
                                    				<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?>
                                  				</span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
              											<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label>
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                										</div> 
										            </div>
            										</form>
              										</div>
              										<div class="modal-footer">
                										<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                										<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              										</div>
            										</div>
            									</div>
          									</div>
        								</div>
        								<script>
                                    
                                    
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>	
                                <script>
                                    
                                    
                                    $('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                     var product=<?php echo $r->qa_per; ?>; 
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
        								<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
		                     			<script>
		                     			$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
										});
		                     			
		               					$(function(){
											$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
							  					var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con3=$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:con3}
										$.ajax({
											'url':'<?php echo base_url();?>admin/jobscreen/qaadd2',
											'type':'POST',
											'data':data,
											'success':function(message)
											{	
												$( "#maindiv" ).load( "jobscreen/reloaded" );
		
											}
											});
											});
					   						});
		               					</script>
                     				
        						<!-- QA is RED  -->
									<?php } else { ?>
									<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              					<span>
                                    				<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?>
                                  				</span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
              											<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label>
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                										</div> 
										            </div>
            										</form>
              										</div>
              										<div class="modal-footer">
                										<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                										<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              										</div>
            										</div>
            									</div>
          									</div>
        								</div>
        								<script>
                                    
                                    
                                    $('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->qa_per; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>	
                                <script>
                                    
                                    
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
        								<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
		                     			<script>
		                     			$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
										});
		                     			
		               					$(function(){
											$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
							  					var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con3=$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:con3}
										$.ajax({
											'url':'<?php echo base_url();?>admin/jobscreen/qaadd2',
											'type':'POST',
											'data':data,
											'success':function(message)
											{	
												$( "#maindiv" ).load( "jobscreen/reloaded" );
		
											}
											});
											});
					   						});
		               					</script>
               						
               						<?php } ?>		
               						
               						<!-- QA is Green -->	   
                     			 	<?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     			   	<?php if($r->qa !=0 ) {?>
                     				<div class="col-md-3">
                       					<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              					<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				
                     				<?php } else { ?> 
                     					
                     					<div class="col-md-3">
                       					<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              					<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			
                     				<?php } ?>
                     				<?php }  ?>
                     		
                     		
                     				
                     		<!--  QC is gether then 25% and less 100%  -->
                     		<?php } elseif($qcl > 25) { ?>
		                    	
		                    	<!--  QC is equal ANSA  -->
		                    	<?php if($r->qc_completed == $r->ansa_completed) { ?>
                     			<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->ansa_completed-$r->qc_completed; ?></span>   
                              				      <span>
                                    			<?php      if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?>
                                  					</span>                     
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qcl; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					<?php echo $r->qc_comment; ?>
															
                                  					</span>
                                 	 			</div>
                            			</div>
                        			</div>
                     			</div>                     	
                     	
                     			<!-- QA Condition -->
                     			
								<?php if($r->qa > $r->qa_completed) { ?>  
									
									<!-- QA is purple  -->
									<?php if($r->qa_rem !=0) { ?>
										
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              					<span>
                                    					<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?>
                                  				</span>                             
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
              											<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label>
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                										</div> 
										            </div>
            										</form>
              										</div>
              										<div class="modal-footer">
                										<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                										<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              										</div>
            										</div>
            									</div>
          									</div>
        								</div>
        								<script>
                                    
                                    
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>	
                                <script>
                                    
                                    
                                    $('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->qa_per; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>	
        								<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
		                     			<script>
		                     			$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
										});
		                     			
		               					$(function(){
											$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
							  					var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con3=$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:con3}
										$.ajax({
											'url':'<?php echo base_url();?>admin/jobscreen/qaadd2',
											'type':'POST',
											'data':data,
											'success':function(message)
											{	
												$( "#maindiv" ).load( "jobscreen/reloaded" );
		
											}
											});
											});
					   						});
		               					</script>
		                     			
									<!-- QA is blue  -->
									<?php } elseif($r->qa_rem == '0') { ?>
										
										<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              					<span>
                                    				<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?>
                                  				</span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
              											<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label>
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                										</div> 
										            </div>
            										</form>
              										</div>
              										<div class="modal-footer">
                										<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                										<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              										</div>
            										</div>
            									</div>
          									</div>
        								</div>
        								<script>
                                    
                                    
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>	
                                <script>
                                    
                                    
                                    $('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                     var product=<?php echo $r->qa_per; ?>; 
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
        								<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
		                     			<script>
		                     			$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
										});
		                     			
		               					$(function(){
											$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
							  					var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con3=$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:con3}
										$.ajax({
											'url':'<?php echo base_url();?>admin/jobscreen/qaadd2',
											'type':'POST',
											'data':data,
											'success':function(message)
											{	
												$( "#maindiv" ).load( "jobscreen/reloaded" );
		
											}
											});
											});
					   						});
		               					</script>
                     				
        						<!-- QA is RED  -->
									<?php } else { ?>		                   	
                     				<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              					<span>
                                    				<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?>
                                  				</span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
              											<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label>
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                										</div> 
										            </div>
            										</form>
              										</div>
              										<div class="modal-footer">
                										<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                										<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              										</div>
            										</div>
            									</div>
          									</div>
        								</div>
        								<script>
                                    
                                    
                                    $('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->qa_per; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>	
                                <script>
                                    
                                    
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
        								<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
		                     			<script>
		                     			$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
										});
		                     			
		               					$(function(){
											$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
							  					var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con3=$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:con3}
										$.ajax({
											'url':'<?php echo base_url();?>admin/jobscreen/qaadd2',
											'type':'POST',
											'data':data,
											'success':function(message)
											{	
												$( "#maindiv" ).load( "jobscreen/reloaded" );
		
											}
											});
											});
					   						});
		               					</script>
               						
               						<?php } ?>		
               						
               						<!-- QA is Green -->	   
                     			 	<?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     				<div class="col-md-3">
                       					<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              					<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			
                     				<?php }  ?>
                     						
                     						
                     			<?php } else  { ?>		
                     			<!-- QC is Yellow  -->
                     			<div class="col-md-3">
                       				<div class="info-box bg-yellow" data-toggle="modal" data-target="#QC<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                              					<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->ansa_completed-$r->qc_completed; ?></span>                              
                								<span>
                                    				<?php      if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?>
                                  				</span>
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qcl; ?>%"></div>
                              					</div>
                                 				<span class="progress-description">
                                    				
															<?php echo $r->qc_comment; ?>
                                  				</span>
                                 	 			</div>
                            			</div>
                        			</div>
                     			</div>
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="QC<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - QC Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<input type="hidden" id="ansaqcid<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<div class="form-group">
                											<label>Requested Products :- <?php echo $r->ansa_completed-$r->qc; ?></label><br>
                											<input type="hidden" id="ansaqccon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qc; ?>" />
                											<input type="hidden" id="ansaqccon2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->ansa_completed; ?>" />
                  											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="ansaqccon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control number" />
                										</div>
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="ansaqcbtn<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
                     			<script>
                                    
                                    
                                    $('#ansaqccon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                     var product=<?php echo $r->ansa_completed-$r->qc; ?>; 
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>	
                               
                     			<script>
               						$(function(){
												$("#ansaqcbtn<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id=$("#ansaqcid<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con=$("#ansaqccon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con2=$("#ansaqccon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con3=$("#ansaqccon2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {id:id,ansaqc1:con,ansaqc2:con2,ansaqc3:con3}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/qcadd2',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>	
                     	
                     	
                     			<!-- QA Condition -->
								<?php if($r->qa > $r->qa_completed) { ?>  
									<!-- QA is purple -->
									
									<?php if($r->qa_rem !=0) { ?>
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              					<span>
                                    				<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?>
                                  				</span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
              											<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label>
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                										</div> 
										            </div>
            										</form>
              										</div>
              										<div class="modal-footer">
                										<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                										<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              										</div>
            										</div>
            									</div>
          									</div>
        								</div>
        								<script>
                                    
                                    
                                    $('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->qa_per; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>	
                               <script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
        								<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
		                     			<script>
		                     			$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
										});
		                     			
		               					$(function(){
											$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
							  					var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con3=$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:con3}
										$.ajax({
											'url':'<?php echo base_url();?>admin/jobscreen/qaadd2',
											'type':'POST',
											'data':data,
											'success':function(message)
											{	
												$( "#maindiv" ).load( "jobscreen/reloaded" );
		
											}
											});
											});
					   						});
		               					</script>
									
									<!-- QA is blue  -->
									<?php } elseif($r->qa_rem == '0') { ?>
										
									<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
												<span>
                                    				 <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?>
                                  				</span>                            
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
              											<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label>
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                										</div> 
										            </div>
            										</form>
              										</div>
              										<div class="modal-footer">
                										<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                										<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              										</div>
            										</div>
            									</div>
          									</div>
        								</div>
        								<script>
                                    
                                    
                                    $('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->qa_per; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>	
                                <script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
        								<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
		                     			<script>
		                     			$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
										});
		                     			
		               					$(function(){
											$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
							  					var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con3=$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:con3}
										$.ajax({
											'url':'<?php echo base_url();?>admin/jobscreen/qaadd2',
											'type':'POST',
											'data':data,
											'success':function(message)
											{	
												$( "#maindiv" ).load( "jobscreen/reloaded" );
		
											}
											});
											});
					   						});
		               					</script>
									
									<!-- QA is RED  -->
										
									<?php } else { ?>		                   	
                     				
                     				<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              					<span>
                                    				 <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?>
                                  				</span>                             
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
              											<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label>
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                										</div> 
										            </div>
            										</form>
              										</div>
              										<div class="modal-footer">
                										<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                										<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              										</div>
            										</div>
            									</div>
          									</div>
        								</div>
        								<script>
                                   
                                    
                                    $('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                       var product=<?php echo $r->qa_per; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>	
                                <script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
        								<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
		                     			<script>
		                     			$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
										});
		                     			
		               					$(function(){
											$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
							  					var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con3=$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:con3}
										$.ajax({
											'url':'<?php echo base_url();?>admin/jobscreen/qaadd2',
											'type':'POST',
											'data':data,
											'success':function(message)
											{	
												$( "#maindiv" ).load( "jobscreen/reloaded" );
		
											}
											});
											});
					   						});
		               					</script>
               						<?php } ?>		
               						
               						<!-- QA is Green -->	   
                     			 	<?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     					<div class="col-md-3">
                       						<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            					<div class="info-box-content">
                              						<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>  
                              						 <span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                        
                									<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              						</div>
                                 		 			
                                 	 				</div>
                            					</div>
                        					</div>
                     					</div>
                     			
                     				<?php }  ?>
                     	           				
                     			<?php } ?>

                    		<?php } ?> 
                    		
                    		<?php if($r->wh < $r->wh_completed) { ?>   
                    			    		
                        		<?php if($whl < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span>
                                    			 <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?>
                                  				</span>                             
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($whl == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span>
                                    			<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?>
                                  				</span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span>
                                    			<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?>
                                  				</span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh ==  $r->wh_completed) { ?>
                     				
                     			<?php if($whl == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span>
                                    				<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?>
                                  				</span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span>
                                    				<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?>
                                  				</span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			
        						<?php } elseif($r->wh > $r->wh_completed) { ?>
        							
        						<?php if($whl < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal" data-target="#WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span>
                                    			<?php      if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?>
                                  				</span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - WH Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<input type="hidden" id="wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
										            	<label>Requested Products :- <?php echo $r->wh-$r->wh_completed; ?></label>
										            	<input type="text" id="wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control" />
										            </div>
            										</form>
              									</div>
              									<div class="modal-footer"> 
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Completed</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
                     			<script>
                                    
                                    
                                    $('#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->wh-$r->wh_completed; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>	
                                
                     			 <script>
               						$(function(){
												$("#wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var qty=$("#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
					  							var id=$("#wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {qid:id,qt:qty}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/whadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>  
                				                				
                				<?php } elseif($whl == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span>
                                    			<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?>
                                  				</span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" data-target="#WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span>
                                    				<?php      if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?>
                                  				</span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - WH Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<input type="hidden" id="wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
										            	<label>Requested Products :- <?php echo $r->wh-$r->wh_completed; ?></label>
										            	<input type="text" id="wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control" />
										            </div>
            										</form>
              									</div>
              									<div class="modal-footer"> 
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Completed</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
                     			<script>
                                    
                                    
                                    $('#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->wh-$r->wh_completed; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                     			 <script>
               						$(function(){
												$("#wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var qty=$("#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
					  							var id=$("#wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {qid:id,qt:qty}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/whadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
                     			<?php } ?>
        							
        						<?php } ?>  
                		 		
                		 		
                	<!-- ANSA is 25 > and < 100 -->
                	<?php } else { ?>
						
						<!-- ANSA is Yellow  -->
						<div class="col-md-3">
                       		<div class="info-box bg-yellow" data-toggle="modal" data-target="#ANSA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            	<div class="info-box-content">
                              		<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qty-$r->ansa_completed; ?></span>
                              		<span>
                                    <?php  if($r->ansa_updated == '0000-00-00 00:00:00')
											{ echo "-"; }
											else {
                                    			$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
											}
                                    		?>
                                  	</span>                              
                					<div class="progress1">
                              		<div class="progress">
                                		<div class="progress-bar" style="width: <?php echo $ansal; ?>%"></div>
                              		</div>
                                 	
                                 	</div>
                            	</div>
                        	</div>
                     	</div>
                     	<div class="example-modal">
        					<div class="modal modal-primary" id="ANSA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          						<div class="modal-dialog">
								    <div class="modal-content">
								     	<div class="modal-header">
                							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  							<span aria-hidden="true">&times;</span></button>
                							<h4 class="modal-title">Container Settings - ANSA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              							</div>
              							<div class="modal-body">
               							<form role="form" name="container-action" method="post">
              								<div class="box-body">
                							<input type="hidden" id="ansa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                							<div class="form-group">
                								<label>Pending Products :- <?php echo $r->qty-$r->ansa_completed; ?></label><br />
                								<input type="hidden" id="ansa2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->ansa; ?>" />
                  								<label for="examplecomment">Number of pcs Pass</label>
                  								<input type="text" id="ansa3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control number" />
                							</div>
										    </div>
            							</form>
              							</div>
              							<div class="modal-footer">
                							<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                							<button type="button" id="ansa4<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Save Changes</button>
              							</div>
            						</div>
            					</div>
          					</div>
        				</div>
        				<script>
                                    
                                    
                                    $('#ansa3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                     var product=<?php echo $r->qty-$r->ansa_completed; ?>; 
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                     	<script>
               				$(function(){
								$("#ansa4<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  				var id1=$("#ansa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  					var ansa1=$("#ansa2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  					var ansa2=$("#ansa3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  					var data= {id:id1,ansa1:ansa1,ansa2:ansa2}
										$.ajax({
												'url':'<?php echo base_url();?>admin/jobscreen/ansaadd1',
												'type':'POST',
												'data':data,
												'success':function(message)
												{	
													$( "#maindiv" ).load( "jobscreen/reloaded" );
												}
												});
												});
			   				});
               			</script>
               			
                     	<!-- QC is less then 25%  -->		
                     	<?php if($r->qc_completed < $r->ansa_completed) { ?>
                     		<!-- QC is RED  -->
                     		<div class="col-md-3">
                       				<div class="info-box bg-red" data-toggle="modal" data-target="#QC<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->ansa_completed-$r->qc_completed; ?></span>
                              				<span><?php  if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qcl; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					
                                    							<?php echo $r->qc_comment; ?>
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="QC<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - QC Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<input type="hidden" id="ansaqcid<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<div class="form-group">
                											<label>Requested Products :- <?php echo $r->ansa_completed-$r->qc; ?></label><br />
                											<input type="hidden" id="ansaqccon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qc; ?>" />
                											<input type="hidden" id="ansaqccon2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->ansa_completed; ?>" />
                  											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="ansaqccon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control number" />
                										</div>
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="ansaqcbtn<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
                     		<script>
                                    
                                    
                                    $('#ansaqccon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->ansa_completed-$r->qc; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>	
                     		<script>
               					$(function(){
												$("#ansaqcbtn<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id=$("#ansaqcid<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con=$("#ansaqccon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con2=$("#ansaqccon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con3=$("#ansaqccon2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {id:id,ansaqc1:con,ansaqc2:con2,ansaqc3:con3}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/qcadd2',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               				</script>	
               				
                     		<!-- QA is Black  -->
                     		<div class="col-md-3">
                        		<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                              			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              			 <span>
				                        	<?php if($r->qa_updated == '0000-00-00 00:00:00')
															{ echo "-"; }
															else {
                                    						$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
															}
													?>
				                       	</span>                              
				                		<div class="progress1">
				                        <div class="progress">
				                        	<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
				                        </div>
				                       
                                  		</div>
                            		</div>
                            	</div>
                     		</div>
                     		
                     		<!-- QC is more then 25%  -->		
                     		<?php } elseif($qcl > 25) { ?>
                     			
                     			<!-- QC is equal to ansa  -->
                     			<?php if($r->qc_completed == $r->ansa_completed) { ?>
                     						
                     				<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->ansa_completed-$r->qc_completed; ?></span>
                              				<span><?php      if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?>   </span>                           
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qcl; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    				
															<?php echo $r->qc_comment; ?>
															
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>                     	
                     	
                     			<!-- QA Condition -->
								<?php if($r->qa > $r->qa_completed) { ?>  
									<!-- QA is RED  -->
									<?php if($r->qa_rem !=0) { ?>
										
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              					<span>
                                    				<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?>
                                  				</span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
              											<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label>
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                										</div> 
										            </div>
            										</form>
              										</div>
              										<div class="modal-footer">
                										<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                										<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              										</div>
            										</div>
            									</div>
          									</div>
        								</div>
        								<script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                                <script>
                                    
                                    
                                    $('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->qa_per; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>	
        								<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
		                     			<script>
		                     			$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
										});
		                     			
		               					$(function(){
											$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
							  					var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con3=$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:con3}
										$.ajax({
											'url':'<?php echo base_url();?>admin/jobscreen/qaadd2',
											'type':'POST',
											'data':data,
											'success':function(message)
											{	
												$( "#maindiv" ).load( "jobscreen/reloaded" );
		
											}
											});
											});
					   						});
		               					</script>
										<!-- QA is blue  -->
										<?php } elseif($r->qa_rem == '0') { ?>
										
										<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              					<span>
                                    				<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?>
                                  				</span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
              											<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label>
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                										</div> 
										            </div>
            										</form>
              										</div>
              										<div class="modal-footer">
                										<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                										<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              										</div>
            										</div>
            									</div>
          									</div>
        								</div>
        								<script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                                <script>
                                    
                                    
                                    $('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->qa_per; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
        								<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
		                     			<script>
		                     			$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
										});
		                     			
		               					$(function(){
											$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
							  					var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con3=$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:con3}
										$.ajax({
											'url':'<?php echo base_url();?>admin/jobscreen/qaadd2',
											'type':'POST',
											'data':data,
											'success':function(message)
											{	
												$( "#maindiv" ).load( "jobscreen/reloaded" );
		
											}
											});
											});
					   						});
		               					</script>
										<!-- QA is red  -->
										<?php } else { ?>		                   	
                     					<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              					<span>
                                    				 <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?>
                                  				</span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
              											<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label>
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                										</div> 
										            </div>
            										</form>
              										</div>
              										<div class="modal-footer">
                										<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                										<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              										</div>
            										</div>
            									</div>
          									</div>
        								</div>
        								<script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                                <script>
                                    
                                    
                                    $('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->qa_per; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
        								<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
		                     			<script>
		                     			$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
										});
		                     			
		               					$(function(){
											$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
							  					var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con3=$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:con3}
										$.ajax({
											'url':'<?php echo base_url();?>admin/jobscreen/qaadd2',
											'type':'POST',
											'data':data,
											'success':function(message)
											{	
												$( "#maindiv" ).load( "jobscreen/reloaded" );
		
											}
											});
											});
					   						});
		               					</script>
               					<?php } ?>		
               						
               					<!-- QA is Green -->	   
                     			 <?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     				<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              						</div>
                                 		 			
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			
                     			<?php } ?>
                     						
                     						
                     			<?php } else  { ?>		
                     				<!-- QC is Yellow  -->
                     				<div class="col-md-3">
                       				<div class="info-box bg-yellow" data-toggle="modal" data-target="#QC<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->ansa_completed-$r->qc_completed; ?></span>
                              				<span>
                                    			<?php if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																	else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																	}
															?>
                                  					</span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qcl; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    						<?php echo $r->qc_comment; ?>
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="QC<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - QC Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<input type="hidden" id="ansaqcid<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<div class="form-group">
                											<label>Requested Products :- <?php echo $r->ansa_completed-$r->qc; ?></label><br />
                											<input type="hidden" id="ansaqccon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qc; ?>" />
                											<input type="hidden" id="ansaqccon2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->ansa_completed; ?>" />
                  											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="ansaqccon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control number" />
                										</div>
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="ansaqcbtn<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
                     			<script>
                                    
                                    
                                    $('#ansaqccon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=<?php echo $r->ansa_completed-$r->qc; ?>;  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>	
                                
                     			<script>
               						$(function(){
												$("#ansaqcbtn<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id=$("#ansaqcid<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con=$("#ansaqccon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con2=$("#ansaqccon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con3=$("#ansaqccon2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {id:id,ansaqc1:con,ansaqc2:con2,ansaqc3:con3}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/qcadd2',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>	
                     	
                     	
                     			<!-- QA Condition -->
                     			
								<?php if($r->qa > $r->qa_completed) { ?>  
									<!-- QA is purple  -->
									<?php if($r->qa_rem !=0) { ?>
										
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              					<span>
                                    				 <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?>
                                  				</span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
              											<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label>
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                										</div> 
										            </div>
            										</form>
              										</div>
              										<div class="modal-footer">
                										<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                										<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              										</div>
            										</div>
            									</div>
          									</div>
        								</div>
        								<script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                                <script>
                                    
                                    
                                    $('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->qa_per; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>	
        								<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
		                     			<script>
		                     			$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
										});
		                     			
		               					$(function(){
											$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
							  					var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con3=$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:con3}
										$.ajax({
											'url':'<?php echo base_url();?>admin/jobscreen/qaadd2',
											'type':'POST',
											'data':data,
											'success':function(message)
											{	
												$( "#maindiv" ).load( "jobscreen/reloaded" );
		
											}
											});
											});
					   						});
		               					</script>
									
									<?php } elseif($r->qa_rem == '0') { ?>
										<!-- QA is blue  -->
									<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              					<span>
                                    					<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?>
                                  				</span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
              											<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label>
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                										</div> 
										            </div>
            										</form>
              										</div>
              										<div class="modal-footer">
                										<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                										<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              										</div>
            										</div>
            									</div>
          									</div>
        								</div>
        									<script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                                <script>
                                    
                                    
                                    $('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                     var product=<?php echo $r->qa_per; ?>; 
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
        								<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
		                     			<script>
		                     			$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
										});
		                     			
		               					$(function(){
											$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
							  					var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con3=$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:con3}
										$.ajax({
											'url':'<?php echo base_url();?>admin/jobscreen/qaadd2',
											'type':'POST',
											'data':data,
											'success':function(message)
											{	
												$( "#maindiv" ).load( "jobscreen/reloaded" );
		
											}
											});
											});
					   						});
		               					</script>
										
									<?php } else { ?>	
									<!-- QA is RED  -->		                   	
                     				<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              					<span>
                                    				<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?>
                                  				</span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
              											<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label>
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                										</div> 
										            </div>
            										</form>
              										</div>
              										<div class="modal-footer">
                										<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                										<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              										</div>
            										</div>
            									</div>
          									</div>
        								</div>
        							<script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                                <script>
                                    
                                    
                                    $('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->qa_per; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
        								<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
		                     			<script>
		                     			$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
										});
		                     			
		               					$(function(){
											$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
							  					var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con3=$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
						  						var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
						  						var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:con3}
										$.ajax({
											'url':'<?php echo base_url();?>admin/jobscreen/qaadd2',
											'type':'POST',
											'data':data,
											'success':function(message)
											{	
												$( "#maindiv" ).load( "jobscreen/reloaded" );
		
											}
											});
											});
					   						});
		               					</script>
               						<?php } ?>		
               						
               					<!-- QA is Green -->	   
                     			 <?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     				<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>    
                              				<span> <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                          
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              						</div>
                                 		 			
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			
                     			<?php }  ?>
                     				
                     				
                     			<?php } ?>
                     			
                     			
                     			<?php } ?>			
                        		
                        		<?php if($r->wh < $r->wh_completed) { ?>   
                    			    		
                        		<?php if($whl < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($whl == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>    
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                           
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh ==  $r->wh_completed) { ?>
                     				
                     			<?php if($whl == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh > $r->wh_completed) { ?>
        							
        						<?php if($whl < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal" data-target="#WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span> <?php      if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - WH Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<input type="hidden" id="wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
										            	<label>Requested Products :- <?php echo $r->wh-$r->wh_completed; ?></label>
										            	<input type="text" id="wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control" />
										            </div>
            										</form>
              									</div>
              									<div class="modal-footer"> 
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Completed</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
                     				<script>
                                    
                                    
                                    $('#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->wh-$r->wh_completed; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>	
                                
                     			 <script>
               						$(function(){
												$("#wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var qty=$("#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
					  							var id=$("#wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {qid:id,qt:qty}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/whadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>  
                				                				
                				<?php } elseif($whl == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" data-target="#WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span><?php      if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - WH Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<input type="hidden" id="wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
										            	<label>Requested Products :- <?php echo $r->wh-$r->wh_completed; ?></label>
										            	<input type="text" id="wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control" />
										            </div>
            										</form>
              									</div>
              									<div class="modal-footer"> 
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Completed</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
                     			<script>
                                    
                                    
                                    $('#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->wh-$r->wh_completed; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>	
                     			 <script>
               						$(function(){
												$("#wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var qty=$("#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
					  							var id=$("#wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {qid:id,qt:qty}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/whadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
                     			<?php } ?>
        							
        						<?php } ?>
                     			
					<?php } ?>
						
                	</div>
                  		</td>
                  		<td class="col-md-1">
                  			<?php $totalp = (($qcl+$qal+$whl+$ansal)/4) ?>
                  				<span class="badge bg-red"><?php echo round($totalp); ?>%</span><?php if($totalp == 100) { echo "Ready";  } else { echo "Not Ready";  } ?>
                  		</td>
                	</tr>
                		
                	<?php } else { ?>  <!-- ANSA Not -->
                	
                	<tr>
                  		<td class="col-md-2">
                  			<!--<h4 class="bg-green" style="width:100%; text-align:center;"><?php echo $r->container_type; ?></h4>-->
                  			<h4 class="bg-blue" style="width:100%"><?php echo $r->job_name; ?></h4> 
                  			<div class="col-md-8"><h5><a style="color: #000;" data-toggle="modal" data-target="#TPD<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">TPD: <?php $tpd = get_tpd_by_id($r->tpd); foreach($tpd as $t) { echo $t->name; } ?> | QTY: <?php echo $r->qty; ?></a></h5></div>
                  			<div class="col-md-4"><h5><?php echo $r->container_type; ?></h5></div>
                  			<div class="col-md-8"><h5><?php echo $r->customer_name; ?></h5></div>
                  			<div class="col-md-4"><h5><?php echo date ("j M,y",strtotime($r->container_date)); ?></h5></div>
                  			<!-- TPD PopUp Srart -->
                  			<div class="example-modal">
        							<div class="modal modal-primary" id="TPD<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Change TPD. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
              											<label>Select TPD</label>
                										<input type="hidden" id="tpdid<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->container_job_id; ?>" />
                										<div class="form-group">
                											<?php $all_tpd=get_tpd_by_job(); ?>
                											<select class="form-control" id="tpdname<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
                													<?php foreach ($all_tpd as $ntpd) { ?>
                														<option value="<?php echo $ntpd->id; ?>" <?php if($ntpd->id==$r->tpd) echo 'selected="selected"' ?>><?php echo $ntpd->name; ?></option>
                													<?php } ?>
                											</select>
                										</div>
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="tpdbtn<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
        						
        						<script>
               						$(function(){
												$("#tpdbtn<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id=$("#tpdid<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var tpd=$("#tpdname<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {ids:id,tpdname:tpd}
												$.ajax({
														'url':'<?php echo base_url();?>admin/jobscreen/tpdchange',
														'type':'POST',
														'data':data,
														'success':function(message)
														{	
															$( "#maindiv" ).load( "jobscreen/reloaded" );
					
														}
														});
														});
								   						});
               					</script>
        						
        						<!-- TPD PopUp End -->
                  		</td> 
                  
                 	 	<td class="col-md-9">
                 	 		<?php $qul = (($r->qc*100)/$r->qty) ?>	
                     		<?php $qap = (($r->qa_completed*100)/$r->qty) ?>
                     		<?php $whp = (($r->wh_completed*100)/$r->qty) ?>
                  			<div class="row">
                  				<!-- Start Row -->
                  				<div class="col-md-3"> 
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
	                            		<style>
	                						.info-box-content{
	                							margin-left: 0;
	                							padding: 5px 5px;
	                						}
	                					</style>
	                            		<div class="info-box-content">                       
		                					<span class="progress-description">
		                                  	</span>
		                              		<div class="progress">
		                                		<div class="progress-bar" style="width: 0%"></div>
		                              		</div>
		                                  	<span class="info-box-text">Not Applicable</span> 
	                            		</div>
                        			</div>
                     			</div><!-- End First Col-md-3 -->
                     				
                     		
                     		<!-- if qc is < 25%  -->
                     		
                     			<?php if($qul < 25) { ?>
                    			<!-- QC is RED  -->
                    			<div class="col-md-3">
                       				<div class="info-box bg-red" data-toggle="modal" data-target="#QC<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qty-$r->qc_completed; ?></span>
                              				<span><?php  if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qul; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					
                                    							<?php echo $r->qc_comment; ?>
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="QC<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - QC Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
              											<label>Pending Products :- <?php echo $r->qty-$r->qc_completed; ?></label>
                										<input type="hidden" id="qcid11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<div class="form-group">
                											<input type="hidden" id="qcconn11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qc; ?>" />
                  											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qccon11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control number" />
                										</div>
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="qc11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
                     			<script>
                                    
                                    
                                    $('#qccon11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->qty-$r->qc_completed; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>	
                     			 <script>
               						$(function(){
												$("#qc11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id=$("#qcid11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con=$("#qccon11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con2=$("#qcconn11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {qid:id,qcon:con,qcon2:con2}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/qcadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );
										
									}
									});
									
									});
			   						});
			   						
               						</script>	
                     			<!-- QA is Black  -->
                     			<!-- QA Condition -->
								<?php if($r->qa > $r->qa_completed) { ?>  
									<!-- QA is Purple  -->
									<?php if($r->qa_rem !=0) { ?>
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
	                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                  									<span aria-hidden="true">&times;</span></button>
	                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label><br />
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                  											
                  											
                  											
                										</div> 
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
        						<script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                                <script>
                                    
                                    
                                    $('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->qa_per; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
        						<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
        						
        						
                     			<script>
                     				$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
									});
                     				
               						$(function(){
												$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var chk =$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:chk}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/qaadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
									
									<?php } elseif($r->qa_rem == '0') { ?>
										<!-- QA is Blue  -->
									<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
	                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                  									<span aria-hidden="true">&times;</span></button>
	                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label><br />
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                  											
                  											
                  											
                										</div> 
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
        						<script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                                <script>
                                    
                                    
                                    $('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->qa_per; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
        						<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
        						
        						
                     			<script>
                     				$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
									});
                     				
               						$(function(){
												$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var chk =$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:chk}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/qaadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
										
									<?php } else { ?>		                   	
                     				<!-- QA is RED  -->
                     				<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
	                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                  									<span aria-hidden="true">&times;</span></button>
	                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label><br />
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                  											
                  											
                  											
                										</div> 
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
        						<script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                                <script>
                                    
                                    
                                    $('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->qa_per; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
        						<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
        						
        						
                     			<script>
                     				$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
									});
                     				
               						$(function(){
												$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var chk =$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:chk}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/qaadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
               						<?php } ?>		
               						
               					<!-- QA is Green -->	   
                     			 <?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     				<div class="col-md-3">
                       				<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			<?php } ?>
                     			                     			
                     			<?php if($r->wh < $r->wh_completed) { ?>   
                    			    		
                        		<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh ==  $r->wh_completed) { ?>
                     				
                     			<?php if($whp == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                             
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                				
                				<?php } elseif($r->wh > $r->wh_completed) { ?>
        							
        						<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal" data-target="#WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - WH Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<input type="hidden" id="wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
										            	<label>Requested Products :- <?php echo $r->wh-$r->wh_completed; ?></label>
										            	<input type="text" id="wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control" />
										            </div>
            										</form>
              									</div>
              									<div class="modal-footer"> 
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Completed</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
                     			<script>
                                    
                                    
                                    $('#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->wh-$r->wh_completed; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                                
                     			 <script>
               						$(function(){
												$("#wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var qty=$("#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
					  							var id=$("#wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {qid:id,qt:qty}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/whadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>  
                				                				
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" data-target="#WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - WH Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<input type="hidden" id="wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
										            	<label>Requested Products :- <?php echo $r->wh-$r->wh_completed; ?></label>
										            	<input type="text" id="wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control" />
										            </div>
            										</form>
              									</div>
              									<div class="modal-footer"> 
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Completed</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
                     			<script>
                                    
                                    
                                    $('#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->wh-$r->wh_completed; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                     			 <script>
               						$(function(){
												$("#wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var qty=$("#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
					  							var id=$("#wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {qid:id,qt:qty}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/whadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
                     			<?php } ?>
        							
        						<?php } ?>
                     			
                     			<!-- if qc is Completed  -->
                     		
                     			<?php } elseif($qul == 100) { ?>
                     				<!-- QC is Green  -->
                     				<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qty-$r->qc_completed; ?></span>  
                              				<span><?php      if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                            
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qul; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					
															<?php echo $r->qc_comment; ?>
															
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>                     	
                     	
                     			<!-- QA Condition -->
								<?php if($r->qa > $r->qa_completed) { ?>  
									<!-- QA is Purple  -->
									<?php if($r->qa_rem !=0) { ?>
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
	                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                  									<span aria-hidden="true">&times;</span></button>
	                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label><br />
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                  											
                  											
                  											
                										</div> 
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
        						<script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                                <script>
                                    
                                    
                                    $('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->qa_per; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
        						<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
        						
        						
                     			<script>
                     				$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
									});
                     				
               						$(function(){
												$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var chk =$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:chk}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/qaadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
									
									<?php } elseif($r->qa_rem == '0') { ?>
										<!-- QA is Blue  -->
									<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
	                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                  									<span aria-hidden="true">&times;</span></button>
	                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label><br />
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                  											
                  											
                  											
                										</div> 
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
        						<script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                                <script>
                                   
                                    
                                    $('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                       var product=<?php echo $r->qa_per; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
        						<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
        						
        						
                     			<script>
                     				$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
									});
                     				
               						$(function(){
												$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var chk =$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:chk}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/qaadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
										
									<?php } else { ?>		                   	
                     				<!-- QA is RED  -->
                     				<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
	                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                  									<span aria-hidden="true">&times;</span></button>
	                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label><br />
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                  											
                  											
                  											
                										</div> 
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
        						<script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                                <script>
                                    
                                    
                                    $('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->qa_per; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
        						<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
                     			<script>
                     				$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
									});
                     				
               						$(function(){
												$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var chk =$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:chk}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/qaadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
               						<?php } ?>		
               						
               					<!-- QA is Green -->	   
                     			 <?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     				<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			
                     			<?php }  ?>
                     			   
                        		<?php if($r->wh < $r->wh_completed) { ?>   
                    			    		
                        		<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span>
                                    				<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?>
                                  				</span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span>
                                    				<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?>
                                  				</span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span>
                                    				<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?>
                                  				</span>                                
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh ==  $r->wh_completed) { ?>
                     				
                     			<?php if($whp == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span>
                                    				<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?>
                                  				</span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span>
                                    				<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?>
                                  				</span>                                
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh > $r->wh_completed) { ?>
        							
        						<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal" data-target="#WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span>
                                    				<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?>
                                  				</span>                                
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - WH Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<input type="hidden" id="wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
										            	<label>Requested Products :- <?php echo $r->wh-$r->wh_completed; ?></label>
										            	<input type="text" id="wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control" />
										            	</div>
            										</form>
              									</div>
              									<div class="modal-footer"> 
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Completed</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
                     			<script>
                                    
                                    
                                    $('#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->wh-$r->wh_completed; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                                 <script>
               						$(function(){
												$("#wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var qty=$("#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
					  							var id=$("#wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
					  							var data= {qid:id,qt:qty}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/whadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>  
                				                				
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span>
                                    				<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?>
                                  				</span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" data-target="#WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span>
                                    				<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?>
                                  				</span>                                
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - WH Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<input type="hidden" id="wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
										            	<label>Requested Products :- <?php echo $r->wh-$r->wh_completed; ?></label>
										            	<input type="text" id="wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control" />
										            </div>
            										</form>
              									</div>
              									<div class="modal-footer"> 
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Completed</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
                     			<script>
                                    
                                    
                                    $('#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->wh-$r->wh_completed; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                     			 <script>
               						$(function(){
												$("#wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var qty=$("#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
					  							var id=$("#wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {qid:id,qt:qty}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/whadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
                     			<?php } ?>
        							
        						<?php } ?>
                     		
                     		<!-- if qc is Middel  -->
                     		
                     		<?php } else { ?>		
                     			<!-- QC is yellow  -->		
        						<div class="col-md-3">
                       				<div class="info-box bg-yellow" data-toggle="modal" data-target="#QC<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qty-$r->qc_completed; ?></span>
                              				<span>
                                    				<?php      if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
																		$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																		$t1 = strtotime($today)*1000;
																		$t2 = strtotime($adate)*1000;
																		$tt = $t1-$t2;
																		$elapsed = (($tt / 1000)/60)/60;
                        												$a = $elapsed;
                        												$f = sprintf ("%.2f", $a);
                        												$nn=$f;
                        												$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																		echo $nnn." Hrs Ago";
																	}
															?>
                                  				</span>                               
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qul; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					
															<?php echo $r->qc_comment; ?>
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="QC<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - QC Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
              											<label>Pending Products :- <?php echo $r->qty-$r->qc_completed; ?></label>
                										<input type="hidden" id="qcid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<div class="form-group">
                											<input type="hidden" id="qcconn1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qc; ?>" />
                  											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qccon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control number" />
                										</div>
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="qc1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
                     			<script>
                                    
                                    
                                    $('#qccon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->qty-$r->qc_completed; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                     			 <script>
               						$(function(){
												$("#qc1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id=$("#qcid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con=$("#qccon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con2=$("#qcconn1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {qid:id,qcon:con,qcon2:con2}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/qcadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
                     	
                     	
                     			<!-- QA Condition -->
								<?php if($r->qa > $r->qa_completed) { ?>  
									<!-- QA is Purple  -->
									<?php if($r->qa_rem !=0) { ?>
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
	                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                  									<span aria-hidden="true">&times;</span></button>
	                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label><br />
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                  											
                  											
                  											
                										</div> 
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
        						<script>
                                    
                                    
                                    $('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->qa_per; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                                <script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
        						<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
        						
        						
                     			<script>
                     				$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
									});
                     				
               						$(function(){
												$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var chk =$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:chk}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/qaadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
									
									<?php } elseif($r->qa_rem == '0') { ?>
										<!-- QA is Blue  -->
									<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
	                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                  									<span aria-hidden="true">&times;</span></button>
	                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label><br />
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                  											
                  											
                  											
                										</div> 
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
        						<script>
                                    
                                    
                                    $('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->qa_per; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                                <script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
        						<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
        						
        						
                     			<script>
                     				$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
									});
                     				
               						$(function(){
												$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var chk =$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:chk}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/qaadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
										
									<?php } else { ?>		                   	
                     				<!-- QA is RED  -->
                     				<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
	                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                  									<span aria-hidden="true">&times;</span></button>
	                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label><br />
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                  								
                										</div> 
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
        						<script>
                                    
                                    
                                    $('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->qa_per; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                                <script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
        						<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
        						
        						
                     			<script>
                     				$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
									});
                     				
               						$(function(){
												$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var chk =$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:chk}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/qaadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
               						<?php } ?>		
               						
               					<!-- QA is Green -->	   
                     			 <?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     				<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			
                     			<?php }  ?>
                     			
                     			<?php if($r->wh < $r->wh_completed) { ?>   
                    			    		
                        		<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                             
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh ==  $r->wh_completed) { ?>
                     			
                     			<?php if($whp == 0) { ?>
                     				
                     			<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } else { ?>
                     				
                     			<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>	
                     			<?php } ?>	
                     			
                     			<?php } elseif($r->wh > $r->wh_completed) { ?>
        							
        						<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal" data-target="#WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - WH Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<input type="hidden" id="wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
										            	<label>Requested Products :- <?php echo $r->wh-$r->wh_completed; ?></label>
										            	<input type="text" id="wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control" />
										            </div>
            										</form>
              									</div>
              									<div class="modal-footer"> 
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Completed</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
                     			<script>
                                    
                                    
                                    $('#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->wh-$r->wh_completed; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                                
                     			 <script>
               						$(function(){
												$("#wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var qty=$("#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
					  							var id=$("#wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {qid:id,qt:qty}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/whadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>  
                				                				
                				<?php } elseif($whp == 100) { ?>	
                				
                				<?php if($whp == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>   
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                            
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" data-target="#WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - WH Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<input type="hidden" id="wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
										            	<label>Requested Products :- <?php echo $r->wh-$r->wh_completed; ?></label>
										            	<input type="text" id="wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control" />
										            </div>
            										</form>
              									</div>
              									<div class="modal-footer"> 
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Completed</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
                     			<script>
                                    
                                    
                                    $('#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                     var product=<?php echo $r->wh-$r->wh_completed; ?>; 
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                     			 <script>
               						$(function(){
												$("#wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var qty=$("#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
					  							var id=$("#wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {qid:id,qt:qty}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/whadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
                     			<?php } ?>
        							
        						<?php } ?>	 	
        					
        					<?php } ?>	
        					
        					<!-- End Row -->	
                    		</div>
                  		</td>
                  		<td class="col-md-1">
                  			<?php $totalp = (($qul+$qap+$whp)/3) ?>	
                  				<span class="badge bg-red"><?php echo round($totalp); ?>%</span><?php if($totalp == 100) { echo "Ready";  } else { echo "Not Ready";  } ?> 
                  		</td>
                	</tr>
                	<?php } ?>
                	
                	
                	<!-- ANSA Container is ready -->
                	
                	<?php } else { ?>
                	<tr>
                  		<td class="col-md-2">
                  			<!--<h4 class="bg-green" style="width:100%; text-align:center;"><?php echo $r->container_type; ?></h4>-->
                  			<h4 class="bg-blue" style="width:100%"><?php echo $r->job_name; ?></h4> 
                  			<div class="col-md-8"><h5><a style="color: #000;" data-toggle="modal" data-target="#TPD<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">TPD: <?php $tpd = get_tpd_by_id($r->tpd); foreach($tpd as $t) { echo $t->name; } ?> | QTY: <?php echo $r->qty; ?></a></h5></div>
                  			<div class="col-md-4"><h5><?php echo $r->container_type; ?></h5></div>
                  			<div class="col-md-8"><h5><?php echo $r->customer_name; ?></h5></div>
                  			<div class="col-md-4"><h5><?php echo date ("j M,y",strtotime($r->container_date)); ?></h5></div>
                  			<!-- TPD PopUp Srart -->
                  			<div class="example-modal">
        							<div class="modal modal-primary" id="TPD<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Change TPD. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
              											<label>Select TPD</label>
                										<input type="hidden" id="tpdid<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->container_job_id; ?>" />
                										<div class="form-group">
                											<?php $all_tpd=get_tpd_by_job(); ?>
                											<select class="form-control" id="tpdname<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
                													<?php foreach ($all_tpd as $ntpd) { ?>
                														<option value="<?php echo $ntpd->id; ?>" <?php if($ntpd->id==$r->tpd) echo 'selected="selected"' ?>><?php echo $ntpd->name; ?></option>
                													<?php } ?>
                											</select>
                										</div>
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="tpdbtn<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
        						
        						<script>
               						$(function(){
												$("#tpdbtn<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id=$("#tpdid<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var tpd=$("#tpdname<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {ids:id,tpdname:tpd}
												$.ajax({
														'url':'<?php echo base_url();?>admin/jobscreen/tpdchange',
														'type':'POST',
														'data':data,
														'success':function(message)
														{	
															$( "#maindiv" ).load( "jobscreen/reloaded" );
					
														}
														});
														});
								   						});
               					</script>
        						
        						<!-- TPD PopUp End -->
                  		</td> 
                  
                 	 	<td class="col-md-9">
                 	 		<?php $qul = (($r->qc*100)/$r->qty) ?>	
                     		<?php $qap = (($r->qa_completed*100)/$r->qty) ?>
                     		<?php $whp = (($r->wh_completed*100)/$r->qty) ?>
                     		<?php $ansap = (($r->ansa_completed*100)/$r->qty) ?>
                  			<div class="row">
                  				<!-- Start Row -->
                  				
                  				
                  				<?php if($r->ansa < $r->ansa_completed) { ?>   
                    			    		
                        		<?php if($ansap < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->wh_completed-$r->ansa_completed; ?></span>  
                             	 			<span><?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $ansap; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($ansap == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->wh_completed-$r->ansa_completed; ?></span>  
                             	 			<span><?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                             
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $ansap; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->wh_completed-$r->ansa_completed; ?></span>  
                             	 			<span><?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                             
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $ansap; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->ansa ==  $r->ansa_completed) { ?>
                     			
                     			<?php if($ansap == 0) { ?>
                     				
                     			<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->wh_completed-$r->ansa_completed; ?></span> 
                             	 			<span><?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>
                             	 			                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $ansap; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } else { ?>
                     				
                     			<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->wh_completed-$r->ansa_completed; ?></span> 
                             	 			<span><?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $ansap; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>	
                     			<?php } ?>	
                     			
                     			<?php } elseif($r->ansa > $r->ansa_completed) { ?>
        							
        						<?php if($ansap < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal" data-target="#ANSA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->wh_completed-$r->ansa_completed; ?></span>
                             	 			<span><?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $ansap; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="ANSA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - WH Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<input type="hidden" id="ansa11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
										            	<label>Requested Products :- <?php echo $r->ansa-$r->ansa_completed; ?></label>
										            	<input type="text" id="ansa33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control" />
										            </div>
            										</form>
              									</div>
              									<div class="modal-footer"> 
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="ansa22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Completed</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
                     			<script>
                                    
                                    
                                    $('#ansa33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->ansa-$r->ansa_completed; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                     			 <script>
               						$(function(){
												$("#ansa22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var qty=$("#ansa33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
					  							var id=$("#ansa11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {qid:id,qt:qty}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/ansadata',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>  
                				                				
                				<?php } elseif($ansap == 100) { ?>	
                				
                				<?php if($ansap == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->wh_completed-$r->ansa_completed; ?></span>  
                             	 			<span><?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                             
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $ansap; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->wh_completed-$r->ansa_completed; ?></span> 
                             	 			<span><?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $ansap; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" data-target="#ANSA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->wh_completed-$r->ansa_completed; ?></span>
                             	 			<span><?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $ansap; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="ANSA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - WH Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<input type="hidden" id="ansa11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
										            	<label>Requested Products :- <?php echo $r->ansa-$r->ansa_completed; ?></label>
										            	<input type="text" id="ansa33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control" />
										            </div>
            										</form>
              									</div>
              									<div class="modal-footer"> 
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="ansa22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Completed</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
                     			<script>
                                    
                                    
                                    $('#ansa33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->ansa-$r->ansa_completed; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                     			 <script>
               						$(function(){
												$("#ansa22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var qty=$("#ansa33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
					  							var id=$("#ansa11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {qid:id,qt:qty}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/ansadata',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
                     			<?php } ?>
        							
        						<?php } ?>
                  				
                  				
                  				<!-- End First Col-md-3 -->
                     				
                     		
                     		<!-- if qc is < 25%  -->
                     		
                     			<?php if($qul < 25) { ?>
                    			<!-- QC is RED  -->
                    			<div class="col-md-3">
                       				<div class="info-box bg-red" data-toggle="modal" data-target="#QC<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qty-$r->qc_completed; ?></span> 
                              				<span><?php  if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                             
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qul; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					
                                    							<?php echo $r->qc_comment; ?>
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="QC<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - QC Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
              											<label>Pending Products :- <?php echo $r->qty-$r->qc_completed; ?></label>
                										<input type="hidden" id="qcid11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<div class="form-group">
                											<input type="hidden" id="qcconn11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qc; ?>" />
                  											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qccon11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" max="<?php echo $r->qty-$r->qc_completed; ?>" class="form-control number"  />
                										</div>
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="qc11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
        						
        						<script>
									$('#qccon11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
									var product=<?php echo $r->qty-$r->qc_completed; ?>;  
									  if ($(this).val() > product){
									    alert("Please Enter Valid Products");
									   $(this).val('');
									  }
									  else
										{
											
									}
									});
								</script>
                     			 <script>
               						$(function(){
												$("#qc11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id=$("#qcid11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con=$("#qccon11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con2=$("#qcconn11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  							var chk = '<?php echo $r->qty; ?>';
				  							
				  							
				  											
				  					var data= {qid:id,qcon:con,qcon2:con2}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/qcadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );
										
									}
									});
									
									});
			   						});
			   						
               						</script>	
                     			<!-- QA is Black  -->
                     			<!-- QA Condition -->
								<?php if($r->qa > $r->qa_completed) { ?>  
									<!-- QA is Purple  -->
									<?php if($r->qa_rem !=0) { ?>
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              				<span><?php  if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                               
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
	                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                  									<span aria-hidden="true">&times;</span></button>
	                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label><br />
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                  											
                  											
                  											
                										</div> 
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
        						<script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
								<script>
        							
        							
									$('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
									  var product=<?php echo $r->qa_per; ?>;
									  if ($(this).val() > product){
									    alert("Please Enter Valid Products");
									   $(this).val('');
									  }
									  else
										{
											
									}
									});
								</script>
        						<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
        						
        						
                     			<script>
                     				$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
									});
                     				
               						$(function(){
												$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var chk =$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:chk}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/qaadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
									
									<?php } elseif($r->qa_rem == '0') { ?>
										<!-- QA is Blue  -->
									<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>  
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                             
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
	                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                  									<span aria-hidden="true">&times;</span></button>
	                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label><br />
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                  											
                  											
                  											
                										</div> 
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
        						<script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
								<script>
        							
        							
									$('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
									  var product=<?php echo $r->qa_per; ?>;
									  if ($(this).val() > product){
									    alert("Please Enter Valid Products");
									   $(this).val('');
									  }
									  else
										{
											
									}
									});
								</script>
        						<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
        						
        						
                     			<script>
                     				$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
									});
                     				
               						$(function(){
												$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var chk =$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:chk}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/qaadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
										
									<?php } else { ?>		                   	
                     				<!-- QA is RED  -->
                     				<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
	                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                  									<span aria-hidden="true">&times;</span></button>
	                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label><br />
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                  											
                  											
                  											
                										</div> 
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
        						<script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
								<script>
        							
        							
									$('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
									  var product=<?php echo $r->qa_per; ?>;
									  if ($(this).val() > product){
									    alert("Please Enter Valid Products");
									   $(this).val('');
									  }
									  else
										{
											
									}
									});
								</script>
        						<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
        						
        						
                     			<script>
                     				$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
									});
                     				
               						$(function(){
												$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var chk =$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:chk}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/qaadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
               						<?php } ?>		
               						
               					<!-- QA is Green -->	   
                     			 <?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     				<div class="col-md-3">
                       				<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			<?php } ?>
                     			                     			
                     			<?php if($r->wh < $r->wh_completed) { ?>   
                    			    		
                        		<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh ==  $r->wh_completed) { ?>
                     				
                     			<?php if($whp == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>    
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                            
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                				
                				<?php } elseif($r->wh > $r->wh_completed) { ?>
        							
        						<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal" data-target="#WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - WH Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<input type="hidden" id="wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
										            	<label>Requested Products :- <?php echo $r->wh-$r->wh_completed; ?></label>
										            	<input type="text" id="wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control" />
										            </div>
            										</form>
              									</div>
              									<div class="modal-footer"> 
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Completed</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
                     			<script>
        							
        							
									$('#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
									  var product=<?php echo $r->wh-$r->wh_completed; ?>;
									  if ($(this).val() > product){
									    alert("Please Enter Valid Products");
									   $(this).val('');
									  }
									  else
										{
											
									}
									});
								</script>
								
                     			 <script>
               						$(function(){
												$("#wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var qty=$("#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
					  							var id=$("#wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {qid:id,qt:qty}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/ansawhadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>  
                				                				
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" data-target="#WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - WH Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<input type="hidden" id="wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
										            	<label>Requested Products :- <?php echo $r->wh-$r->wh_completed; ?></label>
										            	<input type="text" id="wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control" />
										            </div>
            										</form>
              									</div>
              									<div class="modal-footer"> 
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Completed</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
                     			<script>
        							
        							
									$('#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
									  var product=<?php echo $r->wh-$r->wh_completed; ?>;
									  if ($(this).val() > product){
									    alert("Please Enter Valid Products");
									   $(this).val('');
									  }
									  else
										{
											
									}
									});
								</script>
                     			 <script>
               						$(function(){
												$("#wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var qty=$("#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
					  							var id=$("#wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {qid:id,qt:qty}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/ansawhadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
                     			<?php } ?>
        							
        						<?php } ?>
                     			
                     			<!-- if qc is Completed  -->
                     		
                     			<?php } elseif($qul == 100) { ?>
                     				<!-- QC is Green  -->
                     				<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qty-$r->qc_completed; ?></span> 
                              				<span><?php      if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qul; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					
															<?php echo $r->qc_comment; ?>
															
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>                     	
                     	
                     			<!-- QA Condition -->
								<?php if($r->qa > $r->qa_completed) { ?>  
									<!-- QA is Purple  -->
									<?php if($r->qa_rem !=0) { ?>
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              				<span> <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                               
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
	                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                  									<span aria-hidden="true">&times;</span></button>
	                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label><br />
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                  											
                  											
                  											
                										</div> 
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
        						<script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
								<script>
        							
        							
									$('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
									  var product=<?php echo $r->qa_per; ?>;
									  if ($(this).val() > product){
									    alert("Please Enter Valid Products");
									   $(this).val('');
									  }
									  else
										{
											
									}
									});
								</script>
        						<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
        						
        						
                     			<script>
                     				$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
									});
                     				
               						$(function(){
												$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var chk =$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:chk}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/qaadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
									
									<?php } elseif($r->qa_rem == '0') { ?>
										<!-- QA is Blue  -->
									<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              				<span> <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
	                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                  									<span aria-hidden="true">&times;</span></button>
	                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label><br />
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                  											
                  											
                  											
                										</div> 
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
        						<script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
								<script>
        							
        							
									$('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
									  var product=<?php echo $r->qa_per; ?>;
									  if ($(this).val() > product){
									    alert("Please Enter Valid Products");
									   $(this).val('');
									  }
									  else
										{
											
									}
									});
								</script>
        						<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
        						
        						
                     			<script>
                     				$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
									});
                     				
               						$(function(){
												$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var chk =$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:chk}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/qaadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
										
									<?php } else { ?>		                   	
                     				<!-- QA is RED  -->
                     				<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              				<span> <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
	                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                  									<span aria-hidden="true">&times;</span></button>
	                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label><br />
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                  											
                  											
                  											
                										</div> 
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
        						<script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
								<script>
        							
        							
									$('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
									  var product=<?php echo $r->qa_per; ?>;
									  if ($(this).val() > product){
									    alert("Please Enter Valid Products");
									   $(this).val('');
									  }
									  else
										{
											
									}
									});
								</script>
        						<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
        						        						
        						
                     			<script>
                     				$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
									});
                     				
               						$(function(){
												$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var chk =$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:chk}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/qaadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
               						<?php } ?>		
               						
               					<!-- QA is Green -->	   
                     			 <?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     				<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              				<span> <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			
                     			<?php }  ?>
                     			   
                        		<?php if($r->wh < $r->wh_completed) { ?>   
                    			    		
                        		<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                                
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh ==  $r->wh_completed) { ?>
                     				
                     			<?php if($whp == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                                
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh > $r->wh_completed) { ?>
        							
        						<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal" data-target="#WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>   
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - WH Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<input type="hidden" id="wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
										            	<label>Requested Products :- <?php echo $r->wh-$r->wh_completed; ?></label>
										            	<input type="text" id="wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control" />
										            	</div>
            										</form>
              									</div>
              									<div class="modal-footer"> 
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Completed</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
                     			<script>
        							
        							
									$('#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
									  var product=<?php echo $r->wh-$r->wh_completed; ?>;
									  if ($(this).val() > product){
									    alert("Please Enter Valid Products");
									   $(this).val('');
									  }
									  else
										{
											
									}
									});
								</script>
								<script>
               						$(function(){
												$("#wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var qty=$("#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
					  							var id=$("#wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
					  							var data= {qid:id,qt:qty}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/ansawhadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>  
                				                				
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" data-target="#WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                                
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - WH Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<input type="hidden" id="wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
										            	<label>Requested Products :- <?php echo $r->wh-$r->wh_completed; ?></label>
										            	<input type="text" id="wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control" />
										            </div>
            										</form>
              									</div>
              									<div class="modal-footer"> 
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Completed</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
                     			<script>
        							
        							
									$('#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
									  var product=<?php echo $r->wh-$r->wh_completed; ?>;
									  if ($(this).val() > product){
									    alert("Please Enter Valid Products");
									   $(this).val('');
									  }
									  else
										{
											
									}
									});
								</script>
                     			 <script>
               						$(function(){
												$("#wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var qty=$("#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
					  							var id=$("#wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {qid:id,qt:qty}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/ansawhadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
                     			<?php } ?>
        							
        						<?php } ?>
                     		
                     		<!-- if qc is Middel  -->
                     		
                     		<?php } else { ?>		
                     			<!-- QC is yellow  -->		
        						<div class="col-md-3">
                       				<div class="info-box bg-yellow" data-toggle="modal" data-target="#QC<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qty-$r->qc_completed; ?></span> 
                              				<span> <?php      if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?></span>                               
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qul; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    				
															<?php echo $r->qc_comment; ?>
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="QC<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - QC Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
              											<label>Pending Products :- <?php echo $r->qty-$r->qc_completed; ?></label>
                										<input type="hidden" id="qcid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<div class="form-group">
                											<input type="hidden" id="qcconn1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qc; ?>" />
                  											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qccon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control number" />
                										</div>
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="qc1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
                     			<script>
        							
        							
									$('#qccon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
									  var product=<?php echo $r->qty-$r->qc_completed; ?>;
									  if ($(this).val() > product){
									    alert("Please Enter Valid Products");
									   $(this).val('');
									  }
									  else
										{
											
									}
									});
								</script>
                     			 <script>
               						$(function(){
												$("#qc1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id=$("#qcid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con=$("#qccon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con2=$("#qcconn1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {qid:id,qcon:con,qcon2:con2}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/qcadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
                     	
                     	
                     			<!-- QA Condition -->
								<?php if($r->qa > $r->qa_completed) { ?>  
									<!-- QA is Purple  -->
									<?php if($r->qa_rem !=0) { ?>
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>  
                              				<span> <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
	                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                  									<span aria-hidden="true">&times;</span></button>
	                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label><br />
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                  											
                  											
                  											
                										</div> 
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
        						<script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
								<script>
        							
        							
									$('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
									  var product=<?php echo $r->qa_per; ?>;
									  if ($(this).val() > product){
									    alert("Please Enter Valid Products");
									   $(this).val('');
									  }
									  else
										{
											
									}
									});
								</script>
        						<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
        						
        						
                     			<script>
                     				$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
									});
                     				
               						$(function(){
												$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var chk =$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:chk}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/qaadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
									
									<?php } elseif($r->qa_rem == '0') { ?>
										<!-- QA is Blue  -->
									<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              				<span> <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
	                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                  									<span aria-hidden="true">&times;</span></button>
	                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label><br />
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                										</div> 
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
        						<script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
								<script>
        							
        							
									$('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
									  var product=<?php echo $r->qa_per; ?>;
									  if ($(this).val() > product){
									    alert("Please Enter Valid Products");
									   $(this).val('');
									  }
									  else
										{
											
									}
									});
								</script>
        						<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
        						
        						
                     			<script>
                     				$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									}
 	 									
									});
                     				
               						$(function(){
												$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var chk =$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:chk}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/qaadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
										
									<?php } else { ?>		                   	
                     				<!-- QA is RED  -->
                     				<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal" data-target="#QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              				<span> <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				<div class="example-modal">
        								<div class="modal modal-primary" id="QA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          									<div class="modal-dialog">
								            	<div class="modal-content">
								            		<div class="modal-header">
	                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                  									<span aria-hidden="true">&times;</span></button>
	                									<h4 class="modal-title">Container Settings - QA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              										</div>
              										<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<label>Requested Products :- <?php echo $r->qa_per; ?> <span id="output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">All Rejected</span></label><br />
                										<input type="hidden" id="qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<input type="hidden" id="qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa; ?>" />
                										<input type="hidden" id="qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_per; ?>" />
                										<input type="hidden" id="qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->qa_completed; ?>" />
                										<div class="form-group">
                											
                											<label for="examplecomment">Number of pcs audited</label>
                  											<input type="text" id="qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control" />
                											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="0" class="form-control number" />
                  											<label for="examplecomment">Defects</label>
                  											<input id="search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" type="text" class="form-control" />
                  											<div class="suggesstion-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>"></div>
                  								
                										</div> 
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" class="btn btn-outline btnsave" id="qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
        						<script>
                                
                                    $('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                    var product=Number($("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val());  
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
								<script>
        							
        							
									$('#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
									  var product=<?php echo $r->qa_per; ?>;
									  if ($(this).val() > product){
									    alert("Please Enter Valid Products");
									   $(this).val('');
									  }
									  else
										{
											
									}
									});
								</script>
        						<script>
                                        $(function() {
                                            function split( val ) {
                                                return val.split( /,\s*/ );
                                            }
                                            function extractLast( term ) {
                                                return split( term ).pop();
                                            }
                                           
                                            $("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").bind( "keydown", function( event ) {
                                                if ( event.keyCode === $.ui.keyCode.TAB &&
                                                    $( this ).autocomplete( "instance" ).menu.active ) {
                                                    event.preventDefault();
                                                }
                                            })
                                            .autocomplete({
                                                minLength: 1,
                                                source: function( request, response ) {
                                                    // delegate back to autocomplete, but extract the last term
                                                    $.getJSON("settings/get_defacts1", { term : extractLast( request.term )},response);
                                                },
                                                focus: function() {
                                                    // prevent value inserted on focus
                                                    return false;
                                                },
                                                select: function( event, ui ) {
                                                    var terms = split( this.value );
                                                    // remove the current input
                                                    terms.pop();
                                                    // add the selected item
                                                    terms.push( ui.item.value );
                                                    // add placeholder to get the comma-and-space at the end
                                                    terms.push( "" );
                                                    this.value = terms.join( ", " );
                                                    return false;
                                                }
                                            });
                                        });
                                        </script>
        						
        						
                     			<script>
                     				$('#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').on("input", function() {
  										var dInput = this.value;
  										var chlval =$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
 	 									if(dInput == chlval)
 	 									{
 	 									$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("completed");
 	 									}
 	 									else if(dInput < chlval)
 	 									{
 	 										if(dInput == '0' || dInput == '')
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("All Rejected");
 	 										}
 	 										else
 	 										{
 	 											$('#output<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').text("Half Rejected");	
 	 										}
 	 									} 
 	 									
									});
                     				
               						$(function(){
												$("#qa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id=$("#qaid1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con=$("#qacon1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var con2=$("#search-box<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data1=$("#qadata1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data2=$("#qadata2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data3=$("#qadata3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var chk =$("#qacon3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val(); 
				  								var data= {qaid:id,qacon1:con,qacon2:con2,qa:data1,qaper:data2,qacom:data3,chk:chk}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/qaadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
               						<?php } ?>		
               						
               					<!-- QA is Green -->	   
                     			 <?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     				<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>   
                              				<span> <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
															?></span>                            
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			
                     			<?php }  ?>
                     			
                     			<?php if($r->wh < $r->wh_completed) { ?>   
                    			    		
                        		<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                                 
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                                 
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh ==  $r->wh_completed) { ?>
                     			
                     			<?php if($whp == 0) { ?>
                     				
                     			<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                                
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } else { ?>
                     				
                     			<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                                
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>	
                     			<?php } ?>	
                     			
                     			<?php } elseif($r->wh > $r->wh_completed) { ?>
        							
        						<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal" data-target="#WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                                 
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - WH Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<input type="hidden" id="wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
										            	<label>Requested Products :- <?php echo $r->wh-$r->wh_completed; ?></label>
										            	<input type="text" id="wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control" />
										            </div>
            										</form>
              									</div>
              									<div class="modal-footer"> 
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Completed</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
                     			<script>
        							
        							
									$('#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
					 				  var product=<?php echo $r->wh-$r->wh_completed; ?>;
									  if ($(this).val() > product){
									    alert("Please Enter Valid Products");
									   $(this).val('');
									  }
									  else
										{
											
									}
									});
								</script>
								
                     			 <script>
               						$(function(){
												$("#wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var qty=$("#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
					  							var id=$("#wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {qid:id,qt:qty}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/ansawhadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>  
                				                				
                				<?php } elseif($whp == 100) { ?>	
                				
                				<?php if($whp == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>    
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>   
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" data-target="#WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";
																		}
                                    							?></span>                                 
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="WH<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - WH Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<input type="hidden" id="wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
										            	<label>Requested Products :- <?php echo $r->wh-$r->wh_completed; ?></label>
										            	<input type="text" id="wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control" />
										            </div>
            										</form>
              									</div>
              									<div class="modal-footer"> 
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Completed</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
                     			<script>
        							
        							
									$('#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
					 				  var product=<?php echo $r->wh-$r->wh_completed; ?>;
									  if ($(this).val() > product){
									    alert("Please Enter Valid Products");
									   $(this).val('');
									  }
									  else
										{
											
									}
									});
								</script>
                     			 <script>
               						$(function(){
												$("#wh22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var qty=$("#wh33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
					  							var id=$("#wh11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {qid:id,qt:qty}
									$.ajax({
									'url':'<?php echo base_url();?>admin/jobscreen/ansawhadd',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
                     			<?php } ?>
        							
        						<?php } ?>	 	
        					
        					<?php } ?>	
        					
        					<!-- End Row -->	
                    		</div>
                  		</td>
                  		<td class="col-md-1">
                  			<?php $totalp = (($qul+$qap+$whp+$ansap)/4) ?>	
                  				<span class="badge bg-red"><?php echo round($totalp); ?>%</span><?php if($totalp == 100) { echo "Ready";  } else { echo "Not Ready";  } ?> 
                  		</td>
                	</tr>
                		
                		
                		
                		
                	<?php } ?>	
                 	<?php } ?>
                </tbody>
           	</table>
        </div>