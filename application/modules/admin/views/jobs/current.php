<?php $this->layout('layouts::default') ?>
<?php
$ci=&get_instance();
$ci->load->helper('message');
?>
<div class="row">
	<div class="col-lg-12">
		<div class="box box-primary">
			<div class="box-header with-border">
            	<h3 class="box-title">Exchange Jobs</h3>
            </div>
            <div class="box-body">
            <form method="post" enctype="multipart/form-data" action="<?php echo base_url(); ?>admin/jobs/replace">
            
            <div class="form-group col-md-12">
            	<label>Select Job:-</label>
            	<select name="job" class="form-control">
            		<option value="">Select</option>
            		<?php foreach($result as $rs) { ?>
						<option value="<?php echo $rs->container_id.'-'.$rs->container_job_id.'-'.$rs->qty; ?>"><?php echo $rs->container_type.", job:- ".$rs->job_name.", Qty:- ".$rs->qty; ?></option>	
					<?php } ?>		
            	</select>
            </div>
            
            <div class="form-group col-md-12">
            	<label>Select Container:-</label>
            	<select name="container" class="form-control">
            		<option value="">Select</option>
            		<?php $data = get_current_container(); ?>
            		<?php foreach($data as $r) { ?>
						<option value="<?php echo $r->container_id; ?>"><?php echo $r->container_type; ?></option>	
					<?php } ?>		
            	</select>
            </div>
            
            <div class="form-group col-md-12">
            	<input type="submit" value="Replace" />
            </div>	
            
            </form>
            
            </div>
		</div>
	</div>
</div>		