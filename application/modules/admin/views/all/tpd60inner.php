<?php
$ci=&get_instance();
$ci->load->helper('common');
?>
<style>
	.info-box-content
	{ margin: 0px !important; }
	.info-box
	{ margin-bottom: 0px !important; }
	#country-list 
	{ float: left;	list-style: outside none none; margin: 0; padding: 0; }
	#country-list > li 
	{ cursor: pointer;	padding: 2px 0; }
	.ui-menu.ui-widget.ui-widget-content.ui-autocomplete.ui-front
    { z-index: 999999!important; }
    h5
    { margin-top:2px!important;	margin-bottom:2px!important; }
    h4 
    { margin-top:0px!important;	margin-bottom:5px!important; }
    h3
    { margin-top:5px!important; margin-bottom:5px!important; }
</style>
<div class="box-body no-padding">
	<table class="table table-striped">
    	<thead>
        	<tr>
            	<th>
              		<center><h3 style="font-weight: bold">Job Name</h3></center>	
              	</th>
              	<th>
              		<div class="col-md-3"><center><h3 style="font-weight: bold">ANSA</h3></center></div>
              		<div class="col-md-3"><center><h3 style="font-weight: bold">QC</h3></center></div>
              		<div class="col-md-3"><center><h3 style="font-weight: bold">QA</h3></center></div>
              		<div class="col-md-3"><center><h3 style="font-weight: bold">WH</h3></center></div>	
              	</th>
              	<th>
              		<center><h3 style="font-weight: bold">Status</h3></center>
              	</th>
           	</tr>
       	</thead>
        <tbody>
        <?php foreach($result as $r){ ?>
        	<?php if($r->container_name != 'ANSA') { ?>
            	<!-- if check the ANSA or Not  -->	
                <?php if($r->decoration == 1) { ?>  <!-- ANSA Yes -->	
                <tr class="countred removeclass">
                	<td class="col-md-3">
                  		<h4 class="bg-blue" style="width:100%"><?php echo $r->job_name; ?></h4> 
                  		<div class="col-md-12" style="padding: 0px;"><h5><b>TPD:</b> <?php $tpd = get_tpd_by_id($r->tpd); foreach($tpd as $t) { echo $t->name; } ?> | <b>QTY:</b> <?php echo $r->qty; ?> | <?php echo $r->container_type; ?> | <b><?php echo date ("j M,y",strtotime($r->container_date)); ?></b></h5></div>
                  		<div class="col-md-12" style="padding: 0px;"><h5 style="text-transform: uppercase;"><?php echo $r->customer_name; ?></h5></div>
                  	</td> 
                  	<td class="col-md-8">
                 		<?php $qcl = (($r->qc*100)/$r->qty); ?>	
                     	<?php $qal = (($r->qa_completed*100)/$r->qty); ?>
                     	<?php $whl = (($r->wh_completed*100)/$r->qty); ?>
                     	<?php $ansal = (($r->ansa*100)/$r->qty); ?>
                  		
                  		<div class="row"> 
                  		<!--  ANSA is less then 25%   -->
                  		<?php if($ansal < 25) { ?>	
                  			<div class="col-md-3">
                       			<div class="info-box bg-red" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                              			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qty-$r->ansa_completed; ?></span>
                              			<span> <?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																{ echo "-"; }
																else {
                                    							$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																}
                                    							?>
                                    	</span>   
                              			<div class="progress1">
	                              			<div class="progress">
	                                			<div class="progress-bar" style="width: <?php echo $ansal; ?>%"></div>
	                              			</div>
                                 		</div>
                            		</div>
                        		</div>
                     		</div>
                     		
                     		<div class="col-md-3">
                       			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                              			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->ansa_completed-$r->qc_completed; ?></span> 
                              			<span>
                                    		<?php  if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
                                    							?>
                                  		</span>                             
                						<div class="progress1">
                              				<div class="progress">
                                				<div class="progress-bar" style="width: <?php echo $qcl; ?>%"></div>
                              				</div>
                                 		</div>
                            		</div>
                        		</div>
                     		</div>
                     		<!-- QA Condition -->
							<?php if($r->qa > $r->qa_completed) { ?>  
							<!-- QA is Purple  -->
							<?php if($r->qa_rem !=0) { ?>
							<div class="col-md-3">
                       			<div class="info-box bg-purple" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                              			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                						<span>
                                    			<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?>
                                  		</span>
                						<div class="progress1">
                              				<div class="progress">
                                				<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              				</div>
                                 		</div>
                            		</div>
                        		</div>
                     		</div>
                     		<?php } elseif($r->qa_rem == '0') { ?>
							<!-- QA is Blue  -->
							<div class="col-md-3">
                       			<div class="info-box bg-blue" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                              			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              			<span>
                                    					 <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
															?>
                                  		</span>                             
                						<div class="progress1">
                              				<div class="progress">
                                				<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              				</div>
                                 		</div>
                            		</div>
                        		</div>
                     		</div>
                     		<?php } else { ?>		                   	
                     		<!-- QA is RED  -->
                     		<div class="col-md-3">
                       			<div class="info-box bg-red" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                              			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              			<span>
                                    					<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?>
                                  		</span>                             
                						<div class="progress1">
                              				<div class="progress">
                                				<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              				</div>
                                 		</div>
                            		</div>
                        		</div>
                     		</div>
                     		<?php } ?>		
               				<!-- QA is Green -->	   
                     		<?php } elseif($r->qa == $r->qa_completed && $r->qa_completed != 0) { ?>  
                     		<div class="col-md-3">
                       			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                            			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              			<span>
                                    			<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?>
                                  		</span>
                              			<div class="progress1">
                              				<div class="progress">
                                				<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              				</div>
                                 		</div>
                            		</div>
                        		</div>
                     		</div>
                     		<?php } elseif($r->qa_completed == 0) { ?>
                     		<div class="col-md-3">
                       			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                              			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              			<span>
                                    			<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
															?>
                                  		</span>                              
                						<div class="progress1">
                              				<div class="progress">
                                				<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              				</div>
                                 		</div>
                            		</div>
                        		</div>
                     		</div>
                     		<?php } ?>   
                     		<?php if($r->wh < $r->wh_completed) { ?>   
                    		<?php if($whl < 25) { ?>	
                        	<div class="col-md-3">
                        		<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                             			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             			<span>
                                    			<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?>
                                  		</span>                               
				                		<div class="progress1">
				                        	<div class="progress">
				                               	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                            </div>
                                  		</div>
                            		</div>
                            	</div>
                     		</div>  
                			<?php } elseif($whl == 100) { ?>	
                			<div class="col-md-3">
                        		<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                             			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             			<span>
                                    			<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?>
                                  		</span>                                   
				                		<div class="progress1">
				                           	<div class="progress">
				                               	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                           	</div>
                                  		</div>
                            		</div>
                            	</div>
                     		</div>
                     		<?php } else { ?>
                     		<div class="col-md-3">
                        		<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                             			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             			<span>
                                    			<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
                                    							?>
                                  		</span>                                   
				                		<div class="progress1">
				                           	<div class="progress">
				                              	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                           	</div>
                                  		</div>
                            		</div>
                            	</div>
                     		</div>
                     		<?php } ?>
                     		<?php } elseif($r->wh ==  $r->wh_completed) { ?>
                     		<?php if($whl == 0) { ?>	
                        	<div class="col-md-3">
                        		<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                             			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             			<span>
                                    			<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?>
                                  		</span>                                    
				                		<div class="progress1">
				                        	<div class="progress">
				                            	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                            </div>
                                  		</div>
                            		</div>
                            	</div>
                     		</div>  
                			<?php } else { ?>
                     		<div class="col-md-3">
                        		<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                             			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 		<span>
                                    			<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?>
                                  		</span>                                  
				                		<div class="progress1">
				                        	<div class="progress">
				                            	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                            </div>
                                  		</div>
                            		</div>
                            	</div>
                     		</div>
                     		<?php } ?>
                     		<?php } elseif($r->wh > $r->wh_completed) { ?>
        					<?php if($whl < 25) { ?>	
                        	<div class="col-md-3">
                        		<div class="info-box bg-red" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                             	 		<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 		<span>
                                    			<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?>
                                  		</span>                                   
				                		<div class="progress1">
				                           	<div class="progress">
				                               	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                           	</div>
                                  		</div>
                            		</div>
                            	</div>
                     		</div> 
                     		<?php } elseif($whl == 100) { ?>	
                			<div class="col-md-3">
                        		<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                             			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>   
                             			<span>
                                    			<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?>
                                  		</span>                                 
				                		<div class="progress1">
				                        	<div class="progress">
				                            	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                            </div>
                                  		</div>
                            		</div>
                            	</div>
                     		</div>
                     		<?php } else { ?>
                     		<div class="col-md-3">
                        		<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                             	 		<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span>
                                    			<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
                                    							?>
                                  			</span>                                  
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                            </div>
                                  		</div>
                            		</div>
                            	</div>
                     		</div> 
                     		<?php } ?>
        					<?php } ?>	
                			<!--  ANSA is 100% Completed  -->
                			<?php } elseif($ansal == 100) { ?>
	                		<div class="col-md-3">
                       			<div class="info-box bg-green" id="qc-mat-81" >
                            		<div class="info-box-content">
                              			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qty-$r->ansa_completed; ?></span> 
                              			<span>
                                    	<?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																{ echo "-"; }
																else {
                                    							$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																}
                                    				      ?>
                                  		</span>                             
                						<div class="progress1">
                              				<div class="progress">
                                				<div class="progress-bar" style="width: <?php echo $ansal; ?>%"></div>
                              				</div>
                                 		</div>
                            		</div>
                        		</div>
                     		</div>
                     		<!-- Qc is less then 25% -->
                     		<?php if($r->qc_completed < $r->ansa_completed) { ?>
                     		<!-- QC  -->
                     		<div class="col-md-3">
                       			<div class="info-box bg-red" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                              			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->ansa_completed-$r->qc_completed; ?></span> 
                              			<span>
                                    	<?php  if($r->qc_updated == '0000-00-00 00:00:00')
															{ echo "-"; }
															else {
                                    							$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
															}
                                    						?>
                                  		</span>                             
                						<div class="progress1">
                              				<div class="progress">
                                				<div class="progress-bar" style="width: <?php echo $qcl; ?>%"></div>
                              				</div>
                                 		 	<span class="progress-description">
                                    		
                                    						<?php echo $r->qc_comment; ?>
                                  			</span>
                                 	 	</div>
                            		</div>
                        		</div>
                     		</div>
                     		<!-- QA  -->
                     		<?php if($r->qa > $r->qa_completed) { ?>  
							<!-- QA is purple  -->
							<?php if($r->qa_rem !=0) { ?>
							<div class="col-md-3">
                       			<div class="info-box bg-purple" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                              			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              			<span>
                                    					<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?>
                                  		</span>                             
                						<div class="progress1">
                              				<div class="progress">
                                				<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              				</div>
                                 		</div>
                            		</div>
                        		</div>
                     		</div>
                     		<!-- QA is blue  -->
							<?php } elseif($r->qa_rem == '0') { ?>
							<div class="col-md-3">
                       			<div class="info-box bg-blue" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                              			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              			<span>
                                    				<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
															?>
                                  		</span>                              
                						<div class="progress1">
                              				<div class="progress">
                                				<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              				</div>
                                 		</div>
                            		</div>
                        		</div>
                     		</div>
                     		<!-- QA is RED  -->
							<?php } else { ?>
							<div class="col-md-3">
                       			<div class="info-box bg-red" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                              			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              			<span>
                                    		<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?>
                                  		</span>                              
                						<div class="progress1">
                              				<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              				</div>
                                 		 </div>
                            		</div>
                        		</div>
                     		</div>
                     		<?php } ?>		
               				<!-- QA is Green -->	   
                     		<?php } elseif($r->qa == $r->qa_completed) { ?>  
                     		<?php if($r->qa !=0 ) {?>
                     		<div class="col-md-3">
                       			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                              			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              			<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
															?>
										</span>                              
                						<div class="progress1">
                              				<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              				</div>
                                 		 </div>
                            		</div>
                        		</div>
                     		</div>
                     		<?php } else { ?> 
                     		<div class="col-md-3">
                       			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                              			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              			<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?>
										</span>                              
                						<div class="progress1">
                              				<div class="progress">
                                				<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              				</div>
                                 		 </div>
                            		</div>
                        		</div>
                     		</div>
                     		<?php } ?>
                     		<?php }  ?>
                     		<!--  QC is gether then 25% and less 100%  -->
                     		<?php } elseif($qcl > 25) { ?>
		                    <!--  QC is equal ANSA  -->
		                    <?php if($r->qc_completed == $r->ansa_completed) { ?>
                     		<div class="col-md-3">
                       			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                              			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->ansa_completed-$r->qc_completed; ?></span>   
                              		    <span>
                                    			<?php      if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?>
                                  		</span>                     
                						<div class="progress1">
                              				<div class="progress">
                                				<div class="progress-bar" style="width: <?php echo $qcl; ?>%"></div>
                              				</div>
                                 		 	<span class="progress-description">
                                    			<?php echo $r->qc_comment; ?>
											</span>
                                 	 	</div>
                            		</div>
                        		</div>
                     		</div>                     	
                     		<!-- QA Condition -->
                     		<?php if($r->qa > $r->qa_completed) { ?>  
							<!-- QA is purple  -->
							<?php if($r->qa_rem !=0) { ?>
							<div class="col-md-3">
                       			<div class="info-box bg-purple" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                            			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              			<span>
                                    					<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?>
                                  		</span>                             
                						<div class="progress1">
                              				<div class="progress">
                                				<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              				</div>
                                 		</div>
                            		</div>
                        		</div>
                     		</div>
                     		<!-- QA is blue  -->
							<?php } elseif($r->qa_rem == '0') { ?>
							<div class="col-md-3">
                       			<div class="info-box bg-blue" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                              			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              			<span>
                                    				<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
															?>
                                		</span>                              
                						<div class="progress1">
                              				<div class="progress">
                                				<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              				</div>
                                 		</div>
                            		</div>
                        		</div>
                     		</div>
                     		<!-- QA is RED  -->
							<?php } else { ?>		                   	
                     		<div class="col-md-3">
                       			<div class="info-box bg-red" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                            			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              			<span>
                                    				<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
															?>
                                  		</span>                              
                						<div class="progress1">
                              				<div class="progress">
                                				<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              				</div>
                                 		</div>
                            		</div>
                        		</div>
                     		</div>
                     		<?php } ?>		
               				<!-- QA is Green -->	   
                     		<?php } elseif($r->qa == $r->qa_completed) { ?>  
                     		<div class="col-md-3">
                       			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                              			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              			<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?>
										</span>                              
                						<div class="progress1">
                              				<div class="progress">
                                				<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              				</div>
                                 		</div>
                            		</div>
                        		</div>
                     		</div>
                     		<?php }  ?>
                     		<?php } else  { ?>		
                     		<!-- QC is Yellow  -->
                     		<div class="col-md-3">
                       			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                              			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->ansa_completed-$r->qc_completed; ?></span>                              
                						<span>
                                    				<?php      if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?>
                                  		</span>
                						<div class="progress1">
                              				<div class="progress">
                                				<div class="progress-bar" style="width: <?php echo $qcl; ?>%"></div>
                              				</div>
                                 			<span class="progress-description">
                                    			<?php echo $r->qc_comment; ?>
                                  			</span>
                                 	 	</div>
                            		</div>
                        		</div>
                     		</div>
                     		<!-- QA Condition -->
							<?php if($r->qa > $r->qa_completed) { ?>  
							<!-- QA is purple -->
							<?php if($r->qa_rem !=0) { ?>
							<div class="col-md-3">
                       			<div class="info-box bg-purple" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                              			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              			<span>
                                    				<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
															?>
                                  		</span>                              
                						<div class="progress1">
                              				<div class="progress">
                                				<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              				</div>
                                 		</div>
                            		</div>
                        		</div>
                     		</div>
                     		<!-- QA is blue  -->
							<?php } elseif($r->qa_rem == '0') { ?>
							<div class="col-md-3">
                       			<div class="info-box bg-blue" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                              			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
										<span>
                                    				 <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?>
                                  		</span>                            
                						<div class="progress1">
                              				<div class="progress">
                                				<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              				</div>
                                 		</div>
                            		</div>
                        		</div>
                     		</div>
                     		<!-- QA is RED  -->
							<?php } else { ?>		                   	
                     		<div class="col-md-3">
                       			<div class="info-box bg-red" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                              			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              			<span>
                                    				 <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?>
                                  		</span>                             
                						<div class="progress1">
                              				<div class="progress">
                                				<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              				</div>
                                 		</div>
                            		</div>
                        		</div>
                     		</div>
                     		<?php } ?>		
               				<!-- QA is Green -->	   
                     		<?php } elseif($r->qa == $r->qa_completed) { ?>  
                     		<div class="col-md-3">
                       			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                              			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>  
                              			<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?></span>                        
                						<div class="progress1">
                              				<div class="progress">
                                				<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              				</div>
                                 		</div>
                            		</div>
                        		</div>
                     		</div>
                     		<?php }  ?>
                     	    <?php } ?>
							<?php } ?> 
                    		<?php if($r->wh < $r->wh_completed) { ?>   
                    		<?php if($whl < 25) { ?>	
                        	<div class="col-md-3">
                        		<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                             	 		<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 		<span>
                                    			 <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?>
                                  				</span>                             
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($whl == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span>
                                    			<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
                                    							?>
                                  				</span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span>
                                    			<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
                                    							?>
                                  				</span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh ==  $r->wh_completed) { ?>
                     				
                     			<?php if($whl == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span>
                                    				<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?>
                                  				</span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span>
                                    				<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?>
                                  				</span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			
        						<?php } elseif($r->wh > $r->wh_completed) { ?>
        							
        						<?php if($whl < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span>
                                    			<?php      if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
															?>
                                  				</span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			<?php } elseif($whl == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span>
                                    			<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?>
                                  				</span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span>
                                    				<?php      if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?>
                                  				</span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			<?php } ?>
        							
        						<?php } ?>  
                		 		
                		 		
                	<!-- ANSA is 25 > and < 100 -->
                	<?php } else { ?>
						
						<!-- ANSA is Yellow  -->
						<div class="col-md-3">
                       		<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            	<div class="info-box-content">
                              		<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qty-$r->ansa_completed; ?></span>
                              		<span>
                                    <?php  if($r->ansa_updated == '0000-00-00 00:00:00')
											{ echo "-"; }
											else {
                                    			$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
											}
                                    		?>
                                  	</span>                              
                					<div class="progress1">
                              		<div class="progress">
                                		<div class="progress-bar" style="width: <?php echo $ansal; ?>%"></div>
                              		</div>
                                 	
                                 	</div>
                            	</div>
                        	</div>
                     	</div>
                     	<!-- QC is less then 25%  -->		
                     	<?php if($r->qc_completed < $r->ansa_completed) { ?>
                     		<!-- QC is RED  -->
                     		<div class="col-md-3">
                       				<div class="info-box bg-red" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->ansa_completed-$r->qc_completed; ?></span>
                              				<span><?php  if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qcl; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					
                                    							<?php echo $r->qc_comment; ?>
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			<!-- QA is Black  -->
                     		<div class="col-md-3">
                        		<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                              			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              			 <span>
				                        	<?php if($r->qa_updated == '0000-00-00 00:00:00')
															{ echo "-"; }
															else {
                                    						$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
															}
													?>
				                       	</span>                              
				                		<div class="progress1">
				                        <div class="progress">
				                        	<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
				                        </div>
				                       
                                  		</div>
                            		</div>
                            	</div>
                     		</div>
                     		
                     		<!-- QC is more then 25%  -->		
                     		<?php } elseif($qcl > 25) { ?>
                     			
                     			<!-- QC is equal to ansa  -->
                     			<?php if($r->qc_completed == $r->ansa_completed) { ?>
                     						
                     				<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->ansa_completed-$r->qc_completed; ?></span>
                              				<span><?php      if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?>   </span>                           
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qcl; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    				
															<?php echo $r->qc_comment; ?>
															
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>                     	
                     	
                     			<!-- QA Condition -->
								<?php if($r->qa > $r->qa_completed) { ?>  
									<!-- QA is RED  -->
									<?php if($r->qa_rem !=0) { ?>
										
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal" id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              					<span>
                                    				<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?>
                                  				</span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	<!-- QA is blue  -->
										<?php } elseif($r->qa_rem == '0') { ?>
										
										<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal" id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              					<span>
                                    				<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?>
                                  				</span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	
										<!-- QA is red  -->
										<?php } else { ?>		                   	
                     					<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal" id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              					<span>
                                    				 <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
															?>
                                  				</span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	
               					<?php } ?>		
               						
               					<!-- QA is Green -->	   
                     			 <?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     				<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              						</div>
                                 		 			
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			
                     			<?php } ?>
                     						
                     						
                     			<?php } else  { ?>		
                     				<!-- QC is Yellow  -->
                     				<div class="col-md-3">
                       				<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->ansa_completed-$r->qc_completed; ?></span>
                              				<span>
                                    			<?php if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																	else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																	}
															?>
                                  					</span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qcl; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    						<?php echo $r->qc_comment; ?>
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			                     			<!-- QA Condition -->
                     			
								<?php if($r->qa > $r->qa_completed) { ?>  
									<!-- QA is purple  -->
									<?php if($r->qa_rem !=0) { ?>
										
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal" id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              					<span>
                                    				 <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?>
                                  				</span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	
									
									<?php } elseif($r->qa_rem == '0') { ?>
										<!-- QA is blue  -->
									<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal" id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              					<span>
                                    					<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
															?>
                                  				</span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	
									<?php } else { ?>	
									<!-- QA is RED  -->		                   	
                     				<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal" id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              					<span>
                                    				<?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?>
                                  				</span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	
               						<?php } ?>		
               						
               					<!-- QA is Green -->	   
                     			 <?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     				<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>    
                              				<span> <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?></span>                          
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              						</div>
                                 		 			
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			
                     			<?php }  ?>
                     				
                     				
                     			<?php } ?>
                     			
                     			
                     			<?php } ?>			
                        		
                        		<?php if($r->wh < $r->wh_completed) { ?>   
                    			    		
                        		<?php if($whl < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($whl == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>    
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                           
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh ==  $r->wh_completed) { ?>
                     				
                     			<?php if($whl == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh > $r->wh_completed) { ?>
        							
        						<?php if($whl < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span> <?php      if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
															?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                				                				
                				<?php } elseif($whl == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span><?php      if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<?php } ?>
        							
        						<?php } ?>
                     			
					<?php } ?>
						
                	</div>
               </td>
               		<td class="col-md-1">
                  		<?php $totalp = (($qcl+$qal+$whl+$ansal)/4) ?>
                  		<span class="badge bg-red"><?php echo round($totalp); ?>%</span><?php if($totalp == 100) { echo "Ready";  } else { echo "Not Ready";  } ?>
                  	</td>
       		</tr>
           	<?php } else { ?>  <!-- ANSA Not -->
            <tr class="countred removeclass">
            <td class="col-md-3">
                <!--<h4 class="bg-green" style="width:100%; text-align:center;"><?php echo $r->container_type; ?></h4>-->
                <h4 class="bg-blue" style="width:100%"><?php echo $r->job_name; ?></h4> 
                <div class="col-md-12" style="padding: 0px;"><h5><b>TPD:</b> <?php $tpd = get_tpd_by_id($r->tpd); foreach($tpd as $t) { echo $t->name; } ?> | <b>QTY:</b> <?php echo $r->qty; ?> | <?php echo $r->container_type; ?> | <b><?php echo date ("j M,y",strtotime($r->container_date)); ?></b></h5></div>
                <div class="col-md-12" style="padding: 0px;"><h5 style="text-transform: uppercase;"><?php echo $r->customer_name; ?></h5></div>
           	</td> 
            <td class="col-md-8">
            	<?php $qul = (($r->qc*100)/$r->qty) ?>	
                <?php $qap = (($r->qa_completed*100)/$r->qty) ?>
                <?php $whp = (($r->wh_completed*100)/$r->qty) ?>
                
                
                <div class="row">
                  				<!-- Start Row -->
                  				<div class="col-md-3"> 
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
	                            		<style>
	                						.info-box-content{
	                							margin-left: 0;
	                							padding: 5px 5px;
	                						}
	                					</style>
	                            		<div class="info-box-content">                       
		                					<span class="progress-description">
		                                  	</span>
		                              		<div class="progress">
		                                		<div class="progress-bar" style="width: 0%"></div>
		                              		</div>
		                                  	<span class="info-box-text">Not Applicable</span> 
	                            		</div>
                        			</div>
                     			</div><!-- End First Col-md-3 -->
                     				
                     		
                     		<!-- if qc is < 25%  -->
                     		
                     			<?php if($qul < 25) { ?>
                    			<!-- QC is RED  -->
                    			<div class="col-md-3">
                       				<div class="info-box bg-red" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qty-$r->qc_completed; ?></span>
                              				<span><?php  if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qul; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					
                                    							<?php echo $r->qc_comment; ?>
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			<!-- QA is Black  -->
                     			<!-- QA Condition -->
								<?php if($r->qa > $r->qa_completed) { ?>  
									<!-- QA is Purple  -->
									<?php if($r->qa_rem !=0) { ?>
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				
									
									<?php } elseif($r->qa_rem == '0') { ?>
										<!-- QA is Blue  -->
									<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				
										
									<?php } else { ?>		                   	
                     				<!-- QA is RED  -->
                     				<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				
               						<?php } ?>		
               						
               					<!-- QA is Green -->	   
                     			 <?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     				<div class="col-md-3">
                       				<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			<?php } ?>
                     			                     			
                     			<?php if($r->wh < $r->wh_completed) { ?>   
                    			    		
                        		<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh ==  $r->wh_completed) { ?>
                     				
                     			<?php if($whp == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                             
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                				
                				<?php } elseif($r->wh > $r->wh_completed) { ?>
        							
        						<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                				                				
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<?php } ?>
        							
        						<?php } ?>
                     			
                     			<!-- if qc is Completed  -->
                     		
                     			<?php } elseif($qul == 100) { ?>
                     				<!-- QC is Green  -->
                     				<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qty-$r->qc_completed; ?></span>  
                              				<span><?php      if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
															?></span>                            
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qul; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					
															<?php echo $r->qc_comment; ?>
															
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>                     	
                     	
                     			<!-- QA Condition -->
								<?php if($r->qa > $r->qa_completed) { ?>  
									<!-- QA is Purple  -->
									<?php if($r->qa_rem !=0) { ?>
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				<?php } elseif($r->qa_rem == '0') { ?>
										<!-- QA is Blue  -->
									<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				
										
									<?php } else { ?>		                   	
                     				<!-- QA is RED  -->
                     				<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				
               						<?php } ?>		
               						
               					<!-- QA is Green -->	   
                     			 <?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     				<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			
                     			<?php }  ?>
                     			   
                        		<?php if($r->wh < $r->wh_completed) { ?>   
                    			    		
                        		<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span>
                                    				<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?>
                                  				</span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span>
                                    				<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
                                    							?>
                                  				</span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span>
                                    				<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?>
                                  				</span>                                
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh ==  $r->wh_completed) { ?>
                     				
                     			<?php if($whp == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span>
                                    				<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?>
                                  				</span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span>
                                    				<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?>
                                  				</span>                                
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh > $r->wh_completed) { ?>
        							
        						<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span>
                                    				<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?>
                                  				</span>                                
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                				                				
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span>
                                    				<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
                                    							?>
                                  				</span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span>
                                    				<?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
                                    							?>
                                  				</span>                                
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<?php } ?>
        							
        						<?php } ?>
                     		
                     		<!-- if qc is Middel  -->
                     		
                     		<?php } else { ?>		
                     			<!-- QC is yellow  -->		
        						<div class="col-md-3">
                       				<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qty-$r->qc_completed; ?></span>
                              				<span>
                                    				<?php      if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?>
                                  				</span>                               
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qul; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					
															<?php echo $r->qc_comment; ?>
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			
                     			<!-- QA Condition -->
								<?php if($r->qa > $r->qa_completed) { ?>  
									<!-- QA is Purple  -->
									<?php if($r->qa_rem !=0) { ?>
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				
									
									<?php } elseif($r->qa_rem == '0') { ?>
										<!-- QA is Blue  -->
									<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				
										
									<?php } else { ?>		                   	
                     				<!-- QA is RED  -->
                     				<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				
               						<?php } ?>		
               						
               					<!-- QA is Green -->	   
                     			 <?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     				<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			
                     			<?php }  ?>
                     			
                     			<?php if($r->wh < $r->wh_completed) { ?>   
                    			    		
                        		<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                             
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh ==  $r->wh_completed) { ?>
                     			
                     			<?php if($whp == 0) { ?>
                     				
                     			<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } else { ?>
                     				
                     			<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>	
                     			<?php } ?>	
                     			
                     			<?php } elseif($r->wh > $r->wh_completed) { ?>
        							
        						<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			<?php } elseif($whp == 100) { ?>	
                				
                				<?php if($whp == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>   
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
                                    							?></span>                            
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<?php } ?>
        							
        						<?php } ?>	 	
        					
        					<?php } ?>	
        					
        					<!-- End Row -->	
                    		</div>
                  		</td>
                  		<td class="col-md-1">
                  			<?php $totalp = (($qul+$qap+$whp)/3) ?>	
                  				<span class="badge bg-red"><?php echo round($totalp); ?>%</span><?php if($totalp == 100) { echo "Ready";  } else { echo "Not Ready";  } ?> 
                  		</td>
                	</tr>
                	<?php } ?>
                	
                	
                	<!-- ANSA Container is ready -->
                	 
                	<?php } else { ?>
                	<tr class="countred removeclass">
                  		<td class="col-md-3">
                  			<h4 class="bg-blue" style="width:100%"><?php echo $r->job_name; ?></h4> 
                  			<div class="col-md-12" style="padding: 0px;"><h5><b>TPD:</b> <?php $tpd = get_tpd_by_id($r->tpd); foreach($tpd as $t) { echo $t->name; } ?> | <b>QTY:</b> <?php echo $r->qty; ?> | <?php echo $r->container_type; ?> | <b><?php echo date ("j M,y",strtotime($r->container_date)); ?></b></h5></div>
                  			<div class="col-md-12" style="padding: 0px;"><h5 style="text-transform: uppercase;"><?php echo $r->customer_name; ?></h5></div>
                  		</td> 
                  
                 	 	<td class="col-md-8"> 
                 	 		<?php $qul = (($r->qc*100)/$r->qty) ?>	
                     		<?php $qap = (($r->qa_completed*100)/$r->qty) ?>
                     		<?php $whp = (($r->wh_completed*100)/$r->qty) ?>
                     		<?php $ansap = (($r->ansa_completed*100)/$r->qty) ?>
                  			
                  			
                  			<div class="row">
                  				<!-- Start Row -->
                  				
                  				
                  				<?php if($r->ansa < $r->ansa_completed) { ?>   
                    			    		
                        		<?php if($ansap < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->wh_completed-$r->ansa_completed; ?></span>  
                             	 			<span><?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $ansap; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($ansap == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->wh_completed-$r->ansa_completed; ?></span>  
                             	 			<span><?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                             
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $ansap; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->wh_completed-$r->ansa_completed; ?></span>  
                             	 			<span><?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                             
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $ansap; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->ansa ==  $r->ansa_completed) { ?>
                     			
                     			<?php if($ansap == 0) { ?>
                     				
                     			<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->wh_completed-$r->ansa_completed; ?></span> 
                             	 			<span><?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>
                             	 			                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $ansap; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } else { ?>
                     				
                     			<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->wh_completed-$r->ansa_completed; ?></span> 
                             	 			<span><?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $ansap; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>	
                     			<?php } ?>	
                     			
                     			<?php } elseif($r->ansa > $r->ansa_completed) { ?>
        							
        						<?php if($ansap < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->wh_completed-$r->ansa_completed; ?></span>
                             	 			<span><?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $ansap; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			<?php } elseif($ansap == 100) { ?>	
                				
                				<?php if($ansap == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->wh_completed-$r->ansa_completed; ?></span>  
                             	 			<span><?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
                                    							?></span>                             
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $ansap; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->wh_completed-$r->ansa_completed; ?></span> 
                             	 			<span><?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $ansap; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->wh_completed-$r->ansa_completed; ?></span>
                             	 			<span><?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $ansap; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<?php } ?>
        							
        						<?php } ?>
                  				
                  				
                  				<!-- End First Col-md-3 -->
                     				
                     		
                     		<!-- if qc is < 25%  -->
                     		
                     			<?php if($qul < 25) { ?>
                    			<!-- QC is RED  -->
                    			<div class="col-md-3">
                       				<div class="info-box bg-red" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qty-$r->qc_completed; ?></span> 
                              				<span><?php  if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
                                    							?></span>                             
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qul; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					
                                    							<?php echo $r->qc_comment; ?>
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     				
                     			<!-- QA is Black  -->
                     			<!-- QA Condition -->
								<?php if($r->qa > $r->qa_completed) { ?>  
									<!-- QA is Purple  -->
									<?php if($r->qa_rem !=0) { ?>
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>
                              				<span><?php  if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                               
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				<?php } elseif($r->qa_rem == '0') { ?>
										<!-- QA is Blue  -->
									<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>  
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
															?></span>                             
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				<?php } else { ?>		                   	
                     				<!-- QA is RED  -->
                     				<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				
               						<?php } ?>		
               						
               					<!-- QA is Green -->	   
                     			 <?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     				<div class="col-md-3">
                       				<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              				<span><?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			<?php } ?>
                     			                     			
                     			<?php if($r->wh < $r->wh_completed) { ?>   
                    			    		
                        		<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh ==  $r->wh_completed) { ?>
                     				
                     			<?php if($whp == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>    
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                            
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                				
                				<?php } elseif($r->wh > $r->wh_completed) { ?>
        							
        						<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                				                				
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal">
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span><?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<?php } ?>
        							
        						<?php } ?>
                     			
                     			<!-- if qc is Completed  -->
                     		
                     			<?php } elseif($qul == 100) { ?>
                     				<!-- QC is Green  -->
                     				<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qty-$r->qc_completed; ?></span> 
                              				<span><?php      if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qul; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					
															<?php echo $r->qc_comment; ?>
															
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>                     	
                     	
                     			<!-- QA Condition -->
								<?php if($r->qa > $r->qa_completed) { ?>  
									<!-- QA is Purple  -->
									<?php if($r->qa_rem !=0) { ?>
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              				<span> <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
															?></span>                               
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				
									
									<?php } elseif($r->qa_rem == '0') { ?>
										<!-- QA is Blue  -->
									<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              				<span> <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				<?php } else { ?>		                   	
                     				<!-- QA is RED  -->
                     				<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              				<span> <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				
               						<?php } ?>		
               						
               					<!-- QA is Green -->	   
                     			 <?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     				<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              				<span> <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			
                     			<?php }  ?>
                     			   
                        		<?php if($r->wh < $r->wh_completed) { ?>   
                    			    		
                        		<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
                                    							?></span>                                
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh ==  $r->wh_completed) { ?>
                     				
                     			<?php if($whp == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                                
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh > $r->wh_completed) { ?>
        							
        						<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>   
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                				                				
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                                
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<?php } ?>
        							
        						<?php } ?>
                     		
                     		<!-- if qc is Middel  -->
                     		
                     		<?php } else { ?>		
                     			<!-- QC is yellow  -->		
        						<div class="col-md-3">
                       				<div class="info-box bg-yellow" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qty-$r->qc_completed; ?></span> 
                              				<span> <?php      if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?></span>                               
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qul; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    				
															<?php echo $r->qc_comment; ?>
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			
                     	
                     	
                     			<!-- QA Condition -->
								<?php if($r->qa > $r->qa_completed) { ?>  
									<!-- QA is Purple  -->
									<?php if($r->qa_rem !=0) { ?>
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>  
                              				<span> <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				<?php } elseif($r->qa_rem == '0') { ?>
										<!-- QA is Blue  -->
									<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              				<span> <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				
										
									<?php } else { ?>		                   	
                     				<!-- QA is RED  -->
                     				<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span> 
                              				<span> <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
															?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				
               						<?php } ?>		
               						
               					<!-- QA is Green -->	   
                     			 <?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     				<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qc_completed-$r->qa_completed; ?></span>   
                              				<span> <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
															?></span>                            
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			
                     			<?php }  ?>
                     			
                     			<?php if($r->wh < $r->wh_completed) { ?>   
                    			    		
                        		<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                                 
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                                 
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh ==  $r->wh_completed) { ?>
                     			
                     			<?php if($whp == 0) { ?>
                     				
                     			<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                                
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } else { ?>
                     				
                     			<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>  
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
                                    							?></span>                                
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>	
                     			<?php } ?>	
                     			
                     			<?php } elseif($r->wh > $r->wh_completed) { ?>
        							
        						<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
                                    							?></span>                                 
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			<?php } elseif($whp == 100) { ?>	
                				
                				<?php if($whp == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>    
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                              
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span>   
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago";  
																		}
                                    							?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text col-md-6" style="padding: 0px;"><?php echo $r->qa_completed-$r->wh_completed; ?></span> 
                             	 			<span> <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$t1 = strtotime($today)*1000;
																$t2 = strtotime($adate)*1000;
																$tt = $t1-$t2;
																$elapsed = (($tt / 1000)/60)/60;
                        										$a = $elapsed;
                        										$f = sprintf ("%.2f", $a);
                        										$nn=$f;
                        										$nnn=sprintf('%02d:%02d', (int) $nn, fmod($nn, 1) * 60);
																echo $nnn." Hrs Ago"; 
																		}
                                    							?></span>                                 
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<?php } ?>
        							
        						<?php } ?>	 	
        					 
        					<?php } ?>	
        					
        					<!-- End Row -->	
                    		</div>
                  		</td>
                  		<td class="col-md-1">
                  			<?php $totalp = (($qul+$qap+$whp+$ansap)/4) ?>	
                  				<span class="badge bg-red"><?php echo round($totalp); ?>%</span><?php if($totalp == 100) { echo "Ready";  } else { echo "Not Ready";  } ?> 
                  		</td>
                	</tr>
                	<?php } ?>	 
                 	<?php } ?>
                </tbody>
           	</table>
        </div>