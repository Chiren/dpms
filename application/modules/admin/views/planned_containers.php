<?php
$ci=&get_instance();
$ci->load->helper('home');
?>
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable({
    	"ordering": false
    });
} ); 
</script>
<div class="col-md-12 text-center">
	<img src="<?php echo base_url();?>assets/ajax-loader.gif" id="loading" style="display: none;" />
</div>
<div id="showhidediv">
				<table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
         			<thead>
			            <tr>    
			                <th>Container No</th>
			                <th>Date Planned On</th>
			                <th style="text-align: center">Status</th>
			           	</tr>
        			</thead>
			        <tfoot>
			            <tr>
			                <th>Container No</th>
			                <th>Date Planned On</th>
			                <th style="text-align: center">Status</th>
			            </tr>
			        </tfoot>
        			<tbody>

					<?php $containers = get_containers(); ?>	
            		<?php foreach($containers as $con) { ?>
            			<tr>
			                <td><?php echo $con->container_type; ?></td>
			                <td><?php echo date ("j M,y",strtotime($con->container_date)); ?></td>
			                <td style="text-align: center">
			                	<?php $today = date("Y-m-d"); ?>
			                	<?php $c_date = date ("Y-m-d",strtotime($con->container_date)); ?>
			                		<?php if($con->con_status == 0) { ?>
			                	    	<a class="label label-warning" onclick="book(<?php echo $con->container_id; ?>);" >Booked</a>
			                	    	<a onclick="canceled(<?php echo $con->container_id; ?>);" class="label label-info">cancel</a>
			                		<?php } elseif ($con->con_status == 1) { ?> 
			                			
			                			<?php if($con->chk_date == '0000-00-00') { ?>
			                				<a onclick="reported(<?php echo $con->container_id; ?>);" class="label label-success">Reported</a>
			                				<a onclick="canceled(<?php echo $con->container_id; ?>);" class="label label-info">cancel</a>
			                				<a onclick="completed(<?php echo $con->container_id; ?>);" class="label label-info">Completed</a>
			                				
			                			<?php } else { ?>	
			                			<?php if($con->chk_date >= $today) {  ?>	
			                				<a onclick="reported(<?php echo $con->container_id; ?>);" class="label label-success">Reported</a>
			                				<a onclick="canceled(<?php echo $con->container_id; ?>);" class="label label-info">cancel</a>
			                				<a onclick="completed(<?php echo $con->container_id; ?>);" class="label label-info">Completed</a>
										<?php } elseif($con->chk_date <= $today) { ?>
											<?php 	$date1 = new DateTime($today);
													$date2 = new DateTime($con->chk_date);
													$diff = $date1->diff($date2);
											?> 
											<input type="hidden" id="<?php echo $con->container_id; ?>" value="<?php echo $con->container_id; ?>" />
											<a onclick="detain(<?php echo $con->container_id; ?>);" class="label label-danger">Detaining</a> <small>Since <?php printf('%d', $diff->d); ?> Days</small>
			                				<a onclick="canceled(<?php echo $con->container_id; ?>);" class="label label-info">cancel</a>
			                				<a onclick="completed(<?php echo $con->container_id; ?>);" class="label label-info">Completed</a>
			                				<script>
			                					$(document).ready(function() {
			                						var value = $('#<?php echo $con->container_id; ?>').val();
			                						
    												$.ajax({
    													
															'url':'<?php echo base_url();?>admin/home/detaining/<?php echo $con->container_id; ?>',
															'success':function(message)
															{	
																
						
															}
														});	
												});
			                				</script>
			                				
										<?php } ?>	
										<?php } ?>
									<?php } elseif ($con->con_status == 2) { ?>
									<?php 	$date1 = new DateTime($today);
											$date2 = new DateTime($con->chk_date);
											$diff = $date1->diff($date2);
									?>
			                			<a onclick="detain(<?php echo $con->container_id; ?>);" class="label label-danger">Detaining</a> <small>Since <?php printf('%d', $diff->d); ?> Days</small>
			                			<a onclick="canceled(<?php echo $con->container_id; ?>);" class="label label-info">cancel</a>
			                			<a onclick="completed(<?php echo $con->container_id; ?>);" class="label label-info">Completed</a>
			                		<?php } elseif ($con->con_status == 3) { ?>
			                			<span class="label label-success">Canceled</span>	
			                		<?php } elseif ($con->con_status == 4) { ?>
			                			<span class="label label-success">Completed</span>
			                		<?php }?>	 	
			                </td>
			            </tr>
			        <?php } ?>
			      </tbody>
				</table>  
				
				</div>


<script>
	function book(id){
		$("#loading").show();
		$("#showhidediv").hide();
		$.ajax({
    		'url':'<?php echo base_url(); ?>admin/home/booked/'+id,
			'success':function(message)
			{
				$("#refresh").load("home/reloaded" );
				$("#loading").hide();
				$("#showhidediv").show();
			}
			
		});	
	}
	
	function canceled(id)
	{
		$("#loading").show();
		$("#showhidediv").hide();
		$.ajax({
    		'url':'<?php echo base_url(); ?>admin/home/canceled/'+id,
			'success':function(message)
			{
				$("#refresh").load("home/reloaded" );	
				$("#loading").hide();
				$("#showhidediv").show();
			}
		});
	}
	
	function completed(id)
	{
		$("#loading").show();
		$("#showhidediv").hide();
		$.ajax({
    		'url':'<?php echo base_url(); ?>admin/home/completed/'+id,
			'success':function(message)
			{
				$("#refresh").load("home/reloaded" );
				$("#loading").hide();
				$("#showhidediv").show();	
			}
		});
	}
	
	function reported(id)
	{
		$("#loading").show();
		$("#showhidediv").hide();
		$.ajax({
    		'url':'<?php echo base_url(); ?>admin/home/reported/'+id,
			'success':function(message)
			{
				$("#refresh").load("home/reloaded" );
				$("#loading").hide();
				$("#showhidediv").show();	
			}
		});
	}
	
	function detain(id)
	{
		$("#loading").show();
		$("#showhidediv").hide();
		$.ajax({
    		'url':'<?php echo base_url(); ?>admin/home/detain/'+id,
			'success':function(message)
			{
				$("#refresh").load("home/reloaded" );
				$("#loading").hide();
				$("#showhidediv").show();	
			}
		});
	}
</script>