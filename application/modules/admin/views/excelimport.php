<?php $this->layout('layouts::default') ?>
<div class="box" id="auto_load_div">
	<div class="box-body">
	<h3>Import Container</h3>
	<hr />
		<form action="<?php echo base_url(); ?>admin/uploadcsv/import" method="post" enctype="multipart/form-data">
			<div class="form-group">
				<label>File :-</label>
				<input type="file" name="file" id="file">
			</div>
			<button type="submit" id="submit" name="import" class="btn btn-primary button-loading">Import</button>
		</form>
	</div>
</div>			
