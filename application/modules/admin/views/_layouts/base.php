<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">	
	<base href="<?php echo $base_url; ?>" />
	<title><?=$this->e($title)?></title>
	<?=$this->section('meta')?>
	<script src='<?php echo dist_url('adminlte.min.js'); ?>'></script>
	<script src='<?php echo dist_url('admin.min.js'); ?>'></script>
	<?=$this->section('scripts_head')?>
	<?=$this->section('scripts_crud')?>

	<link href='<?php echo dist_url('adminlte.min.css'); ?>' rel='stylesheet'>
	<link href='<?php echo dist_url('admin.min.css'); ?>' rel='stylesheet'>
	<?=$this->section('styles')?>
	<style>
			.input-xs {
			  height: 25px;
			  padding: 2px 5px;
			  font-size: 15px;
			  line-height: 1.5; /* If Placeholder of the input is moved up, rem/modify this. */
			}
			.sidebar-mini.sidebar-collapse .main-sidebar
			{
				position: fixed;
			}
			
	</style>
</head> 
<body class="<?php echo $body_class; ?> sidebar-collapse sidebar-mini">
	<?php // Main content (from inner view, or nested layout) ?>
	<?=$this->section('content')?>

	<?php // Scripts at page end ?>
	<?=$this->section('scripts_foot')?>
	
</body>
</html>
