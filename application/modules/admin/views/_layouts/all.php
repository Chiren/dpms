<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">	
	<base href="<?php echo $base_url; ?>" />
	
	<?php // Meta data ?>
	<title><?=$this->e($title)?></title>
	<?=$this->section('meta')?>

	<?php // Scripts at page start ?>
	<script src='<?php echo dist_url('adminlte.min.js'); ?>'></script>
	<script src='<?php echo dist_url('admin.min.js'); ?>'></script>
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<?=$this->section('scripts_head')?>

	<?php // Scripts and Stylesheets for Grocery CRUD ?>
	<?=$this->section('scripts_crud')?>

	<?php // Stylesheets ?>
	<link href='<?php echo dist_url('adminlte.min.css'); ?>' rel='stylesheet'>
	<link href='<?php echo dist_url('admin.min.css'); ?>' rel='stylesheet'>
	<?=$this->section('styles')?>
	<style>
			.input-xs {
			  height: 25px;
			  padding: 2px 5px;
			  font-size: 15px;
			  line-height: 1.5; /* If Placeholder of the input is moved up, rem/modify this. */
			}
	</style>
</head> 
<body class="sidebar-collapse sidebar-mini skin-purple">
<header class="main-header">
	<nav class="navbar navbar-static-top" role="navigation" style="margin-left: 0px;">
		<h4 style="color:#fff !important;float:left !important;font-family:fontAwesome !important;margin-top: 15px !important; margin-left: 10px;">Dispatch Planning and Monitoring System (DPMS)</h4>
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<li class="dropdown user user-menu">
					<img src="<?php echo base_url(); ?>uploads/logo/logo.gif" />
				</li>	
			</ul>
		</div>
	</nav>
</header>	
	
	<?php // Main content (from inner view, or nested layout) ?>
	<?=$this->section('content')?>

	<?php // Scripts at page end ?>
	<?=$this->section('scripts_foot')?>
	
</body>
</html>