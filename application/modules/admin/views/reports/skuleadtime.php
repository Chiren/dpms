<?php $this->layout('layouts::default') ?>
<?php
$ci=&get_instance();
$ci->load->helper('reports');
?>
<link  href="<?php echo base_url(); ?>assets/pci/datatable/dataTables.bootstrap.min.css" rel="stylesheet"></style>
<link  href="<?php echo base_url(); ?>assets/pci/datatable/buttons.bootstrap.min.css" rel="stylesheet"></style>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/jquery.dataTables.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/jszip.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/pdfmake.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/vfs_fonts.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/buttons.html5.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/buttons.print.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/buttons.colVis.min.js"></script>
<div class="row">
	<div class="col-lg-12">
		<div class="box box-primary"> 
			<div class="box-body">
				<h3>SKU Lead Time</h3>
					<div class="col-lg-5 form-group">
					<label>Dept:</label>	
					<select class="form-control" id="dept">
						<option value="">Select</option>
						<option value="1">QC</option>
						<option value="2">QA</option>
						<option value="3">WH</option>
						<option value="4">ANSA</option>
					</select>	
					</div>
					<div class="col-lg-5 form-group">
						<label>Select Month and Year:</label>	
						<input type="text" id="datepicker" class="form-control" />
					</div>
					
					<div class="col-lg-2 form-group">
						<label>&nbsp; </label><br>	
						<button class="btn btn-success" id="ok">Ok</button>
					</div>
					
					<div id="show">
				
					</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		 
		$("#ok").click(function(){
			var dep=$("#dept").val();
			var time=$("#datepicker").val();
			var data= {dept:dep,my:time} 
			$.ajax({
					'url':'<?php echo base_url();?>admin/reports/skudisplay',
					'type':'POST',
					'data':data,
					'success':function(message){
					   $("#show").html(message); 
					    
					}
				});			
		});
		
        	
	});
</script>					

<link rel="stylesheet" href="../assets/plugins/datepicker/datepicker3.css" />
<script src="../assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script>
$(function () {
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm',
      minViewMode: 'months'
    });
    $(".timepicker").timepicker({
      showInputs: false,
     
    });
  });
</script>

		