<?php $this->layout('layouts::default') ?>

<link  href="<?php echo base_url(); ?>assets/pci/datatable/dataTables.bootstrap.min.css" rel="stylesheet"></style>
<link  href="<?php echo base_url(); ?>assets/pci/datatable/buttons.bootstrap.min.css" rel="stylesheet"></style>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/jquery.dataTables.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/jszip.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/pdfmake.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/vfs_fonts.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/buttons.html5.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/buttons.print.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/buttons.colVis.min.js"></script> 
<script src="<?php echo base_url(); ?>assets/canvas.js"></script>
		<script src="<?php echo base_url(); ?>assets/pci/pie.js"></script>
		<script src="<?php echo base_url(); ?>assets/pci/light.js"></script>

<div class="row">
	<div class="col-lg-12">
		<div class="box box-primary">
			<div class="box-body">
				<h3> QA Performance</h3>
					<div class="col-md-12">
					<div class="col-lg-5 form-group">
						<label>Select Month and Year:</label>	
						<input type="text" id="datepicker" class="form-control" />
					</div>
					
					<div class="col-lg-2 form-group">
						<label>&nbsp; </label><br>	
						<button class="btn btn-success" id="ok">Ok</button>
					</div>
					</div>
					<div id="show">
				
					</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		 
		$("#ok").click(function(){
			var time=$("#datepicker").val();
			var data= {my:time} 
			$.ajax({
					'url':'<?php echo base_url();?>admin/reports/qaperset',
					'type':'POST',
					'data':data,
					'success':function(message){
					   $("#show").html(message); 
					}
				});			
		});	
	});
</script>					

<link rel="stylesheet" href="../assets/plugins/datepicker/datepicker3.css" />
<script src="../assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script>
$(function () {
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm',
      minViewMode: 'months'
    });
  });
</script>

		