<?php $this->layout('layouts::default') ?>
<?php
$ci=&get_instance();
$ci->load->helper('reports');
?>
<style>
	.dataTables_filter
	{
		text-align: right;
	}
	.dataTables_paginate.paging_simple_numbers
	{
		text-align: right;
	}
</style>
<script src="<?php echo base_url(); ?>assets/pci/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/pci/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/pci/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/pci/responsive.bootstrap.min.js"></script>

<script>
	$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<?php
	$qcalldatayellowfinal = array(); 	
	$qcalldatayellow = array();
	$qcalldatagreenfinal = array();
	$qcalldatagreen = array();
	
	$qaalldatagreenfinal= array();
	$qaalldatagreen = array();
	$qaalldatayellowfinal = array();
	$qaalldatayellow = array();
	
	$whallryfinal= array();
	$whallry = array();
	$whallygfinal = array();
	$whallrg = array();
	
	$ansaallryfinal = array();
	$ansaallry = array();
	
	$ansaallygfinal = array(); 
	$ansaallyg = array();
	
	
	
?>
 
<div class="row">
	<div class="col-lg-12">
		<div class="box box-primary">
			
			<div class="box-body">
			<h3>Qc :-</h3>
			<div class="col-lg-4"> 
				<table id="exampl" class="table table-striped table-bordered" cellspacing="0" width="100%">
            		<thead>
              			<tr>
	              			<th></th>
	              			<th>Red to Yellow</th>
	              			<th>Yellow to Green</th>
	              		</tr>
	              	</thead>
	              	<tbody>
	              		<?php $mm = date('m'); ?>
	              		<?php for ($m=1; $m<=$mm; $m++) { ?>			 
   	              		<tr>
	              			<td>
	              				<?php
     								$month = date("M'Y", mktime(0,0,0,$m, 1, date('Y')));
     								echo $month;
								?>
	              			</td> 
	              			<td>
	              				<?php $qcry = get_qc_redyellow($m);
	        						$qcsum = 0;	
									$qccount = 0;
	              					foreach($qcry as $qcr) 
	              					{
	              						if($qcr->decoration == 0)
										{ 
											$aa = $qcr->start_date;
											$datetime = new DateTime($aa);
											$la_time = new DateTimeZone('Asia/Calcutta');
											$datetime->setTimezone($la_time);
											$date_1 = $datetime->format('Y-m-d H:i:s');
											$date_2	= $qcr->end_time;
											$differenceFormat1 = '%i';
											$differenceFormat2 = '%h';
											$differenceFormat3 = '%d';
											$datetime1 = date_create($date_1);
											$datetime2 = date_create($date_2);
											$interval = date_diff($datetime1, $datetime2);
											$minut = $interval->format($differenceFormat1);
											$hos = $interval->format($differenceFormat2);
											$days = $interval->format($differenceFormat3);
											$his = $hos*60;
											$dis = $days*24*60;
											$tol = (($minut+$his+$dis)/60)/24;
											$qcsum+= $tol;	  
	              							$qccount++;
										}
										else
										{
											$ansaqcjoin = get_ansa_qc($qcr->container_job_id);
											foreach($ansaqcjoin as $qcansa)
											{
												$date_1 = $qcansa->ansa_start_datetime;
												$date_2	= $qcr->end_time;
												$differenceFormat1 = '%i';
												$differenceFormat2 = '%h';
												$differenceFormat3 = '%d';
												$datetime1 = date_create($date_1);
												$datetime2 = date_create($date_2);
												$interval = date_diff($datetime1, $datetime2);
												$minut = $interval->format($differenceFormat1);
												$hos = $interval->format($differenceFormat2);
												$days = $interval->format($differenceFormat3);
												$his = $hos*60;
												$dis = $days*24*60;
												$tol = (($minut+$his+$dis)/60)/24;
												$qcsum+= $tol;	  
	              								$qccount++;
												
											}
										}	
									}
									if($qcsum !=0 )
									{ 
									$finalqc = $qcsum/$qccount;
									}
									else
									{
										$finalqc = '0';
									}
										
									$qcalldatayellow['data'] = number_format($finalqc, 2, '.', ',');
									$qcalldatayellow['month'] = $m;
									$qcalldatayellowfinal[] = $qcalldatayellow;
									
	              					echo number_format($finalqc, 2, '.', ',');
	              				?>
	              			</td>
	              			<td>
	              				<?php $qcyg = get_qc_redyellow2($m);
								
									$qcysum2 = 0;	
									$qcycount2 = 0;
	              					foreach($qcyg as $qay) 
	              					{
										$qagy = get_qc_yellowgreen($qay->container_job_id);
										foreach($qagy as $qag)
										{
											$date_1 = $qay->end_time;
											$date_2 = $qag->end_time;
											
											$differenceFormat1 = '%i';
											$differenceFormat2 = '%h';
											$differenceFormat3 = '%d';
											$datetime1 = date_create($date_1);
											$datetime2 = date_create($date_2);
											$interval = date_diff($datetime1, $datetime2);
											$minut = $interval->format($differenceFormat1);
											$hos = $interval->format($differenceFormat2);
											$days = $interval->format($differenceFormat3);
											$his = $hos*60;
											$dis = $days*24*60;
											$tol = (($minut+$his+$dis)/60)/24;
											$qcysum2+= $tol;	  
	              							$qcycount2++;
											
										}
									}
									if($qcysum2 !=0 )
									{
									$finalqcy = $qcysum2/$qcycount2;
									}
									else
									{
										$finalqcy='0';
									}	
									$qcalldatagreen['data'] = number_format($finalqcy, 2, '.', ',');
									$qcalldatagreen['month'] = $m;
									$qcalldatagreenfinal[] = $qcalldatagreen;
	              					echo number_format($finalqcy, 2, '.', ',');
								?>
	              			</td>
	              		</tr>
	              		<?php } ?>	  
					</tbody>
				</table>
			</div>
			
			<div class="col-lg-8">
				
				
			<div id="chartContainer3" style="width: 100%; height: 300px;display: inline-block;"></div> 	
				
			</div>
			<!-- Qc Completed -->			
	
			
			<h3>Qa :-</h3>
			<div class="col-lg-4"> 
				<table id="exampl" class="table table-striped table-bordered" cellspacing="0" width="100%">
            		<thead>
              			<tr>
	              			<th></th>
	              			<th>Red to Yellow</th>
	              			<th>Yellow to Green</th>
	              		</tr>
	              	</thead>
	              	<tbody>
	              		<?php for ($m=1; $m<=$mm; $m++) { ?>			 
   	              		<tr>
	              			<td>
	              				<?php
     								$month = date("M'Y", mktime(0,0,0,$m, 1, date('Y')));
     								echo $month;
								?>
	              			</td> 
	              			<td>
	              				<?php $qarya = get_qa_redyellow($m);
	        						$qasum = 0;	
									$qacount = 0;
	              					foreach($qarya as $qcra) 
	              					{
	              						$qagya = get_qa_redyellow_end($qcra->container_job_id);
										foreach($qagya as $qaga)
										{
											$date_1 = $qcra->qc_start_datetime;
											$date_2 = $qaga->end_time;
											$differenceFormat1 = '%i';
											$differenceFormat2 = '%h';
											$differenceFormat3 = '%d';
											$datetime1 = date_create($date_1);
											$datetime2 = date_create($date_2);
											$interval = date_diff($datetime1, $datetime2);
											$minut = $interval->format($differenceFormat1);
											$hos = $interval->format($differenceFormat2);
											$days = $interval->format($differenceFormat3);
											$his = $hos*60;
											$dis = $days*24*60;
											$tol = (($minut+$his+$dis)/60)/24;
											$qasum+= $tol;	  
	              							$qacount++;
											
										}
									}
									if($qasum !=0 )
									{
										$finalqa = $qasum/$qacount;
									}
									else
									{
										$finalqa='0';
									}	
									$qaalldatayellow['data'] = number_format($finalqa, 2, '.', ',');
									$qaalldatayellow['month'] = $m;
									$qaalldatayellowfinal[] = $qaalldatayellow;
	              					echo number_format($finalqa, 2, '.', ',');
	              				?>
	              			</td>
	              			<td>
	              				
	              				<?php $qaryg = get_qa_yellow_start($m);
	        						$qasumg = 0;	
									$qacountg = 0;
	              					foreach($qaryg as $qarag) 
	              					{
	              						$qagyag = get_qa_green_end($qarag->container_job_id);
										foreach($qagyag as $qagag)
										{
											$date_1 = $qarag->end_time;
											$date_2 = $qagag->end_time;
											$differenceFormat1 = '%i';
											$differenceFormat2 = '%h';
											$differenceFormat3 = '%d';
											$datetime1 = date_create($date_1);
											$datetime2 = date_create($date_2);
											$interval = date_diff($datetime1, $datetime2);
											$minut = $interval->format($differenceFormat1);
											$hos = $interval->format($differenceFormat2);
											$days = $interval->format($differenceFormat3);
											$his = $hos*60;
											$dis = $days*24*60;
											$tol = (($minut+$his+$dis)/60)/24;
											$qasumg+= $tol;	  
	              							$qacountg++;
											
										}
									}
									if($qasumg !=0 )
									{
										$finalqag = $qasumg/$qacountg;
									}
									else
									{
										$finalqag = '0';
									}	
									$qaalldatagreen['data'] = number_format($finalqag, 2, '.', ',');
									$qaalldatagreen['month'] = $m;
									$qaalldatagreenfinal[] = $qaalldatagreen;
	              					echo number_format($finalqag, 2, '.', ',');
	              				?>
	              				
	              				
	              			</td>
	              		</tr>
	              		<?php } ?>	  
					</tbody>
				</table>
			</div>
			
				<div class="col-lg-8">
			
    		 	
				
			<div id="chartContainer4" style="width: 100%; height: 300px;display: inline-block;"></div> 	
				

			</div>

			<!--QA Completed -->
			
			
			<h3>Wh :-</h3>
			<div class="col-lg-4"> 
				<table id="exampl" class="table table-striped table-bordered" cellspacing="0" width="100%">
            		<thead>
              			<tr>
	              			<th></th>
	              			<th>Red to Yellow</th>
	              			<th>Yellow to Green</th>
	              		</tr>
	              	</thead>
	              	<tbody>
	              		<?php for ($m3=1; $m3<=$mm; $m3++) { ?>			 
   	              		<tr>
	              			<td>
	              				<?php
     								$month = date("M'Y", mktime(0,0,0,$m3, 1, date('Y')));
     								echo $month;
								?>
	              			</td> 
	              			<td>
	              				<?php
	              				 	$whs1 = 0;	
									$whc1 = 0;
	              					$whrystart = get_wh_redyellow($m3); 
	              					foreach($whrystart as $start1)
									{
										$whryend = get_wh_redyellowend($start1->container_job_id);
										
										foreach($whryend as $end1)
										{
											$date_1 = $start1->qa_start_datetime;
											$date_2 = $end1->end_time;
											$differenceFormat1 = '%i';
											$differenceFormat2 = '%h';
											$differenceFormat3 = '%d';
											$datetime1 = date_create($date_1);
											$datetime2 = date_create($date_2);
											$interval = date_diff($datetime1, $datetime2);
											$minut = $interval->format($differenceFormat1);
											$hos = $interval->format($differenceFormat2);
											$days = $interval->format($differenceFormat3);
											$his = $hos*60;
											$dis = $days*24*60;
											$tol = (($minut+$his+$dis)/60)/24;
											$whs1+= $tol;	  
	              							$whc1++;
										}
									}
									if($whs1 != 0)
									{
										$finalwhry = $whs1/$whc1;
									}
									else
									{
										$finalwhry = '0';
									}	
									$whallry['data'] = number_format($finalwhry, 2, '.', ',');
									$whallry['month'] = $m3;
									$whallryfinal[] = $whallry;
	              					echo number_format($finalwhry, 2, '.', ',');
									
	              				?>
	              			</td>
	              			<td>
	              				<?php
	              				 	$whs2 = 0;	
									$whc2 = 0;
	              					$whygstart = get_wh_yellowgreen($m3); 
	              					foreach($whygstart as $start2)
									{
										$whygend = get_wh_yellowgreenend($start2->container_job_id);
										
										foreach($whygend as $end2)
										{
											$date_1 = $start2->end_time;
											$date_2 = $end2->end_time;
											$differenceFormat1 = '%i';
											$differenceFormat2 = '%h';
											$differenceFormat3 = '%d';
											$datetime1 = date_create($date_1);
											$datetime2 = date_create($date_2);
											$interval = date_diff($datetime1, $datetime2);
											$minut = $interval->format($differenceFormat1);
											$hos = $interval->format($differenceFormat2);
											$days = $interval->format($differenceFormat3);
											$his = $hos*60;
											$dis = $days*24*60;
											$tol = (($minut+$his+$dis)/60)/24;
											$whs2+= $tol;	  
	              							$whc2++;
										}
									}
									if($whs2 != 0)
									{
										$finalwhyg = $whs2/$whc2;
									}
									else
									{
										$finalwhyg = '0';
									}	
									$whallrg['data'] = number_format($finalwhyg, 2, '.', ',');
									$whallrg['month'] = $m3;
									$whallygfinal[] = $whallrg;
	              					echo number_format($finalwhyg, 2, '.', ',');
									
	              				?>
	              			</td>
	              		</tr>
	              		<?php } ?>	  
					</tbody>
				</table>
			</div>
			
			<div class="col-lg-8">
				<div id="chartContainer5" style="width: 100%; height: 300px;display: inline-block;"></div> 	
			</div>

			<!--QA Completed -->
			
			<h3>ANSA :-</h3>
			<div class="col-lg-4"> 
				<table id="examp" class="table table-striped table-bordered" cellspacing="0" width="100%">
            		<thead>
              			<tr>
	              			<th></th>
	              			<th>Red to Yellow</th>
	              			<th>Yellow to Green</th>
	              		</tr>
	              	</thead>
	              	<tbody>
	              		<?php for ($m4=1; $m4<=$mm; $m4++) { ?>			 
   	              		<tr>
	              			<td>
	              				<?php
     								$month = date("M'Y", mktime(0,0,0,$m4, 1, date('Y')));
     								echo $month;
								?>
	              			</td> 
	              			<td>
	              				<?php 
	              					$ansas1 = 0;
									$ansac1 = 0;
	              					$ansary = get_ansa_redyellow($m4);
									
									foreach($ansary as $ansastart1)
									{
										$ansayr = get_ansa_redyellowend($ansastart1->container_job_id);
										
										foreach($ansayr as $ary)
										{
											$date_1 = $ansastart1->start_date;
											$date_2 = $ary->end_time;
											$differenceFormat1 = '%i';
											$differenceFormat2 = '%h';
											$differenceFormat3 = '%d';
											$datetime1 = date_create($date_1);
											$datetime2 = date_create($date_2);
											$interval = date_diff($datetime1, $datetime2);
											$minut = $interval->format($differenceFormat1);
											$hos = $interval->format($differenceFormat2);
											$days = $interval->format($differenceFormat3);
											$his = $hos*60;
											$dis = $days*24*60;
											$tol = (($minut+$his+$dis)/60)/24;
											$ansas1+= $tol;	  
	              							$ansac1++;
										}
									}
									
									if($ansas1 != 0)
									{
										$finalansary = $ansas1/$ansac1;
									}
									else
									{
										$finalansary = '0';
									}	
									$ansaallry['data'] = number_format($finalansary, 2, '.', ',');
									$ansaallry['month'] = $m4;
									$ansaallryfinal[] = $ansaallry;
	              					echo number_format($finalansary, 2, '.', ',');
										
	              				?>
	              				
	              			</td>
	              			<td>
	              				<?php 
	              					$ansas2 = 0;
									$ansac2 = 0;
	              					$ansayg = get_ansa_yellowgreen($m4);
									
									foreach($ansayg as $ansastart2)
									{
										$ansayg = get_ansa_yellowgreenend($ansastart2->container_job_id);
										
										foreach($ansayg as $agy)
										{
											$date_1 = $ansastart2->end_time;  
											$date_2 = $agy->end_time;
											$differenceFormat1 = '%i';
											$differenceFormat2 = '%h';
											$differenceFormat3 = '%d';
											$datetime1 = date_create($date_1);
											$datetime2 = date_create($date_2);
											$interval = date_diff($datetime1, $datetime2);
											$minut = $interval->format($differenceFormat1);
											$hos = $interval->format($differenceFormat2);
											$days = $interval->format($differenceFormat3);
											$his = $hos*60;
											$dis = $days*24*60;
											$tol = (($minut+$his+$dis)/60)/24;
											$ansas2+= $tol;	  
	              							$ansac2++;
										}
									}
									
									if($ansas2 != 0)
				 					{
										$finalansayg = $ansas2/$ansac2;
									}
									else
									{
										$finalansayg = '0';
									}	
									$ansaallyg['data'] = number_format($finalansayg, 2, '.', ',');
									$ansaallyg['month'] = $m4;
									$ansaallygfinal[] = $ansaallyg;
	              					echo number_format($finalansayg, 2, '.', ',');
										
	              				?>
	              				
	              				
	              			</td>
	              		</tr>
	              		<?php } ?>	  
					</tbody>
				</table>
			</div>
			
			<div class="col-lg-8">
				<div id="chartContainer10" style="width: 100%; height: 300px;display: inline-block;"></div> 	
			</div>

			<!--ANSA Completed -->
			
			


			</div>
		</div>
	</div>
</div>		
<?php
  					$a=array();
    				$b=array();
    				for($month=1;$month<=12;$month++)
    				{
        				$b=date("M'y", mktime(0,0,0,$month, 1, date('Y')));
        				$a[]=$b;
    				}
				    $month2=$a['0'];
				    $month3=$a['1'];
				    $month4=$a['2'];
				    $month5=$a['3'];
				    $month6=$a['4'];
				    $month7=$a['5'];
				    $month8=$a['6'];
				    $month9=$a['7'];
				    $month10=$a['8'];
				    $month11=$a['9'];
				    $month12=$a['10'];
				    $month13=$a['11'];
			?>
    		<script type="text/javascript">
        		window.onload = function () {
                var chart = new CanvasJS.Chart("chartContainer3", {
                title: {
                    text: "QC",
                    fontSize: 30
                },
		        exportEnabled: true,
                animationEnabled: true,
                axisX: {
 
                     title:"",
                    valueFormatString: "#0.#",
                    maximum: 11,
                    interval:1,
                    gridThickness: 1,
                    tickThickness: 1,
                    gridColor: "lightgrey",
                    tickColor: "lightgrey",
                    lineThickness: 0
 
                },
                toolTip: {
                    shared: true
                },
                theme: "theme2",
                axisY: {
                    gridColor: "Silver",
                    tickColor: "silver"
                },
                legend: {
                    verticalAlign: "bottom",
                    horizontalAlign: "center"
                },
                data: [{
                    type: "line",
                    showInLegend: true,
                    lineThickness: 2,
                    name: "Red to Yellow (in days)",
                    markerType: "square",
                    color: "#F08080",
                    dataPoints: [
                    
                   <?php foreach($qcalldatayellowfinal as $qcall) { ?>
                   	
                   			<?php if($qcall['month'] == '1') { ?>
                   				{ x:0, y:<?php echo $qcall['data']; ?>,label: "<?php echo $month2; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qcall['month'] == '2') { ?>
                   				{ x:1, y: <?php echo $qcall['data']; ?>,label: "<?php echo $month3; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qcall['month'] == '3') { ?>
                   				{ x:2, y: <?php echo $qcall['data']; ?>,label: "<?php echo $month4; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qcall['month'] == '4') { ?>
                   				{ x:3, y: <?php echo $qcall['data']; ?>,label: "<?php echo $month5; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qcall['month'] == '5') { ?>
                   				{ x:4, y: <?php echo $qcall['data']; ?>,label: "<?php echo $month6; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qcall['month'] == '6') { ?>
                   				{ x:5, y: <?php echo $qcall['data']; ?>,label: "<?php echo $month7; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qcall['month'] == '7') { ?>
                   				{ x:6, y: <?php echo $qcall['data']; ?>,label: "<?php echo $month8; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qcall['month'] == '8') { ?>
                   				{ x:7, y: <?php echo $qcall['data']; ?>,label: "<?php echo $month9; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qcall['month'] == '9') { ?>
                   				{ x:8, y: <?php echo $qcall['data']; ?>,label: "<?php echo $month10; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qcall['month'] == '10') { ?>
                   				{ x:9, y: <?php echo $qcall['data']; ?>,label: "<?php echo $month11; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qcall['month'] == '11') { ?>
                   				{ x:10, y: <?php echo $qcall['data']; ?>,label: "<?php echo $month12; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qcall['month'] == '12') { ?>
                   				{ x:11, y: <?php echo $qcall['data']; ?>,label: "<?php echo $month13; ?>" }		
                   			<?php }else { ?>
                   				
                   			<?php } ?>		
                   			
                   <?php }  ?>	  
                    
                    ]
                },
                {
                    type: "line",
                    showInLegend: true,
                    name: "Yellow to Green (in days)",
                    color: "#20B2AA",
                    lineThickness: 2,
 
                    dataPoints: [
                    <?php foreach($qcalldatagreenfinal as $qcallg) { ?>
                   				
                   			<?php if($qcallg['month'] == '1') { ?>
                   				{ x:0, y:<?php echo $qcallg['data']; ?>,label: "<?php echo $month2; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qcallg['month'] == '2') { ?>
                   				{ x:1, y: <?php echo $qcallg['data']; ?>,label: "<?php echo $month3; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qcallg['month'] == '3') { ?>
                   				{ x:2, y: <?php echo $qcallg['data']; ?>,label: "<?php echo $month4; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qcallg['month'] == '4') { ?>
                   				{ x:3, y: <?php echo $qcallg['data']; ?>,label: "<?php echo $month5; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qcallg['month'] == '5') { ?>
                   				{ x:4, y: <?php echo $qcallg['data']; ?>,label: "<?php echo $month6; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qcallg['month'] == '6') { ?>
                   				{ x:5, y: <?php echo $qcallg['data']; ?>,label: "<?php echo $month7; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qcallg['month'] == '7') { ?>
                   				{ x:6, y: <?php echo $qcallg['data']; ?>,label: "<?php echo $month8; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qcallg['month'] == '8') { ?>
                   				{ x:7, y: <?php echo $qcallg['data']; ?>,label: "<?php echo $month9; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qcallg['month'] == '9') { ?>
                   				{ x:8, y: <?php echo $qcallg['data']; ?>,label: "<?php echo $month10; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qcallg['month'] == '10') { ?>
                   				{ x:9, y: <?php echo $qcallg['data']; ?>,label: "<?php echo $month11; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qcallg['month'] == '11') { ?>
                   				{ x:10, y: <?php echo $qcallg['data']; ?>,label: "<?php echo $month12; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qcallg['month'] == '12') { ?>
                   				{ x:11, y: <?php echo $qcallg['data']; ?>,label: "<?php echo $month13; ?>" }		
                   			<?php }else { ?>
                   				
                   			<?php } ?>		
                   			
                   <?php }  ?>
                    ]
                }
                ],
            });
 
            chart.render();
            
            var chart = new CanvasJS.Chart("chartContainer4", {
                title: {
                    text: "QA",
                    fontSize: 30
                },
                exportEnabled: true,
                animationEnabled: true,
                axisX: {
 
                     title:"",
                    valueFormatString: "#0.#",
                    maximum: 11,
                    interval:1,
                    gridThickness: 1,
                    tickThickness: 1,
                    gridColor: "lightgrey",
                    tickColor: "lightgrey",
                    lineThickness: 0
 
                },
                toolTip: {
                    shared: true
                },
                theme: "theme2",
                axisY: {
                    gridColor: "Silver",
                    tickColor: "silver"
                },
                legend: {
                    verticalAlign: "bottom",
                    horizontalAlign: "center"
                },
                data: [{
                    type: "line",
                    showInLegend: true,
                    lineThickness: 2,
                    name: "Red to Yellow (in days)",
                    markerType: "square",
                    color: "#F08080",
                    dataPoints: [
                    
                   <?php foreach($qaalldatayellowfinal as $qaall) { ?>
                   	
                   			<?php if($qaall['month'] == '1') { ?>
                   				{ x:0, y:<?php echo $qaall['data']; ?>,label: "<?php echo $month2; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '2') { ?>
                   				{ x:1, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month3; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '3') { ?>
                   				{ x:2, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month4; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '4') { ?>
                   				{ x:3, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month5; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '5') { ?>
                   				{ x:4, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month6; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '6') { ?>
                   				{ x:5, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month7; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '7') { ?>
                   				{ x:6, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month8; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '8') { ?>
                   				{ x:7, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month9; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '9') { ?>
                   				{ x:8, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month10; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '10') { ?>
                   				{ x:9, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month11; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '11') { ?>
                   				{ x:10, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month12; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '12') { ?>
                   				{ x:11, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month13; ?>" }		
                   			<?php }else { ?>
                   				
                   			<?php } ?>		
                   			
                   <?php }  ?>	  
                    
                    ]
                },
                {
                    type: "line",
                    showInLegend: true,
                    name: "Yellow to Green (in days)",
                    color: "#20B2AA",
                    lineThickness: 2,
 
                    dataPoints: [
                    <?php foreach($qaalldatagreenfinal as $qaallg) { ?>
                   				
                   			<?php if($qaallg['month'] == '1') { ?>
                   				{ x:0, y:<?php echo $qaallg['data']; ?>,label: "<?php echo $month2; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '2') { ?>
                   				{ x:1, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month3; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '3') { ?>
                   				{ x:2, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month4; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '4') { ?>
                   				{ x:3, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month5; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '5') { ?>
                   				{ x:4, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month6; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '6') { ?>
                   				{ x:5, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month7; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '7') { ?>
                   				{ x:6, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month8; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '8') { ?>
                   				{ x:7, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month9; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '9') { ?>
                   				{ x:8, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month10; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '10') { ?>
                   				{ x:9, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month11; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '11') { ?>
                   				{ x:10, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month12; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '12') { ?>
                   				{ x:11, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month13; ?>" }		
                   			<?php }else { ?>
                   				
                   			<?php } ?>		
                   			
                   <?php }  ?>
                    ]
                }
                ],
            });


            chart.render();
            
            var chart = new CanvasJS.Chart("chartContainer5", {
                title: {
                    text: "WH",
                    fontSize: 30
                },
                exportEnabled: true,
                animationEnabled: true,
                axisX: {
 
                     title:"",
                    valueFormatString: "#0.#",
                    maximum: 11,
                    interval:1,
                    gridThickness: 1,
                    tickThickness: 1,
                    gridColor: "lightgrey",
                    tickColor: "lightgrey",
                    lineThickness: 0
 
                },
                toolTip: {
                    shared: true
                },
                theme: "theme2",
                axisY: {
                    gridColor: "Silver",
                    tickColor: "silver"
                },
                legend: {
                    verticalAlign: "bottom",
                    horizontalAlign: "center"
                },
                data: [{
                    type: "line",
                    showInLegend: true,
                    lineThickness: 2,
                    name: "Red to Yellow (in days)",
                    markerType: "square",
                    color: "#F08080",
                    dataPoints: [
                    
                   <?php foreach($whallryfinal as $qaall) { ?>
                   	
                   			<?php if($qaall['month'] == '1') { ?>
                   				{ x:0, y:<?php echo $qaall['data']; ?>,label: "<?php echo $month2; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '2') { ?>
                   				{ x:1, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month3; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '3') { ?>
                   				{ x:2, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month4; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '4') { ?>
                   				{ x:3, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month5; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '5') { ?>
                   				{ x:4, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month6; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '6') { ?>
                   				{ x:5, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month7; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '7') { ?>
                   				{ x:6, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month8; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '8') { ?>
                   				{ x:7, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month9; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '9') { ?>
                   				{ x:8, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month10; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '10') { ?>
                   				{ x:9, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month11; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '11') { ?>
                   				{ x:10, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month12; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '12') { ?>
                   				{ x:11, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month13; ?>" }		
                   			<?php }else { ?>
                   				
                   			<?php } ?>		
                   			
                   <?php }  ?>	  
                    
                    ]
                },
                {
                    type: "line",
                    showInLegend: true,
                    name: "Yellow to Green (in days)",
                    color: "#20B2AA",
                    lineThickness: 2,
 
                    dataPoints: [
                    <?php foreach($whallygfinal as $qaallg) { ?>
                   				
                   			<?php if($qaallg['month'] == '1') { ?>
                   				{ x:0, y:<?php echo $qaallg['data']; ?>,label: "<?php echo $month2; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '2') { ?>
                   				{ x:1, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month3; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '3') { ?>
                   				{ x:2, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month4; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '4') { ?>
                   				{ x:3, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month5; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '5') { ?>
                   				{ x:4, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month6; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '6') { ?>
                   				{ x:5, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month7; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '7') { ?>
                   				{ x:6, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month8; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '8') { ?>
                   				{ x:7, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month9; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '9') { ?>
                   				{ x:8, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month10; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '10') { ?>
                   				{ x:9, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month11; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '11') { ?>
                   				{ x:10, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month12; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '12') { ?>
                   				{ x:11, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month13; ?>" }		
                   			<?php }else { ?>
                   				
                   			<?php } ?>		
                   			
                   <?php }  ?>
                    ]
                }
                ],
            });


            chart.render();
            
            var chart = new CanvasJS.Chart("chartContainer10", {
                title: {
                    text: "ANSA",
                    fontSize: 30
                },
                exportEnabled: true,
                animationEnabled: true,
                axisX: {
 
                     title:"",
                    valueFormatString: "#0.#",
                    maximum: 11,
                    interval:1,
                    gridThickness: 1,
                    tickThickness: 1,
                    gridColor: "lightgrey",
                    tickColor: "lightgrey",
                    lineThickness: 0
 
                },
                toolTip: {
                    shared: true
                },
                theme: "theme2",
                axisY: {
                    gridColor: "Silver",
                    tickColor: "silver"
                },
                legend: {
                    verticalAlign: "bottom",
                    horizontalAlign: "center"
                },
                data: [{ 
                    type: "line",
                    showInLegend: true,
                    lineThickness: 2,
                    name: "Red to Yellow (in days)",
                    markerType: "square",
                    color: "#F08080",
                    dataPoints: [
                    
                   <?php foreach($ansaallryfinal as $qaall) { ?>
                   	
                   			<?php if($qaall['month'] == '1') { ?>
                   				{ x:0, y:<?php echo $qaall['data']; ?>,label: "<?php echo $month2; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '2') { ?>
                   				{ x:1, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month3; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '3') { ?>
                   				{ x:2, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month4; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '4') { ?>
                   				{ x:3, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month5; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '5') { ?>
                   				{ x:4, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month6; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '6') { ?>
                   				{ x:5, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month7; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '7') { ?>
                   				{ x:6, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month8; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '8') { ?>
                   				{ x:7, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month9; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '9') { ?>
                   				{ x:8, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month10; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '10') { ?>
                   				{ x:9, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month11; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '11') { ?>
                   				{ x:10, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month12; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaall['month'] == '12') { ?>
                   				{ x:11, y: <?php echo $qaall['data']; ?>,label: "<?php echo $month13; ?>" }		
                   			<?php }else { ?>
                   				
                   			<?php } ?>		
                   			
                   <?php }  ?>	  
                    
                    ]
                },
                {
                    type: "line",
                    showInLegend: true,
                    name: "Yellow to Green (in days)",
                    color: "#20B2AA",
                    lineThickness: 2,
 
                    dataPoints: [
                    <?php foreach($ansaallygfinal as $qaallg) { ?>
                   				
                   			<?php if($qaallg['month'] == '1') { ?>
                   				{ x:0, y:<?php echo $qaallg['data']; ?>,label: "<?php echo $month2; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '2') { ?>
                   				{ x:1, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month3; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '3') { ?>
                   				{ x:2, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month4; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '4') { ?>
                   				{ x:3, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month5; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '5') { ?>
                   				{ x:4, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month6; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '6') { ?>
                   				{ x:5, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month7; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '7') { ?>
                   				{ x:6, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month8; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '8') { ?>
                   				{ x:7, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month9; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '9') { ?>
                   				{ x:8, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month10; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '10') { ?>
                   				{ x:9, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month11; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '11') { ?>
                   				{ x:10, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month12; ?>" },		
                   			<?php }else { ?>
                   				
                   			<?php } ?>
                   			
                   			<?php if($qaallg['month'] == '12') { ?>
                   				{ x:11, y: <?php echo $qaallg['data']; ?>,label: "<?php echo $month13; ?>" }		
                   			<?php }else { ?>
                   				
                   			<?php } ?>		
                   			
                   <?php }  ?>
                    ]
                }
                ],
            });


            chart.render();
 
            }
    		</script>
    		<script src="<?php echo base_url(); ?>assets/pci/canvasjs.min.js"></script> 
    		