<?php $this->layout('layouts::default') ?>
<?php
$ci=&get_instance();
$ci->load->helper('reports');
?>
<script src="<?php echo base_url(); ?>assets/canvas.js"></script>
		<script src="<?php echo base_url(); ?>assets/pci/pie.js"></script>
		<script src="<?php echo base_url(); ?>assets/pci/light.js"></script>
<div class="row">
	<div class="col-lg-12">
		<div class="box box-primary">
			<div class="box-body">
				<h3> Planning Success Ratio</h3>
					<div class="col-lg-5 form-group">
					<label>Select Tpd:</label>	
					<select class="form-control" id="dept">
						<option value="">Select</option>
						<?php 	$tpd = get_tpds(); 
								foreach($tpd as $t) { ?>
								<option value="<?php echo $t->name; ?>"><?php echo $t->name; ?></option>
						<?php } ?>	
					</select>	
					</div>
					<div class="col-lg-5 form-group">
						<label>Select Month and Year:</label>	
						<input type="text" id="datepicker" class="form-control" />
					</div>
					
					<div class="col-lg-2 form-group">
						<label>&nbsp; </label><br>	
						<button class="btn btn-success" id="ok">Ok</button>
					</div>
					
					<div id="show">
				
					</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready(function(){
		 
		$("#ok").click(function(){
			var dep=$("#dept").val();
			var time=$("#datepicker").val();
			var data= {dept:dep,my:time} 
			$.ajax({
					'url':'<?php echo base_url();?>admin/reports/rationset',
					'type':'POST',
					'data':data,
					'success':function(message){
					   $("#show").html(message); 
					}
				});			
		});	
	});
</script>					

<link rel="stylesheet" href="../assets/plugins/datepicker/datepicker3.css" />
<script src="../assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script>
$(function () {
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm',
      minViewMode: 'months'
    });
    $(".timepicker").timepicker({
      showInputs: false,
     
    });
  });
</script>

		