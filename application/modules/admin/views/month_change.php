<?php $this->layout('layouts::default') ?>
<div class="row">
	<div class="col-lg-6">
		<div class="box box-primary">
    		<form action="<?php echo base_url(); ?>admin/month_change/deletemonth">
        		<div class="box-header with-border">
        			<div class="col-md-12">
            			<h3 class="box-title">Please Change the Month</h3>
            		</div>
            	</div>
            	<div class="box-body">
            	</div>
           	 	<div class="box-footer text-center ">
              		<button class="pull-right btn btn-primary" id="submitdata" type="submit">Change Month</button>
            	</div>
            </form>
          </div>
	</div>
</div>     	