<?php
$ci=&get_instance();
$ci->load->helper('home');
?>
<link  href="<?php echo base_url(); ?>assets/pci/datatable/dataTables.bootstrap.min.css" rel="stylesheet"></style>
<link  href="<?php echo base_url(); ?>assets/pci/datatable/buttons.bootstrap.min.css" rel="stylesheet"></style>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/jquery.dataTables.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/jszip.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/pdfmake.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/vfs_fonts.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/buttons.html5.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/buttons.print.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/pci/datatable/buttons.colVis.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
   var tabletwo = $('#exampletwo').DataTable({
    	ordering: false,
    	lengthChange: false,
    	buttons: [
        					{
            					extend: 'excel',
            					text: 'Excel',
            					header: true
                				
            				}
        			],
    });
    tabletwo.buttons().container().appendTo( '#exampletwo_wrapper .col-sm-6:eq(0)' );
     
} );
</script>

<table id="exampletwo" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
		         			<thead>
					            <tr>    
					                <th>Sr</th>
					                <th>User</th>
					                <th>Container</th>
					                <th>Jobs</th>
					                <th>Note</th>
					                <th>Date</th>
					                <th>Time</th>
					           	</tr>
		        			</thead>
					        <tfoot>
					            <tr>
					                <th>Sr</th>
					                <th>User</th>
					                <th>Container</th>
					                <th>Jobs</th>
					                <th>Note</th>
					                <th>Date</th>
					                <th>Time</th>
					            </tr>
					        </tfoot>
		        			<tbody>

<?php $ii = 1; ?>
<?php $activity = get_activity(); ?>
<?php foreach($activity as $av) { ?>
<tr>
	<td><?php echo $ii; $ii++; ?></td>
	<td>
		<?php 	$user = get_user($av->user); 
				foreach($user as $us)
				{
					echo $us->first_name;
				}
		?>
	</td>
	<td>
		<span class="label label-success">
		<?php 	$containe = get_containe($av->container);
				foreach($containe as $c)
				{
					echo $c->container_type;
				} 
		?>	
		</span>
	</td>
	<td>
		<?php 	$jobs = get_jobs($av->jobs);
				foreach($jobs as $j)
				{
					echo $j->job_name;
				} 
		?>
	</td>
	<td><div class="sparkbar" data-color="#00a65a" data-height="20"><?php echo $av->note; ?></div></td>
	<td><?php echo date("d-m-Y", strtotime($av->date)); ?></td>
	<td><span class="label label-success"><?php echo $av->time; ?></span></td>
</tr>
<?php } ?>
  </tbody>
						</table>