<?php $this->layout('layouts::default') ?>
<?php
$ci=&get_instance();
$ci->load->helper('common');
?> 
<style>
	.input-xs {
  height: 25px;
  padding: 2px 5px;
  font-size: 15px;
  line-height: 1.5; 
}
</style>

<div class="row">
<div class="col-lg-5">
	<div class="box box-primary">
        	<div class="box-header with-border">
            	<h3 class="box-title">Container List</h3>
            </div>
            <?php foreach($result as $r) { ?>
            <div class="box-body">
              	<div class="col-lg-12 form-group">
              		<label>Customer Name :- </label>
              		<?php echo $r->customer_name; ?>
             	</div>
             	<div class="col-lg-12 form-group">
              		<label>Container Type :- </label>
              		<?php echo $r->container_type; ?>
             	</div> 
              	
              	<div class="col-lg-12 form-group">
		        	<label>Container Number :- </label>
		            <?php $whatIWant = substr($r->container_type, strpos($r->container_type, "-") + 1); ?>
		            <?php echo $whatIWant; ?>
		        </div>	
              	
              	<div class="col-lg-12 form-group">
              		<label>Date :- </label>
              		<?php echo $r->container_date; ?>
              	</div>
              	
              	<div class="col-lg-12 form-group">
              		<label>Total Quantity :- </label>
              		<?php echo $r->total_quility; ?>
              	</div>
				
				<div class="row">
              		<div class="col-md-12">
              			<table class="table" id="POITable">
              				<thead>
              					<tr>
              						<th>Select Jobs</th>
              						<th>No. Qty</th>
              					</tr>
              				</thead>
              				<tfoot>
              					<?php foreach($job as $j) { ?>
              				 	</tr>	
              				 		<td><?php $name = get_job($j->job_id); 
              				 					foreach($name as $n)
												{
													echo $n->job_name;
												}
              				 	 		?>
              				 		</td>
              				 		<td><?php echo $j->qty; ?></td>	
              					</tr>
              					<?php } ?>
              				</tfoot>
              			</table>
              		</div>				
              	</div>              
            </div>
            <?php } ?>    
    	</div>
	</div>
</div>
