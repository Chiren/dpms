<?php $this->layout('layouts::default') ?>
<?php
$ci=&get_instance();
$ci->load->helper('common');
?>
<style>
	.input-xs {
  height: 25px;
  padding: 2px 5px;
  font-size: 15px;
  line-height: 1.5; 
}
</style>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/pci/chosen.min.css">
    <script src="<?php echo base_url(); ?>assets/pci/chosen.jquery.min.js"></script>
<div class="abc"></div>
<div class="row">
<div class="col-lg-8">
	<div class="box box-primary">
    	<form action="container/update">
    		
        	<div class="box-header with-border">
            	<h3 class="box-title">Edit Container</h3>
            </div>
            <?php foreach($result as $r) {
            	
				 ?>
            	<input type="hidden" name="id" value="<?php echo $r->container_id; ?>" />
            <div class="box-body">
              	<div class="form-group">
              		<label>Customer Name</label>
              		<input type="text" name="customer_name" value="<?php echo $r->customer_name; ?>" class="form-control input-xs" placeholder="Customer Name"/>
             	</div>
              	
              	<div class="row">
              		<div class="col-lg-6">
              			<div class="form-group">
		              		<label>Name<span>(Ex. ANSA 04)</span></label>
		              		<select class="form-control input-xs" name="container_type">
		              			<?php $type = container_type(); ?>
		              			<?php foreach($type as $t) { ?>
		              			<option value="<?php echo $t->name; ?>" <?php if($r->container_name == $t->name) { echo "selected='selected'"; } ?> ><?php echo $t->name; ?></option>
		              			<?php } ?>
		              		</select>
	              		</div>	
              		</div>
              		<div class="col-lg-6">
              			<div class="form-group">
		              		<label>Container Number<span>(Ex. 04)</span></label>
		              		<?php $whatIWant = substr($r->container_type, strpos($r->container_type, "-") + 1); ?>
		              		<input type="text" name="container_number" class="form-control input-xs" value="<?php echo $whatIWant; ?>" placeholder="04"/>
		          		</div>	
              		</div>
              	</div>
              	
              	<div class="form-group">
              		<label>Date</label>
              		<input type="text" name="date" class="form-control input-xs" value="<?php echo $r->container_date; ?>" id="datepicker" placeholder="Date" />
              	</div>
				
				<div class="row">
              		<div class="col-md-12">
              			<table class="table" id="POITable">
              				<thead>
              					<tr>
              						<th class="col-md-3">Select Jobs</th>
              						<th class="col-md-2">No. Qty</th>
              						<th class="col-md-2">TPD</th>
              						<th class="col-md-2">Decoration</th>
              						<th class="col-md-2">Price</th>
              						<th><button type="button" class="btn btn-box-tool input-sm add_field_button" style="background-color: green; color:white" ><i class="fa fa-plus"></i></button></th>
              					</tr>
              				</thead>
              			</table>
              		</div>
              		<script>
				 		$(document).ready(function() {
					 var max_fields      = 100; //maximum input boxes allowed
				    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
				    var add_button      = $(".add_field_button"); //Add button ID
				   	var fieldHTML = `<div class="col-md-12"><div class="col-md-3 form-group"><input type="hidden" name="container_job_id[]" value="" /><select required="required" class="form-control input-xs livesearch" style="width: 100%; " name="jobs[]" ><?php foreach ($resultjob as $k) { ?><option value="<?php print_r($k->job_id); ?>"><?php print_r($k->job_name); ?></option><?php } ?></select></div><div class="col-md-2 form-group"><input required="required" type="text" class="txt form-control flat input-xs" name="qty[]" /></div><div class="col-md-2 form-group"><select class="form-control input-xs" name="tpd[]"><?php $tpd = get_tpd(); ?><?php foreach($tpd as $tp) { ?><option value="<?php echo $tp->id; ?>"><?php echo $tp->name; ?></option><?php } ?></select></div><div class="col-md-2 form-group"><select class="form-control input-xs" name="plain[]"><option value="0">Plain</option><option value="1">Decoration</option></select></div><div class="col-md-2 form-group"><input type="text" name="price[]" class="form-control input-xs" required/></div><button type="button" class="remove_field btn btn-box-tool input-sm" style="background-color: red; color:white" ><i class="fa fa-minus"></i></button></div>`;
				    var x = 1; //initlal text box count
				    $(add_button).click(function(e){ //on add input button click
				    	$('.abc').load('<?php echo base_url(); ?>admin/container/demo');
					        e.preventDefault();
					        if(x < max_fields){ //max input box allowed
					            x++; //text box increment
					            $(wrapper).append(fieldHTML); //add input box
					        }
				    });
				    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
					        e.preventDefault(); $(this).parent('div').remove(); x--;
				    })
					});
					</script>
					<script>
				 		$(document).ready(function() {
											
				    var max_fields      = 100; //maximum input boxes allowed
				    var wrapper         = $(".input_fields_wrap1"); //Fields wrapper
				   
				    $(wrapper).on("click",".remove_field1", function(e){ //user click on remove text
					        e.preventDefault(); $(this).parent('div').remove(); x--;
				    })
					});
					</script>
					<script>function deletejob(id) {
				    if (confirm('Are you sure?')) {
				        $.ajax({ 
				            url: 'container/deletejobs',
				            type: 'post',
				            data: {id:id},
				           success: function (message) {
				               
				            },
				            error: function () {
				                alert('Delete Failure');
				            }
				        });
				    } else {
				        alert('This Record not deleted');
				    }
				}</script>
              	<?php foreach($job as $j) { 
              			$name = get_job($j->job_id); 
              				foreach($name as $n)
							{
								$n->job_name;
				?>
				<div class="col-md-12 input_fields_wrap1">
              		<div class="col-md-3 form-group row-fluid">
              			<input type="hidden" name="container_job_id[]" value="<?php echo $j->container_job_id; ?>" />
              				<select class="form-control input-xs livesearch" name="jobs[]" id="jobs" data-live-search="true">
              					<?php foreach ($resultjob as $k) {  ?>
									<option value="<?php print_r($k->job_id); ?>" <?php if($n->job_name==$k->job_name) echo 'selected="selected"'; ?> ><?php print_r($k->job_name); ?></option>
								<?php } ?>
              				</select>
              		</div>
              			
              		<div class="col-md-2 form-group">
              			<input type="text" name="qty[]" value="<?php echo $j->qty; ?>" class="form-control input-xs" id="qty"/> 
              		</div>	
              		
              		<div class="col-md-2 form-group">
              			<?php $tpd = get_tpd_by_id($j->tpd); ?>
              				<?php foreach($tpd as $tt){  $tpdgnew=$tt->name; } ?>
              		<?php } ?>
              			<select class="form-control input-xs" name="tpd[]">
		              		<?php $tpd = get_tpd(); ?>
		              		<?php foreach($tpd as $tp) { ?>
		              		<option value="<?php echo $tp->id; ?>" <?php if($tpdgnew==$tp->name) echo 'selected="selected"'; ?> ><?php echo $tp->name; ?></option>
		              		<?php } ?>
		              	</select>
              		</div>
              		
              		<div class="col-md-2 form-group">
              			<select class="form-control input-xs" name="plain[]">
		              		<option value="0" <?php if($j->decoration=='0') echo 'selected="selected"'; ?>>Plain</option>
		              		<option value="1" <?php if($j->decoration=='1') echo 'selected="selected"'; ?>>Decoration</option>
		              	</select>
              		</div>
              		
              		<div class="col-md-2 form-group">
              			<input type="text" name="price[]" value="<?php echo $j->job_price; ?>" class="form-control input-xs" id="qty"/> 
              		</div>
              		
              			<button type="button" class="remove_field1 btn btn-box-tool input-sm " onclick="deletejob(<?php echo $j->container_job_id ?>)" style="background-color: red; color:white" ><i class="fa fa-minus"></i></button>
              		
              		<!--div class="col-md-12">	
              			<div class="col-md-6 form-group"><label>Total Quentity</label></div>
              			<div class="col-md-6 form-group">
              				<input type="text" name="total_qty" class="form-control input-xs" value="<?php echo $r->total_quility; ?>"/>
              			</div>	
              		</div-->			
              				
              	</div>
              	<?php } } ?>
              	<div class="input_fields_wrap">
            </div>              
            </div>
           
            
            
		         <div class="box-footer text-center ">
		          	<button class="pull-right btn btn-primary" type="submit">Save</button>
			    </div>
         	</form>
    	</div>
	</div>
</div>
<script type="text/javascript">
      $(".livesearch").chosen();
</script>
<link rel="stylesheet" href="../assets/plugins/datepicker/datepicker3.css" />
<script src="../assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script>
$(function () {
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
    
  });
</script>