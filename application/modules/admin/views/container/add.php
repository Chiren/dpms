<?php $this->layout('layouts::default') ?>
<?php
$ci=&get_instance();
$ci->load->helper('common');
?> 
<style>
	.input-xs {
  height: 25px;
  padding: 2px 5px;
  font-size: 15px;
  line-height: 1.5; 
}

</style>
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/pci/chosen.min.css">
    <script src="<?php echo base_url(); ?>assets/pci/chosen.jquery.min.js"></script>
<div class="abc"></div>
<div class="row">
<div class="col-lg-6">
	<div class="box box-primary">
    	<form action="container/save">
        	<div class="box-header with-border">
        		<div class="col-md-6">
            		<h3 class="box-title">Add New Container</h3>
            	</div>
            	<div class="col-md-6 text-right">
            		<a href="<?php echo base_url(); ?>admin/uploadcsv" class="btn btn-info">Import</a>
            	</div>
            </div>
            <div class="box-body">
              	<div class="form-group">
              		<label>Customer Name</label>
              		<input type="text" name="customer_name" class="form-control input-xs" placeholder="Customer Name" required/>
             	</div>
              	<div class="row">
              		<div class="col-lg-6"> 
              			<div class="form-group">
		              		<label>Name<span>(Ex. ANSA 04)</span></label>
		              		<select class="form-control input-xs" id="con_types" name="container_type" required>
		              			<option value="">--Select One--</option>
		              			<?php $type = container_type(); ?>
		              			<?php foreach($type as $t) { ?>
		              			<option value="<?php echo $t->name; ?>"><?php echo $t->name; ?></option>
		              			<?php } ?>
		              		</select>
	              		</div>	
              		</div>
              		<div class="col-lg-6">
              			<div class="form-group">
		              		<label>Container Number<span>(Ex. 04)</span></label>
		              		<input type="text" name="container_number" class="form-control input-xs" id="chkdata" placeholder="04" required/>
		          			<label id="msgshowhide" style="display: none;">Already this Container Add Please Edit</label>
		          		</div>	
              		</div>
              	</div>
              	<div class="form-group">
              		<label>Date</label>
              		<input type="text" name="date" class="form-control input-xs" id="datepicker" placeholder="Date" required/>
              	</div>
				<div class="row">
              		<div class="col-md-12">
              			<table class="table" id="POITable">
              				<thead>
              					<tr>
              						<th class="col-md-3">Select Jobs</th>
              						<th class="col-md-2">No. Qty</th>
              						<th class="col-md-2">TPD</th>
              						<th class="col-md-2">Decoration</th>
              						<th class="col-md-2">Price</th>
              						<th><button type="button" class="btn btn-box-tool input-sm add_field_button" style="background-color: green; color:white" ><i class="fa fa-plus"></i></button></th>
              					</tr>
              				</thead>
              			</table>
              		</div>	
              			<div class="input_fields_wrap">
              				<div class="col-md-12">
              				<div class="col-md-3 form-group row-fluid">
              					<select class="form-control input-xs livesearch" name="jobs[]" id="jobs" required="required" data-live-search="true">
              						<?php foreach ($result['jobs'] as $k) { ?>
									<option value="<?php print_r($k->job_id); ?>"><?php print_r($k->job_name); ?></option>
									<?php } ?>
              					</select> 
              				</div>		
              				<div class="col-md-2 form-group">
              						<input type="text" name="qty[]" class="txt form-control input-xs" id="qty" required/> 
              				</div>
              				<div class="col-md-2 form-group">
              					<select class="form-control input-xs" name="tpd[]" required>
		              				<?php $tpd = get_tpd(); ?>
		              				<?php foreach($tpd as $tp) { ?>
		              					<option value="<?php echo $tp->id; ?>"><?php echo $tp->name; ?></option>
		              				<?php } ?>
		              			</select>
              				</div>
              				
              				<div class="col-md-2 form-group">
              					<select class="form-control input-xs" name="plain[]" required>
		              				<option value="0">Plain</option>
		              				<option value="1">Decoration</option>
		              			</select>
              				</div>
              				<div class="col-md-2 form-group">
              					<input type="text" name="price[]" class="form-control input-xs" required/>
              				</div>
              					
              				</div>	
              			</div>
              			<!--div class="col-md-12">	
              				<div class="col-md-6 form-group"><label>Total Quentity</label></div>
              				<div class="col-md-6 form-group">
              					
              						<input type="text" id="total" name="total_qty" class="form-control input-xs"/>
              				</div>	
              			</div-->			
              			
              			<script>
				 		$(document).ready(function() {
					    	var max_fields      = 100; //maximum input boxes allowed
				    		var wrapper         = $(".input_fields_wrap"); //Fields wrapper
				    		var add_button      = $(".add_field_button"); //Add button ID
				   			var fieldHTML = `<div class="col-md-12"><div class="col-md-3 form-group row-fluid"><select required="required" class="form-control input-xs livesearch" name="jobs[]" data-live-search="true"><?php foreach ($result['jobs'] as $k) { ?><option value="<?php print_r($k->job_id); ?>"><?php print_r($k->job_name); ?></option><?php } ?></select></div><div class="col-md-2 form-group"><input required="required" type="text" class="txt form-control flat input-xs" name="qty[]" /></div><div class="col-md-2 form-group"><select class="form-control input-xs" name="tpd[]"><?php $tpd = get_tpd(); ?><?php foreach($tpd as $tp) { ?><option value="<?php echo $tp->id; ?>"><?php echo $tp->name; ?></option><?php } ?></select></div><div class="col-md-2 form-group"><select class="form-control input-xs" name="plain[]"><option value="0">Plain</option><option value="1">Decoration</option></select></div><div class="col-md-2 form-group"><input type="text" name="price[]" class="form-control input-xs" required/></div><button type="button" class="remove_field btn btn-box-tool input-sm" style="background-color: red; color:white" ><i class="fa fa-minus"></i></button></div>`;
				    		var x = 1; //initlal text box count
				    		$(add_button).click(function(e){ //on add input button click
					        	$('.abc').load('<?php echo base_url(); ?>admin/container/demo');
					        	e.preventDefault();
					        	if(x < max_fields){ //max input box allowed
					            x++; //text box increment
					            $(wrapper).append(fieldHTML); //add input box
					        	}
				    		});
				    		$(wrapper).on("click",".remove_field", function(e){ //user click on remove text
					    		e.preventDefault(); $(this).parent('div').remove(); x--;
				    		})
						});
						
						</script>
              			
              			
              			
              		
              	</div>              
            </div>
            <div class="box-footer text-center ">
              	<button class="pull-right btn btn-primary" id="submitdata" type="submit">ADD</button>
            </div>
            </form>
          </div>
       	</div>
       	
       	
       	
<div class="col-lg-6">
	<div class="box box-primary">
    	<div class="box-header with-border">
        	<h3 class="box-title">Container List</h3>
        	<div class="box-tools pull-right">
            	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
             </div>
		</div>
        
        <div class="box-body">
        	<form method="post" enctype="multipart/form-data" action="container/multipledelete">
        	<table class="table">
            	<thead>  
              		<tr>
              			<th>Sr</th>
              			<th>Number</th>
              			<th>Date</th>
              			<th>Name</th>
              			<th>Qty</th>
              			<th>Option</th>
              			<th><button type="submit" class="btn btn-box-tool input-sm" style="background-color: red; color:white"><i class="fa fa-trash"></i></button></th>
              		</tr>
              	</thead>
              	<tbody>
              		<?php $i = 1; foreach ($result['tabledata'] as $r) { ?>
					<tr>
              			<td><?php echo $i; $i++; ?></td>
              			<td><?php print_r($r->container_type); ?></td> 
              			<td><?php print_r($r->updatedOn); ?></td>
              			<td><?php print_r($r->container_name); ?></td>
              			<td><?php print_r($r->total_quility); ?></td>
              			<td>
              				<a href="container/view/<?php print_r($r->container_id); ?>" class="btn btn-box-tool input-sm" style="background-color: green; color:white" ><i class="fa fa-eye"></i></a>
              				<a href="container/edit/<?php print_r($r->container_id); ?>" class="btn btn-box-tool input-sm" style="background-color: #605CA8; color:white" ><i class="fa fa-pencil"></i></a>
              				<a href="container/delete/<?php print_r($r->container_id); ?>" class="btn btn-box-tool input-sm" style="background-color: red; color:white" ><i class="fa fa-trash"></i></a> 
              			</td>
              			<td>
              				<input type="checkbox" name="deletem[]" value="<?php echo $r->container_id; ?>" />
              			</td>
              		</tr>	  
					 <?php } ?>
              	</tbody>
              </table>
              </form>
            </div>
          </div>
       	</div>
</div>
<script>
$(document).ready(function(){
    $("#chkdata").keyup(function(){
        var step1 = $("#con_types").val();
        var step2 = $("#chkdata").val();
        var final = step1+'-'+step2;
        $.ajax({
        		'url':'<?php echo base_url();?>admin/container/chkcon/'+final,
				'type':'POST',
				'success':function(message)
				{	
					if(message == 1)
					{
						$("#submitdata").prop('disabled', true);
						$("#msgshowhide").show();
					}
					else
					{
						$("#submitdata").prop('disabled',false);
						$("#msgshowhide").hide();
					}
				}
			});
		});
 });
</script>
<script type="text/javascript">
      $(".livesearch").chosen();
</script>
<link rel="stylesheet" href="../assets/plugins/datepicker/datepicker3.css" />
<script src="../assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script>
$(document).ready(function(){
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd'
    });
  });
</script>
