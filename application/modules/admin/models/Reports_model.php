<?php 
class Reports_model  extends CI_Model  {

	function get_qc($month,$year)
	{
		$sql="SELECT * FROM container_job join job_master ON job_master.job_id = container_job.job_id WHERE MONTH(container_job.start_date)='".$month."' AND YEAR(container_job.start_date)='".$year."'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	function get_qc_redyellowend($id)
	{
		$sql="SELECT * FROM qc_log WHERE container_job_id='".$id."' AND status='0'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	function get_qc_redyellowansastart($id)
	{
		$sql="SELECT * FROM ansa_start WHERE container_job_id='".$id."'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	function get_qc_redyellowansaend($id)
	{
		$sql="SELECT * FROM qc_log WHERE container_job_id='".$id."' AND status='0'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	function get_qc_yellowgreenstart($id)
	{
		$sql="SELECT * FROM qc_log WHERE container_job_id='".$id."' AND status='0'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	function get_qc_yellowgreenend($id)
	{
		$sql="SELECT * FROM qc_log WHERE container_job_id='".$id."' AND status='1'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	//QC End 
	
	
	function get_qa_redyellowstart($id)
	{
		$sql="SELECT * FROM qc_start WHERE container_job_id='".$id."' order by container_job_id ASC";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	function get_qa_redyellowend($id)
	{
		$sql="SELECT * FROM qa_log WHERE container_job_id='".$id."' AND status='0' order by container_job_id ASC";
		$query=$this->db->query($sql);
		return $query->result();
	}
	function get_qa_yellowgreenend($id)
	{
		$sql="SELECT * FROM qa_log WHERE container_job_id='".$id."' AND status='1' order by container_job_id ASC";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	// QA End
	
	function get_wh_redyellowstart($id)
	{
		$sql="SELECT * FROM qa_start WHERE container_job_id='".$id."' order by container_job_id ASC";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	function get_wh_redyellowend($id)
	{
		$sql="SELECT * FROM wh_log WHERE container_job_id='".$id."' AND status='0' order by container_job_id ASC";
		$query=$this->db->query($sql);
		return $query->result();
	}
	function get_wh_yellowgreenend($id)
	{
		$sql="SELECT * FROM wh_log WHERE container_job_id='".$id."' AND status='1' order by container_job_id ASC";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	// WH End
	
	function get_ansa($month,$year)
	{
		$sql="SELECT * FROM container_job join job_master ON job_master.job_id = container_job.job_id WHERE MONTH(container_job.start_date)='".$month."' AND YEAR(container_job.start_date)='".$year."' AND container_job.decoration='1'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	function get_ansa_redyellowend($id)
	{
		$sql="SELECT * FROM ansa_log WHERE container_job_id='".$id."' AND status='0' order by container_job_id ASC";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	function get_ansa_yellowgreenend($id)
	{
		$sql="SELECT * FROM ansa_log WHERE container_job_id='".$id."' AND status='1' order by container_job_id ASC";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	// End ANSA
	
	// start ratio
	
	function get_tpd100ratio($month,$year)
	{
		$sql="SELECT * FROM container_master join container_job on container_job.container_id=container_master.container_id join tpd_master on tpd_master.id=container_job.tpd where tpd_master.name='100' AND MONTH(container_job.start_date)='".$month."' AND YEAR(container_job.start_date)='".$year."'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	function get_tpd100ratioqcend($id)
	{
		$sql="SELECT * FROM qc_log WHERE container_job_id='".$id."' AND status='1'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	function get_tpd100ratioqaend($id)
	{
		$sql="SELECT * FROM qa_log WHERE container_job_id='".$id."' AND status='1'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	function get_tpd100ratiowhend($id)
	{
		$sql="SELECT * FROM wh_log WHERE container_job_id='".$id."' AND status='1'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	function get_tpd100ratioansaend($id)
	{
		$sql="SELECT * FROM ansa_log WHERE container_job_id='".$id."' AND status='1'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	// end ratio tpd100
	
	function get_tpd60ratio($month,$year)
	{
		$sql="SELECT * FROM container_master join container_job on container_job.container_id=container_master.container_id join tpd_master on tpd_master.id=container_job.tpd where tpd_master.name='60' AND MONTH(container_job.start_date)='".$month."' AND YEAR(container_job.start_date)='".$year."'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	function get_tpd60ratioqcend($id)
	{
		$sql="SELECT * FROM qc_log WHERE container_job_id='".$id."' AND status='1'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	function get_tpd60ratioqaend($id)
	{
		$sql="SELECT * FROM qa_log WHERE container_job_id='".$id."' AND status='1'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	function get_tpd60ratiowhend($id)
	{
		$sql="SELECT * FROM wh_log WHERE container_job_id='".$id."' AND status='1'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	function get_tpd60ratioansaend($id)
	{
		$sql="SELECT * FROM ansa_log WHERE container_job_id='".$id."' AND status='1'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	// end ratio tpd60
	
	function get_tpd95ratio($month,$year)
	{
		$sql="SELECT * FROM container_master join container_job on container_job.container_id=container_master.container_id join tpd_master on tpd_master.id=container_job.tpd where tpd_master.name='95' AND MONTH(container_job.start_date)='".$month."' AND YEAR(container_job.start_date)='".$year."'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	function get_tpd95ratioqcend($id)
	{
		$sql="SELECT * FROM qc_log WHERE container_job_id='".$id."' AND status='1'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	function get_tpd95ratioqaend($id)
	{
		$sql="SELECT * FROM qa_log WHERE container_job_id='".$id."' AND status='1'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	function get_tpd95ratiowhend($id)
	{
		$sql="SELECT * FROM wh_log WHERE container_job_id='".$id."' AND status='1'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	function get_tpd95ratioansaend($id)
	{
		$sql="SELECT * FROM ansa_log WHERE container_job_id='".$id."' AND status='1'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	// end ratio tpd95
	
	function get_tpdngratio($month,$year)
	{
		$sql="SELECT * FROM container_master join container_job on container_job.container_id=container_master.container_id join tpd_master on tpd_master.id=container_job.tpd where tpd_master.name='NG' AND MONTH(container_job.start_date)='".$month."' AND YEAR(container_job.start_date)='".$year."'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	function get_tpdngratioqcend($id)
	{
		$sql="SELECT * FROM qc_log WHERE container_job_id='".$id."' AND status='1'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	function get_tpdngratioqaend($id)
	{
		$sql="SELECT * FROM qa_log WHERE container_job_id='".$id."' AND status='1'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	function get_tpdngratiowhend($id)
	{
		$sql="SELECT * FROM wh_log WHERE container_job_id='".$id."' AND status='1'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	function get_tpdngratioansaend($id)
	{
		$sql="SELECT * FROM ansa_log WHERE container_job_id='".$id."' AND status='1'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	// end ratio tpdng
	
	function get_qaperformance($month,$year)
	{
		$sql="SELECT jobscreen_master.qa_ready,jobscreen_master.qa_before,jobscreen_master.qa_same,jobscreen_master.qa_late FROM container_job join jobscreen_master on jobscreen_master.container_job_id=container_job.container_job_id where MONTH(container_job.start_date)='".$month."' AND YEAR(container_job.start_date)='".$year."'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	function get_qaperformancetabel($month,$year)
	{
		$sql="SELECT jobscreen_master.qa_ready,jobscreen_master.qa_before,jobscreen_master.qa_same,jobscreen_master.qa_late,container_job.job_id,container_job.container_id FROM container_job join jobscreen_master on jobscreen_master.container_job_id=container_job.container_job_id where MONTH(container_job.start_date)='".$month."' AND YEAR(container_job.start_date)='".$year."'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	
	function get_jobs($id)
	{
		$sql="SELECT job_name from job_master where job_id=$id";
		$query=$this->db->query($sql);
		return $query->result();
	}
	
	function get_repoted($id)
	{
		$sql="SELECT chk_date from container_master where container_id=$id";
		$query=$this->db->query($sql);
		return $query->result();
	}
	//qa performance
		
}
 
