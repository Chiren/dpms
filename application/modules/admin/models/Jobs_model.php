<?php 
class Jobs_model  extends CI_Model  {
	
	function save($data)
	{
		$msg = $this->db->insert('job_master', $data);
		
	}
	function get_all(){
		$this->db->where('status', '0');
		return $this->db->get('job_master')->result();
	}
	function get($id){
		$this->db->where('job_id', $id);
		return $this->db->get('job_master')->result();
	}
	function update($id, $data){
		$this->db->where('job_id',$id);
		return $this->db->update('job_master',$data);
	}
	function delete($id){
		$this->db->where('job_id',$id);
		$this->db->delete('job_master');
	}
	
	function get_all_jobs(){
		return $this->db->get('job_master')->result();
	}
	
	function get_current_jobs()
	{
		$this->db->select('container_job.*,container_master.*, job_master.*');
		$this->db->from('container_job');
		$this->db->join('container_master', 'container_job.container_id = container_master.container_id');
		$this->db->join('job_master', 'job_master.job_id = container_job.job_id'); 
		$this->db->where("con_status='0' OR con_status='1' OR con_status='2'");
		return $this->db->get()->result();
	}
	
	function get_replace_jobs($container,$con_id,$con_job_id,$qty)
	{
		$sql1 = "select total_quility from container_master where container_id=$con_id";
		$query1 = $this->db->query($sql1);
		$data1 = $query1->result();
		
		foreach($data1 as $d1)
		{
			$total1 = $d1->total_quility-$qty;
			$sql2 = "update container_master set total_quility='".$total1."' where container_id='".$con_id."'";
			$query2 = $this->db->query($sql2);
		}

		$sql3 = "select total_quility from container_master where container_id=$container";
		$query3 = $this->db->query($sql3);
		$data2 = $query3->result();
		
		foreach($data2 as $d2)
		{
			$total2 = $d2->total_quility+$qty;
			$sql4 = "update container_master set total_quility='".$total2."' where container_id='".$container."'";
			$query4 = $this->db->query($sql4);
		}
		
		$sql5 = "update container_job set container_id='".$container."' where container_job_id='".$con_job_id."'";
		$query5 = $this->db->query($sql5);
 		
	}
	
}
?>

