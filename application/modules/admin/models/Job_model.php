<?php 
class Job_model  extends CI_Model  {
	
	function get_all_100()
	{
		$this->db->select('container_job.*,container_master.*, job_master.*,jobscreen_master.*');
		$this->db->from('container_job');
		$this->db->join('container_master', 'container_job.container_id = container_master.container_id');
		$this->db->join('job_master', 'job_master.job_id = container_job.job_id'); 
		$this->db->join('jobscreen_master', 'jobscreen_master.container_job_id = container_job.container_job_id'); 
		$this->db->join('tpd_master', 'container_job.tpd = tpd_master.id');
		$this->db->where("tpd_master.name","100"); 
		$this->db->where("job_status","0");
		$this->db->order_by("con_status", "DESC");
		$this->db->order_by("container_date", "asc");
		return $this->db->get()->result();
	}
	
	function get_all_60()
	{
		$this->db->select('container_job.*,container_master.*, job_master.*,jobscreen_master.*');
		$this->db->from('container_job');
		$this->db->join('container_master', 'container_job.container_id = container_master.container_id');
		$this->db->join('job_master', 'job_master.job_id = container_job.job_id'); 
		$this->db->join('jobscreen_master', 'jobscreen_master.container_job_id = container_job.container_job_id'); 
		$this->db->join('tpd_master', 'container_job.tpd = tpd_master.id');
		$this->db->where("tpd_master.name","60");
		$this->db->where("job_status","0");
		$this->db->order_by("con_status", "DESC");
		$this->db->order_by("container_date", "asc");
		return $this->db->get()->result();
	}
	
	function get_all_95()
	{
		$this->db->select('container_job.*,container_master.*, job_master.*,jobscreen_master.*');
		$this->db->from('container_job');
		$this->db->join('container_master', 'container_job.container_id = container_master.container_id');
		$this->db->join('job_master', 'job_master.job_id = container_job.job_id'); 
		$this->db->join('jobscreen_master', 'jobscreen_master.container_job_id = container_job.container_job_id'); 
		$this->db->join('tpd_master', 'container_job.tpd = tpd_master.id');
		$this->db->where("tpd_master.name","95");
		$this->db->where("job_status","0");
		$this->db->order_by("con_status", "DESC");
		$this->db->order_by("container_date", "asc");
		return $this->db->get()->result();
	}
	
	function get_all_ng()
	{
		$this->db->select('container_job.*,container_master.*, job_master.*,jobscreen_master.*');
		$this->db->from('container_job');
		$this->db->join('container_master', 'container_job.container_id = container_master.container_id');
		$this->db->join('job_master', 'job_master.job_id = container_job.job_id'); 
		$this->db->join('jobscreen_master', 'jobscreen_master.container_job_id = container_job.container_job_id'); 
		$this->db->where("job_status","0");
		$this->db->order_by("con_status", "DESC");
		$this->db->order_by("container_date", "asc");
		$this->db->where("container_job.ng = '1'");
		return $this->db->get()->result();
	}

	function get_all_ansa()
	{
		$this->db->select('container_job.*,container_master.*, job_master.*,jobscreen_master.*');
		$this->db->from('container_job');
		$this->db->join('container_master', 'container_job.container_id = container_master.container_id');
		$this->db->join('job_master', 'job_master.job_id = container_job.job_id'); 
		$this->db->join('jobscreen_master', 'jobscreen_master.container_job_id = container_job.container_job_id'); 
		$this->db->where("job_status","0");
		$this->db->order_by("con_status", "DESC");
		$this->db->order_by("container_date", "asc");
		$this->db->where("container_master.container_name = 'ANSA'");
		return $this->db->get()->result();
	}

	
	
}