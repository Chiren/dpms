<?php 
class Container_model  extends CI_Model  {
	
	
	function chkname($name)
	{
		$sql = "select * from container_master where container_type='".$name."' AND MONTH(container_date)=MONTH(NOW()) AND YEAR(container_date)=YEAR(NOW())";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
	
	
	function save($data, $job)
	{
		$ci = &get_instance();
		$ci -> load -> library('session');
		$x = $ci -> session -> userdata('user_group');
		$y = $ci -> session -> userdata();
		$users = $y['user_id'];
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$dates = date('Y-m-d');
		
		$msg = $this->db->insert('container_master', $data);
		$cid = $this->db->insert_id();
		
		$l = count($job['jobs']);
		$sum = 0;
		for($i = 0; $i < $l; $i++){
				
			$tpddata ="select * from tpd_master where id='".$job['tpd'][$i]."' AND name='NG'";	
			$querytpd = $this->db->query($tpddata);
			$tpdchk = $querytpd->num_rows();
			
			if($tpdchk != 0)
			{
				$ng = 1;
			}
			else
			{
				$ng = 0;
			}
			
			$data = array(
				'container_id' => $cid,
				'job_id' => $job['jobs'][$i],
				'qty' => $job['qty'][$i],
				'tpd' => $job['tpd'][$i],
				'decoration' => $job['plain'][$i],
				'ng' => $ng,
				'job_price' => $job['job_price'][$i]
			);
			
			$avt = array(
			
				'user' => $users,
				'jobs' => $job['jobs'][$i],
				'container' => $cid,
				'tpd' => $job['tpd'][$i],
				'time' => $time,
				'date' => $dates,
				'note' => "Job Add",
				
			);
			
			$this->db->insert('activity', $avt);
			
			$sum+=$job['qty'][$i];
			$this->db->insert('container_job', $data);
			$jids = $this->db->insert_id();
			
			$sqltwo="insert into jobscreen_master (container_job_id) value ($jids)";
			$querytwo = $this->db->query($sqltwo);
		}
		
		$sql="update container_master set total_quility='".$sum."' where container_id='".$cid."'";
		$query=$this->db->query($sql);
		
		
	}
	
	function get_all()
	{
		
		return $this->db->get('container_master')->result();
	}
	
	function get_all_incompletd()
	{
		$this->db->where("con_status = '0' OR con_status = '1' OR con_status = '2'");
		return $this->db->get('container_master')->result();
	}
	
	function get($id)
	{
		$this->db->where('container_id', $id);
		return $this->db->get('container_master')->result();
	}
	
	function get_job($id)
	{
		$this->db->where('container_id', $id);
		return $this->db->get('container_job')->result();
	} 
	
	function update($id, $data,$job)
	{
		
		
		$this->db->where('container_id',$id);
		$this->db->update('container_master',$data);
		
		$selectcon = "select * from container_master where container_id='".$id."'";
		$queryselect = $this->db->query($selectcon);
		$setcon = $queryselect->result();
		
		foreach($setcon as $scon)
		{
			$conname = $scon->container_name;
		}
	
		$l = count($job['jobs']);
		$sum = 0;
		for($i = 0; $i < $l; $i++)
		{
			$ids = $job['jobs'][$i];
			
			$jobid = $job['jobsid'][$i];
			$tpd = $job['tpd'][$i];
			$decoration = $job['plain'][$i];
			$qty = $job['qty'][$i];
			$job_price = $job['job_price'][$i];
				
			$sum+=$job['qty'][$i];
			
			if(!empty($ids))
			{
				$tpddata ="select * from tpd_master where id='".$tpd."' AND name='NG'";	
				$querytpd = $this->db->query($tpddata);
				$tpdchk = $querytpd->num_rows();
				
				$getdata = "select * from jobscreen_master where container_job_id=$ids";
				$getquery = $this->db->query($getdata);
				$setdata = $getquery->result();
				foreach($setdata as $sdata)
				{
					if($qty < $sdata->qc)
					{
						$updatedata1 = "update jobscreen_master set qc='".$qty."',qc_completed='".$qty."',qa='".$qty."',qa_per='".$qty."',qa_rem='null' where container_job_id='".$ids."'";
						$updatequery1 = $this->db->query($updatedata1); 
					}
					if($qty < $sdata->qa)
					{
						$updatedata2 = "update jobscreen_master set qa_completed='".$qty."',wh='".$qty."' where container_job_id='".$ids."'";
						$updatequery2 = $this->db->query($updatedata2);
					}
					if($qty < $sdata->wh && $conname!='ANSA')
					{
						$updatedata3 = "update jobscreen_master set wh_completed='".$qty."' where container_job_id='".$ids."'";
						$updatequery3 = $this->db->query($updatedata3);
					}
					if($qty < $sdata->wh && $conname=='ANSA')
					{ 
						$updatedata4 = "update jobscreen_master set wh_completed='".$qty."',ansa='".$qty."' where container_job_id='".$ids."'";
						$updatequery4 = $this->db->query($updatedata4);
					}
					if($qty < $sdata->ansa && $sdata->ansa!='0')
					{
						$updatedata5 = "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."' where container_job_id='".$ids."'";
						$updatequery5 = $this->db->query($updatedata5);
					}	
				}
				if($tpdchk != 0)
				{
					$ng = 1;
				}
				else
				{
					$ng = 0;
				}
					$sql="update container_job set job_id=$jobid,tpd=$tpd,qty=$qty,decoration=$decoration,ng=$ng,job_price=$job_price where container_job_id = $ids";
					$query = $this->db->query($sql);
				}
			
			else {
						
				$tpddata ="select * from tpd_master where id='".$tpd."' AND name='NG'";	
				$querytpd = $this->db->query($tpddata);
				$tpdchk = $querytpd->num_rows();
				
				if($tpdchk != 0)
				{
					$ng = 1;
				}
				else
				{
					$ng = 0;
				}
				$sqlinsert="insert into container_job (container_id,job_id,qty,tpd,decoration,ng,job_price) value ('".$id."','".$jobid."','".$qty."','".$tpd."','".$decoration."','".$ng."','".$job_price."')";
				$queryinsert = $this->db->query($sqlinsert);
				$lid=$this->db->insert_id();
				
				$sqlinsert1="insert into jobscreen_master (container_job_id) value ('".$lid."')";
				$queryinsert1 = $this->db->query($sqlinsert1);
				
			}
				
				
			
			
		}
		
		$sql="update container_master set total_quility='".$sum."' where container_id='".$id."'";
		$query=$this->db->query($sql);
		
	}
	function delete($id){
				
		$sql1 ="select * from container_job where container_id=$id";
		$query1 =$this->db->query($sql1);
		$data = $query1->result();
		
		foreach($data as $d)
		{
			$c_j_id = $d->container_job_id;
			
			$sql2="delete from container_job where container_job_id =$c_j_id";
			$query2 = $this->db->query($sql2);
			
			$sql3 ="delete from jobscreen_master where container_job_id =$c_j_id";
			$query3 = $this->db->query($sql3);
			
		}
		
		$sql4 ="delete from container_master where container_id =$id";
		$query4 = $this->db->query($sql4);
	}
	function deletejobs($id){
				
			$sql2="delete from container_job where container_job_id =$id";
			$query2 = $this->db->query($sql2);
			
			$sql3 ="delete from jobscreen_master where container_job_id =$id";
			$query3 = $this->db->query($sql3);
		
	}
	
	public function insert_con_excel($customer_name,$container_name,$container_no,$qty,$job,$tpd,$container_date)
	{
				
		$ci = &get_instance();
		$ci -> load -> library('session');
		$x = $ci -> session -> userdata('user_group');
		$y = $ci -> session -> userdata();
		$users = $y['user_id'];
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$dates = date('Y-m-d');
		
		$date = date("Y-m-d",strtotime($container_date));
		$c_type = $container_name."-".$container_no;
		
		$sqlsix = "select * from container_master where container_date='".$date."' AND customer_name='".$customer_name."'";
		$querysix = $this->db->query($sqlsix);
		$check = $querysix->num_rows();
		
		
		if($check == 0)
		{
			
			$sql = "insert into container_master (customer_name,container_type,container_name,container_date,updatedOn) value ('".$customer_name."','".$c_type."','".$container_name."','".$date."','".$date."')";	
			$query = $this->db->query($sql);
			$cid = $this->db->insert_id();
			// Insert Container 
		
			$l = count($job);
			$sum = 0;
			
			for($i = 0; $i < $l; $i++)
			{
				
				$sqljob = "select * from job_master where job_name='".$job[$i]."'";
				$queryjob = $this->db->query($sqljob);
				$chkjob = $queryjob->num_rows();
				
				if($chkjob == 0)
				{
					$jobadd = "insert into job_master (job_name,date) value ('".$job[$i]."','".$dates."')";
					$jobquery = $this->db->query($jobadd);
					$job_id =$this->db->insert_id();
				}
				else 
				{
					$sqltwo = "select * from job_master where job_name='".$job[$i]."'";
					$querytwo = $this->db->query($sqltwo);
					$jobsid = $querytwo->result_array();
					$job_id = $jobsid[0]['job_id'];
				}
				
				$sqlthree = "select * from tpd_master where name='".$tpd."'";
				$querythree = $this->db->query($sqlthree);
				$tpdid = $querythree->result_array();
				$tpd_id = $tpdid[0]['id'];
				
				
				$data = array(
					'container_id' => $cid,
					'job_id' => $job_id,
					'qty' => $qty[$i],
					'tpd' => $tpd_id
					);
				
				$avt = array(
					'user' => $users,
					'jobs' => $job_id,
					'container' => $cid,
					'tpd' => $tpd_id,
					'time' => $time,
					'date' => $dates,
					'note' => "Job Add",
					);
				
				$this->db->insert('activity', $avt);
			
				$sum+=$qty[$i];
				$this->db->insert('container_job', $data);
				$jids = $this->db->insert_id();
			
				$sqltwoo="insert into jobscreen_master (container_job_id) value ($jids)";
				$querytwoo = $this->db->query($sqltwoo);
			
			}
			
			$sqll="update container_master set total_quility='".$sum."' where container_id='".$cid."'";
			$queryl=$this->db->query($sqll);
		}
		else
		{
			
		}	
		
		
	}


	function insert_con_excelsheet1($customer_name,$container_type,$container_name,$qty,$jobs,$tpd,$container_date,$fg_code,$amount_value,$plant,$status,$amount_value)
	{	
			
		$ci = &get_instance();
		$ci -> load -> library('session');
		$x = $ci -> session -> userdata('user_group');
		$y = $ci -> session -> userdata();
		$users = $y['user_id'];
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$dates = date('Y-m-d'); 
		if($container_date=='TBC')
		{
			$date=date("Y-m-d");
		}
		else {
			
			$dat = str_replace("/","-",$container_date);
			$chkcondate = (explode("-",$dat));
			$year=$chkcondate[2]+2000;
			$date=$year."-".$chkcondate[0]."-".$chkcondate[1];	
			
			}
		
			
		if($plant == "PLAIN" || $plant== "Plain")
		{
			$pl= 0;
		}
		else
		{
			$pl= 1;
		}
		//echo $date;
		//exit;
		//$date = date("d-m-y",strtotime($container_date));
		
		//echo $date;
		//exit;
		
		//$sqlsix = "select * from container_master where container_date='".$date."' AND container_type='".$container_type."'";
		$sqlsix = "select * from container_master where container_type='".$container_type."' AND (con_status='0' OR con_status='1' OR con_status='2')";
		$querysix = $this->db->query($sqlsix);
		$check = $querysix->num_rows();
		$excelresult = $querysix->result();	
		
			
		
		if($check == 0)
		{
			
			$sql = "insert into container_master (customer_name,container_type,container_name,container_date,updatedOn) value ('".$customer_name."','".$container_type."','".$container_name."','".$date."','".$date."')";	
			$query = $this->db->query($sql);
			$cid = $this->db->insert_id();
			
			if($tpd =='100' || $tpd == '95' || $tpd == '60' || $tpd == 'NG')
			{
				$sqljob = "select * from job_master where fg_code='".$fg_code."'";
				$queryjob = $this->db->query($sqljob);
				$chkjob = $queryjob->num_rows();
				
				if($chkjob == 0)
				{
					$jobadd = "insert into job_master (job_name,date,fg_code,amount_value) value ('".$jobs."','".$dates."','".$fg_code."','".$amount_value."')";
					$jobquery = $this->db->query($jobadd);
					$job_id =$this->db->insert_id();
				}
				else 
				{
					$sqltwo = "select * from job_master where fg_code='".$fg_code."'";
					$querytwo = $this->db->query($sqltwo);
					$jobsid = $querytwo->result_array();
					$job_id = $jobsid[0]['job_id'];
				}
				
				$sqlthree = "select * from tpd_master where name='".$tpd."'";
				$querythree = $this->db->query($sqlthree);
				$tpdid = $querythree->result_array();
				$tpd_id = $tpdid[0]['id'];
				
				if($tpd == 'NG')
				{
					$ng = 1;
				}
				else
				{
					$ng = 0;
				}
				
				$data = array(
					'container_id' => $cid,
					'job_id' => $job_id,
					'qty' => $qty,
					'tpd' => $tpd_id,
					'decoration' => $pl,
					'ng' => $ng,
					'job_price' => $amount_value
					);
				
				$avt = array(
					'user' => $users,
					'jobs' => $job_id,
					'container' => $cid,
					'tpd' => $tpd_id,
					'time' => $time,
					'date' => $dates,
					'note' => "Job Add",
					);
				
				$this->db->insert('activity', $avt);
			
				$this->db->insert('container_job', $data);
				$jids = $this->db->insert_id();
			
				$sqltwoo="insert into jobscreen_master (container_job_id) value ($jids)";
				$querytwoo = $this->db->query($sqltwoo);
				$jonscreen_id = $this->db->insert_id();
				if (strpos($status, 'Resorting') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}		
				}
				if (strpos($status, 'resorting') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
				}
				if (strpos($status, 'Repacking') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
				}
				if (strpos($status, 'repacking') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
    				}
				}
				if (strpos($status, 'Ready') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."',qc='".$qty."',qc_completed='".$qty."',qa='".$qty."',qa_per='".$qty."',qa_ready='1' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
    				else
					{
						$jonscreendata1= "update jobscreen_master set qc='".$qty."',qc_completed='".$qty."',qa='".$qty."',qa_per='".$qty."',qa_ready='1' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}	
				}
				if (strpos($status, 'ready') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."',qc='".$qty."',qc_completed='".$qty."',qa='".$qty."',qa_per='".$qty."',qa_ready='1' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
    				else
					{
						$jonscreendata1= "update jobscreen_master set qc='".$qty."',qc_completed='".$qty."',qa='".$qty."',qa_per='".$qty."',qa_ready='1' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
				}
				
				
				$sqlfive="update container_master set total_quility='".$qty."' where container_id='".$cid."'";
				$queryfive=$this->db->query($sqlfive);
				
				
			}
			else
			{
				
			}	
			
		}
		else
		{
			$conid=$excelresult['0']->container_id;
			$constatus=$excelresult['0']->con_status;
			$query7="select * from container_job join job_master on container_job.job_id=job_master.job_id where container_job.container_id='".$conid."' and job_master.fg_code='".$fg_code."'";
			$resultquery7 = $this->db->query($query7);
			$check7 = $resultquery7->num_rows();
			$excelresult7 = $resultquery7->result();
			if($check7 == 0)
			{
				if($tpd =='100' || $tpd == '95' || $tpd == '60' || $tpd == 'NG')
				{
					$sqljob = "select * from job_master where fg_code='".$fg_code."'";
					$queryjob = $this->db->query($sqljob);
					$chkjob = $queryjob->num_rows();
					
					if($chkjob == 0)
					{
						$jobadd = "insert into job_master (job_name,date,fg_code,amount_value) value ('".$jobs."','".$dates."','".$fg_code."','".$amount_value."')";
						$jobquery = $this->db->query($jobadd);
						$job_id =$this->db->insert_id();
					}
					else 
					{
						$sqltwo = "select * from job_master where fg_code='".$fg_code."'";
						$querytwo = $this->db->query($sqltwo);
						$jobsid = $querytwo->result_array();
						$job_id = $jobsid[0]['job_id'];
					}
					
					$sqlthree = "select * from tpd_master where name='".$tpd."'";
					$querythree = $this->db->query($sqlthree);
					$tpdid = $querythree->result_array();
					$tpd_id = $tpdid[0]['id'];
					
					if($tpd == 'NG')
					{
						$ng = 1;
					}
					else
					{
						$ng = 0;
					}
					
					$data = array(
						'container_id' => $conid,
						'job_id' => $job_id,
						'qty' => $qty,
						'tpd' => $tpd_id,
						'decoration' => $pl,
						'ng' => $ng,
						'job_price' => $amount_value
						);
					
					$avt = array(
						'user' => $users,
						'jobs' => $job_id,
						'container' => $conid,
						'tpd' => $tpd_id,
						'time' => $time,
						'date' => $dates,
						'note' => "Job Add",
						);
					
					$this->db->insert('activity', $avt);
				
					$this->db->insert('container_job', $data);
					$jids = $this->db->insert_id();
				
					$sqltwoo="insert into jobscreen_master (container_job_id) value ($jids)";
					$querytwoo = $this->db->query($sqltwoo);
					$jonscreen_id = $this->db->insert_id();
				if (strpos($status, 'Resorting') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}		
				}
				if (strpos($status, 'resorting') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
				}
				if (strpos($status, 'Repacking') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
				}
				if (strpos($status, 'repacking') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
    				}
				}
				if (strpos($status, 'Ready') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."',qc='".$qty."',qc_completed='".$qty."',qa='".$qty."',qa_per='".$qty."',qa_ready='1' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
    				else
					{
						$jonscreendata1= "update jobscreen_master set qc='".$qty."',qc_completed='".$qty."',qa='".$qty."',qa_per='".$qty."',qa_ready='1' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}	
				}
				if (strpos($status, 'ready') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."',qc='".$qty."',qc_completed='".$qty."',qa='".$qty."',qa_per='".$qty."',qa_ready='1' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
    				else
					{
						$jonscreendata1= "update jobscreen_master set qc='".$qty."',qc_completed='".$qty."',qa='".$qty."',qa_per='".$qty."',qa_ready='1' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
				}
					
					$sqlfive="update container_master set total_quility='".$qty."' where container_id='".$conid."'";
					$queryfive=$this->db->query($sqlfive);
					
					
				}
				else
				{
					
				}
			}
			else 
			{
				if($tpd =='100' || $tpd == '95' || $tpd == '60' || $tpd == 'NG')
				{
				
				
				$sql8 = "update container_master set customer_name='".$customer_name."',container_type='".$container_type."',container_name='".$container_name."',container_date='".$date."',updatedOn='".$date."',total_quility='".$qty."' where container_id='".$conid."'";	
				$query8 = $this->db->query($sql8);
				
					$sqltpd9 = "select * from tpd_master where name='".$tpd."'";
					$querytpd9 = $this->db->query($sqltpd9);
					$tpdid = $querytpd9->result_array();
					$tpd_id = $tpdid[0]['id'];
					
					if($tpd == 'NG')
					{
						$ng = 1;
					}
					else
					{
						$ng = 0;
					}
				
				$con_job_id=$excelresult7['0']->container_job_id;
				$sql9 = "update container_job set qty='".$qty."',tpd='".$tpd_id."',decoration='".$pl."',ng='".$ng."' where container_job_id='".$con_job_id."'";	
				$query9 = $this->db->query($sql9);
				
				if (strpos($status, 'Resorting') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."' where container_job_id='".$con_job_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}		
				}
				if (strpos($status, 'resorting') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."' where container_job_id='".$con_job_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
				}
				if (strpos($status, 'Repacking') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."' where container_job_id='".$con_job_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
				}
				if (strpos($status, 'repacking') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."' where container_job_id='".$con_job_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
    				}
				}
				if (strpos($status, 'Ready') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."',qc='".$qty."',qc_completed='".$qty."',qa='".$qty."',qa_per='".$qty."',qa_ready='1' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
    				else
					{
						$jonscreendata1= "update jobscreen_master set qc='".$qty."',qc_completed='".$qty."',qa='".$qty."',qa_per='".$qty."',qa_ready='1' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}	
				}
				if (strpos($status, 'ready') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."',qc='".$qty."',qc_completed='".$qty."',qa='".$qty."',qa_per='".$qty."',qa_ready='1' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
    				else
					{
						$jonscreendata1= "update jobscreen_master set qc='".$qty."',qc_completed='".$qty."',qa='".$qty."',qa_per='".$qty."',qa_ready='1' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
				}
				
				
				
				
				}
				
				
			}		
			
		}	
		
	}  
	
	
	function insert_con_excelsheet2($qty,$jobs,$tpd,$plant,$fg_code,$amount_value,$ctype,$cdate,$status,$amount_value)
	{
						
		$ci = &get_instance();
		$ci -> load -> library('session');
		$x = $ci -> session -> userdata('user_group');
		$y = $ci -> session -> userdata();
		$users = $y['user_id'];
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$dates = date('Y-m-d');
		
		if($cdate=='TBC')
		{
			$date=date("Y-m-d");
		}
		else {
			$dat = str_replace("/","-",$cdate);
			$chkcondate = (explode("-",$dat));
			$year=$chkcondate[2]+2000;
			$date=$year."-".$chkcondate[0]."-".$chkcondate[1];	
		}
		
		
		$sql ="select * from container_master where container_type='".$ctype."' AND (con_status='0' OR con_status='1' OR con_status='2')";
		$query = $this->db->query($sql);
		$c_id = $query->result();
		$cid = $c_id['0']->container_id;
		$qt = $c_id['0']->total_quility;
		
		if($plant == "PLAIN" || $plant== "Plain")
		{
			$pl= 0;
		}
		else
		{
			$pl= 1;
		}
		
		$sqlsix = "select * from container_master join container_job on container_job.container_id=container_master.container_id join job_master on job_master.job_id=container_job.job_id where job_master.fg_code='".$fg_code."' AND container_master.container_id='".$cid."'";
		$querysix = $this->db->query($sqlsix);
		$check = $querysix->num_rows();
		$excelresult = $querysix->result();
		
		if($check == 0)
		{
			
			if($tpd =='100' || $tpd == '95' || $tpd == '60' || $tpd == 'NG')
			{
				$sqljob = "select * from job_master where fg_code='".$fg_code."'";
				$queryjob = $this->db->query($sqljob);
				$chkjob = $queryjob->num_rows();
				
				if($chkjob == 0)
				{
					$jobadd = "insert into job_master (job_name,date,fg_code,amount_value) value ('".$jobs."','".$dates."','".$fg_code."','".$amount_value."')";
					$jobquery = $this->db->query($jobadd);
					$job_id =$this->db->insert_id();
				}
				else 
				{
					$sqltwo = "select * from job_master where fg_code='".$fg_code."'";
					$querytwo = $this->db->query($sqltwo);
					$jobsid = $querytwo->result_array();
					$job_id = $jobsid[0]['job_id'];
				}
				
				$sqlthree = "select * from tpd_master where name='".$tpd."'";
				$querythree = $this->db->query($sqlthree);
				$tpdid = $querythree->result_array();
				$tpd_id = $tpdid[0]['id'];
				
				if($tpd == 'NG')
				{
					$ng = 1;
				}
				else
				{
					$ng = 0;
				}	
				$data = array(
					'container_id' => $cid,
					'job_id' => $job_id,
					'qty' => $qty,
					'tpd' => $tpd_id,
					'decoration' => $pl,
					'ng' => $ng,
					'job_price' => $amount_value
					);
				
				$avt = array(
					'user' => $users,
					'jobs' => $job_id,
					'container' => $cid,
					'tpd' => $tpd_id,
					'time' => $time,
					'date' => $dates,
					'note' => "Job Add",
					);
				
				$this->db->insert('activity', $avt);
			
				$this->db->insert('container_job', $data);
				$jids = $this->db->insert_id();
			
				$sqltwoo="insert into jobscreen_master (container_job_id) value ($jids)";
				$querytwoo = $this->db->query($sqltwoo);
				$jonscreen_id = $this->db->insert_id();
				if (strpos($status, 'Resorting') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}		
				}
				if (strpos($status, 'resorting') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
				}
				if (strpos($status, 'Repacking') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
				}
				if (strpos($status, 'repacking') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
    				}
				}
				if (strpos($status, 'Ready') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."',qc='".$qty."',qc_completed='".$qty."',qa='".$qty."',qa_per='".$qty."',qa_ready='1' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
    				else
					{
						$jonscreendata1= "update jobscreen_master set qc='".$qty."',qc_completed='".$qty."',qa='".$qty."',qa_per='".$qty."',qa_ready='1' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}	
				}
				if (strpos($status, 'ready') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."',qc='".$qty."',qc_completed='".$qty."',qa='".$qty."',qa_per='".$qty."',qa_ready='1' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
    				else
					{
						$jonscreendata1= "update jobscreen_master set qc='".$qty."',qc_completed='".$qty."',qa='".$qty."',qa_per='".$qty."',qa_ready='1' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
				}
				
				$sum=$qt+$qty;
				$sqlfive="update container_master set total_quility='".$sum."' where container_id='".$cid."'";
				$queryfive=$this->db->query($sqlfive);
				
			}
			else
			{
				
			}	
			
		}
		else
		{
			$conid=$excelresult['0']->container_id;
			$constatus=$excelresult['0']->con_status;
			$query7="select * from container_job join job_master on container_job.job_id=job_master.job_id where container_job.container_id='".$conid."' and job_master.fg_code='".$fg_code."'";
			$resultquery7 = $this->db->query($query7);
			$check7 = $resultquery7->num_rows();
			$excelresult7 = $resultquery7->result();
			
			if($check7 == 0)
			{
				if($tpd =='100' || $tpd == '95' || $tpd == '60' || $tpd == 'NG')
				{
					$sqljob = "select * from job_master where fg_code='".$fg_code."'";
					$queryjob = $this->db->query($sqljob);
					$chkjob = $queryjob->num_rows();
					
					if($chkjob == 0)
					{
						$jobadd = "insert into job_master (job_name,date,fg_code,amount_value) value ('".$jobs."','".$dates."','".$fg_code."','".$amount_value."')";
						$jobquery = $this->db->query($jobadd);
						$job_id =$this->db->insert_id();
					}
					else 
					{
						$sqltwo = "select * from job_master where fg_code='".$fg_code."'";
						$querytwo = $this->db->query($sqltwo);
						$jobsid = $querytwo->result_array();
						$job_id = $jobsid[0]['job_id'];
					}
					
					$sqlthree = "select * from tpd_master where name='".$tpd."'";
					$querythree = $this->db->query($sqlthree);
					$tpdid = $querythree->result_array();
					$tpd_id = $tpdid[0]['id'];
					
					if($tpd == 'NG')
					{
						$ng = 1;
					}
					else
					{
						$ng = 0;
					}
					
					$data = array(
						'container_id' => $conid,
						'job_id' => $job_id,
						'qty' => $qty,
						'tpd' => $tpd_id,
						'decoration' => $pl,
						'ng' => $ng,
						'job_price' => $amount_value
						);
					
					$avt = array(
						'user' => $users,
						'jobs' => $job_id,
						'container' => $conid,
						'tpd' => $tpd_id,
						'time' => $time,
						'date' => $dates,
						'note' => "Job Add",
						);
					
					$this->db->insert('activity', $avt);
				
					$this->db->insert('container_job', $data);
					$jids = $this->db->insert_id();
				
					$sqltwoo="insert into jobscreen_master (container_job_id) value ($jids)";
					$querytwoo = $this->db->query($sqltwoo);
					$jonscreen_id = $this->db->insert_id();
				if (strpos($status, 'Resorting') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}		
				}
				if (strpos($status, 'resorting') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
				}
				if (strpos($status, 'Repacking') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
				}
				if (strpos($status, 'repacking') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
    				}
				}
				if (strpos($status, 'Ready') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."',qc='".$qty."',qc_completed='".$qty."',qa='".$qty."',qa_per='".$qty."',qa_ready='1' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
    				else
					{
						$jonscreendata1= "update jobscreen_master set qc='".$qty."',qc_completed='".$qty."',qa='".$qty."',qa_per='".$qty."',qa_ready='1' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}	
				}
				if (strpos($status, 'ready') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."',qc='".$qty."',qc_completed='".$qty."',qa='".$qty."',qa_per='".$qty."',qa_ready='1' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
    				else
					{
						$jonscreendata1= "update jobscreen_master set qc='".$qty."',qc_completed='".$qty."',qa='".$qty."',qa_per='".$qty."',qa_ready='1' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
				}
					
					$sqlfive="update container_master set total_quility='".$qty."' where container_id='".$conid."'";
					$queryfive=$this->db->query($sqlfive);
					
					
				}
				else
				{
					
				}
			}
			else 
			{
				if($tpd =='100' || $tpd == '95' || $tpd == '60' || $tpd == 'NG')
				{
					$sql10 = "select * from container_master where container_id='".$conid."'";	
					$query10 = $this->db->query($sql10);
					$sumqty = $query10->result();
					
					$sumsqty=$sumqty['0']->total_quility+$qty;
					$sql8 = "update container_master set total_quility='".$sumsqty."' where container_id='".$conid."'";	
					$query8 = $this->db->query($sql8);
				
					$sqltpd9 = "select * from tpd_master where name='".$tpd."'";
					$querytpd9 = $this->db->query($sqltpd9);
					$tpdid = $querytpd9->result_array();
					$tpd_id = $tpdid[0]['id'];
					
					if($tpd == 'NG')
					{ 
						$ng = 1;
					}
					else
					{
						$ng = 0;
					}
				
				$con_job_id=$excelresult7['0']->container_job_id;
				$sql9 = "update container_job set qty='".$qty."',tpd='".$tpd_id."',decoration='".$pl."',ng='".$ng."' where container_job_id='".$con_job_id."'";	
				$query9 = $this->db->query($sql9);
				
				
				if (strpos($status, 'Resorting') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."' where container_job_id='".$con_job_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}		
				}
				if (strpos($status, 'resorting') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."' where container_job_id='".$con_job_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
				}
				if (strpos($status, 'Repacking') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."' where container_job_id='".$con_job_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
				}
				if (strpos($status, 'repacking') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."' where container_job_id='".$con_job_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
    				}
				}
				if (strpos($status, 'Ready') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."',qc='".$qty."',qc_completed='".$qty."',qa='".$qty."',qa_per='".$qty."',qa_ready='1' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
    				else
					{
						$jonscreendata1= "update jobscreen_master set qc='".$qty."',qc_completed='".$qty."',qa='".$qty."',qa_per='".$qty."',qa_ready='1' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}	
				}
				if (strpos($status, 'ready') !== false) 
				{
    				if($pl == 1)
    				{
    					$jonscreendata1= "update jobscreen_master set ansa='".$qty."',ansa_completed='".$qty."',qc='".$qty."',qc_completed='".$qty."',qa='".$qty."',qa_per='".$qty."',qa_ready='1' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
    				else
					{
						$jonscreendata1= "update jobscreen_master set qc='".$qty."',qc_completed='".$qty."',qa='".$qty."',qa_per='".$qty."',qa_ready='1' where id='".$jonscreen_id."'";	
    					$queryjobscreen = $this->db->query($jonscreendata1);
					}
				}
				
				
				}
				
				
			}		
			
		}	
			
		
		
		
	}


	public function insert_deficts($code,$name)
	{
		$sqltwoo="insert into defects_master (code,defects) value ('".$code."','".$name."')";
		$querytwoo = $this->db->query($sqltwoo);
	}
	
	function delete_con($allfggroup){
		
		$jid=array();
		$allararry=array();
		$alljid=array();
		foreach($allfggroup as $row)
		{
			$fgcode=$row['fgcode'];
			$ctype=$row['ctype'];
			
			$sql1 ="select * from container_master join container_job on container_master.container_id=container_job.container_id join job_master on container_job.job_id=job_master.job_id where container_master.container_type='".$ctype."' AND job_master.fg_code='".$fgcode."' AND (container_master.con_status='0' OR container_master.con_status='1' OR container_master.con_status='2')";
		    $query1 =$this->db->query($sql1);
			$data = $query1->result();
			
			
			if($data==null)
			{
				
			}
			else {
				$conid=$data['0']->container_id;
				$jobid=$data['0']->job_id;
				$cid=$data['0']->container_job_id;
				$jid['con_id']=$conid;
				$jid['job_id']=$jobid;
				$allararry[] = '"'.$cid.'"';
				$alljid[]=$jid;
			}
			
		}
		$allsearch=array();
		foreach($alljid as $all)
		{
			$conssid = $all['con_id'];
			$jobsssid = $all['job_id'];
			$datasearch ="select container_job_id from container_job where container_id=$conssid";
			$querysearch = $this->db->query($datasearch);
			$resultsearch = $querysearch->result();
			foreach($resultsearch as $rs)
			{
				$allsearch[] = '"'.$rs->container_job_id.'"';
			}
		} 
			
		$diff = array_diff($allsearch, $allararry);
		$result = array_unique($diff);
		foreach($result as $d)
		{
			$sqldelete="delete from container_job where container_job_id=$d";
			$querydelete = $this->db->query($sqldelete);
			
			$sqldelete2="delete from jobscreen_master where container_job_id=$d";
			$querydelete2 = $this->db->query($sqldelete2);
				
		}		
		
	}
	
	
	function insertjob($data)
	{
		$this->db->insert('job_master', $data);
	}
	
	
}
?>

