<?php $this->layout('layouts::default') ?>
<?php
$ci=&get_instance();
$ci->load->helper('home');
?> 
<script src="<?php echo base_url(); ?>assets/pci/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/pci/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/pci/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/pci/responsive.bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable({
    	"ordering": false
    });
} );
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#exampletwo').DataTable({
    	"ordering": false
    });
} );
</script>
<style>
.dataTables_paginate.paging_simple_numbers
{
	text-align: right;
}
.pagination
{
	margin: 0px;
}
.dataTables_filter
{
	text-align: right;
}
#chartdiv {
 width: 50%;
  height: 300px;
  }												
#chartdiv1 {
 
  width: 50%;
  height: 300px;
}
#chartdiv2 {
 
  width: 50%;
  height: 300px;
}
#chartdiv3 {
 
   width: 50%;
  height: 300px;
}
.raphael-group-7-background
{
	background-color: rgb(219, 227, 236)!important;
}
rect
{
	fill:rgb(236,240,245) !important;
}
</style>
<section class="content" id="homecon">
	<div class="col-md-12">
		<H3>For Month: <? echo date('F-Y'); ?> (SKUs Under TPD)</H3>
		<H3>Summary <small>>> For Users</small></H3>
	</div>
	<div class="col-md-12">
		<div id="chartdiv" class="col-md-4" style="margin-top: 15px;"></div>
			<script src="<?php echo base_url(); ?>assets/canvas.js"></script>
			<script src="<?php echo base_url(); ?>assets/pci/pie.js"></script>
			<script src="<?php echo base_url(); ?>assets/pci/light.js"></script>
			<?php $tpd60ansa = get_tpd60_ansanew(); ?> 
<?php $tpd60qc1 = get_tpd60_allnewqc(); ?>
<?php $tpd60qc2 = get_tpd60_allnewqc1(); ?>
<?php $tpd60qc = $tpd60qc1+$tpd60qc2; ?>
<?php $tpd60qa = get_tpd60_allnewqa(); ?>
<?php $tpd60wh = get_tpd60_allnewwh(); ?>
			<script>
				var chart = AmCharts.makeChart( "chartdiv", {
					  "type": "pie",
					  "theme": "light",
					  "titles": [ {
					    "text": "60 TPD",
					    "color":"#333333",
					    "size": 16
					    
					  } ],
  
					  "dataProvider": [ {
					    "bottle": "ANSA",
					    "bottle_no": <?php echo $tpd60ansa; ?>
					    
					    
					  }, {
				    "bottle": "QC",
				    "bottle_no": <?php echo $tpd60qc; ?>
				    
				  }, {
				    "bottle": "QA",
				    "bottle_no": <?php echo $tpd60qa; ?>
				    
				  }, {
				    "bottle": "WH",
				    "bottle_no": <?php echo $tpd60wh; ?>
				    
				  } ],
				  "valueField": "bottle_no",
				  "titleField": "bottle",
				  "startEffect": "elastic",
				  "startDuration": 2,
				  "labelRadius": 15,
				  "innerRadius": "50%",
				  "depth3D": 10,
				  "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
				  "angle": 15,
				  "labelText":"[[title]]:[[value]]",
				  "export": {
				    "enabled": false
				  }
				} );
			</script>
			
		<div id="chart-container" class="col-md-6" style="margin-top: 15px;">
			<?php 	$totalc60 = get_total_container60();
					$detainingtotalc60 = get_total_detainingcontainer60(); 
					$comtotalc60 = get_total_completedcontainer60();
					
					$totalc60p = get_total_container60p(); 
					$detainingtotalc60d = get_total_detainingcontainer60d(); 
	 			 	$comtotalc60c = get_total_completedcontainer60c(); 
			?>
		</div>
		<script type="text/javascript" src="http://static.fusioncharts.com/code/latest/fusioncharts.js"></script>
		<script type="text/javascript" src="http://static.fusioncharts.com/code/latest/themes/fusioncharts.theme.fint.js?cacheBust=56"></script>
		<script type="text/javascript">
			  FusionCharts.ready(function(){
			    var fusioncharts = new FusionCharts({
			    type: 'multilevelpie',
			    renderAt: 'chart-container',
			    id: "myChart",
			    width: '500',
			    height: '500',
			    dataFormat: 'json',
			    dataSource: {
			        "chart": {
			            "showPlotBorder": "1",
			            "piefillalpha": "60",
			            "pieborderthickness": "2",
			            "hoverfillcolor": "#CCCCCC",
			            "piebordercolor": "#FFFFFF",
			            "hoverfillcolor": "#CCCCCC",
			            
			            //Theme
			            "theme": "fint"
			        },
			        "category": [{
			            "label": "Total{br}Qty: <?php echo $comtotalc60; ?>",
			            "color": "#ffffff",
			            "value": "150",
			            "category": [{
			                "label": "Planned {br}Containers {br}till date,{br}<?php echo $totalc60p; ?>",
			                "color": "#f8bd19",
			                "value": "33.3",
			                
			                "category": [{
			                    "label": "<?php echo $totalc60; ?>",
			                    "color": "#926FB2",
			                    "value": "<?php echo $totalc60; ?>"
			                }]
			            }, {
			                "label": "Reported/ {br}Detaining {br}Containers {br}till date,{br}<?php echo $detainingtotalc60d; ?>",
			                "color": "#33ccff",
			                "value": "33.3",
			                
			                "category": [{
			                    "label": "<?php echo $detainingtotalc60; ?>",
			                    "color": "#926FB2",
			                    "value": "<?php echo $detainingtotalc60; ?>"
			                }]
			            }, {
			                "label": "Dispatched {br}Containers {br}till date,{br}<?php echo $comtotalc60c; ?>",
			                "color": "#FF6462",
			                "value": "33.4",
			                
			                "category": [{
			                    "label": "<?php echo $comtotalc60; ?>",
			                    "color": "#926FB2",
			                    "value": "<?php echo $comtotalc60; ?>"
			                }]
			            	}]
			        	}]
			    	}
				}
				);
			    fusioncharts.render();
				});
				</script>
		
	</div>
	

	<!-- Main row -->
	<div class="col-md-12">&nbsp;</div>
    <div class="row">
    	<div class="col-md-12">
        	<div class="row">
            	
			<div class="col-md-12">
            	<div class="box box-danger">
                	<div class="box-header with-border">
                  		<h3 class="box-title">Today's Activity History</h3>
						<div class="box-tools pull-right">
                    		<span class="label label-danger">
                    			<?php $jobcount = get_activitycount(); ?>
                    			<?php echo $jobcount; ?> Activity
                    		</span>
                    		<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    		<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  		</div>
                	</div>
                	<div class="box-body">
                		<table id="exampletwo" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
		         			<thead>
					            <tr>    
					                <th>Sr</th>
					                <th>User</th>
					                <th>Container</th>
					                <th>Jobs</th>
					                <th>Note</th>
					                <th>Date</th>
					                <th>Time</th>
					           	</tr>
		        			</thead>
					        <tfoot>
					            <tr>
					                <th>Sr</th>
					                <th>User</th>
					                <th>Container</th>
					                <th>Jobs</th>
					                <th>Note</th>
					                <th>Date</th>
					                <th>Time</th>
					            </tr>
					        </tfoot>
		        			<tbody>
		        				<?php include('activity.php'); ?>
					        </tbody>
						</table>
              		</div>
                </div>
           	</div>
    	</div>
        
        <div class="box box-info">
        	<div class="box-header with-border">
            	<h3 class="box-title">Recently Planned Containers</h3>
              		<div class="box-tools pull-right">
                		<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                		<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              		</div>
            </div>
            <div class="box-body">
            	<table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
         			<thead>
			            <tr>    
			                <th>Container No</th>
			                <th>Date Planned On</th>
			                <th style="text-align: center">Status</th>
			           	</tr>
        			</thead>
			        <tfoot>
			            <tr>
			                <th>Container No</th>
			                <th>Date Planned On</th>
			                <th style="text-align: center">Status</th>
			            </tr>
			        </tfoot>
        			<tbody>
        			<?php include("planned_containers.php"); ?>    
					</tbody>
				</table>	            	
        	</div>
      	</div>
	</div>
	</div>
</section>