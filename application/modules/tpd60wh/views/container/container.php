<?php
$ci=&get_instance();
$ci->load->helper('container');
?>
<style>
table 	
{
border-collapse: collapse;
}
table, td, th 
{
	border: 1px solid white;
}
.t
{
	width:100%;
}
</style>
<!-- Default box -->
	<div class="col-md-12 text-right">
		<a href="<?php echo base_url(); ?>tpd60wh/containerscreen/completed" class="btn btn-primary">Completed Containers</a>
	</div>
	<div class="box-body no-padding">
    	<table class="table table-striped">
        	<tbody>
        		<tr>
                  	<th style="width: 10px">#</th>
                  	<th><center><h3 style="font-weight: bold">Name</h3></center></th>
                  	<th><center><h3 style="font-weight: bold">Jobs</h3></center></th>
                  	<th>
                  		<div class="row">
                  			<div class="col-md-3">
                  				<center><h3 style="font-weight: bold">ANSA Dept.</h3></center>		
                  			</div>
                  			<div class="col-md-3">
                  				<center><h3 style="font-weight: bold">QC Dept.</h3></center>		
                  			</div>
                  			<div class="col-md-3">
                  				<center><h3 style="font-weight: bold">PDI/QA</h3></center>		
                  			</div>
                  			<div class="col-md-3">
                  				<center><h3 style="font-weight: bold">Warehouse</h3></center>	
                  			</div>
                  		</div>	
					</th>
                 </tr>
                 	<?php 	$ii =1;
                 	?>
                	<?php foreach($result as $r) { ?>
                		
                	<?php $check = get_job_status($r->container_id); ?>
                	<?php $chk = array(); ?>
                	<?php foreach($check as $c) { ?>
                			<?php $chk[] = $c->job_status; ?>
                	<?php } ?>	
                	
                	<?php if (in_array("0", $chk)) { ?>	
					<tr>
                  		<td><?php echo $ii; ?>.<?php $ii++; ?></td>
                  		<td style="width: 10%">
                  			<h2 class="bg-blue"><?php echo $r->container_type; ?></h2> 
                  			<strong>Dt: <?php echo date ("d-m-Y",strtotime($r->container_date)); ?></strong> 
                  			<span class="badge bg-red">
                  			<?php	$avg = array(); 
                  				 	$avarge = get_job_status($r->container_id); 
                  				 	foreach($avarge as $aa) 
                  				 	{
                  						if($aa->decoration == '0')
										{ 
	                  						$qcl = (($aa->qc_completed*100)/$aa->qty); 	
	                     					$qal = (($aa->qa_completed*100)/$aa->qty); 
	                     					$whl = (($aa->wh_completed*100)/$aa->qty); 
	                     					$totalp = (($qcl+$qal+$whl)/3); 
	                  						$avg[] = round($totalp);
										}
										elseif($aa->decoration == '1')
										{
											$qcl = (($aa->qc_completed*100)/$aa->qty); 	
	                     					$qal = (($aa->qa_completed*100)/$aa->qty); 
	                     					$whl = (($aa->wh_completed*100)/$aa->qty); 
	                     					$ansal = (($aa->ansa_completed*100)/$aa->qty); 
	                  						$totalp = (($qcl+$qal+$whl+$ansal)/4); 
	                  						$avg[] = round($totalp);
											
										}	
                  					}
                  				$divid = count($avg); 
                  				$all = array_sum($avg); 
                  				echo round($all/$divid).'%';
                  			 ?>		
                  			</span>
                  			<small><?php if($r->con_status == 0) { ?>
                  							<label class="label label-warning">Booked</label>
                  					<?php } elseif ($r->con_status == 1) { ?>
                  							<label class="label label-success">Reported</label>
                  					<?php } elseif ($r->con_status == 2) { ?>
                  							<label class="label label-danger">Detaining</label>
                  					<?php } ?>		 
                  			</small>
                  		</td>
                  		
                  		<td>
                  			<div class="info-box bg-purple">
                  			<table class="t" style="border-color: white">
                            	<tr>
                                	<th>&nbsp;</th>
                               	</tr>
                                <?php  $job = get_job_name_by_con($r->container_id); ?>
                                <?php foreach($job as $j) { ?>
                                <tr>
                                	<td><?php echo $j->job_name; ?></td>
                                </tr>
                         		<?php } ?>
                         	</table>
                         	</div>
                  		</td>
                  		
                  		
                  		<td>
                  			<div class="row">
                  				
                  				<div class="col-md-3">   
                        			<?php 	$ansaavg = array(); 
                  				 			$ansaavarge = get_job_status($r->container_id); 
                  				 			foreach($ansaavarge as $ansaaa) 
                  				 			{
                  				 				if($ansaaa->decoration=='1') { 
                  								$ansatotal = (($ansaaa->ansa_completed*100)/$ansaaa->qty); 	
                     							$ansaavg[] = round($ansatotal);
												}
												else
												{
													$ansaavg[] = 100;
												}			
                  				 			}
                  							$ansadivid = count($ansaavg); 
                  							$ansaall = array_sum($ansaavg);
                  							$ansatotal = ($ansaall/$ansadivid); 
                  					?>
                  					<?php if($ansatotal < 25) { ?>
                  					<div class="info-box bg-red">
                    	            <?php } elseif($ansatotal == 100) {  ?>
                    	            <div class="info-box bg-green">	
                    	            <?php } else { ?>
                    	            <div class="info-box bg-yellow">	
                    	            <?php } ?>
                            			<table class="t" style="border-color: white">
                            				<tr>      		
		                                  		<th>Ready</th>
		                                  		<th>Pending</th>
		                                  	</tr>
		                                  	<?php $jobstatus = get_job_status($r->container_id); ?>
			                                <?php foreach($jobstatus as $js) {
			                                 if($js->decoration=='1') { ?>
			                                <tr>
			                                	<td><?php echo $js->ansa_completed; ?></td>
			                                	<td><?php echo $js->qty-$js->ansa_completed; ?></td>
			                                </tr>
			                                <?php }else { ?>
			                                <tr>
			                                	<td>-</td>
			                                	<td>-</td>
			                                </tr>
			                                <?php } } ?>
                         				</table>
                        			</div>
                     			</div>
                    			
                    			<div class="col-md-3">
                    				<?php 	$qcavg = array(); 
                  				 			$qcavarge = get_job_status($r->container_id); 
                  				 			foreach($qcavarge as $qcaa) 
                  				 			{ 
                  								$qctotal = (($qcaa->qc_completed*100)/$qcaa->qty); 	
                     							$qcavg[] = round($qctotal);
                  				 			}
                  							$qcdivid = count($qcavg); 
                  							$qcall = array_sum($qcavg);
                  							$qctotal = ($qcall/$qcdivid); 
                  					?>
                  					<?php if($qctotal < 25) { ?>
                  					<div class="info-box bg-red">
                    	            <?php } elseif($qctotal == 100) {  ?>
                    	            <div class="info-box bg-green">	
                    	            <?php } else { ?>
                    	            <div class="info-box bg-yellow">	
                    	            <?php } ?>		
                    	            <table class="t" style="border-color: white">
			                            	<tr>  		
			                                  	<th>Ready</th>
			                                  	<th>Pending</th>
			                               	</tr>
			                               	<?php $jobstatus = get_job_status($r->container_id); ?>
			                                <?php foreach($jobstatus as $js) { ?>
			                                <tr>
			                                	<td><?php echo $js->qc_completed; ?></td>
			                                	<td><?php echo $js->qty-$js->qc_completed; ?></td>
			                                </tr>
			                                <?php } ?>
                         				</table>               
                            		</div>
                    	 		</div>   
                     
                     			<div class="col-md-3">
                        			<?php 	$qaavg = array(); 
                  				 			$qaavarge = get_job_status($r->container_id); 
                  				 			foreach($qaavarge as $qaaa) 
                  				 			{ 
                  								$qatotal = (($qaaa->qa_completed*100)/$qaaa->qty); 	
                     							$qaavg[] = round($qatotal);
                  				 			}
                  							$qadivid = count($qaavg); 
                  							$qaall = array_sum($qaavg);
                  							$qatotal = ($qaall/$qadivid); 
                  					?>
                  					<?php if($qatotal < 25) { ?>
                  					<div class="info-box bg-red">
                    	            <?php } elseif($qatotal == 100) {  ?>
                    	            <div class="info-box bg-green">	
                    	            <?php } else { ?>
                    	            <div class="info-box bg-yellow">	
                    	            <?php } ?>
                            			<table class="t" style="border-color: white">
		                                  	<tr>      		
		                                  		<th>Ready</th>
		                                  		<th>Pending</th>
		                                  	</tr>
		                                  	<?php $jobstatus = get_job_status($r->container_id); ?>
			                                <?php foreach($jobstatus as $js) { ?>
			                                <tr>
			                                	<td><?php echo $js->qa_completed; ?></td>
			                                	<td><?php echo $js->qty-$js->qa_completed; ?></td>
			                                </tr>
			                                <?php } ?>
                     					</table>
                     				</div>
                     			</div>   
                        
                        		<div class="col-md-3">
                        			<?php 	$whavg = array(); 
                  				 			$whavarge = get_job_status($r->container_id); 
                  				 			foreach($whavarge as $whaa) 
                  				 			{ 
                  								$whtotal = (($whaa->wh_completed*100)/$whaa->qty); 	
                     							$whavg[] = round($whtotal);
                  				 			}
                  							$whdivid = count($whavg); 
                  							$whall = array_sum($whavg);
                  							$whtotal = ($whall/$whdivid); 
                  					?>
                  					<?php if($whtotal < 25) { ?>
                  					<div class="info-box bg-red">
                    	            <?php } elseif($whtotal == 100) {  ?>
                    	            <div class="info-box bg-green">	
                    	            <?php } else { ?>
                    	            <div class="info-box bg-yellow">	
                    	            <?php } ?>
                            			<table class="t" style="border-color: white">
		                                  	<tr>
		                                  		<th>Ready</th>
		                                  		<th>Pending</th>
		                                  	</tr>
		                                  	<?php $jobstatus = get_job_status($r->container_id); ?>
			                                <?php foreach($jobstatus as $js) { ?>
			                                <tr>
			                                	<td><?php echo $js->wh_completed; ?></td>
			                                	<td><?php echo $js->qty-$js->wh_completed; ?></td>
			                                </tr>
			                                <?php } ?>
                            			</table>           
                        			</div>
                     			</div>  
                    		</div> 
                  		</td>
                  		
                  	</tr>
                <?php } else { ?>	
                <?php } ?>	
               <?php } ?>
               </tbody>
       	</table>
	</div> 
