<?php $this->layout('layouts::default') ?>
<?php
$ci=&get_instance();
$ci->load->helper('container');
?>
<script src="<?php echo base_url(); ?>assets/pci/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/pci/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/pci/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/pci/responsive.bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable({
    	"ordering": false
    });
} );
</script>
<style>
.dataTables_paginate.paging_simple_numbers
{
	text-align: right;
}
.pagination
{
	margin: 0px;
}
.dataTables_filter
{
	text-align: right;
}
</style>
<div class="box" id="auto_load_div">
	<div class="col-md-12 text-right"><a href="<?php echo base_url(); ?>tpd60wh/containerscreen" class="btn btn-primary">Running Containers</a></div>
	<div class="box-body no-padding">
		<hr />
		<table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
        	<thead>
				<tr>    
			    	<th>Container Name</th>
			        <th>Jobs</th>
			        <th>Date Planned On</th>
			        <th>Date Completed</th>
			 	</tr>
        	</thead>
			<tfoot>
				<tr>
			    	<th>Container No</th>
			        <th>Jobs</th>
			        <th>Date Planned On</th>
			        <th>Date Completed</th>
			 	</tr>
			</tfoot>
        	<tbody>
        		<?php $result = completed_container(); ?>
        		<?php foreach($result as $r) { ?>
        		<tr>
        			<td><?php echo $r->container_type; ?></td>
        			<td>
        				<?php  $job = get_job_name_by_con($r->container_id); ?>
                   		<?php foreach($job as $j) { ?>
                    		<?php echo $j->job_name; ?>,<br />
                    	<?php } ?>
                    </td>
        			<td><?php echo date ("j M,y",strtotime($r->container_date)); ?></td>
        			<td>
        				<?php echo date ("j M,y",strtotime($r->completed_date)); ?>
        			</td>			
        		</tr>
      			<?php }?>
        	</tbody>
        </table>			
	</div>	
</div>