<?php $this->layout('layouts::default') ?>
<?php
$ci=&get_instance();
$ci->load->helper('home');
?> 
<script src="<?php echo base_url(); ?>assets/pci/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/pci/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/pci/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/pci/responsive.bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable({
    	"ordering": false
    });
} );
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#exampletwo').DataTable({
    	"ordering": false
    });
} );
</script>
<style>
.dataTables_paginate.paging_simple_numbers
{
	text-align: right;
}
.pagination
{
	margin: 0px;
}
.dataTables_filter
{
	text-align: right;
}
#chartdiv {
 width: 50%;
  height: 300px;
  }												
#chartdiv1 {
 
  width: 50%;
  height: 300px;
}
#chartdiv2 {
 
  width: 50%;
  height: 300px;
}
#chartdiv3 {
 
   width: 50%;
  height: 300px;
}
.raphael-group-7-background
{
	background-color: rgb(219, 227, 236)!important;
}
rect
{
	fill:rgb(236,240,245) !important;
}
</style>
<section class="content" id="homecon">
	<div class="col-md-12">
		<H3>For Month: <? echo date('F-Y'); ?> (SKUs Under TPD)</H3>
		<H3>Summary <small>>> For Users</small></H3>
	</div>
	<div class="col-md-12">
		<div id="chartdiv" class="col-md-4" style="margin-top: 15px;"></div>
   			<script src="<?php echo base_url(); ?>assets/canvas.js"></script>
			<script src="<?php echo base_url(); ?>assets/pci/pie.js"></script>
			<script src="<?php echo base_url(); ?>assets/pci/light.js"></script>	
			<?php $tpds100ansa = get_tpd100_monthnew(); ?>
			<?php $tpds100qc1 = get_tpd100_monthallnewqc();  ?>
			<?php $tpds100qc2 = get_tpd100_monthallnewqc1();  ?>
			<?php $tpds100qc = $tpds100qc1+$tpds100qc2; ?>
			<?php $tpds100qa = get_tpd100_monthallnewqa(); ?>
			<?php $tpds100wh = get_tpd100_monthallnewwh(); ?>
			<script>
				var chart = AmCharts.makeChart( "chartdiv", {
					  "type": "pie",
					  "theme": "light",
					  "titles": [ {
					    "text": "100 TPD",
					    "color":"#333333",
					    "size": 16
					    
					  } ],
  
					  "dataProvider": [ {
					    "bottle": "ANSA",
					    "bottle_no": <?php echo $tpds100ansa; ?>
					    
					    
					  }, {
				    "bottle": "QC",
				    "bottle_no": <?php echo $tpds100qc; ?>
				    
				  }, {
				    "bottle": "QA",
				    "bottle_no": <?php echo $tpds100qa; ?>
				    
				  }, {
				    "bottle": "WH",
				    "bottle_no": <?php echo $tpds100wh; ?>
				    
				  } ],
				  "valueField": "bottle_no",
				  "titleField": "bottle",
				  "startEffect": "elastic",
				  "startDuration": 2,
				  "labelRadius": 15,
				  "innerRadius": "50%",
				  "depth3D": 10,
				  "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
				  "angle": 15,
				  "labelText":"[[title]]:[[value]]",
				  "export": {
				    "enabled": false
				  }
				} );
			</script>
			
    	<div id="chart-container" class="col-md-6" style="margin-top: 15px;">
			<?php 	$totalc100 = get_total_container100();
					$detainingtotalc100 = get_total_detainingcontainer100(); 
			 		$comtotalc100 = get_total_completedcontainer100(); 
			 	
					$totalc100p = get_total_container100p();
					$detainingtotalc100d = get_total_detainingcontainer100d();
					$comtotalc100c = get_total_completedcontainer100c();
			 ?>
		</div>
		<script type="text/javascript" src="http://static.fusioncharts.com/code/latest/fusioncharts.js"></script>
		<script type="text/javascript" src="http://static.fusioncharts.com/code/latest/themes/fusioncharts.theme.fint.js?cacheBust=56"></script>
		<script type="text/javascript">
			  FusionCharts.ready(function(){
			    var fusioncharts = new FusionCharts({
			    type: 'multilevelpie',
			    renderAt: 'chart-container',
			    id: "myChart",
			    width: '500',
			    height: '500',
			    dataFormat: 'json',
			    dataSource: {
			        "chart": {
			            "showPlotBorder": "1",
			            "piefillalpha": "60",
			            "pieborderthickness": "2",
			            "hoverfillcolor": "#CCCCCC",
			            "piebordercolor": "#FFFFFF",
			            "hoverfillcolor": "#CCCCCC",
			            
			            //Theme
			            "theme": "fint"
			        },
			        "category": [{
			            "label": "Total{br}Qty: <?php echo $comtotalc100; ?>",
			            "color": "#ffffff",
			            "value": "150",
			            "category": [{
			                "label": "Planned {br}Containers {br}till date,{br}<?php echo $totalc100p; ?>",
			                "color": "#f8bd19",
			                "value": "33.3",
			                
			                "category": [{
			                    "label": "<?php echo $totalc100; ?>",
			                    "color": "#F0E68C",
			                    "value": "<?php echo $totalc100; ?>"
			                }]
			            }, {
			                "label": "Reported/ {br}Detaining {br}Containers {br}till date,{br}<?php echo $detainingtotalc100d; ?>",
			                "color": "#33ccff",
			                "value": "33.3",
			                
			                "category": [{
			                    "label": "<?php echo $detainingtotalc100; ?>",
			                    "color": "#F0E68C",
			                    "value": "<?php echo $detainingtotalc100; ?>"
			                }]
			            }, {
			                "label": "Dispatched {br}Containers {br}till date,{br}<?php echo $comtotalc100c; ?>",
			                "color": "#FF6462",
			                "value": "33.4",
			                
			                "category": [{
			                    "label": "<?php echo $comtotalc100; ?>",
			                    "color": "#F0E68C",
			                    "value": "<?php echo $comtotalc100; ?>"
			                }]
			            	}]
			        	}]
			    	}
				}
				);
			    fusioncharts.render();
				});
				</script>
 		
	</div>
	
	<!-- Main row -->
	<div class="col-md-12">&nbsp;</div>
    <div class="row">
    	<div class="col-md-12">
        	<div class="row">
            	<!--div class="col-md-4">
	            	<div class="box box-warning direct-chat direct-chat-warning" style="height: 380px;">
		                <div class="box-header with-border">
		                  	<h3 class="box-title">Direct Messages</h3>
							<div class="box-tools pull-right">
                    			<span data-toggle="tooltip" title="3 New Messages" class="badge bg-yellow">3</span>
                    			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    			<button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle"><i class="fa fa-comments"></i></button>
                    			<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  			</div>
                		</div>
                		<div class="box-body">
                  			<div class="direct-chat-messages">
                    			<div class="direct-chat-msg">
                      				<div class="direct-chat-info clearfix">
                        				<span class="direct-chat-name pull-left">Chiren Desai</span>
                        				<span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
                      				</div>
                      				<img class="direct-chat-img" src="dist/img/user1-128x128.jpg" alt="message user image">
                      				<div class="direct-chat-text">
                        				Is the container MAT-512 Ready to dispatch?
                      				</div>
                      			</div>
                    			<div class="direct-chat-msg right">
			                    	<div class="direct-chat-info clearfix">
			                        	<span class="direct-chat-name pull-right">Sarah Bullock</span>
                        				<span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span>
                      				</div>
                      				<img class="direct-chat-img" src="dist/img/user3-128x128.jpg" alt="message user image">
				                    <div class="direct-chat-text">
				                    	No We have a blockage from the TPD-45 Wharehouse. Can you please check?
                      				</div>
                      			</div>
                    	        <div class="direct-chat-msg">
			                      	<div class="direct-chat-info clearfix">
			                        	<span class="direct-chat-name pull-left">Alexander Pierce</span>
                        				<span class="direct-chat-timestamp pull-right">23 Jan 5:37 pm</span>
                      				</div>
                      				<img class="direct-chat-img" src="dist/img/user1-128x128.jpg" alt="message user image">
                      					<div class="direct-chat-text">
                        					Ok let me check. Get back to you in a while.
                      					</div>
                      			</div>
                                <div class="direct-chat-msg right">
			                      	<div class="direct-chat-info clearfix">
			                        	<span class="direct-chat-name pull-right">Sarah Bullock</span>
			                        	<span class="direct-chat-timestamp pull-left">23 Jan 6:10 pm</span>
			                      	</div>
                      				<img class="direct-chat-img" src="dist/img/user3-128x128.jpg" alt="message user image">
				                      	<div class="direct-chat-text">
				                        	Great.. Thanks..!!
				                      	</div>
                      			</div>
                    	   	</div>
                 			<div class="direct-chat-contacts" >
		                    	<ul class="contacts-list">
                      				<li>
                        				<a href="#">
                          				<img class="contacts-list-img" src="dist/img/user1-128x128.jpg" alt="User Image">
                          				<div class="contacts-list-info">
			                                <span class="contacts-list-name">
			                                  Dharmendra Raninga
			                                  <small class="contacts-list-date pull-right">2/28/2015</small>
			                                </span>
                            				<span class="contacts-list-msg">Bottles are ready for MAT32</span>
                          				</div>
                          				</a>
                      				</li>
                      				<li>
                       		 			<a href="#">
                          				<img class="contacts-list-img" src="dist/img/user8-128x128.jpg" alt="User Image">
                          				<div class="contacts-list-info">
			                                <span class="contacts-list-name">
			                                  Parth J.
			                                  <small class="contacts-list-date pull-right">1/4/2015</small>
			                                </span>
                            				<span class="contacts-list-msg">Ok sir will update you on that...</span>
                          				</div>
                          				</a>
                      				</li>
                      				<li>
				                        <a href="#">
				                          	<img class="contacts-list-img" src="dist/img/user6-128x128.jpg" alt="User Image">
											<div class="contacts-list-info">
					                            <span class="contacts-list-name">
					                            Saurabh Mukherjee
					                            	<small class="contacts-list-date pull-right">1/27/2015</small>
				                                </span>
				                            	<span class="contacts-list-msg">Can I take a look at...</span>
				                          	</div>
				                     	</a>
				                  	</li>
                      				<li>
				                    	<a href="#">
				                        <img class="contacts-list-img" src="dist/img/user8-128x128.jpg" alt="User Image">
										<div class="contacts-list-info">
				                        	<span class="contacts-list-name">
				                                  Kenneth M.
				                                  <small class="contacts-list-date pull-right">1/4/2015</small>
				                            </span>
				                            <span class="contacts-list-msg">Never mind I found...</span>
				                      	</div>
				                        </a>
                      				</li>
                      			</ul>
                    		</div>
                  		</div>
                		
                		<div class="box-footer">
                  			<form action="#" method="post">
                    			<div class="input-group">
	                      			<input type="text" name="message" placeholder="Type Message ..." class="form-control">
	                          		<span class="input-group-btn">
	                            		<button type="button" class="btn btn-warning btn-flat">Send</button>
	                          		</span>
                    			</div>
                  			</form>
                		</div>
                	</div>
            </div-->
			<div class="col-md-12">
            	<div class="box box-danger">
                	<div class="box-header with-border">
                  		<h3 class="box-title">Today's Activity History</h3>
						<div class="box-tools pull-right">
                    		<span class="label label-danger">
                    			<?php $jobcount = get_activitycount(); ?>
                    			<?php echo $jobcount; ?> Activity
                    		</span>
                    		<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    		<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  		</div>
                	</div>
                	<div class="box-body">
                		<table id="exampletwo" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
		         			<thead>
					            <tr>    
					                <th>Sr</th>
					                <th>User</th>
					                <th>Container</th>
					                <th>Jobs</th>
					                <th>Note</th>
					                <th>Date</th>
					                <th>Time</th>
					           	</tr>
		        			</thead>
					        <tfoot>
					            <tr>
					                <th>Sr</th>
					                <th>User</th>
					                <th>Container</th>
					                <th>Jobs</th>
					                <th>Note</th>
					                <th>Date</th>
					                <th>Time</th>
					            </tr>
					        </tfoot>
		        			<tbody>
		        				<?php include('activity.php'); ?>
					        </tbody>
						</table>
              		</div>
                </div>
           	</div>
    	</div>
        
        <div class="box box-info">
        	<div class="box-header with-border">
            	<h3 class="box-title">Recently Planned Containers</h3>
              		<div class="box-tools pull-right">
                		<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                		<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              		</div>
            </div>
            <div class="box-body">
            	<table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
         			<thead>
			            <tr>    
			                <th>Container No</th>
			                <th>Date Planned On</th>
			                <th style="text-align: center">Status</th>
			           	</tr>
        			</thead>
			        <tfoot>
			            <tr>
			                <th>Container No</th>
			                <th>Date Planned On</th>
			                <th style="text-align: center">Status</th>
			            </tr>
			        </tfoot>
        			<tbody>
        			<?php include("planned_containers.php"); ?>    
					</tbody>
				</table>	            	
        	</div>
      	</div>
	</div>
	</div>
</section>