<?php 

function get_containers()
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_master ORDER BY createdOn DESC";
	$query=$ci->db->query($sql);
	return $query->result();
}

function get_con_part($id)
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_job join jobscreen_master on jobscreen_master.container_job_id=container_job.container_job_id where container_id=$id";
	$query=$ci->db->query($sql);
	return $query->result();
}

function get_activity()
{
	$ci=&get_instance();
	$sql="SELECT activity.* FROM activity join tpd_master on tpd_master.id=activity.tpd where tpd_master.name ='100' ORDER BY a_id DESC";
	$query=$ci->db->query($sql);
	return $query->result();
}

function get_user($id)
{
	$ci=&get_instance();
	$sql="SELECT first_name FROM users where id=$id";
	$query=$ci->db->query($sql);
	return $query->result();
}

function get_jobs($id)
{
	$ci=&get_instance();
	$sql="SELECT job_name FROM job_master where job_id=$id";
	$query=$ci->db->query($sql);
	return $query->result();
}

function get_containe($id)
{
	$ci=&get_instance();
	$sql="SELECT container_type FROM container_master where container_id=$id";
	$query=$ci->db->query($sql);
	return $query->result();
}

function get_activitycount()
{
	$ci=&get_instance();
	$sql="SELECT * FROM activity where DATE(date)=DATE(NOW())";
	$query=$ci->db->query($sql);
	return $query->num_rows();
}

function get_tpd100_month()
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_master join container_job on container_job.container_id=container_master.container_id join tpd_master on tpd_master.id=container_job.tpd where MONTH(container_master.container_date)=MONTH(NOW()) AND container_master.container_name='ANSA' AND tpd_master.name='100'";
	$query=$ci->db->query($sql);
	return $query->result();
}

function get_tpd100ansa($id)
{
	$ci=&get_instance();
	$sql="SELECT * FROM jobscreen_master join container_job on container_job.container_job_id=jobscreen_master.container_job_id where container_job.container_id=$id";
	$query=$ci->db->query($sql);
	return $query->result();
}

function get_tpd100_monthall()
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_master join container_job on container_job.container_id=container_master.container_id join tpd_master on tpd_master.id=container_job.tpd where MONTH(container_master.container_date)=MONTH(NOW()) AND tpd_master.name='100'";
	$query=$ci->db->query($sql);
	return $query->result();
}


function get_total_container()
{
	$ci=&get_instance();
	$sql="SELECT container_id FROM container_master where MONTH(container_date)=MONTH(NOW())";
	$query=$ci->db->query($sql);
	return $query->result();
}

function get_total_container100()
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_master join container_job on container_master.container_id=container_job.container_id join tpd_master on container_job.tpd=tpd_master.id where MONTH(container_master.container_date)=MONTH(NOW()) AND tpd_master.name='100'";
	$query=$ci->db->query($sql);
	return $query->num_rows();
}


function get_total_completedcontainer()
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_master where MONTH(container_date)=MONTH(NOW()) AND con_status='4'";
	$query=$ci->db->query($sql);
	return $query->result();
}

function get_total_completedcontainer100()
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_master join container_job on container_master.container_id=container_job.container_id join tpd_master on container_job.tpd=tpd_master.id where MONTH(container_master.container_date)=MONTH(NOW()) AND container_master.con_status='4' AND tpd_master.name='100'";
	$query=$ci->db->query($sql);
	return $query->num_rows();
}

function get_total_detainingcontainer()
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_master where MONTH(container_date)=MONTH(NOW()) AND con_status='2'";
	$query=$ci->db->query($sql);
	return $query->result();	
}

function get_total_detainingcontainer100()
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_master join container_job on container_master.container_id=container_job.container_id join tpd_master on container_job.tpd=tpd_master.id where MONTH(container_master.container_date)=MONTH(NOW()) AND (container_master.con_status='2' OR container_master.con_status='1') AND tpd_master.name='100'";
	$query=$ci->db->query($sql);
	return $query->num_rows();	
}
function get_tpd100_monthnew()
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_master join container_job on container_job.container_id=container_master.container_id join tpd_master on tpd_master.id=container_job.tpd where MONTH(container_master.container_date)=MONTH(NOW()) AND container_job.decoration='1' AND tpd_master.name='100'";
	$query=$ci->db->query($sql);
	return $query->num_rows();
}
function get_tpd100_monthallnew()
{
	$ci=&get_instance();
	$sql="SELECT * FROM container_master join container_job on container_job.container_id=container_master.container_id join tpd_master on tpd_master.id=container_job.tpd where MONTH(container_master.container_date)=MONTH(NOW()) AND tpd_master.name='100'";
	$query=$ci->db->query($sql);
	return $query->num_rows();
}
?>