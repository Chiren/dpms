<?php
$ci=&get_instance();
$ci->load->helper('common');
date_default_timezone_set('Asia/Calcutta');
?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/pci/jquery-ui.css">
<script src="<?php echo base_url(); ?>assets/pci/jquery-ui.js"></script> 
<style>
	.info-box-content
	{
		margin: 0px !important;
	}
	#country-list {
    float: left;
    list-style: outside none none;
    margin: 0;
    padding: 0;
	}
	#country-list > li {
    cursor: pointer;
    padding: 2px 0;
	}
	.ui-menu.ui-widget.ui-widget-content.ui-autocomplete.ui-front
    {
        z-index: 999999!important;
        
    }
    h5{
    	margin-top:5px!important;
    	margin-bottom:5px!important;
    }
</style>
<script>
			$(document).ready(function () {
  
  				$(".number").keypress(function (e) {
    
   	  			if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
   	     			return false;
   				}

   				});
			});
		</script>        
        <div class="box-body no-padding">
        		 
              <table class="table table-striped">
              	<thead>
              		<tr>
              			
              			<th>
              				<center><h3 style="font-weight: bold">Job Name</h3></center>	
              			</th>
              			<th>
              				<div class="col-md-3"><center><h3 style="font-weight: bold">ANSA</h3></center></div>
              				<div class="col-md-3"><center><h3 style="font-weight: bold">QC</h3></center></div>
              				<div class="col-md-3"><center><h3 style="font-weight: bold">QA</h3></center></div>
              				<div class="col-md-3"><center><h3 style="font-weight: bold">WH</h3></center></div>	
              			</th>
              			<th>
              				<center><h3 style="font-weight: bold">Status</h3></center>
              			</th>
              			
              		</tr>
              	</thead>
                <tbody>
                	<?php foreach($result as $r){ ?>
                	
                	<?php if($r->container_name != 'ANSA') { ?>
                	             	
                	<!-- if check the ANSA or Not  -->	
                	<?php if($r->decoration == 1) { ?>  <!-- ANSA Yes -->	
                	
                	<tr>
                  		<td class="col-md-2">
                  			<h4 class="bg-blue" style="width:100%"><?php echo $r->job_name; ?></h4> 
                  			<div class="col-md-8"><h5>TPD: <?php $tpd = get_tpd_by_id($r->tpd); foreach($tpd as $t) { echo $t->name; } ?> | QTY: <?php echo $r->qty; ?></a></h5></div>
                  			<div class="col-md-4"><h5><?php echo $r->container_type; ?></h5></div>
                  			<div class="col-md-8"><h5><?php echo $r->customer_name; ?></h5></div>
                  			<div class="col-md-4"><h5><?php echo date ("j M,y",strtotime($r->container_date)); ?></h5></div>
                  			
                  		</td> 
                  
                 	 	<td class="col-md-9">
                 	 		<?php $qcl = (($r->qc*100)/$r->qty); ?>	
                     		<?php $qal = (($r->qa_completed*100)/$r->qty); ?>
                     		<?php $whl = (($r->wh_completed*100)/$r->qty); ?>
                     		<?php $ansal = (($r->ansa*100)/$r->qty); ?>
                  			<div class="row">
                  			
                  			
                  			<!--  ANSA is less then 25%   -->
                  			
                  			<?php if($ansal < 25) { ?>	
                  				
                  				<div class="col-md-3">
                       				<div class="info-box bg-red" data-toggle="modal" data-target="#ANSA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qty-$r->ansa_completed; ?></span>                              
                							<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $ansal; ?>%"></div>
                              					</div>
                                 		 		<span class="progress-description">
                                    			Updated: <?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																{ echo "-"; }
																else {
                                    							$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																}
                                    							?>
                                  				</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="ANSA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - ANSA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
              											<label>Pendding Products :- <?php echo $r->qty-$r->ansa_completed; ?></label>
                										<input type="hidden" id="ansa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                										<div class="form-group">
                											<input type="hidden" id="ansa2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->ansa; ?>" />
                  											<label for="examplecomment">Number of pcs Pass</label>
                  											<input type="text" id="ansa3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control number" />
                										</div>
										              </div>
            										</form>
              									</div>
              									<div class="modal-footer">
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="ansa4<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Save Changes</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
        						<script>  
                                    $('#ansa3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->qty-$r->ansa_completed; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                             
                                    }
                                    });
                                </script>
                     			<script>
               						$(function(){
												$("#ansa4<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var id1=$("#ansa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var ansa1=$("#ansa2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var ansa2=$("#ansa3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {id:id1,ansa1:ansa1,ansa2:ansa2}
									$.ajax({
									'url':'<?php echo base_url();?>ansa/jobscreen/ansaadd1',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               					</script>
                     			
                    			<div class="col-md-3">
                       				<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->ansa_completed-$r->qc_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qcl; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php  if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  					</span>
                                 	 			</div>
                            			</div>
                        			</div>
                     			</div>
                     	
                     			<!-- QA is Black  -->
                     			<!-- QA Condition -->
								<?php if($r->qa > $r->qa_completed) { ?>  
									<!-- QA is Purple  -->
									<?php if($r->qa_rem !=0) { ?>
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal"  id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	
									<?php } elseif($r->qa_rem == '0') { ?>
										<!-- QA is Blue  -->
									<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal"  id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   		
									<?php } else { ?>		                   	
                     				<!-- QA is RED  -->
                     				<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal"  id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	
               						<?php } ?>		
               						
               					<!-- QA is Green -->	   
                     			 <?php } elseif($r->qa == $r->qa_completed && $r->qa_completed != 0) { ?>  
                     			   
                     			<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			
                     			<?php } elseif($r->qa_completed == 0) { ?>
                     			   
                     			   <div class="col-md-3">
                       				<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			   
                     			<?php } ?>   
                     			
                     			<?php if($r->wh < $r->wh_completed) { ?>   
                    			    		
                        		<?php if($whl < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($whl == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh ==  $r->wh_completed) { ?>
                     				
                     			<?php if($whl == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
        						<?php } elseif($r->wh > $r->wh_completed) { ?>
        							
        						<?php if($whl < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php      if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                				                				
                				<?php } elseif($whl == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qty-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php      if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<?php } ?>
        							
        						<?php } ?>	
                				
                	
                	
                	<!--  ANSA is 100% Completed  -->
                	
                	<?php } elseif($ansal == 100) { ?>
                		 		
                		<div class="col-md-3">
                       		<div class="info-box bg-green" id="qc-mat-81" >
                            	<div class="info-box-content">
                              		<span class="info-box-text"><?php echo $r->qty-$r->ansa_completed; ?></span>                              
                					<div class="progress1">
                              			<div class="progress">
                                			<div class="progress-bar" style="width: <?php echo $ansal; ?>%"></div>
                              			</div>
                                 		<span class="progress-description">
                                    			Updated: <?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																{ echo "-"; }
																else {
                                    							$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																}
                                    				      ?>
                                  		</span>
                                 	 </div>
                            	</div>
                        	</div>
                     	</div>
                     	
                     	<!-- Qc is less then 25% -->
                     			
                 		<?php if($r->qc_completed < $r->ansa_completed) { ?>
                     		
                     		<!-- QC  -->
                     		<div class="col-md-3">
                       			<div class="info-box bg-red" data-toggle="modal"  id="qc-mat-81" >
                            		<div class="info-box-content">
                              			<span class="info-box-text"><?php echo $r->ansa_completed-$r->qc_completed; ?></span>                              
                						<div class="progress1">
                              				<div class="progress">
                                				<div class="progress-bar" style="width: <?php echo $qcl; ?>%"></div>
                              				</div>
                                 		 	<span class="progress-description">
                                    		Updated: <?php  if($r->qc_updated == '0000-00-00 00:00:00')
															{ echo "-"; }
															else {
                                     							$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
															}
                                    						?><br />
                                    						<?php echo $r->qc_comment; ?>
                                  			</span>
                                 	 	</div>
                            		</div>
                        		</div>
                     		</div>
                     		
               				
                     		<!-- QA  -->
                     		<div class="col-md-3">
                        		<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                              			<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
				                		<div class="progress1">
				                        	<div class="progress">
				                            	<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
				                            </div>
				                            <span class="progress-description">
				                            	Updated: <?php  if($r->qa_updated == '0000-00-00 00:00:00')
																{ echo "-"; }
																else 
																{
                                    							$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																}
															?>
				                          	</span>
                                  		</div>
                            		</div>
                            	</div>
                     		</div>
                     				
                     		<!--  QC is gether then 25% and less 100%  -->
                     		<?php } elseif($qcl > 25) { ?>
		                    	
		                    	<!--  QC is equal ANSA  -->
		                    	<?php if($r->qc_completed == $r->ansa_completed) { ?>
                     			<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qc_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qcl; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?><br>
															<?php echo $r->qc_comment; ?>
															
                                  					</span>
                                 	 			</div>
                            			</div>
                        			</div>
                     			</div>                     	
                     	
                     			<!-- QA Condition -->
                     			
								<?php if($r->qa > $r->qa_completed) { ?>  
									
									<!-- QA is purple  -->
									<?php if($r->qa_rem !=0) { ?>
										
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal"  id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	
		                     			
									<!-- QA is blue  -->
									<?php } elseif($r->qa_rem == '0') { ?>
										
										<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal"  id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   		
        						<!-- QA is RED  -->
									<?php } else { ?>		                   	
                     				<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal"  id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	       						
               						<?php } ?>		
               						
               						<!-- QA is Green -->	   
                     			 	<?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     				<div class="col-md-3">
                       					<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			
                     				<?php }  ?>
                     						
                     						
                     			<?php } else  { ?>		
                     			<!-- QC is Yellow  -->
                     			<div class="col-md-3">
                       				<div class="info-box bg-yellow" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                              					<span class="info-box-text"><?php echo $r->ansa_completed-$r->qc_completed; ?></span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qcl; ?>%"></div>
                              					</div>
                                 				<span class="progress-description">
                                    					Updated: <?php      if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?><br />
															<?php echo $r->qc_comment; ?>
                                  				</span>
                                 	 			</div>
                            			</div>
                        			</div>
                     			</div>
                     			
                     	
                     			<!-- QA Condition -->
								<?php if($r->qa > $r->qa_completed) { ?>  
									<!-- QA is purple -->
									
									<?php if($r->qa_rem !=0) { ?>
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal"  id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   								
									<!-- QA is blue  -->
									<?php } elseif($r->qa_rem == '0') { ?>
										
									<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal"  id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   								
									<!-- QA is RED  -->
										
									<?php } else { ?>		                   	
                     				
                     				<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal"  id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	
               						<?php } ?>		
               						
               						<!-- QA is Green -->	   
                     			 	<?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     					<div class="col-md-3">
                       						<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            					<div class="info-box-content">
                              						<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                									<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 				</div>
                            					</div>
                        					</div>
                     					</div>
                     			
                     				<?php }  ?>
                     	           				
                     			<?php } ?>

                    		<?php } ?> 
                    		
                    		<?php if($r->wh < $r->wh_completed) { ?>   
                    			    		
                        		<?php if($whl < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($whl == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh ==  $r->wh_completed) { ?>
                     				
                     			<?php if($whl == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			
        						<?php } elseif($r->wh > $r->wh_completed) { ?>
        							
        						<?php if($whl < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php      if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                				                				
                				<?php } elseif($whl == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php      if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<?php } ?>
        							
        						<?php } ?>  
                		 		
                		 		
                	<!-- ANSA is 25 > and < 100 -->
                	<?php } else { ?>
						
						<!-- ANSA is Yellow  -->
						<div class="col-md-3">
                       		<div class="info-box bg-yellow" data-toggle="modal" data-target="#ANSA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            	<div class="info-box-content">
                              		<span class="info-box-text"><?php echo $r->qty-$r->ansa_completed; ?></span>                              
                					<div class="progress1">
                              		<div class="progress">
                                		<div class="progress-bar" style="width: <?php echo $ansal; ?>%"></div>
                              		</div>
                                 	<span class="progress-description">
                                    Updated: <?php  if($r->ansa_updated == '0000-00-00 00:00:00')
											{ echo "-"; }
											else {
                                    			$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
											}
                                    		?>
                                  	</span>
                                 	</div>
                            	</div>
                        	</div>
                     	</div>
                     	<div class="example-modal">
        					<div class="modal modal-primary" id="ANSA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          						<div class="modal-dialog">
								    <div class="modal-content">
								     	<div class="modal-header">
                							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  							<span aria-hidden="true">&times;</span></button>
                							<h4 class="modal-title">Container Settings - ANSA Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              							</div>
              							<div class="modal-body">
               							<form role="form" name="container-action" method="post">
              								<div class="box-body">
                							<input type="hidden" id="ansa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
                							<div class="form-group">
                								<label>Pendding Products :- <?php echo $r->qty-$r->ansa_completed; ?></label><br />
                								<input type="hidden" id="ansa2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->ansa; ?>" />
                  								<label for="examplecomment">Number of pcs Pass</label>
                  								<input type="text" id="ansa3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control number" />
                							</div>
										    </div>
            							</form>
              							</div>
              							<div class="modal-footer">
                							<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                							<button type="button" id="ansa4<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Save Changes</button>
              							</div>
            						</div>
            					</div>
          					</div>
        				</div>
        				<script>
                                    
                                    
                                    $('#ansa3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                     var product=<?php echo $r->qty-$r->ansa_completed; ?>; 
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                     	<script>
               				$(function(){
								$("#ansa4<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  				var id1=$("#ansa1<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  					var ansa1=$("#ansa2<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  					var ansa2=$("#ansa3<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  					var data= {id:id1,ansa1:ansa1,ansa2:ansa2}
										$.ajax({
												'url':'<?php echo base_url();?>ansa/jobscreen/ansaadd1',
												'type':'POST',
												'data':data,
												'success':function(message)
												{	
													$( "#maindiv" ).load( "jobscreen/reloaded" );
												}
												});
												});
			   				});
               			</script>
               			
                     	<!-- QC is less then 25%  -->		
                     	<?php if($r->qc_completed < $r->ansa_completed) { ?>
                     		<!-- QC is RED  -->
                     		<div class="col-md-3">
                       				<div class="info-box bg-red" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->ansa_completed-$r->qc_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qcl; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php  if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?><br>
                                    							<?php echo $r->qc_comment; ?>
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			               				
                     		<!-- QA is Black  -->
                     		<div class="col-md-3">
                        		<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            		<div class="info-box-content">
                              			<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
				                		<div class="progress1">
				                        <div class="progress">
				                        	<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
				                        </div>
				                        <span class="progress-description">
				                        	Updated: <?php if($r->qa_updated == '0000-00-00 00:00:00')
															{ echo "-"; }
															else {
                                    						$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
															}
													?>
				                       	</span>
                                  		</div>
                            		</div>
                            	</div>
                     		</div>
                     		
                     		<!-- QC is more then 25%  -->		
                     		<?php } elseif($qcl > 25) { ?>
                     			
                     			<!-- QC is equal to ansa  -->
                     			<?php if($r->qc_completed == $r->ansa_completed) { ?>
                     						
                     				<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->ansa_completed-$r->qc_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qcl; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?><br>
															<?php echo $r->qc_comment; ?>
															
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>                     	
                     	
                     			<!-- QA Condition -->
								<?php if($r->qa > $r->qa_completed) { ?>  
									<!-- QA is RED  -->
									<?php if($r->qa_rem !=0) { ?>
										
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal"  id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	
										<!-- QA is blue  -->
										<?php } elseif($r->qa_rem == '0') { ?>
										
										<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal"  id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	
										<!-- QA is red  -->
										<?php } else { ?>		                   	
                     					<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal"  id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	
               					<?php } ?>		
               						
               					<!-- QA is Green -->	   
                     			 <?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     				<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			
                     			<?php } ?>
                     						
                     						
                     			<?php } else  { ?>
                     						
                     				<!-- QC is Yellow  -->
                     				<div class="col-md-3">
                       				<div class="info-box bg-yellow" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->ansa_completed-$r->qc_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qcl; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																	else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																	}
															?><br />
															<?php echo $r->qc_comment; ?>
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     					<!-- QA Condition -->
                     			
								<?php if($r->qa > $r->qa_completed) { ?>  
									<!-- QA is purple  -->
									<?php if($r->qa_rem !=0) { ?>
										
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal"  id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	
									
									<?php } elseif($r->qa_rem == '0') { ?>
										<!-- QA is blue  -->
									<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal"  id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   									
									<?php } else { ?>	
									<!-- QA is RED  -->		                   	
                     				<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal"  id="qc-mat-81" >
                            				<div class="info-box-content">
                              					<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              					<div class="progress">
                                					<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              					</div>
                                 		 		<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     			   	
               						<?php } ?>		
               						
               					<!-- QA is Green -->	   
                     			 <?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     				<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qal; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			
                     			<?php }  ?>
                     				
                     				
                     			<?php } ?>
                     			
                     			
                     			<?php } ?>			
                        		
                        		<?php if($r->wh < $r->wh_completed) { ?>   
                    			    		
                        		<?php if($whl < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($whl == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh ==  $r->wh_completed) { ?>
                     				
                     			<?php if($whl == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh > $r->wh_completed) { ?>
        							
        						<?php if($whl < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php      if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                				                				
                				<?php } elseif($whl == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whl; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php      if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<?php } ?>
        							
        						<?php } ?>
                     			
					<?php } ?>
						
                	</div>
                  		</td>
                  		<td class="col-md-1">
                  			<?php $totalp = (($qcl+$qal+$whl+$ansal)/4) ?>
                  				<span class="badge bg-red"><?php echo round($totalp); ?>%</span><?php if($totalp == 100) { echo "Ready";  } else { echo "Not Ready";  } ?>
                  		</td>
                	</tr>
                		
                	<?php } else { ?>  <!-- ANSA Not -->
                	
                	<tr>
                  		<td class="col-md-2">
                  			<!--<h4 class="bg-green" style="width:100%; text-align:center;"><?php echo $r->container_type; ?></h4>-->
                  			<h4 class="bg-blue" style="width:100%"><?php echo $r->job_name; ?></h4> 
                  			<div class="col-md-8"><h5>TPD: <?php $tpd = get_tpd_by_id($r->tpd); foreach($tpd as $t) { echo $t->name; } ?> | QTY: <?php echo $r->qty; ?></a></h5></div>
                  			<div class="col-md-4"><h5><?php echo $r->container_type; ?></h5></div>
                  			<div class="col-md-8"><h5><?php echo $r->customer_name; ?></h5></div>
                  			<div class="col-md-4"><h5><?php echo date ("j M,y",strtotime($r->container_date)); ?></h5></div>
                  			
                  		</td> 
                  
                 	 	<td class="col-md-9">
                 	 		<?php $qul = (($r->qc*100)/$r->qty) ?>	
                     		<?php $qap = (($r->qa_completed*100)/$r->qty) ?>
                     		<?php $whp = (($r->wh_completed*100)/$r->qty) ?>
                  			<div class="row">
                  				<!-- Start Row -->
                  				<div class="col-md-3"> 
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
	                            		<style>
	                						.info-box-content{
	                							margin-left: 0;
	                							padding: 5px 5px;
	                						}
	                					</style>
	                            		<div class="info-box-content">                       
		                					<span class="progress-description">
		                                  	</span>
		                              		<div class="progress">
		                                		<div class="progress-bar" style="width: 0%"></div>
		                              		</div>
		                                  	<span class="info-box-text">Not Applicable</span> 
	                            		</div>
                        			</div>
                     			</div><!-- End First Col-md-3 -->
                     				
                     		
                     		<!-- if qc is < 25%  -->
                     		
                     			<?php if($qul < 25) { ?>
                    			<!-- QC is RED  -->
                    			<div class="col-md-3">
                       				<div class="info-box bg-red" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qty-$r->qc_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qul; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php  if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?><br>
                                    							<?php echo $r->qc_comment; ?>
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			<!-- QA is Black  -->
                     			<!-- QA Condition -->
								<?php if($r->qa > $r->qa_completed) { ?>  
									<!-- QA is Purple  -->
									<?php if($r->qa_rem !=0) { ?>
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				
									
									<?php } elseif($r->qa_rem == '0') { ?>
										<!-- QA is Blue  -->
									<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				<?php } else { ?>		                   	
                     				<!-- QA is RED  -->
                     				<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				
               						<?php } ?>		
               						
               					<!-- QA is Green -->	   
                     			 <?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     				<div class="col-md-3">
                       				<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			<?php } ?>
                     			                     			
                     			<?php if($r->wh < $r->wh_completed) { ?>   
                    			    		
                        		<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh ==  $r->wh_completed) { ?>
                     				
                     			<?php if($whp == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                				
                				<?php } elseif($r->wh > $r->wh_completed) { ?>
        							
        						<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php      if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                				                				
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php      if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<?php } ?>
        							
        						<?php } ?>
                     			
                     			<!-- if qc is Completed  -->
                     		
                     			<?php } elseif($qul == 100) { ?>
                     				<!-- QC is Green  -->
                     				<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qty-$r->qc_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qul; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?><br>
															<?php echo $r->qc_comment; ?>
															
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>                     	
                     	
                     			<!-- QA Condition -->
								<?php if($r->qa > $r->qa_completed) { ?>  
									<!-- QA is Purple  -->
									<?php if($r->qa_rem !=0) { ?>
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     													
									<?php } elseif($r->qa_rem == '0') { ?>
										<!-- QA is Blue  -->
									<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				
									<?php } else { ?>		                   	
                     				<!-- QA is RED  -->
                     				<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				
               						<?php } ?>		
               						
               					<!-- QA is Green -->	   
                     			 <?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     				<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			
                     			<?php }  ?>
                     			   
                        		<?php if($r->wh < $r->wh_completed) { ?>   
                    			    		
                        		<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh ==  $r->wh_completed) { ?>
                     				
                     			<?php if($whp == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh > $r->wh_completed) { ?>
        							
        						<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php      if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                				                				
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php      if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<?php } ?>
        							
        						<?php } ?>
                     		
                     		<!-- if qc is Middel  -->
                     		
                     		<?php } else { ?>		
                     			<!-- QC is yellow  -->		
        						<div class="col-md-3">
                       				<div class="info-box bg-yellow" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qty-$r->qc_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qul; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?><br />
															<?php echo $r->qc_comment; ?>
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			                     			<!-- QA Condition -->
								<?php if($r->qa > $r->qa_completed) { ?>  
									<!-- QA is Purple  -->
									<?php if($r->qa_rem !=0) { ?>
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				
									
									<?php } elseif($r->qa_rem == '0') { ?>
										<!-- QA is Blue  -->
									<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				
										
									<?php } else { ?>		                   	
                     				<!-- QA is RED  -->
                     				<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				
               						<?php } ?>		
               						
               					<!-- QA is Green -->	   
                     			 <?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     				<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			
                     			<?php }  ?>
                     			
                     			<?php if($r->wh < $r->wh_completed) { ?>   
                    			    		
                        		<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh ==  $r->wh_completed) { ?>
                     			
                     			<?php if($whp == 0) { ?>
                     				
                     			<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } else { ?>
                     				
                     			<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>	
                     			<?php } ?>	
                     			
                     			<?php } elseif($r->wh > $r->wh_completed) { ?>
        							
        						<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php      if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			         				
                				<?php } elseif($whp == 100) { ?>	
                				
                				<?php if($whp == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php      if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<?php } ?>
        							
        						<?php } ?>	 	
        					
        					<?php } ?>	
        					
        					<!-- End Row -->	
                    		</div>
                  		</td>
                  		<td class="col-md-1">
                  			<?php $totalp = (($qul+$qap+$whp)/3) ?>	
                  				<span class="badge bg-red"><?php echo round($totalp); ?>%</span><?php if($totalp == 100) { echo "Ready";  } else { echo "Not Ready";  } ?> 
                  		</td>
                	</tr>
                	<?php } ?>
                	
                	
                	<!-- ANSA Container is ready -->
                	
                	<?php } else { ?>
                	<tr>
                  		<td class="col-md-2">
                  			<!--<h4 class="bg-green" style="width:100%; text-align:center;"><?php echo $r->container_type; ?></h4>-->
                  			<h4 class="bg-blue" style="width:100%"><?php echo $r->job_name; ?></h4> 
                  			<div class="col-md-8"><h5>TPD: <?php $tpd = get_tpd_by_id($r->tpd); foreach($tpd as $t) { echo $t->name; } ?> | QTY: <?php echo $r->qty; ?></a></h5></div>
                  			<div class="col-md-4"><h5><?php echo $r->container_type; ?></h5></div>
                  			<div class="col-md-8"><h5><?php echo $r->customer_name; ?></h5></div>
                  			<div class="col-md-4"><h5><?php echo date ("j M,y",strtotime($r->container_date)); ?></h5></div>
                  			
                  		</td> 
                  
                 	 	<td class="col-md-9">
                 	 		<?php $qul = (($r->qc*100)/$r->qty) ?>	
                     		<?php $qap = (($r->qa_completed*100)/$r->qty) ?>
                     		<?php $whp = (($r->wh_completed*100)/$r->qty) ?>
                     		<?php $ansap = (($r->ansa_completed*100)/$r->qty) ?>
                  			<div class="row">
                  				<!-- Start Row -->
                  				
                  				
                  				<?php if($r->ansa < $r->ansa_completed) { ?>   
                    			    		
                        		<?php if($ansap < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->wh_completed-$r->ansa_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $ansap; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($ansap == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->wh_completed-$r->ansa_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $ansap; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->wh_completed-$r->ansa_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $ansap; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->ansa ==  $r->ansa_completed) { ?>
                     			
                     			<?php if($ansap == 0) { ?>
                     				
                     			<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->wh_completed-$r->ansa_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $ansap; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } else { ?>
                     				
                     			<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->wh_completed-$r->ansa_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $ansap; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>	
                     			<?php } ?>	
                     			
                     			<?php } elseif($r->ansa > $r->ansa_completed) { ?>
        							
        						<?php if($ansap < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal" data-target="#ANSA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->wh_completed-$r->ansa_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $ansap; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php      if($r->ansa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="ANSA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - WH Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<input type="hidden" id="ansa11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
										            	<label>Requested Products :- <?php echo $r->ansa-$r->ansa_completed; ?></label>
										            	<input type="text" id="ansa33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control" />
										            </div>
            										</form>
              									</div>
              									<div class="modal-footer"> 
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="ansa22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Completed</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
                     			<script>
                                    
                                    
                                    $('#ansa33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->ansa-$r->ansa_completed; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                     			 <script>
               						$(function(){
												$("#ansa22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var qty=$("#ansa33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
					  							var id=$("#ansa11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {qid:id,qt:qty}
									$.ajax({
									'url':'<?php echo base_url();?>ansa/jobscreen/ansadata',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>  
                				                				
                				<?php } elseif($ansap == 100) { ?>	
                				
                				<?php if($ansap == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->wh_completed-$r->ansa_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $ansap; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->wh_completed-$r->ansa_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $ansap; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->ansa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" data-target="#ANSA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->wh_completed-$r->ansa_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $ansap; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php      if($r->ansa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->ansa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<div class="example-modal">
        							<div class="modal modal-primary" id="ANSA<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>">
          								<div class="modal-dialog">
								            <div class="modal-content">
								            	<div class="modal-header">
                									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  									<span aria-hidden="true">&times;</span></button>
                									<h4 class="modal-title">Container Settings - WH Dept. <?php echo $r->container_type; ?> (<?php echo $r->job_name; ?>)</h4>
              									</div>
              									<div class="modal-body">
               										<form role="form" name="container-action" method="post">
              										<div class="box-body">
                										<input type="hidden" id="ansa11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" value="<?php echo $r->id; ?>" />
										            	<label>Requested Products :- <?php echo $r->ansa-$r->ansa_completed; ?></label>
										            	<input type="text" id="ansa33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="form-control" />
										            </div>
            										</form>
              									</div>
              									<div class="modal-footer"> 
                									<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                									<button type="button" id="ansa22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>" class="btn btn-outline btnsave" data-dismiss="modal" >Completed</button>
              									</div>
            								</div>
            							</div>
          							</div>
        						</div>
                     			<script>
                                    
                                    
                                    $('#ansa33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>').keyup(function(){
                                      var product=<?php echo $r->ansa-$r->ansa_completed; ?>;
                                      if ($(this).val() > product){
                                        alert("Please Enter Valid Products");
                                       $(this).val('');
                                      }
                                      else
                                        {
                                            
                                    }
                                    });
                                </script>
                     			 <script>
               						$(function(){
												$("#ansa22<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").click(function(){
					  							var qty=$("#ansa33<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
					  							var id=$("#ansa11<?php echo $r->container_type.'-'.preg_replace('/([^A-Za-z0-9\-])/', '',str_replace(' ', '', $r->job_name)).$r->qty; ?>").val();
				  								var data= {qid:id,qt:qty}
									$.ajax({
									'url':'<?php echo base_url();?>ansa/jobscreen/ansadata',
									'type':'POST',
									'data':data,
									'success':function(message)
									{	
										$( "#maindiv" ).load( "jobscreen/reloaded" );

									}
									});
									});
			   						});
               						</script>
                     			<?php } ?>
        							
        						<?php } ?>
                  				
                  				
                  				<!-- End First Col-md-3 -->
                     				
                     		
                     		<!-- if qc is < 25%  -->
                     		
                     			<?php if($qul < 25) { ?>
                    			<!-- QC is RED  -->
                    			<div class="col-md-3">
                       				<div class="info-box bg-red" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qty-$r->qc_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qul; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php  if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?><br>
                                    							<?php echo $r->qc_comment; ?>
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     				
                     			<!-- QA is Black  -->
                     			<!-- QA Condition -->
								<?php if($r->qa > $r->qa_completed) { ?>  
									<!-- QA is Purple  -->
									<?php if($r->qa_rem !=0) { ?>
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				
									
									<?php } elseif($r->qa_rem == '0') { ?>
										<!-- QA is Blue  -->
									<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				
										
									<?php } else { ?>		                   	
                     				<!-- QA is RED  -->
                     				<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				
               						<?php } ?>		
               						
               					<!-- QA is Green -->	   
                     			 <?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     				<div class="col-md-3">
                       				<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			<?php } ?>
                     			                     			
                     			<?php if($r->wh < $r->wh_completed) { ?>   
                    			    		
                        		<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh ==  $r->wh_completed) { ?>
                     				
                     			<?php if($whp == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                				
                				<?php } elseif($r->wh > $r->wh_completed) { ?>
        							
        						<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php      if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			  
                				                				
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php      if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			<?php } ?>
        							
        						<?php } ?>
                     			
                     			<!-- if qc is Completed  -->
                     		
                     			<?php } elseif($qul == 100) { ?>
                     				<!-- QC is Green  -->
                     				<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qty-$r->qc_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qul; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?><br>
															<?php echo $r->qc_comment; ?>
															
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>                     	
                     	
                     			<!-- QA Condition -->
								<?php if($r->qa > $r->qa_completed) { ?>  
									<!-- QA is Purple  -->
									<?php if($r->qa_rem !=0) { ?>
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				
									
									<?php } elseif($r->qa_rem == '0') { ?>
										<!-- QA is Blue  -->
									<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     					
									<?php } else { ?>		                   	
                     				<!-- QA is RED  -->
                     				<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
                     				
               						<?php } ?>		
               						
               					<!-- QA is Green -->	   
                     			 <?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     				<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			
                     			<?php }  ?>
                     			   
                        		<?php if($r->wh < $r->wh_completed) { ?>   
                    			    		
                        		<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh ==  $r->wh_completed) { ?>
                     				
                     			<?php if($whp == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh > $r->wh_completed) { ?>
        							
        						<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php      if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                				                				
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php      if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<?php } ?>
        							
        						<?php } ?>
                     		
                     		<!-- if qc is Middel  -->
                     		
                     		<?php } else { ?>		
                     			<!-- QC is yellow  -->		
        						<div class="col-md-3">
                       				<div class="info-box bg-yellow" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qty-$r->qc_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qul; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qc_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qc_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?><br />
															<?php echo $r->qc_comment; ?>
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			<!-- QA Condition -->
								<?php if($r->qa > $r->qa_completed) { ?>  
									<!-- QA is Purple  -->
									<?php if($r->qa_rem !=0) { ?>
									<div class="col-md-3">
                       					<div class="info-box bg-purple" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
									
									<?php } elseif($r->qa_rem == '0') { ?>
										<!-- QA is Blue  -->
									<div class="col-md-3">
                       					<div class="info-box bg-blue" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>
	
									<?php } else { ?>		                   	
                     				<!-- QA is RED  -->
                     				<div class="col-md-3">
                       					<div class="info-box bg-red" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 			</div>
                            				</div>
                        				</div>
                     				</div>

               						<?php } ?>		
               						
               					<!-- QA is Green -->	   
                     			 <?php } elseif($r->qa == $r->qa_completed) { ?>  
                     			   
                     				<div class="col-md-3">
                       				<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                              				<span class="info-box-text"><?php echo $r->qc_completed-$r->qa_completed; ?></span>                              
                								<div class="progress1">
                              						<div class="progress">
                                						<div class="progress-bar" style="width: <?php echo $qap; ?>%"></div>
                              						</div>
                                 		 			<span class="progress-description">
                                    					Updated: <?php      if($r->qa_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->qa_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  					</span>
                                 	 		</div>
                            			</div>
                        			</div>
                     			</div>
                     			
                     			<?php }  ?>
                     			
                     			<?php if($r->wh < $r->wh_completed) { ?>   
                    			    		
                        		<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } elseif($whp == 100) { ?>	
                				
                				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } elseif($r->wh ==  $r->wh_completed) { ?>
                     			
                     			<?php if($whp == 0) { ?>
                     				
                     			<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } else { ?>
                     				
                     			<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>	
                     			<?php } ?>	
                     			
                     			<?php } elseif($r->wh > $r->wh_completed) { ?>
        							
        						<?php if($whp < 25) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-red" data-toggle="modal"  id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php      if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                				
                				<?php } elseif($whp == 100) { ?>	
                				
                				<?php if($whp == 0) { ?>	
                        		<div class="col-md-3">
                        			<div class="info-box bg-black" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>  
                				<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-green" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php  if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
                                    							?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div>
                     			<?php } ?>
                     			
                     			<?php } else { ?>
                     				
                     				<div class="col-md-3">
                        			<div class="info-box bg-yellow" data-toggle="modal" id="qc-mat-81" >
                            			<div class="info-box-content">
                             	 			<span class="info-box-text"><?php echo $r->qa_completed-$r->wh_completed; ?></span>                               
				                			<div class="progress1">
				                            	<div class="progress">
				                                	<div class="progress-bar" style="width: <?php echo $whp; ?>%"></div>
				                              	</div>
                                  				<span class="progress-description">
                                    				Updated: <?php      if($r->wh_updated == '0000-00-00 00:00:00')
																		{ echo "-"; }
																		else {
                                    									$today = date("H:i d-m-Y");    	
                                    							$adate = date ("H:i d-m-Y",strtotime($r->wh_updated)); 
																$dteStart = new DateTime($today);
   																$dteEnd   = new DateTime($adate); 
																$interval = $dteStart->diff($dteEnd); 
    															echo $interval->format("%H:%I Hrs Ago"); 
																		}
															?>
                                  				</span>
                                  			</div>
                            			</div>
                            		</div>
                     			</div> 
                     			
                     			<?php } ?>
        							
        						<?php } ?>	 	
        					
        					<?php } ?>	
        					
        					<!-- End Row -->	
                    		</div>
                  		</td>
                  		<td class="col-md-1">
                  			<?php $totalp = (($qul+$qap+$whp+$ansap)/4) ?>	
                  				<span class="badge bg-red"><?php echo round($totalp); ?>%</span><?php if($totalp == 100) { echo "Ready";  } else { echo "Not Ready";  } ?> 
                  		</td>
                	</tr>
                		
                		
                		
                		
                	<?php } ?>	 
                 	<?php } ?>
                </tbody>
           	</table>
        </div>