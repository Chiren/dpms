<?php 
class Containerscreen_model  extends CI_Model  {
	
	function save($data, $job)
	{
		$msg = $this->db->insert('container_master', $data);
		$cid = $this->db->insert_id();
		
		$l = count($job['jobs']);
		//echo $l;
		for($i = 0; $i < $l; $i++){
			$data = array(
				'container_id' => $cid,
				'job_id' => $job['jobs'][$i],
				'qty' => $job['qty'][$i]
			);
			//echo "<pre>"; 
			//print_r($data);	 
			$this->db->insert('container_job', $data);
		}
		
		
	}
	function get_all(){
		$this->db->select('container_master.*');
		$this->db->from('container_master');
		$this->db->join('container_job', 'container_job.container_id = container_master.container_id');
		//$this->db->join('tpd_master', 'container_job.tpd = tpd_master.id');
		//$this->db->where("tpd_master.name","100");
		//$this->db->where("container_job.decoration","1"); 
		$this->db->where("container_master.container_name","ANSA");
		$this->db->order_by("con_status", "DESC");
		$this->db->order_by("container_date", "asc");
		$this->db->where("con_status !='3' OR con_status='4'"); 
		$this->db->group_by('container_id');
		return $this->db->get()->result();
	}
	function get($id){
		$this->db->where('id', $id);
		return $this->db->get('container_master')->result();
	}
	function update($id, $data){
		$this->db->where('id',$id);
		return $this->db->update('container_master',$data);
	}
	function delete($id){
		$this->db->where('id',$id);
		$this->db->delete('container_master');
	}
}
?>

