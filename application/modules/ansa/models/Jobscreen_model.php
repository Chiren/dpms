<?php 
class Jobscreen_model  extends CI_Model  {
	
	function get_all(){
		$this->db->select('container_job.*,container_master.*, job_master.*,jobscreen_master.*');
		$this->db->from('container_job');
		$this->db->join('container_master', 'container_job.container_id = container_master.container_id');
		$this->db->join('job_master', 'job_master.job_id = container_job.job_id'); 
		$this->db->join('jobscreen_master', 'jobscreen_master.container_job_id = container_job.container_job_id'); 
		$this->db->where("container_master.container_name","ANSA");
		//$this->db->where("container_job.decoration","1");
		//$this->db->join('tpd_master', 'container_job.tpd = tpd_master.id');
		//$this->db->where("tpd_master.name","100"); 
		$this->db->where("job_status","0");
		//$this->db->where("con_status='0' OR con_status='1' OR con_status='2'");
		$this->db->order_by("con_status", "DESC");
		$this->db->order_by("container_date", "asc");
		return $this->db->get()->result();
	}
	
	function qcadd($id,$data,$activity,$sound)
	{
		$this->db->where("id",$id);
		$this->db->update('jobscreen_master',$data);
		$this->db->insert('activity', $activity);
		$this->db->insert('activity_master', $sound);
	}
	
	function qaadd($id,$data,$activity,$sound)
	{
		$this->db->where("id",$id);
		$this->db->update('jobscreen_master',$data);
		$this->db->insert('activity', $activity);
		$this->db->insert('activity_master', $sound);
	}
	
	function whadd($id,$data,$activity,$sound)
	{
		$this->db->where("id",$id);
		$this->db->update('jobscreen_master',$data);
		$this->db->insert('activity', $activity);
		$this->db->insert('activity_master', $sound);
	}
	
	function ansaadd($id,$data,$activity,$sound)
	{
		$this->db->where("id",$id);
		$this->db->update('jobscreen_master',$data);
		$this->db->insert('activity', $activity);
		$this->db->insert('activity_master', $sound);
	}

	function get_job_container($id)
	{
		$sql = "select container_job.container_id,container_job.job_id,container_job.tpd,jobscreen_master.qa_per,jobscreen_master.wh_completed,jobscreen_master.ansa_completed from jobscreen_master join container_job on container_job.container_job_id=jobscreen_master.container_job_id where id=$id";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function get_job_name($jobs)
	{
		$sql = "select job_name from job_master where job_id=$jobs";
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	function get_container_name($id)
	{
		$sql = "select container_type from container_master where container_id=$id";
		$query = $this->db->query($sql);
		return $query->result();
	}
	function tpdchange($id,$tpd)
	{
		$sql = "update container_job set tpd='".$tpd."' where container_job_id='".$id."'";
		$query = $this->db->query($sql);
		
	}

}
?>

