<?php 

function get_notifications_num()
{
	$ci=&get_instance();
	$sql="SELECT * FROM activity where status='0' ORDER BY a_id DESC";
	$query=$ci->db->query($sql);
	return $query->num_rows();
}

function get_notifications()
{
	$ci=&get_instance();
	$sql="SELECT * FROM activity where status='0' ORDER BY a_id DESC";
	$query=$ci->db->query($sql);
	return $query->result();
}

function get_user_by_noti($id) 
{
	$ci=&get_instance();
	$sql="SELECT first_name FROM users where id=$id";
	$query=$ci->db->query($sql);
	return $query->result();	
}

function get_jobs_noti($id)
{
	$ci=&get_instance();
	$sql="SELECT job_name FROM job_master where job_id=$id";
	$query=$ci->db->query($sql);
	return $query->result();
}
?>