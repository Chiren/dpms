<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Uploadcsv extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_builder');
	}
	public function index()
	{
		$this->render('excelimport');
	}
	public function import()
	{
		$this->load->helper(array('form', 'url')); 
		$this->load->helper('file');
		$config['upload_path'] = './uploads/';
		$config['allowed_types'] = 'xlsx|csv|xls';
		$config['file_name'] = date('YmdHisu').rand(0,100);
	 	$this->load->library('upload',$config);
		
	    $this->upload->do_upload('file');
		$filexcel=$this->upload->data();
		$exxel = $filexcel['file_name'];
		
		$fullpath = base_url();
		$all = $fullpath."uploads/".$exxel;

		include 'PHPExcel/IOFactory.php';
		$inputFileName = FCPATH."uploads/".$exxel; 
		
		try 
		{
			$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
		} 
		catch(Exception $e) 
		{
			die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
		}

		$allDataInSheet = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
		$arrayCount = count($allDataInSheet);
		
		for($i=2;$i<=$arrayCount;$i++)
		{
			$customer_name = trim($allDataInSheet[$i]["A"]);
			$container_name = trim($allDataInSheet[$i]["B"]);
			$container_no = trim($allDataInSheet[$i]["C"]);
			$tpd = trim($allDataInSheet[$i]["D"]);
			$container_date = trim($allDataInSheet[$i]["E"]);
			$job = array();
			$qty = array();
			$j = 0;
			for ($char = 'F'; $char <= 'Z'; $char++) {
			$abc = null;	
			$abc=trim($allDataInSheet[$i][$char]);
		
			if(!empty($abc))
			{
				if ($j % 2 == 0) 
				{
					$job[] = $abc;
				}	
				else
				{
					$qty[] = $abc;
				}
			}
			else
			{
				break;
			}
    		$j++;			
			
			}		
			$this->load->model('container_model');
			$inserdata=$this->container_model->insert_con_excel($customer_name,$container_name,$container_no,$qty,$job,$tpd,$container_date);		
		}
		
		
		redirect('admin/containerscreen');
	}
 

}