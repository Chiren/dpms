<?php 
class Container_model  extends CI_Model  {
	
	function save($data, $job)
	{
		$ci = &get_instance();
		$ci -> load -> library('session');
		$x = $ci -> session -> userdata('user_group');
		$y = $ci -> session -> userdata();
		$users = $y['user_id'];
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$dates = date('Y-m-d');
		
		$msg = $this->db->insert('container_master', $data);
		$cid = $this->db->insert_id();
		
		$l = count($job['jobs']);
		$sum = 0;
		for($i = 0; $i < $l; $i++){
			$data = array(
				'container_id' => $cid,
				'job_id' => $job['jobs'][$i],
				'qty' => $job['qty'][$i],
				'tpd' => $job['tpd'][$i]
			);
			
			$avt = array(
			
				'user' => $users,
				'jobs' => $job['jobs'][$i],
				'container' => $cid,
				'tpd' => $job['tpd'][$i],
				'time' => $time,
				'date' => $dates,
				'note' => "Job Add",
				
			);
			
			$this->db->insert('activity', $avt);
			
			$sum+=$job['qty'][$i];
			$this->db->insert('container_job', $data);
			$jids = $this->db->insert_id();
			
			$sqltwo="insert into jobscreen_master (container_job_id) value ($jids)";
			$querytwo = $this->db->query($sqltwo);
		}
		
		$sql="update container_master set total_quility='".$sum."' where container_id='".$cid."'";
		$query=$this->db->query($sql);
		
		
	}
	
	function get_all()
	{
		return $this->db->get('container_master')->result();
	}
	
	function get($id)
	{
		$this->db->where('container_id', $id);
		return $this->db->get('container_master')->result();
	}
	
	function get_job($id)
	{
		$this->db->where('container_id', $id);
		return $this->db->get('container_job')->result();
	} 
	
	function update($id, $data,$job)
	{
		$this->db->where('container_id',$id);
		$this->db->update('container_master',$data);
		
		$l = count($job['jobs']);
		$sum = 0;
		for($i = 0; $i < $l; $i++)
		{
			$ids = $job['jobs'][$i];
			$qty = $job['qty'][$i];	
			$sum+=$job['qty'][$i];
			$sql="update container_job set qty=$qty where container_job_id = $ids";
			$query = $this->db->query($sql);
		}
		$sql="update container_master set total_quility='".$sum."' where container_id='".$id."'";
		$query=$this->db->query($sql);
		
	}
	function delete($id){
				
		$sql1 ="select * from container_job where container_id=$id";
		$query1 =$this->db->query($sql1);
		$data = $query1->result();
		
		foreach($data as $d)
		{
			$c_j_id = $d->container_job_id;
			
			$sql2="delete from container_job where container_job_id =$c_j_id";
			$query2 = $this->db->query($sql2);
			
			$sql3 ="delete from jobscreen_master where container_job_id =$c_j_id";
			$query3 = $this->db->query($sql3);
			
		}
		
		$sql4 ="delete from container_master where container_id =$id";
		$query4 = $this->db->query($sql4);
	}
	
	
	public function insert_con_excel($customer_name,$container_name,$container_no,$qty,$job,$tpd,$container_date)
	{
				
		$ci = &get_instance();
		$ci -> load -> library('session');
		$x = $ci -> session -> userdata('user_group');
		$y = $ci -> session -> userdata();
		$users = $y['user_id'];
		date_default_timezone_set('Asia/Calcutta');
		$time = date('H:i:s');
		$dates = date('Y-m-d');
		
		$date = date("Y-m-d",strtotime($container_date));
		$c_type = $container_name."-".$container_no;
		
		$sqlsix = "select * from container_master where container_date='".$date."' AND customer_name='".$customer_name."'";
		$querysix = $this->db->query($sqlsix);
		$check = $querysix->num_rows();
		
		
		if($check == 0)
		{
			
			$sql = "insert into container_master (customer_name,container_type,container_name,container_date,updatedOn) value ('".$customer_name."','".$c_type."','".$container_name."','".$date."','".$date."')";	
			$query = $this->db->query($sql);
			$cid = $this->db->insert_id();
			// Insert Container 
		
			$l = count($job);
			$sum = 0;
			
			for($i = 0; $i < $l; $i++)
			{
				$sqltwo = "select * from job_master where job_name='".$job[$i]."'";
				$querytwo = $this->db->query($sqltwo);
				$jobsid = $querytwo->result_array();
				$job_id = $jobsid[0]['job_id'];

				$sqlthree = "select * from tpd_master where name='".$tpd."'";
				$querythree = $this->db->query($sqlthree);
				$tpdid = $querythree->result_array();
				$tpd_id = $tpdid[0]['id'];
				
				
				$data = array(
					'container_id' => $cid,
					'job_id' => $job_id,
					'qty' => $qty[$i],
					'tpd' => $tpd_id
					);
				
				$avt = array(
					'user' => $users,
					'jobs' => $job_id,
					'container' => $cid,
					'tpd' => $tpd_id,
					'time' => $time,
					'date' => $dates,
					'note' => "Job Add",
					);
				
				$this->db->insert('activity', $avt);
			
				$sum+=$qty[$i];
				$this->db->insert('container_job', $data);
				$jids = $this->db->insert_id();
			
				$sqltwo="insert into jobscreen_master (container_job_id) value ($jids)";
				$querytwo = $this->db->query($sqltwo);
			
			}
			
			$sql="update container_master set total_quility='".$sum."' where container_id='".$cid."'";
			$query=$this->db->query($sql);
		}
		else
		{
			
		}	
		
		
	}
	
	
	
	
}
?>

