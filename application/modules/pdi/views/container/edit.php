<?php $this->layout('layouts::default') ?>
<?php
$ci=&get_instance();
$ci->load->helper('common');
?> 
<style>
	.input-xs {
  height: 25px;
  padding: 2px 5px;
  font-size: 15px;
  line-height: 1.5; 
}
</style>

<div class="row">
<div class="col-lg-5">
	<div class="box box-primary">
    	<form action="container/update">
    		
        	<div class="box-header with-border">
            	<h3 class="box-title">Edit Container</h3>
            </div>
            <?php foreach($result as $r) { ?>
            	<input type="hidden" name="id" value="<?php echo $r->container_id; ?>" />
            <div class="box-body">
              	<div class="form-group">
              		<label>Customer Name</label>
              		<input type="text" name="customer_name" value="<?php echo $r->customer_name; ?>" class="form-control input-xs" placeholder="Customer Name"/>
             	</div>
              	
              	<div class="row">
              		<div class="col-lg-6">
              			<div class="form-group">
		              		<label>Name<span>(Ex. ANSA 04)</span></label>
		              		<select class="form-control input-xs" name="container_type">
		              			<?php $type = container_type(); ?>
		              			<?php foreach($type as $t) { ?>
		              			<option value="<?php echo $t->name; ?>" <?php if($r->container_name == $t->name) { echo "selected='selected'"; } ?> ><?php echo $t->name; ?></option>
		              			<?php } ?>
		              		</select>
	              		</div>	
              		</div>
              		<div class="col-lg-6">
              			<div class="form-group">
		              		<label>Container Number<span>(Ex. 04)</span></label>
		              		<?php $whatIWant = substr($r->container_type, strpos($r->container_type, "-") + 1); ?>
		              		<input type="text" name="container_number" class="form-control input-xs" value="<?php echo $whatIWant; ?>" placeholder="04"/>
		          		</div>	
              		</div>
              	</div>
              	
              	<div class="form-group">
              		<label>Date</label>
              		<input type="text" name="date" class="form-control input-xs" value="<?php echo $r->container_date; ?>" id="datepicker" placeholder="Date" />
              	</div>
				
				<div class="row">
              		<div class="col-md-12">
              			<table class="table" id="POITable">
              				<thead>
              					<tr>
              						<th>Select Jobs</th>
              						<th>No. Qty</th>
              						<th>TPD</th>
              					</tr>
              				</thead>
              			</table>
              		</div>	
              		<?php foreach($job as $j) { ?>
              		<div class="col-md-12">
              			<div class="col-md-4 form-group">
              				 <input type="hidden" name="container_job_id[]" value="<?php echo $j->container_job_id; ?>" />
              				 
              				 <label><?php $name = get_job($j->job_id); 
              				 				
              				 				foreach($name as $n)
											{
												echo $n->job_name;
											}
              				 	
              				 		?>
              				 </label>
              			
              			</div>		
              			<div class="col-md-4 form-group">
              					<input type="text" name="qty[]" value="<?php echo $j->qty; ?>" class="form-control input-xs" id="qty"/> 
              			</div>	
              			<div class="col-md-4 form-group">
              				<?php $tpd = get_tpd_by_id($j->tpd); ?>
              				<?php foreach($tpd as $tt){  echo $tt->name; } ?>
              			</div>
              		</div>	
              		<?php } ?>
              		<!--div class="col-md-12">	
              			<div class="col-md-6 form-group"><label>Total Quentity</label></div>
              			<div class="col-md-6 form-group">
              				<input type="text" name="total_qty" class="form-control input-xs" value="<?php echo $r->total_quility; ?>"/>
              			</div>	
              		</div-->			
              				
              	</div>              
            </div>
            <?php } ?>
		         <div class="box-footer text-center ">
		          	<button class="pull-right btn btn-primary" type="submit">Edit</button>
			    </div>
         	</form>
    	</div>
	</div>
</div>

<link rel="stylesheet" href="../assets/plugins/datepicker/datepicker3.css" />
<script src="../assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script>
$(function () {
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy/mm/dd'
    });
    $(".timepicker").timepicker({
      showInputs: false,
     
    });
  });
</script>