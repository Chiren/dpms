<?php $this->layout('layouts::default') ?>
<?php
$ci=&get_instance();
$ci->load->helper('common');
?> 
<style>
	.input-xs {
  height: 25px;
  padding: 2px 5px;
  font-size: 15px;
  line-height: 1.5; 
}
</style>

<div class="row">
<div class="col-lg-5">
	<div class="box box-primary">
    	<form action="container/save">
        	<div class="box-header with-border">
            	<h3 class="box-title">Add New Container</h3>
            </div>
            <div class="box-body">
              	<div class="form-group">
              		<label>Customer Name</label>
              		<input type="text" name="customer_name" class="form-control input-xs" placeholder="Customer Name"/>
             	</div>
              	<div class="row">
              		<div class="col-lg-6">
              			<div class="form-group">
		              		<label>Name<span>(Ex. ANSA 04)</span></label>
		              		<select class="form-control input-xs" name="container_type">
		              			<?php $type = container_type(); ?>
		              			<?php foreach($type as $t) { ?>
		              			<option value="<?php echo $t->name; ?>"><?php echo $t->name; ?></option>
		              			<?php } ?>
		              		</select>
	              		</div>	
              		</div>
              		<div class="col-lg-6">
              			<div class="form-group">
		              		<label>Container Number<span>(Ex. 04)</span></label>
		              		<input type="text" name="container_number" class="form-control input-xs" placeholder="04"/>
		          		</div>	
              		</div>
              	</div>
              	<div class="form-group">
              		<label>Date</label>
              		<input type="text" name="date" class="form-control input-xs" id="datepicker" placeholder="Date" />
              	</div>
				<div class="row">
              		<div class="col-md-12">
              			<table class="table" id="POITable">
              				<thead>
              					<tr>
              						<th>Select Jobs</th>
              						<th>No. Qty</th>
              						<th>TPD</th>
              						<th><button type="button" class="btn btn-box-tool input-sm add_field_button" style="background-color: green; color:white" ><i class="fa fa-plus"></i></button></th>
              					</tr>
              				</thead>
              			</table>
              		</div>	
              			<div class="input_fields_wrap">
              				<div class="col-md-12">
              				<div class="col-md-4 form-group">
              					<select class="form-control input-xs" name="jobs[]" id="jobs">
              						<?php foreach ($result['jobs'] as $k) { ?>
									<option value="<?php print_r($k->job_id); ?>"><?php print_r($k->job_name); ?></option>
									<?php } ?>
              					</select> 
              				</div>		
              				<div class="col-md-3 form-group">
              						<input type="text" name="qty[]" class="txt form-control input-xs" id="qty"/> 
              				</div>
              				<div class="col-md-3 form-group">
              					<select class="form-control input-xs" name="tpd[]">
		              				<?php $tpd = get_tpd(); ?>
		              				<?php foreach($tpd as $tp) { ?>
		              					<option value="<?php echo $tp->id; ?>"><?php echo $tp->name; ?></option>
		              				<?php } ?>
		              			</select>
              				</div>
              					
              				</div>	
              			</div>
              			
              			<!--div class="col-md-12">	
              				<div class="col-md-6 form-group"><label>Total Quentity</label></div>
              				<div class="col-md-6 form-group">
              					
              						<input type="text" id="total" name="total_qty" class="form-control input-xs"/>
              				</div>	
              			</div-->			
              			
              			<script>
				 		$(document).ready(function() {
											
				    var max_fields      = 100; //maximum input boxes allowed
				    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
				    var add_button      = $(".add_field_button"); //Add button ID
				   	var fieldHTML = '<div class="col-md-12"><div class="col-md-4 form-group"><select required="required" class="form-control input-xs" style="width: 100%; " name="jobs[]"><?php foreach ($result['jobs'] as $k) { ?><option value="<?php print_r($k->job_id); ?>"><?php print_r($k->job_name); ?></option><?php } ?></select></div><div class="col-md-3 form-group"><input required="required" type="text" class="txt form-control flat input-xs" name="qty[]" /></div><div class="col-md-3 form-group"><select class="form-control input-xs" name="tpd[]"><?php $tpd = get_tpd(); ?><?php foreach($tpd as $tp) { ?><option value="<?php echo $tp->id; ?>"><?php echo $tp->name; ?></option><?php } ?></select></div><button type="button" class="remove_field btn btn-box-tool input-sm" style="background-color: red; color:white" ><i class="fa fa-minus"></i></button></div>';
				    var x = 1; //initlal text box count
				    $(add_button).click(function(e){ //on add input button click
					        e.preventDefault();
					        if(x < max_fields){ //max input box allowed
					            x++; //text box increment
					            $(wrapper).append(fieldHTML); //add input box
					        }
				    });
				    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
					        e.preventDefault(); $(this).parent('div').remove(); x--;
				    })
					});
					</script>
              			
              			
              			
              		
              	</div>              
            </div>
            <div class="box-footer text-center ">
              	<button class="pull-right btn btn-primary" type="submit">ADD</button>
            </div>
            </form>
          </div>
       	</div>
       	
       	
       	
<div class="col-lg-7">
	<div class="box box-primary">
    	<div class="box-header with-border">
        	<h3 class="box-title">Container List</h3>
        	<div class="box-tools pull-right">
            	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
             </div>
		</div>
        
        <div class="box-body">
        	<table class="table">
            	<thead>
              		<tr>
              			<th>Sr</th>
              			<th>Number</th>
              			<th>Date</th>
              			<th>Name</th>
              			<th>Qty</th>
              			<th>Option</th>
              		</tr>
              	</thead>
              	<tbody>
              		<?php $i = 1; foreach ($result['tabledata'] as $r) { ?>
					<tr>
              			<td><?php echo $i; $i++; ?></td>
              			<td><?php print_r($r->container_type); ?></td>
              			<td><?php print_r($r->updatedOn); ?></td>
              			<td><?php print_r($r->container_name); ?></td>
              			<td><?php print_r($r->total_quility); ?></td>
              			<td>
              				<a href="container/view/<?php print_r($r->container_id); ?>" class="btn btn-box-tool input-sm" style="background-color: green; color:white" ><i class="fa fa-eye"></i></a>
              				<a href="container/edit/<?php print_r($r->container_id); ?>" class="btn btn-box-tool input-sm" style="background-color: #605CA8; color:white" ><i class="fa fa-pencil"></i></a>
              				<a href="container/delete/<?php print_r($r->container_id); ?>" class="btn btn-box-tool input-sm" style="background-color: red; color:white" ><i class="fa fa-trash"></i></a> 
              			</td>
              		</tr>	  
					 <?php } ?>
              	</tbody>
              </table>
            </div>
          </div>
       	</div>
</div>

<link rel="stylesheet" href="../assets/plugins/datepicker/datepicker3.css" />
<script src="../assets/plugins/datepicker/bootstrap-datepicker.js"></script>
<script>
$(function () {
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy/mm/dd'
    });
    $(".timepicker").timepicker({
      showInputs: false,
     
    });
  });
</script>