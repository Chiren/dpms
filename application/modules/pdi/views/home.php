<?php $this->layout('layouts::default') ?>
<?php
$ci=&get_instance();
$ci->load->helper('home');
?> 
<script src="<?php echo base_url(); ?>assets/pci/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/pci/dataTables.bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/pci/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url(); ?>assets/pci/responsive.bootstrap.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#example').DataTable({
    	"ordering": false
    });
} );
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#exampletwo').DataTable({
    	"ordering": false
    });
} );
</script>
<style>
.dataTables_paginate.paging_simple_numbers
{
	text-align: right;
}
.pagination
{
	margin: 0px;
}
.dataTables_filter
{
	text-align: right;
}
#chartdiv {
 width: 50%;
  height: 300px;
  }												
#chartdiv1 {
 
  width: 50%;
  height: 300px;
}
#chartdiv2 {
 
  width: 50%;
  height: 300px;
}
#chartdiv3 {
 
   width: 50%;
  height: 300px;
}
.raphael-group-7-background
{
	background-color: rgb(219, 227, 236)!important;
}
rect
{
	fill:rgb(236,240,245) !important;
}
</style>
<section class="content" id="homecon">
	<H3>For Month: <? echo date('F-Y'); ?> (SKUs Under Respective TPDs)</H3>
	<div>
		<div id="chartdiv" class="col-md-6" style="margin-top: 15px;"></div>
    	<div id="chartdiv1" class="col-md-6" style="margin-top: 15px;"></div>
    	<div id="chartdiv2" class="col-md-6" style="margin-top: 15px;"></div>
    	<div id="chartdiv3" class="col-md-6" style="margin-top: 15px;"></div>
    	<script src="<?php echo base_url(); ?>assets/canvas.js"></script>
<script src="<?php echo base_url(); ?>assets/pci/pie.js"></script>
<!--<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />-->
<script src="<?php echo base_url(); ?>assets/pci/light.js"></script>

<?php $tpds100ansa = get_tpd100_monthnew(); ?>
<?php $tpds100qc1 = get_tpd100_monthallnewqc();  ?>
<?php $tpds100qc2 = get_tpd100_monthallnewqc1();  ?>
<?php $tpds100qc = $tpds100qc1+$tpds100qc2;  ?>
<?php $tpds100qa = get_tpd100_monthallnewqa(); ?>
<?php $tpds100wh = get_tpd100_monthallnewwh(); ?>

<?php $tpd95ansa = get_tpd95_ansanew(); ?>
<?php $tpd95qc1 = get_tpd95_allnewqc(); ?>
<?php $tpd95qc2 = get_tpd95_allnewqc1(); ?>
<?php $tpd95qc = $tpd95qc1+$tpd95qc2; ?>
<?php $tpd95qa = get_tpd95_allnewqa(); ?>
<?php $tpd95wh = get_tpd95_allnewwh(); ?>

<?php $tpd60ansa = get_tpd60_ansanew(); ?> 
<?php $tpd60qc1 = get_tpd60_allnewqc(); ?>
<?php $tpd60qc2 = get_tpd60_allnewqc1(); ?>
<?php $tpd60qc = $tpd60qc1+$tpd60qc2; ?>
<?php $tpd60qa = get_tpd60_allnewqa(); ?>
<?php $tpd60wh = get_tpd60_allnewwh(); ?>

<?php $tpdsNGansa = get_tpdNG_monthnew();  ?>
<?php $tpdsNGqc1 = get_tpdNG_monthallnewqc(); ?>
<?php $tpdsNGqc2 = get_tpdNG_monthallnewqc1(); ?>
<?php $tpdsNGqc = $tpdsNGqc1+$tpdsNGqc2; ?>
<?php $tpdsNGqa = get_tpdNG_monthallnewqa(); ?>
<?php $tpdsNGwh = get_tpdNG_monthallnewwh(); ?>

    	<script>
var chart = AmCharts.makeChart( "chartdiv", {
  "type": "pie",
  "theme": "light",
  "titles": [ {
    "text": "100 TPD",
    "color":"#333333",
    "size": 16
    
  } ],
  
  "dataProvider": [ {
    "bottle": "ANSA",
    "bottle_no": <?php echo $tpds100ansa; ?>
    
    
  }, {
    "bottle": "QC",
    "bottle_no": <?php echo $tpds100qc; ?>
    
  }, {
    "bottle": "QA",
    "bottle_no": <?php echo $tpds100qa; ?>
    
  }, {
    "bottle": "WH",
    "bottle_no": <?php echo $tpds100wh; ?>
    
  } ],
  "valueField": "bottle_no",
  "titleField": "bottle",
  "startEffect": "elastic",
  "startDuration": 2,
  "labelRadius": 15,
  "innerRadius": "50%",
  "depth3D": 10,
  "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
  "angle": 15,
  "labelText":"[[title]]:[[value]]",
  "export": {
    "enabled": false
  }
} );
</script>
<script>
var chart = AmCharts.makeChart( "chartdiv1", {
  "type": "pie",
  "theme": "light",
  "titles": [ {
    "text": "95 TPD",
    "color":"#333333",
    "size": 16
  } ],
  "dataProvider": [ {
    "bottle": "ANSA",
    "bottle_no": <?php echo $tpd95ansa; ?>
  }, {
    "bottle": "QC",
    "bottle_no": <?php echo $tpd95qc; ?>
  }, {
    "bottle": "QA",
    "bottle_no": <?php echo $tpd95qa; ?>
  }, {
    "bottle": "WH",
    "bottle_no": <?php echo $tpd95wh; ?>
  } ],
  "valueField": "bottle_no",
  "titleField": "bottle",
  "startEffect": "elastic",
  "startDuration": 2,
  "labelRadius": 15,
  "innerRadius": "50%",
  "depth3D": 10,
  "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
  "angle": 15,
  "labelText":"[[title]]:[[value]]",
  "export": {
    "enabled": false
  }
} );
</script>
 
<script>
var chart = AmCharts.makeChart( "chartdiv2", {
  "type": "pie",
  "theme": "light",
  "titles": [ {
    "text": "60 TPD",
    "color":"#333333",
    "size": 16
  } ],
  "dataProvider": [ {
    "bottle": "ANSA",
    "bottle_no": <?php echo $tpd60ansa; ?>
  }, {
    "bottle": "QC",
    "bottle_no": <?php echo $tpd60qc; ?>
  }, {
    "bottle": "QA",
    "bottle_no": <?php echo $tpd60qa; ?>
  }, {
    "bottle": "WH",
    "bottle_no": <?php echo $tpd60wh; ?>
  } ],
  "valueField": "bottle_no",
  "titleField": "bottle",
  "startEffect": "elastic",
  "startDuration": 2,
  "labelRadius": 15,
  "innerRadius": "50%",
  "depth3D": 10,
  "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
  "angle": 15,
  "labelText":"[[title]]:[[value]]",
  "export": {
    "enabled": false
  }
} );
</script>

<script>
var chart = AmCharts.makeChart( "chartdiv3", {
  "type": "pie",
  "theme": "light",
  "titles": [ {
    "text": "NG TPD",
    "color":"#333333",
    "size": 16
  } ],
  "dataProvider": [ {
    "bottle": "ANSA",
    "bottle_no": <?php echo $tpdsNGansa; ?>
  }, {
    "bottle": "QC",
    "bottle_no": <?php echo $tpdsNGqc; ?>
  }, {
    "bottle": "QA",
    "bottle_no": <?php echo $tpdsNGqa; ?>
  }, {
    "bottle": "WH",
    "bottle_no": <?php echo $tpdsNGwh; ?>
  } ],
  "valueField": "bottle_no",
  "titleField": "bottle",
  "startEffect": "elastic",
  "startDuration": 2,
  "labelRadius": 15,
  "innerRadius": "50%",
  "depth3D": 10,
  "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b></span>",
  "angle": 15,
  "labelText":"[[title]]:[[value]]",
  "export": {
    "enabled": false
  }
} );
</script>
	</div>
	<div class="col-md-12">&nbsp;</div>
	<div class="col-md-12">
		<div class="col-md-6">
			<H3>Summary <small>>> For Users</small></H3>
		</div>
		<div class="col-md-6">
			<H3>Summary <small>>> For Management</small></H3>
		</div>
	</div>
	<div class="col-md-12">
		
			<div class="col-md-2"><div class="col-md-6">TDP 60:</div><div class="col-md-1" style="background-color:#926FB2;">&nbsp;</div></div>
			<div class="col-md-2"><div class="col-md-6">TPD 95:</div><div class="col-md-1" style="background-color:#5995A8;">&nbsp;</div></div>
			<div class="col-md-2"><div class="col-md-6">TPD 100:</div><div class="col-md-1" style="background-color:#F0E68C;">&nbsp;</div></div>
			<div class="col-md-2"><div class="col-md-6">NG:</div><div class="col-md-1" style="background-color:#C07171;">&nbsp;</div></div>
		
	</div>
	<div class="col-md-12">&nbsp;</div>
	<div class="col-md-12">
	<div class="col-md-6" id="chart-container">
	
	<?php $totalc60 = get_total_container60(); ?>
	<?php $totalc95 = get_total_container95(); ?>
	<?php $totalc100 = get_total_container100(); ?>
	<?php $totalcNG = get_total_containerNG(); ?>
	<?php $plannedtotal= get_total_number_of_container_planed(); ?>
	
	<?php $detainingtotalc60 = get_total_detainingcontainer60(); ?>
	<?php $detainingtotalc95 = get_total_detainingcontainer95(); ?>
	<?php $detainingtotalc100 = get_total_detainingcontainer100(); ?>
	<?php $detainingtotalcNG = get_total_detainingcontainerNG(); ?>
	<?php $dtotal=get_total_number_of_container_repoted_detaining(); ?>
	 
	
	<?php $comtotalc60 = get_total_completedcontainer60(); ?>
	<?php $comtotalc95 = get_total_completedcontainer95(); ?>
	<?php $comtotalc100 = get_total_completedcontainer100(); ?>
	<?php $comtotalcNG = get_total_completedcontainerNG(); ?>
	<?php $ctotal=get_total_number_of_container_completed(); ?>
	
	<?php $pptotalnew=$ctotal; ?>
		
		<script type="text/javascript" src="http://static.fusioncharts.com/code/latest/fusioncharts.js"></script>
<script type="text/javascript" src="http://static.fusioncharts.com/code/latest/themes/fusioncharts.theme.fint.js?cacheBust=56"></script>
<script type="text/javascript">
  FusionCharts.ready(function(){
    var fusioncharts = new FusionCharts({
    type: 'multilevelpie',
    renderAt: 'chart-container',
    id: "myChart",
    width: '500',
    height: '500',
    dataFormat: 'json',
    dataSource: {
        "chart": {
            "showPlotBorder": "1",
            "piefillalpha": "60",
            "pieborderthickness": "2",
            "hoverfillcolor": "#CCCCCC",
            "piebordercolor": "#FFFFFF",
            "hoverfillcolor": "#CCCCCC",
            
            //Theme
            "theme": "fint"
        },
        "category": [{
            "label": "Total{br}Qty:<?php echo $pptotalnew; ?>",
            "color": "#ffffff",
            "value": "150",
            "category": [{
                "label": "Planned {br}Containers {br}till date, {br}<?php echo $plannedtotal; ?>",
                "color": "#f8bd19",
                "value": "33.3",
                
                "category": [{
                    "label": "<?php echo $totalc60; ?>",
                    "color": "#926FB2",
                    "value": "<?php echo $totalc60; ?>"
                    
                }, {
                    "label": "<?php echo $totalc95; ?>",
                    "color": "#5995A8",
                    "value": "<?php echo $totalc95; ?>"
                }, {
                    "label": "<?php echo $totalc100; ?>",
                    "color": "#F0E68C",
                    "value": "<?php echo $totalc100; ?>"
                }, {
                    "label": "<?php echo $totalcNG; ?>",
                    "color": "#C07171",
                    "value": "<?php echo $totalcNG; ?>"
                }]
            }, {
                "label": "Reported/ {br}Detaining {br}Containers {br}till date, {br}<?php echo $dtotal; ?>",
                "color": "#33ccff",
                "value": "33.3",
                
                "category": [{
                    "label": "<?php echo $detainingtotalc60; ?>",
                    "color": "#926FB2",
                    "value": "<?php echo $detainingtotalc60; ?>"
                }, {
                    "label": "<?php echo $detainingtotalc95; ?>",
                    "color": "#5995A8",
                    "value": "<?php echo $detainingtotalc95; ?>"
                }, {
                    "label": "<?php echo $detainingtotalc100; ?>",
                    "color": "#F0E68C",
                    "value": "<?php echo $detainingtotalc100; ?>"
                }, {
                    "label": "<?php echo $detainingtotalcNG; ?>",
                    "color": "#C07171",
                    "value": "<?php echo $detainingtotalcNG; ?>"
                }]
            }, {
                "label": "Dispatched {br}Containers {br}till date, {br}<?php echo $ctotal; ?>",
                "color": "#FF6462",
                "value": "33.4",
                
                "category": [{
                    "label": "<?php echo $comtotalc60; ?>",
                    "color": "#926FB2",
                    "value": "<?php echo $comtotalc60; ?>",
                   

                }, {
                    "label": "<?php echo $comtotalc95; ?>",
                    "color": "#5995A8",
                    "value": "<?php echo $comtotalc95; ?>"
                }, {
                    "label": "<?php echo $comtotalc100; ?>",
                    "color": "#F0E68C",
                    "value": "<?php echo $comtotalc100; ?>"
                }, {
                    "label": "<?php echo $comtotalcNG; ?>",
                    "color": "#C07171",
                    "value": "<?php echo $comtotalcNG; ?>"
                }]
            }]
        }]
    }
}
);
    fusioncharts.render();
});
</script>
</div>
	<div class="col-md-6" id="chart-container1">
				<?php $totalcnew60 = get_total_containernew60();
						$total60new=count($totalcnew60);
						$rows60 = "";
						$dataamount60 = "";
						foreach ($totalcnew60 as $t60new) {
							$amount60=$t60new->amount_value;
							$dataamount60=$amount60+$rows60;
							
							
							
						}
						
						 ?>
						 <?php $totalcnew95 = get_total_containernew95();
						$total95new=count($totalcnew95);
						$rows95 = "";
						$dataamount95 = "";
						foreach ($totalcnew95 as $t95new) {
							$amount95=$t95new->amount_value;
							$dataamount95=$amount95+$rows95;
							
							
							
						}
						
						 ?>
						 <?php $totalcnew100 = get_total_containernew100();
						$total100new=count($totalcnew100);
						$rows100 = "";
						$dataamount100 = "";
						foreach ($totalcnew100 as $t100new) {
							$amount100=$t100new->amount_value;
							$dataamount100=$amount100+$rows100;
							
							
							
						}
						
						 ?>
						 <?php $totalcnewNG = get_total_containernewNG();
						$totalNGnew=count($totalcnewNG);
						$rowsNG = "";
						$dataamountNG = "";
						foreach ($totalcnewNG as $tNGnew) {
							$amountNG=$tNGnew->amount_value;
							$dataamountNG=$amountNG+$rowsNG;
							
							
							
						}
						
						 ?>
						 <?php $totaldnew60 = get_total_detainingcontainernew60();
						$total60dnew=count($totaldnew60);
						$rowsd60 = "";
						$dataamountd60 = "";
						foreach ($totaldnew60 as $t60newd) {
							$amountd60=$t60newd->amount_value;
							$dataamountd60=$amountd60+$rowsd60;
							
							
							
						}
						
						 ?>
						 <?php $totaldnew95 = get_total_detainingcontainernew95();
						$total95dnew=count($totaldnew95);
						$rowsd95 = "";
						$dataamountd95 = "";
						foreach ($totaldnew95 as $t95newd) {
							$amountd95=$t95newd->amount_value;
							$dataamountd95=$amountd95+$rowsd95;
							
							
							
						}
						
						 ?>
						 <?php $totaldnew100 = get_total_detainingcontainernew100();
						$total100dnew=count($totaldnew100);
						$rowsd100 = "";
						$dataamountd100 = "";
						foreach ($totaldnew100 as $t100newd) {
							$amountd100=$t100newd->amount_value;
							$dataamountd100=$amountd100+$rowsd100;
							
							
							
						}
						
						 ?>
						 <?php $totaldnewNG = get_total_detainingcontainernewNG();
						$totalNGdnew=count($totaldnewNG);
						$rowsdNG = "";
						$dataamountdNG = "";
						foreach ($totaldnewNG as $tNGnewd) {
							$amountdNG=$tNGnewd->amount_value;
							$dataamountdNG=$amountdNG+$rowsdNG;
							
							
							
						}
						
						 ?>
						 <?php $totalttnew60 = get_total_completedcontainernew60();
						$total60ttnew=count($totalttnew60);
						$rowstt60 = "";
						$dataamounttt60 = "";
						
						foreach ($totalttnew60 as $t60newtt) {
							$amounttt60=$t60newtt->amount_value;
							$dataamounttt60=$amounttt60+$rowstt60;
							
							
							
						}
						
						 ?>
						 <?php $totalttnew95 = get_total_completedcontainernew95();
						$total95ttnew=count($totalttnew95);
						$rowstt95 = "";
						$dataamounttt95 = "";
						foreach ($totalttnew95 as $t95newtt) {
							$amounttt95=$t95newtt->amount_value;
							$dataamounttt95=$amounttt95+$rowstt95;
							
							
							
						}
						
						 ?>
						 <?php $totalttnew100 = get_total_completedcontainernew100();
						$total100ttnew=count($totalttnew100);
						$rowstt100 = "";
						$dataamounttt100 = "";
						foreach ($totalttnew100 as $t100newtt) {
							$amounttt100=$t100newtt->amount_value;
							$dataamounttt100=$amounttt100+$rowstt100;
							
							
							
						}
						
						 ?>
						 <?php $totalttnewNG = get_total_completedcontainernewNG();
						$totalNGttnew=count($totalttnewNG);
						$rowsttNG = "";
						$dataamountttNG = "";
						
						foreach ($totalttnewNG as $tNGnewtt) {
							$amountttNG=$tNGnewtt->amount_value;
							$dataamountttNG=$amountttNG+$rowsttNG;
							
							
							
						}
						
						 ?>
						 
						 <?php
							$tttotal= get_total_number_of_container_planed();
							$ttdtotal=get_total_number_of_container_repoted_detaining();
							$tcdtotal=get_total_number_of_container_completed();
							$alltotalqty=$tcdtotal;
							
							$pamount=$dataamount60+$dataamount95+$dataamount100+$dataamountNG;
							$ramount=$dataamountd60+$dataamountd95+$dataamountd100+$dataamountdNG;
							$camount=$dataamounttt60+$dataamounttt95+$dataamounttt100+$dataamountttNG;	
							$allamounttotal=$camount;				 
						 ?>
  		<script type="text/javascript">
  FusionCharts.ready(function(){
    var fusioncharts = new FusionCharts({
    type: 'multilevelpie',
    renderAt: 'chart-container1',
    id: "myChart1",
    width: '500',
    height: '500',
    dataFormat: 'json',
    dataSource: {
        "chart": {
            "showPlotBorder": "1",
            "piefillalpha": "60",
            "pieborderthickness": "2",
            "hoverfillcolor": "#CCCCCC",
            "piebordercolor": "#FFFFFF",
            "hoverfillcolor": "#CCCCCC",
            
           
            //Theme
            "theme": "fint"
        },
        "category": [{
            "label": "Total{br}Qty:<?php echo $alltotalqty; ?>{br}Amount:<?php echo $allamounttotal; ?> Lacs",
            "color": "#ffffff",
            "value": "150",
            "category": [{
                "label": "Planned {br}Containers {br}till date,{br}<?php echo $tttotal; ?>,{br}<?php echo $pamount; ?> Lacs",
                "color": "#f8bd19",
                "value": "33.3",
                
                "category": [{
                    "label": "<?php echo $total60new; ?>,{br}<?php echo $dataamount60; ?> Lacs",
                    "color": "#926FB2",
                    "value": "<?php echo $total60new; ?>"
                    
                }, {
                    "label": "<?php echo $total95new; ?>,{br}<?php echo $dataamount95; ?> Lacs",
                    "color": "#5995A8",
                    "value": "<?php echo $total95new; ?>"
                }, {
                    "label": "<?php echo $total100new; ?>,{br}<?php echo $dataamount100; ?> Lacs",
                    "color": "#F0E68C",
                    "value": "<?php echo $total100new; ?>"
                }, {
                    "label": "<?php echo $totalNGnew; ?>,{br}<?php echo $dataamountNG; ?> Lacs",
                    "color": "#C07171",
                    "value": "<?php echo $totalNGnew; ?>"
                }]
            }, {
                "label": "Reported/ {br}Detaining {br}Containers {br}till date,{br}<?php echo $ttdtotal; ?>,{br}<?php echo $ramount; ?> Lacs",
                "color": "#33ccff",
                "value": "33.3",
                
                "category": [{
                    "label": "<?php echo $total60dnew; ?>,{br}<?php echo $dataamountd60; ?> Lacs",
                    "color": "#926FB2",
                    "value": "<?php echo $total60dnew; ?>"
                }, {
                    "label": "<?php echo $total95dnew; ?>,{br}<?php echo $dataamountd95; ?> Lacs",
                    "color": "#5995A8",
                    "value": "<?php echo $total95dnew; ?>"
                }, {
                    "label": "<?php echo $total100dnew; ?>,{br}<?php echo $dataamountd100; ?> Lacs",
                    "color": "#F0E68C",
                    "value": "<?php echo $total100dnew; ?>"
                }, {
                    "label": "<?php echo $totalNGdnew; ?>,{br}<?php echo $dataamountdNG; ?> Lacs",
                    "color": "#C07171",
                    "value": "<?php echo $totalNGdnew; ?>"
                }]
            }, {
                "label": "Dispatched {br}Containers {br}till date,{br}<?php echo $tcdtotal; ?>,{br}<?php echo $camount; ?> Lacs",
                "color": "#FF6462",
                "value": "33.4",
                
                "category": [{
                    "label": "<?php echo $total60ttnew; ?>,{br}<?php echo $dataamounttt60; ?> Lacs",
                    "color": "#926FB2",
                    "value": "<?php echo $total60ttnew; ?>",
                   

                }, {
                    "label": "<?php echo $total95ttnew; ?>,{br}<?php echo $dataamounttt95; ?> Lacs",
                    "color": "#5995A8",
                    "value": "<?php echo $total95ttnew; ?>"
                }, {
                    "label": "<?php echo $total100ttnew; ?>,{br}<?php echo $dataamounttt100; ?> Lacs",
                    "color": "#F0E68C",
                    "value": "<?php echo $total100ttnew; ?>"
                }, {
                    "label": "<?php echo $totalNGttnew; ?>,{br}<?php echo $dataamountttNG; ?> Lacs",
                    "color": "#C07171",
                    "value": "<?php echo $totalNGttnew; ?>"
                }]
            }]
        }]
    }
}
);
    fusioncharts.render();
});
</script>
    
	
		</div>
    
    			
	
	</div>
	
	<!-- Main row -->
	<div class="col-md-12">&nbsp;</div>
    <div class="row">
    	<div class="col-md-12">
        	<div class="row">
            	<!--div class="col-md-4">
	            	<div class="box box-warning direct-chat direct-chat-warning" style="height: 380px;">
		                <div class="box-header with-border">
		                  	<h3 class="box-title">Direct Messages</h3>
							<div class="box-tools pull-right">
                    			<span data-toggle="tooltip" title="3 New Messages" class="badge bg-yellow">3</span>
                    			<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    			<button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="Contacts" data-widget="chat-pane-toggle"><i class="fa fa-comments"></i></button>
                    			<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  			</div>
                		</div>
                		<div class="box-body">
                  			<div class="direct-chat-messages">
                    			<div class="direct-chat-msg">
                      				<div class="direct-chat-info clearfix">
                        				<span class="direct-chat-name pull-left">Chiren Desai</span>
                        				<span class="direct-chat-timestamp pull-right">23 Jan 2:00 pm</span>
                      				</div>
                      				<img class="direct-chat-img" src="dist/img/user1-128x128.jpg" alt="message user image">
                      				<div class="direct-chat-text">
                        				Is the container MAT-512 Ready to dispatch?
                      				</div>
                      			</div>
                    			<div class="direct-chat-msg right">
			                    	<div class="direct-chat-info clearfix">
			                        	<span class="direct-chat-name pull-right">Sarah Bullock</span>
                        				<span class="direct-chat-timestamp pull-left">23 Jan 2:05 pm</span>
                      				</div>
                      				<img class="direct-chat-img" src="dist/img/user3-128x128.jpg" alt="message user image">
				                    <div class="direct-chat-text">
				                    	No We have a blockage from the TPD-45 Wharehouse. Can you please check?
                      				</div>
                      			</div>
                    	        <div class="direct-chat-msg">
			                      	<div class="direct-chat-info clearfix">
			                        	<span class="direct-chat-name pull-left">Alexander Pierce</span>
                        				<span class="direct-chat-timestamp pull-right">23 Jan 5:37 pm</span>
                      				</div>
                      				<img class="direct-chat-img" src="dist/img/user1-128x128.jpg" alt="message user image">
                      					<div class="direct-chat-text">
                        					Ok let me check. Get back to you in a while.
                      					</div>
                      			</div>
                                <div class="direct-chat-msg right">
			                      	<div class="direct-chat-info clearfix">
			                        	<span class="direct-chat-name pull-right">Sarah Bullock</span>
			                        	<span class="direct-chat-timestamp pull-left">23 Jan 6:10 pm</span>
			                      	</div>
                      				<img class="direct-chat-img" src="dist/img/user3-128x128.jpg" alt="message user image">
				                      	<div class="direct-chat-text">
				                        	Great.. Thanks..!!
				                      	</div>
                      			</div>
                    	   	</div>
                 			<div class="direct-chat-contacts" >
		                    	<ul class="contacts-list">
                      				<li>
                        				<a href="#">
                          				<img class="contacts-list-img" src="dist/img/user1-128x128.jpg" alt="User Image">
                          				<div class="contacts-list-info">
			                                <span class="contacts-list-name">
			                                  Dharmendra Raninga
			                                  <small class="contacts-list-date pull-right">2/28/2015</small>
			                                </span>
                            				<span class="contacts-list-msg">Bottles are ready for MAT32</span>
                          				</div>
                          				</a>
                      				</li>
                      				<li>
                       		 			<a href="#">
                          				<img class="contacts-list-img" src="dist/img/user8-128x128.jpg" alt="User Image">
                          				<div class="contacts-list-info">
			                                <span class="contacts-list-name">
			                                  Parth J.
			                                  <small class="contacts-list-date pull-right">1/4/2015</small>
			                                </span>
                            				<span class="contacts-list-msg">Ok sir will update you on that...</span>
                          				</div>
                          				</a>
                      				</li>
                      				<li>
				                        <a href="#">
				                          	<img class="contacts-list-img" src="dist/img/user6-128x128.jpg" alt="User Image">
											<div class="contacts-list-info">
					                            <span class="contacts-list-name">
					                            Saurabh Mukherjee
					                            	<small class="contacts-list-date pull-right">1/27/2015</small>
				                                </span>
				                            	<span class="contacts-list-msg">Can I take a look at...</span>
				                          	</div>
				                     	</a>
				                  	</li>
                      				<li>
				                    	<a href="#">
				                        <img class="contacts-list-img" src="dist/img/user8-128x128.jpg" alt="User Image">
										<div class="contacts-list-info">
				                        	<span class="contacts-list-name">
				                                  Kenneth M.
				                                  <small class="contacts-list-date pull-right">1/4/2015</small>
				                            </span>
				                            <span class="contacts-list-msg">Never mind I found...</span>
				                      	</div>
				                        </a>
                      				</li>
                      			</ul>
                    		</div>
                  		</div>
                		
                		<div class="box-footer">
                  			<form action="#" method="post">
                    			<div class="input-group">
	                      			<input type="text" name="message" placeholder="Type Message ..." class="form-control">
	                          		<span class="input-group-btn">
	                            		<button type="button" class="btn btn-warning btn-flat">Send</button>
	                          		</span>
                    			</div>
                  			</form>
                		</div>
                	</div>
            </div-->
			<div class="col-md-12">
            	<div class="box box-danger">
                	<div class="box-header with-border">
                  		<h3 class="box-title">Today's Activity History</h3>
						<div class="box-tools pull-right">
                    		<span class="label label-danger">
                    			<?php $jobcount = get_activitycount(); ?>
                    			<?php echo $jobcount; ?> Activity
                    		</span>
                    		<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    		<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                  		</div>
                	</div>
                	<div class="box-body">
                		<table id="exampletwo" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
		         			<thead>
					            <tr>    
					                <th>Sr</th>
					                <th>User</th>
					                <th>Container</th>
					                <th>Jobs</th>
					                <th>Note</th>
					                <th>Date</th>
					                <th>Time</th>
					           	</tr>
		        			</thead>
					        <tfoot>
					            <tr>
					                <th>Sr</th>
					                <th>User</th>
					                <th>Container</th>
					                <th>Jobs</th>
					                <th>Note</th>
					                <th>Date</th>
					                <th>Time</th>
					            </tr>
					        </tfoot>
		        			<tbody>
		        				<?php include('activity.php'); ?>
					        </tbody>
						</table>
              		</div>
                </div>
           	</div>
    	</div>
        
        <div class="box box-info">
        	<div class="box-header with-border">
            	<h3 class="box-title">Recently Planned Containers</h3>
              		<div class="box-tools pull-right">
                		<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                		<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              		</div>
            </div>
            <div class="box-body">
            	<table id="example" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
         			<thead>
			            <tr>    
			                <th>Container No</th>
			                <th>Date Planned On</th>
			                <th style="text-align: center">Status</th>
			           	</tr>
        			</thead>
			        <tfoot>
			            <tr>
			                <th>Container No</th>
			                <th>Date Planned On</th>
			                <th style="text-align: center">Status</th>
			            </tr>
			        </tfoot>
        			<tbody>
        			<?php include("planned_containers.php"); ?>    
					</tbody>
				</table>	            	
        	</div>
      	</div>
	</div>
	</div>
</section>