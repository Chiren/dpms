<?php 

/**
 * Employer Jobs Model.
 * Author 	: Xeme IT Solutions
 * Version 	: 1.0
 */

class Emp_jobs_m extends MY_Model {

	/* Selecting Questions Section wise */
	public function get_all_cat_m()
	{
		$sql="SELECT * FROM job_category";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function get_skills_m($id)
	{
		$sql="SELECT * FROM job_skills WHERE cat_id=".$id."";
		$query=$this->db->query($sql);
		return $query->result();
		
	}
	public function add_job_m($result,$slug)
	{
		$this->load->helper('accreditation');
		$y=$this->session->userdata();
		$sql="INSERT INTO job_posts (job_data,user_id,slug) VALUES ('".$result."','".$y['user_id']."','".$slug."')";
		if($this->db->query($sql))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public function get_applications($user)
	{
//		$job_post="SELECT job_id FROM job_posts WHERE user_id=".$user."";
		$job_post="SELECT GROUP_CONCAT(application_id) as application,job_id,user_id,date,status FROM job_apply WHERE job_id IN (SELECT job_id FROM job_posts WHERE user_id=".$user.") group by job_id";
		$job_id=$this->db->query($job_post);
		$job=$job_id->result();
		return $job;
	}
	public function emp_applications($slug)
	{
//		$job_post="SELECT job_id FROM job_posts WHERE user_id=".$user."";
		$y=$this->session->userdata();
//		print_r($y['user_id']);
		$job_post="SELECT * FROM job_apply WHERE job_id IN (SELECT job_id FROM job_posts WHERE slug='".$slug."' AND user_id=".$y['user_id'].")";
		$job_id=$this->db->query($job_post);
		$job=$job_id->result();
		return $job;
	}
	public function approve_applications_m($id)
	{
		$y=$this->session->userdata();
		$sql="UPDATE job_apply SET status='approved' WHERE application_id=".$id." AND job_id IN (SELECT job_id FROM job_posts WHERE user_id=".$y['user_id'].")";
		if($this->db->query($sql))
		{
			$data=array('application_id'=>	$id,
						'data'			=>	'Application Approved'
						);
			$data1=json_encode($data);
			$sql1="INSERT INTO notifications (data,user_id) SELECT '".$data1."',user_id FROM job_apply WHERE application_id=".$id."";
			if($this->db->query($sql1))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	public function ignore_applications_m($id)
	{
		$y=$this->session->userdata();
		$sql="UPDATE job_apply SET status='ignored' WHERE application_id=".$id." AND job_id IN (SELECT job_id FROM job_posts WHERE user_id=".$y['user_id'].")";
		if($this->db->query($sql))
		{
			//$sql1="INSERT INTO notifications (data,user_id) values ('application ignored','SELECT user_id FROM job_apply WHERE application_id=".$id."')";
			$data=array('application_id'=>	$id,
						'data'			=>	'Application Ignored'
						);
			$data1=json_encode($data);
			$sql1="INSERT INTO notifications (data,user_id) SELECT '".$data1."',user_id FROM job_apply WHERE application_id=".$id."";
			if($this->db->query($sql1))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
	
}
?>