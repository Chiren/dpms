<?php 

/**
 * Accreditation Model.
 * Author 	: Xeme IT Solutions
 * Version 	: 1.0
 */

class Accreditation_m extends MY_Model {

	/* Selecting Questions Section wise */
	public function section_1($sec)
	{
		$sql="SELECT * FROM accreditation_questions WHERE sec_id=".$sec;
		$query=$this->db->query($sql);
		return $query->result();
	}
	/* Saving Request  */
	public function save_test($data)
	{
		$sql="INSERT INTO accreditation_test (array,user_id) VALUES ('".$data['array']."','".$data['user_id']."')";
		$this->db->query($sql);
		return true;
	}
}