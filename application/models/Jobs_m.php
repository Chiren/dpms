<?php 

/**
 * Public Jobs Model.
 * Author 	: Xeme IT Solutions
 * Version 	: 1.0
 */

class Jobs_m extends MY_Model {

	public function all_jobs()
	{
		$sql="SELECT * FROM job_posts WHERE status='approved'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function single_jobs_m($slug)
	{
		$sql="SELECT * FROM job_posts WHERE slug='".$slug."' AND status='approved'";
		$query=$this->db->query($sql);
		return $query->result();
	}
	public function apply_m($slug)
	{
		$job_id_sql="SELECT job_id FROM job_posts WHERE slug='".$slug."'";
		$job_id_data=$this->db->query($job_id_sql);
		$job_id=$job_id_data->result();
		$job=$job_id[0]->job_id;
		$user_id=$this->session->userdata();
		$user=$user_id['user_id'];
		$sql="INSERT INTO job_apply (job_id,user_id) VALUES ('".$job."','".$user."')";
		if($this->db->query($sql))
		{
			return true;
		}
		else
		{
			return false;
		}

	}	
}
?>