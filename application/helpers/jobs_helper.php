<?php 

/**
 * Jobs And Employer Helper.
 * Author 	: Xeme IT Solutions
 * Version 	: 1.0
 */

function get_category($cat_id)					
{
	$ci=&get_instance();
	$sql="SELECT category_name FROM job_category WHERE cat_id=".$cat_id;
	$query=$ci->db->query($sql);
	return $query->result();
}

function get_skills($skill_id)
{
	$ci=&get_instance();
	$sql="SELECT skill_name FROM job_skills WHERE skill_id=".$skill_id;
	$query=$ci->db->query($sql);
	return $query->result();
}
function get_jobs($id)
{
	$ci=&get_instance();
	$sql="SELECT slug FROM job_posts WHERE job_id=".$id;
	$query=$ci->db->query($sql);
	return $query->result();
}
function get_users($id)
{
	$ci=&get_instance();
	$sql="SELECT first_name,last_name FROM users WHERE id=".$id;
	$query=$ci->db->query($sql);
	return $query->result();
	
}
function  get_job_data($id)
{
	$ci=&get_instance();
	$sql="SELECT * FROM job_posts WHERE job_id=".$id;
	$query=$ci->db->query($sql);
	return $query->result();
	
}
function get_pending_application($id)
{
	$ci=&get_instance();
	$sql="SELECT count(application_id) as pending FROM job_apply WHERE status='pending' AND job_id=".$id;
	$query=$ci->db->query($sql);
	return $query->result();
}
?>