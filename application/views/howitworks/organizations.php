<?php $this->layout('layouts::default') ?>
<div class="container padding-top-page">
	<div class="row">
		<div class="big-title text-left col-md-6 ">
			<h1>How It <span class="accent-color">Works</span></h1>
		</div>
		<div class="big-title text-right col-md-2 active-works-hr">
			<a href="how-it-works/for-professionals"><h3 style="font-weight:100 !important;">For <span class="accent-color">Professionals</span></h3></a>
		</div>
		<div class="big-title text-right col-md-2 active-works">
			<h3  style="font-weight:100 !important;">For <span class="accent-color">Organizations</span></h3>
		</div>
		<div class="big-title text-left col-md-2 active-works-hr">
			<a href="how-it-works/for-enterprises"><h3  style="font-weight:100 !important;">For <span class="accent-color">Enterprises</span></h3></a>
		</div>		
	</div>
</div>	
<div class="row">
	<div class="big-title text-left col-md-12 works-title-div">
		<div class="col-md-6 padding-top-page">
			<h2>Put your skills to <span class="accent-color">good use</span></h2>
			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
			<button class="btn btn-lg btn-warning btn-style">Get Started</button>
		</div>
		<div class="col-md-6">
			<img src="assets/images/no_image.png">
		</div>
	</div>
</div>
<div class="container">
	<div class="row">
		<div class="big-title text-left col-md-8 ">
			<div class="big-title col-md-12">	
				<h2>How to volunteer</h2>
				<hr>
			</div>
			<div class="row padding-bottom-works">
				<div class="col-md-1">
					<span class="num-i">1</span>
				</div>
				<div class="col-md-11">
					<h4>Search</h4>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</p>	
				</div>
			</div>
			<div class="row padding-bottom-works">	
				<div class="col-md-1">
					<span class="num-i">2</span>
				</div>
				<div class="col-md-11">
					<h4>Apply</h4>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</p>	
				</div>
			</div>
			<div class="row padding-bottom-works">
				<div class="col-md-1">
					<span class="num-i">3</span>
				</div>
				<div class="col-md-11">
					<h4>Interview</h4>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</p>	
				</div>
			</div>
			<div class="row padding-bottom-works">
				<div class="col-md-1">
					<span class="num-i">4</span>
				</div>
				<div class="col-md-11">
					<h4>Start</h4>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</p>	
				</div>
			</div>
		</div>
		<div class="big-title text-center col-md-4" style="border: 1px solid rgb(204, 204, 204);">
			<div class="row service-sidebar">
				<div class="col-md-12 big-title">
					<img src="assets/images/howitworks_organization.png">
					<h2>Organizations we partner with</h2>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</p>
				</div>
			</div>
		
			<div class="row service-sidebar">
				<div class="col-md-12 big-title">
					<img src="assets/images/howitworks_organization.png">
					<h2>Projects we offer</h2>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</p>
				</div>
			</div>
		</div>		
	</div>
</div>
<div class="row">
	<div class="big-title text-left col-md-12 works-title-div" style="padding: 13px 58px !important;">
		<div class="col-md-12">
			<h2>Testimonials</h2>
		</div>
		<div class="row" style="padding-top: 45px ! important; padding-bottom: 17px;">
			<div class="col-md-3">
				<p><i class="fa fa-quote-left"></i> &nbsp;&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
				<b style="float:right;">-Mr.Xyz (Abcd ,City)</b>
			</div>
			<div class="col-md-3">
				<p><i class="fa fa-quote-left"></i> &nbsp;&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
				<b style="float:right;">-Mr.Xyz (Abcd ,City)</b>
			</div>
			<div class="col-md-3">
				<p><i class="fa fa-quote-left"></i> &nbsp;&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
				<b style="float:right;">-Mr.Xyz (Abcd ,City)</b>
			</div>
			<div class="col-md-3">
				<p><i class="fa fa-quote-left"></i> &nbsp;&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
				<b style="float:right;">-Mr.Xyz (Abcd ,City)</b>
			</div>	
		</div>
	</div>
</div>
