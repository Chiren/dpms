<?php $this->layout('layouts::default') ?>

<!-- view: errors/custom/error_404.php  -->

  <div id="content">
      <div class="container">
        <div class="page-content">
          <div class="error-page">
            <h1>404</h1>
            <h3>Page not Found</h3>
            <p>We're sorry, but the page you were looking for doesn't exist.</p>
            <div class="text-center"><a href="" class="btn-system btn-small">Back To Home</a></div>
          </div>
        </div>
      </div>
    </div>
    