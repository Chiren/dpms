<?php $this->layout('layouts::default') ?>
<style type="text/css">
.personal-info-li > li {
    border-bottom: 1px solid #ddd;
    padding: 6px 0;
    }

.timeline {
  list-style: none;
  padding: 20px 0 20px;
  position: relative;
}

.timeline:before {
  top: 0;
  bottom: 0;
  position: absolute;
  content: " ";
  width: 3px;
  background-color: #eeeeee;
  left: 50%;
  margin-left: -1.5px;
}

.timeline > li {
  margin-bottom: 20px;
  position: relative;
}

.timeline > li:before,
.timeline > li:after {
  content: " ";
  display: table;
}

.timeline > li:after {
  clear: both;
}

.timeline > li:before,
.timeline > li:after {
  content: " ";
  display: table;
}

.timeline > li:after {
  clear: both;
}

.timeline > li > .timeline-panel {
  width: 46%;
  float: left;
  border: 1px solid #d4d4d4;
  border-radius: 2px;
  padding: 20px;
  position: relative;
  -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
  box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
}

.timeline > li > .timeline-panel:before {
  position: absolute;
  top: 26px;
  right: -15px;
  display: inline-block;
  border-top: 15px solid transparent;
  border-left: 15px solid #ccc;
  border-right: 0 solid #ccc;
  border-bottom: 15px solid transparent;
  content: " ";
}

.timeline > li > .timeline-panel:after {
  position: absolute;
  top: 27px;
  right: -14px;
  display: inline-block;
  border-top: 14px solid transparent;
  border-left: 14px solid #fff;
  border-right: 0 solid #fff;
  border-bottom: 14px solid transparent;
  content: " ";
}

.timeline > li > .timeline-badge {
  color: #fff;
  width: 50px;
  height: 50px;
  line-height: 50px;
  font-size: 1.4em;
  text-align: center;
  position: absolute;
  top: 16px;
  left: 50%;
  margin-left: -25px;
  background-color: #999999;
  z-index: 100;
  border-top-right-radius: 50%;
  border-top-left-radius: 50%;
  border-bottom-right-radius: 50%;
  border-bottom-left-radius: 50%;
}

.timeline > li.timeline-inverted > .timeline-panel {
  float: right;
}

.timeline > li.timeline-inverted > .timeline-panel:before {
  border-left-width: 0;
  border-right-width: 15px;
  left: -15px;
  right: auto;
}

.timeline > li.timeline-inverted > .timeline-panel:after {
  border-left-width: 0;
  border-right-width: 14px;
  left: -14px;
  right: auto;
}

.timeline-badge.primary {
  background-color: #2e6da4 !important;
}

.timeline-badge.success {
  background-color: #3f903f !important;
}

.timeline-badge.warning {
  background-color: #f0ad4e !important;
}

.timeline-badge.danger {
  background-color: #d9534f !important;
}

.timeline-badge.info {
  background-color: #5bc0de !important;
}

.timeline-title {
  margin-top: 0;
  color: inherit;
}

.timeline-body > p,
.timeline-body > ul {
  margin-bottom: 0;
}

.timeline-body > p + p {
  margin-top: 5px;
}
</style>
<?php 
	$ci =& get_instance();
	$ci->load->library('session');
/*	print_r($ci->session->userdata());
	echo"<br>"; */
	$x=$ci->session->userdata('user_group');
	$ci->load->helper('accreditation');
	$y=$ci->session->userdata();
	$request_status=check_request($y['user_id']);
?>
<!--  
<section class="content">
				<div class="container padding-top-page">
	<div class="row">
		<div class="big-title text-left col-md-6 ">
			<h1>Welcome, <span class="accent-color"><?php echo $user->first_name.' '.$user->last_name; ?>!</span></h1>
		</div>	
	</div>
		<?php $ci=&get_instance(); 
		$ci->load->library('session');
		$job_post=$ci->session->flashdata('job_post_success');
		if(!empty($job_post))
		{
		?>
		<div class="alert alert-success">
    		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    		<strong>Success!</strong> You have successfully posted Job
  		</div>
		<?php } ?>
	
</div>	
<div class="row">
	
	<div class="big-title text-left col-md-12 works-account-div">
			 <div class="col-md-6 account-frm">
			 <div class="row">
			 	<lable class="col-md-4"><b>Frist Name</b></lable>
				<lable class="col-md-4"><?php echo $user->first_name ?></lable>
			</div>	
			 <div class="row">
  				<lable class="col-md-4"><b>Last Name</b></lable>
  				<lable class="col-md-4"><?php echo $user->last_name;?></lable>
  			</div>
  			 <div class="row">	
  				<lable class="col-md-4"><b>Email</b></lable>
  				<lable class="col-md-4"><?php echo $user->email; ?></lable>
			</div>	
			<?php
			if($request_status[0]->status=='approved')
			{
				echo"<label class='label label-success'>Approved</label>";
			}
			elseif ($request_status[0]->status=='pending')
			{
				echo"<label class='label label-warning'>Pending</label>";
			}
			else 
			{
				foreach($x as $y)
				{
					if($y->group_id == 5)
					{
			?> 
			 	<div class="row">
			 		<div class="col-md-4">
			 			<a href="accreditation" class="btn btn-warning btn-xs">Click Here for Accreditation Form</a>
			 		</div>
				 </div>
			<?php 
					}
				}
			}
			?>
			</div>
			
			
			
			<div class="col-md-2">
			<img src="assets/images/abc.jpeg">
		    </div> 		
		
		 
	</div>
</div>
	
	</section>
-->	
	<?php $ci=&get_instance(); 
		$ci->load->library('session');
		$job_post=$ci->session->flashdata('job_post_success');
		if(!empty($job_post))
		{
		?>
		<div class="alert alert-success">
    		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    		<strong>Success!</strong> <?php print_r($job_post); ?>
  		</div>
		<?php } ?>
<section class="container">

	<!-- Welcome Profile Block -->
	<div class="row">
		<div class="col-md-4" style="border-right: 1px solid #ddd;  padding: 30px 35px;">
			<div class="big-title text-right">
				<h2><?php echo $user->first_name ?> <span class="accent-color"><?php echo $user->last_name; ?></span></h2>
				<p>Welcome to your Profile</p>
			</div>
		</div>
		<div class="col-md-8" style=" padding: 10px 150px 30px 35px;">
			<div>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer.  </p>
			</div>
		</div>
	</div>
	<!-- End Welcome Profile Block -->	
	
	<!-- Personal Info Block -->
	<div class="row">
		<div class="col-md-4" style="text-align: center;">
		  	
		  	<?php if($x[0]->group_id == 6){ ?>
		  	<label style="font-size: 20px;font-weight: 700; padding: 10px 0;">My Story</label>
			<p style=" padding-bottom: 15px;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer. </p>
			<?php } else{?>
			<a href="employer-jobs/add-jobs" class="btn-system btn-medium border-btn">Add Jobs</a> <br><br>
			<a href="employer-jobs/applications" class="btn-system btn-medium border-btn">View All Applications</a>
			<?php } ?>
		</div>
		<div class="col-md-4" style="text-align: center; padding-top: 20px;">
			<img src="uploads/rahil.jpg" class="img-circle" style="width: 50%; height: 180px;"><br>
			<button class="btn-system btn-medium" style="margin-top: 20px;">Download Resume</button>
		</div>
		<div class="col-md-4">
			<label style="font-size: 20px;font-weight: 700; padding: 10px 0;">Personal Information</label>
			<ul class="personal-info-li">
				<li>Name : <?php echo $user->first_name ?> <?php echo $user->last_name; ?> </li>
				<li>Age : 21 Years Old</li>
				<li>Phone : +91 9727XXXXXX</li>
				<li>Email : <?php echo $user->email; ?></li>
			</ul>
		</div>
	</div>
	<!-- End Personal Info Block -->
	
	<!-- Resume Block -->
	<div class="row">
		<div class="col-md-4" style="border-right: 1px solid #ddd;  padding: 30px 35px;">
			<div class="big-title text-right">
				<i class="fa fa-book fa-4x"></i><br>
				<h2>Resume</h2>
				<p>Know who I am..</p>
			</div>
		</div>
		<div class="col-md-8" style=" padding: 10px 150px 30px 35px;">
			<div>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer.  </p>
			</div>
		</div>
	</div>
	<!-- End Resume Block -->	
<?php 
//print_r();
//if($y['user_group'][0]->group_id == 6)
if($x[0]->group_id == 6)
{
?>	
	<!-- Education Block  -->
	<div class="row">
	<label style="font-size: 20px;font-weight: 700; padding: 10px 0;"><i class="fa fa-graduation-cap"></i> Education</label><br>
	<div class="col-md-9 page-content">

          
            <div class="tabs-section">

              <!-- Nav Tabs -->
              <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#tab-4">Tab Title Without Icon 1</a></li>
                <li><a data-toggle="tab" href="#tab-5">Tab Title 2</a></li>
                <li><a data-toggle="tab" href="#tab-6">Tab Title 3</a></li>
              </ul>

              <!-- Tab Panels -->
              <div class="tab-content">
                <!-- Tab Content 1 -->
                <div id="tab-4" class="tab-pane fade in active">
                  <p><strong class="accent-color">Sed ut perspiciatis</strong> unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explica. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                </div>
                <!-- Tab Content 2 -->
                <div id="tab-5" class="tab-pane fade">
                  <p><img src="images/bussniss-pic.jpg" alt="" style="float:left; width:180px;" class="img-thumbnail image-text"></p>
                  <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explica. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rational encounter consequences that are extremely painful.</p>
                </div>
                <!-- Tab Content 3 -->
                <div id="tab-6" class="tab-pane fade">
                  <p><strong>Lorem ipsum</strong> dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
              </div>
              <!-- End Tab Panels -->

            </div>

          </div>
	</div>	
	<!-- End Education Block  -->
	
	<!-- Experince Block  -->
	<div class="row">
	<label style="font-size: 20px;font-weight: 700; padding: 10px 0;margin-top: 20px;"><i class="fa fa-flask"></i> Experience</label><br>
	<ul class="timeline">
    <li>
      <div class="timeline-badge"><i class="glyphicon glyphicon-check"></i></div>
      <div class="timeline-panel">
        <div class="timeline-heading">
          <h4 class="timeline-title">Mussum ipsum cacilds</h4>
          <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> 11 hours ago via Twitter</small></p>
        </div>
        <div class="timeline-body">
          <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo.
            Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
        </div>
      </div>
    </li>
    <li class="timeline-inverted">
      <div class="timeline-badge warning"><i class="glyphicon glyphicon-credit-card"></i></div>
      <div class="timeline-panel">
        <div class="timeline-heading">
          <h4 class="timeline-title">Mussum ipsum cacilds</h4>
        </div>
        <div class="timeline-body">
          <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo.
            Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
          <p>Suco de cevadiss, é um leite divinis, qui tem lupuliz, matis, aguis e fermentis. Interagi no mé, cursus quis, vehicula ac nisi. Aenean vel dui dui. Nullam leo erat, aliquet quis tempus a, posuere ut mi. Ut scelerisque neque et turpis posuere
            pulvinar pellentesque nibh ullamcorper. Pharetra in mattis molestie, volutpat elementum justo. Aenean ut ante turpis. Pellentesque laoreet mé vel lectus scelerisque interdum cursus velit auctor. Lorem ipsum dolor sit amet, consectetur adipiscing
            elit. Etiam ac mauris lectus, non scelerisque augue. Aenean justo massa.</p>
        </div>
      </div>
    </li>
    <li>
      <div class="timeline-badge danger"><i class="glyphicon glyphicon-credit-card"></i></div>
      <div class="timeline-panel">
        <div class="timeline-heading">
          <h4 class="timeline-title">Mussum ipsum cacilds</h4>
        </div>
        <div class="timeline-body">
          <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo.
            Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
        </div>
      </div>
    </li>
    <li class="timeline-inverted">
      <div class="timeline-panel">
        <div class="timeline-heading">
          <h4 class="timeline-title">Mussum ipsum cacilds</h4>
        </div>
        <div class="timeline-body">
          <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo.
            Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
        </div>
      </div>
    </li>
    <li>
      <div class="timeline-badge info"><i class="glyphicon glyphicon-floppy-disk"></i></div>
      <div class="timeline-panel">
        <div class="timeline-heading">
          <h4 class="timeline-title">Mussum ipsum cacilds</h4>
        </div>
        <div class="timeline-body">
          <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo.
            Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
          <hr>
          <div class="btn-group">
            <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
              <i class="glyphicon glyphicon-cog"></i> <span class="caret"></span>
            </button>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Action</a></li>
              <li><a href="#">Another action</a></li>
              <li><a href="#">Something else here</a></li>
              <li class="divider"></li>
              <li><a href="#">Separated link</a></li>
            </ul>
          </div>
        </div>
      </div>
    </li>
    <li>
      <div class="timeline-panel">
        <div class="timeline-heading">
          <h4 class="timeline-title">Mussum ipsum cacilds</h4>
        </div>
        <div class="timeline-body">
          <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo.
            Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
        </div>
      </div>
    </li>
    <li class="timeline-inverted">
      <div class="timeline-badge success"><i class="glyphicon glyphicon-thumbs-up"></i></div>
      <div class="timeline-panel">
        <div class="timeline-heading">
          <h4 class="timeline-title">Mussum ipsum cacilds</h4>
        </div>
        <div class="timeline-body">
          <p>Mussum ipsum cacilds, vidis litro abertis. Consetis adipiscings elitis. Pra lá , depois divoltis porris, paradis. Paisis, filhis, espiritis santis. Mé faiz elementum girarzis, nisi eros vermeio, in elementis mé pra quem é amistosis quis leo.
            Manduma pindureta quium dia nois paga. Sapien in monti palavris qui num significa nadis i pareci latim. Interessantiss quisso pudia ce receita de bolis, mais bolis eu num gostis.</p>
        </div>
      </div>
    </li>
  </ul>
	</div>	
	<!-- End Experince Block  -->
	
	<!-- Skills Block -->
	<div class="row">
		<div class="col-md-4" style="border-right: 1px solid #ddd;  padding: 30px 35px;">
			<div class="big-title text-right">
				<i class="fa fa-cog fa-4x"></i><br>
				<h2>Skills</h2>
				<p>what my clients get</p>
			</div>
		</div>
		<div class="col-md-8" style=" padding: 10px 150px 30px 35px;">
			<div>
				<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer.  </p>
			</div>
		</div>
	</div>
	<div class="row">
	<div>

            <div class="col-md-8 col-md-offset-2">

              <!-- Classic Heading -->
              <h4 class="classic-title"><span></span></h4>

              <div class="row">

                <!-- Start Service Icon 1 -->
                <div class="col-md-6 service-box service-icon-left-more">
                  <div class="service-icon">
                    <i class="fa fa-magic icon-medium"></i>
                  </div>
                  <div class="service-content">
                    <h4>Photoshop</h4>
                    <p>It is a long <strong class="accent-color">established</strong> fact that a reader will be distracted by the readable content of a page.</p>
                  </div>
                </div>
                <!-- End Service Icon 1 -->

                <!-- Start Service Icon 2 -->
                <div class="col-md-6 service-box service-icon-left-more">
                  <div class="service-icon">
                    <i class="fa fa-users icon-medium"></i>
                  </div>
                  <div class="service-content">
                    <h4>Web Marketing</h4>
                    <p>It is a long <strong class="accent-color">established</strong> fact that a reader will be distracted by the readable content of a page.</p>
                  </div>
                </div>
                <!-- End Service Icon 2 -->

                <!-- Start Service Icon 3 -->
                <div class="col-md-6 service-box service-icon-left-more">
                  <div class="service-icon">
                    <i class="fa fa-globe icon-medium"></i>
                  </div>
                  <div class="service-content">
                    <h4>Web Hosting</h4>
                    <p>It is a long <strong class="accent-color">established</strong> fact that a reader will be distracted by the readable content of a page.</p>
                  </div>
                </div>
                <!-- End Service Icon 3 -->

                <!-- Start Service Icon 4 -->
                <div class="col-md-6 service-box service-icon-left-more">
                  <div class="service-icon">
                    <i class="fa fa-picture-o icon-medium"></i>
                  </div>
                  <div class="service-content">
                    <h4>Phtography</h4>
                    <p>It is a long <strong class="accent-color">established</strong> fact that a reader will be distracted by the readable content of a page.</p>
                  </div>
                </div>
                <!-- End Service Icon 4 -->

                <!-- Start Service Icon 5 -->
                <div class="col-md-6 service-box service-icon-left-more">
                  <div class="service-icon">
                    <i class="fa fa-leaf icon-medium"></i>
                  </div>
                  <div class="service-content">
                    <h4>Product Design</h4>
                    <p>It is a long <strong class="accent-color">established</strong> fact that a reader will be distracted by the readable content of a page.</p>
                  </div>
                </div>
                <!-- End Service Icon 5 -->

                <!-- Start Service Icon 6 -->
                <div class="col-md-6 service-box service-icon-left-more">
                  <div class="service-icon">
                    <i class="fa fa-umbrella icon-medium"></i>
                  </div>
                  <div class="service-content">
                    <h4>Supporting</h4>
                    <p>It is a long <strong class="accent-color">established</strong> fact that a reader will be distracted by the readable content of a page.</p>
                  </div>
                </div>
                <!-- End Service Icon 6 -->

              </div>
            </div>

          </div>
	</div>
	<!-- End Skills Block -->
	
	<!-- Pricing Block -->
	<div class="row">
	<div class="pricing-section" style="padding-top:60px; padding-bottom:60px;">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <!-- Start Big Heading -->
            <div class="big-title text-center">
              <h1>We Have Nice Pricing Plans For <strong>You!</strong></h1>
            </div>
            <!-- End Big Heading -->

            <!-- Text -->
            <p class="text-center" style="margin-bottom:20px;">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
          </div>
        </div>

        <div class="row pricing-tables">

          <div class="col-md-3 col-sm-3">
            <div class="pricing-table">
              <div class="plan-name">
                <h3>Basic</h3>
              </div>
              <div class="plan-price">
                <div class="price-value">$49<span>.00</span></div>
                <div class="interval">per month</div>
              </div>
              <div class="plan-list">
                <ul>
                  <li><strong>40 GB</strong> Storage</li>
                  <li><strong>40GB</strong> Transfer</li>
                  <li><strong>10</strong> Domains</li>
                  <li><strong>20</strong> Projects</li>
                  <li><strong>Free</strong> installation</li>
                </ul>
              </div>
              <div class="plan-signup">
                <a href="#" class="btn-system btn-small">Sign Up Now</a>
              </div>
            </div>
          </div>

          <div class="col-md-3 col-sm-3">
            <div class="pricing-table highlight-plan">
              <div class="plan-name">
                <h3>Advanced</h3>
              </div>
              <div class="plan-price">
                <div class="price-value">$99<span>.00</span></div>
                <div class="interval">per month</div>
              </div>
              <div class="plan-list">
                <ul>
                  <li><strong>40 GB</strong> Storage</li>
                  <li><strong>40GB</strong> Transfer</li>
                  <li><strong>10</strong> Domains</li>
                  <li><strong>20</strong> Projects</li>
                  <li><strong>Free</strong> installation</li>
                </ul>
              </div>
              <div class="plan-signup">
                <a href="#" class="btn-system btn-small border-btn">Sign Up Now</a>
              </div>
            </div>
          </div>


          <div class="col-md-3 col-sm-3">
            <div class="pricing-table">
              <div class="plan-name">
                <h3>Professional</h3>
              </div>
              <div class="plan-price">
                <div class="price-value">$199<span>.00</span></div>
                <div class="interval">per month</div>
              </div>
              <div class="plan-list">
                <ul>
                  <li><strong>40 GB</strong> Storage</li>
                  <li><strong>40GB</strong> Transfer</li>
                  <li><strong>10</strong> Domains</li>
                  <li><strong>20</strong> Projects</li>
                  <li><strong>Free</strong> installation</li>
                </ul>
              </div>
              <div class="plan-signup">
                <a href="#" class="btn-system btn-small">Sign Up Now</a>
              </div>
            </div>
          </div>


          <div class="col-md-3 col-sm-3">
            <div class="pricing-table">
              <div class="plan-name">
                <h3>Extreme</h3>
              </div>
              <div class="plan-price">
                <div class="price-value">$299<span>.00</span></div>
                <div class="interval">per month</div>
              </div>
              <div class="plan-list">
                <ul>
                  <li><strong>40 GB</strong> Storage</li>
                  <li><strong>40GB</strong> Transfer</li>
                  <li><strong>10</strong> Domains</li>
                  <li><strong>20</strong> Projects</li>
                  <li><strong>Free</strong> installation</li>
                </ul>
              </div>
              <div class="plan-signup">
                <a href="#" class="btn-system btn-small">Sign Up Now</a>
              </div>
            </div>
          </div>


        </div>
      </div>
    </div>
		
	</div>
	<!-- End Pricing Block -->
	
	<!-- Contact Block Starts -->
	<div class="row">
	  <h4 class="classic-title"><span>Contact Me</span></h4>

            <!-- Start Contact Form -->
            <form role="form" class="contact-form" id="contact-form" method="post">
              <div class="form-group">
                <div class="controls">
                  <input type="text" placeholder="Name" name="name">
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <input type="email" class="email" placeholder="Email" name="email">
                </div>
              </div>
              <div class="form-group">
                <div class="controls">
                  <input type="text" class="requiredField" placeholder="Subject" name="subject">
                </div>
              </div>

              <div class="form-group">

                <div class="controls">
                  <textarea rows="7" placeholder="Message" name="message"></textarea>
                </div>
              </div>
              <button type="submit" id="submit" class="btn-system btn-large">Send</button>
              <div id="success" style="color:#34495e;"></div>
            </form>
            <!-- End Contact Form -->
	</div>
	<!--  End Contact Form -->
<?php } ?>
</section>
	