<?php $this->layout('layouts::default') ?>
<div class="container padding-top-page">
	<div class="row">
		<div class="big-title text-left col-md-12 ">
			<h1>Accreditation<span class="accent-color"></span></h1>
		</div>
	</div>
</div>	
<div class="container">
	<div class="row">
		<div class="big-title text-left col-md-8 ">
			<div class="big-title col-md-12">	
				<h2><span class="accent-color">Section 2 </span>Supporting, Guiding and Developing Young People</h2>
				<hr>
			</div>
			<div class="row padding-bottom-works accreditation-question">
				<div class="col-md-1">
					<span class="num-i">1</span>
				</div>
				<div class="col-md-11">
					<h4>Are Managers clear about the knowledge, skills and behaviours you require of Young People? </h4>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>No, Managers do not seem to understand what knowledge, skills and behaviours we require from Young People. </p>	
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>Some Managers responsible for Young People understand these attributes, but these are not clearly defined yet. </p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>All Managers responsible for Young People understand these attributes but we could more clearly define them. </p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>Definitely. The knowledge, skills and behaviours we require from Young People is clearly defined and understood and Young People are effectively supported. </p>		
						</div>
					</div>
				</div>
			</div>
			<div class="row padding-bottom-works accreditation-question">
				<div class="col-md-1">
					<span class="num-i">2</span>
				</div>
				<div class="col-md-11">
					<h4>Does the organisation have a strategy that supports the development of Young People?</h4>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>There are no specific resources allocated to support the development of Young People.</p>	
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
								<p>We have a strategy to support the development of all people but there are no specific strategies or resources allocated for Young People. </p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>We have a strategy that specifically supports the development of Young People but it isn’t appropriately resourced and we do not provide qualifications </p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>We have a clearly defined strategy to support the development of Young People in our organisation that all people understand. We make resources available and offer relevant qualifications. </p>		
						</div>
					</div>
				</div>
			</div>
			<div class="row padding-bottom-works accreditation-question">
				<div class="col-md-1">
					<span class="num-i">3</span>
				</div>
				<div class="col-md-11">
					<h4>Do you engage with external stakeholders (including education and training providers) to influence the development of Young People? </h4>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>No we don’t engage with any external stakeholders to influence the development of Young People. </p>	
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
								<p>We have given some thought to engaging with external stakeholders but have not established any dialogue yet. </p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>Leaders are actively engaged with external stakeholders to influence the development of Young People but managers of Young People are not.</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>Leaders and Managers engage with education and training providers and external stakeholders to discuss our organisation’s expectations of Young People to influence the way they are developed.</p>		
						</div>
					</div>
				</div>
			</div>
			<div class="row padding-bottom-works accreditation-question">
				<div class="col-md-1">
					<span class="num-i">4</span>
				</div>
				<div class="col-md-11">
					<h4>Do you think Managers understand their role in leading, managing, developing and effectively deploying Young People? </h4>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>No, Managers do not understand their role in leading, managing and developing Young People. </p>	
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
								<p>Some Managers do understand but we do not develop those that do not. However we have not defined the knowledge, skills and behaviours effective Managers need. </p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>We have gone some way in defining the knowledge, skills and behaviours that Managers need to effectively support Young People. Most Managers understand these and are provided with help to develop these capabilities. </p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>Definitely. All Managers are clear about what we expect of them and develop their capabilities where necessary. Our Young People would confirm that Managers effectively support them. </p>		
						</div>
					</div>
				</div>
			</div>
			<div class="row padding-bottom-works accreditation-question">
				<div class="col-md-1">
					<span class="num-i">5</span>
				</div>
				<div class="col-md-11">
					<h4>Do you have a succession planning strategy and does it take account of Young People?</h4>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>We don’t have a succession planning strategy. </p>	
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
								<p>We have given some thought to a succession planning strategy. </p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>We have a succession planning strategy in place but it does not include Young People. </p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>LWe have a succession planning strategy in place, which includes Young People. </p>		
						</div>
					</div>
				</div>
			</div>
			<div class="row padding-bottom-works accreditation-question">
				<div class="col-md-1">
					<span class="num-i">6</span>
				</div>
				<div class="col-md-11">
					<h4>Are Young People effectively supported when they start working in your organisation? </h4>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>We don’t support Young People when they join the organisation.</p>	
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
								<p>Some Managers understand their role in supporting Young People when they join the organisation but we do not have a structured induction process. </p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>We have an induction process and most Managers support Young People and help them integrate into their team when they join the organisation, but we don’t do this consistently. </p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>Definitely. All managers understand the importance of supporting all Young People's transition into the workplace. They ensure that their induction is effective and they are effectively integrated into their team.</p>		
						</div>
					</div>
				</div>
			</div>
			<div class="row padding-bottom-works accreditation-question">
				<div class="col-md-1">
					<span class="num-i">7</span>
				</div>
				<div class="col-md-11">
					<h4>Do you think Managers understand the working practices and codes of conduct Young People are expected to work to? </h4>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>We don’t seem to have any codes of conduct in the workplace. </p>	
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
								<p>We have given some thought to codes of conduct but Managers do not have a common understanding of what these are. </p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>Most Managers and Young People understand our codes of conduct and working practices.</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>All Managers and Young People understand our working practices and the codes of conduct, which are important in our workplace. </p>		
						</div>
					</div>
				</div>
			</div>
			<div class="row padding-bottom-works accreditation-question">
				<div class="col-md-1">
					<span class="num-i">8</span>
				</div>
				<div class="col-md-11">
					<h4>How effective is the organisation at providing support and guidance for Young People? </h4>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>We are not effective at supporting and guiding Young People.</p>	
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
								<p>Leaders have some understanding of the benefits of supporting and guiding Young People but we don’t know if we are good at this. </p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>Most managers are good at supporting and guiding Young People in our organisation.</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>Leaders and Managers are consistently effective in supporting and guiding Young People in our organisation. Coaching and mentoring is consistently used.</p>		
						</div>
					</div>
				</div>
			</div>
			<div class="row padding-bottom-works accreditation-question">
				<div class="col-md-1">
					<span class="num-i">9</span>
				</div>
				<div class="col-md-11">
					<h4>Does the organisation recognise the contribution that Young People make? </h4>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>We have no policies or processes to recognise the contribution Young People make. </p>	
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
								<p>We recognise the contribution of all our people but not specifically our Young People. </p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>Most managers value the contribution that Young People make, but we could do this more consistently. </p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>Leaders and Managers are consistently effective in recognising the contribution Young People make to our organisation. </p>		
						</div>
					</div>
				</div>
			</div> 
			
			<div class="row text-right">
				<a href="accreditation/section-3" class="btn btn-success">Next Page</a>
			</div>
		</div>
		<div class="big-title text-center col-md-4" style="border: 1px solid rgb(204, 204, 204);">
			<div class="row service-sidebar">
				<div class="col-md-12 big-title">
					<img src="assets/images/howitworks_organization.png">
					<h2>Organizations we partner with</h2>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</p>
				</div>
			</div>
		</div>		
	</div>
</div>
<div class="row">
	<div class="big-title text-left col-md-12 works-title-div" style="padding: 13px 58px !important;">
		<div class="col-md-12">
			<h2>Testimonials</h2>
		</div>
		<div class="row" style="padding-top: 45px ! important; padding-bottom: 17px;">
			<div class="col-md-3">
				<p><i class="fa fa-quote-left"></i> &nbsp;&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
				<b style="float:right;">-Mr.Xyz (Abcd ,City)</b>
			</div>
			<div class="col-md-3">
				<p><i class="fa fa-quote-left"></i> &nbsp;&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
				<b style="float:right;">-Mr.Xyz (Abcd ,City)</b>
			</div>
			<div class="col-md-3">
				<p><i class="fa fa-quote-left"></i> &nbsp;&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
				<b style="float:right;">-Mr.Xyz (Abcd ,City)</b>
			</div>
			<div class="col-md-3">
				<p><i class="fa fa-quote-left"></i> &nbsp;&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
				<b style="float:right;">-Mr.Xyz (Abcd ,City)</b>
			</div>	
		</div>
	</div>
</div>
