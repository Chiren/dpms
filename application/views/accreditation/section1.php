<?php $this->layout('layouts::default') ?>
<?php 
/**
 * Accreditation Section View.
 * Author 	: Xeme IT Solutions
 * Version 	: 1.0
 * 
 * Note : Section 2 & 3 are also use this file for layout
 */
?>
<?php $ci=&get_instance(); ?>
<?php error_reporting(0); $record_num = end($ci->uri->segment_array()); ?>
<div class="container padding-top-page">
	<div class="row">
		<div class="big-title text-left col-md-12 ">
			<h1>Accreditation<span class="accent-color"></span></h1>
		</div>
	</div>
</div>	
<?php
/* Choose form action */
if($record_num=='section-1')
{
	echo"<form action='accreditation/save_section_1' method='post'>";
}
else if($record_num=='section-2')
{
	echo"<form action='accreditation/save_section_2' method='post'>";
}
else if($record_num=='section-3')
{
	echo"<form action='accreditation/save_section_3' method='post'>";
}
/* End choose form action */
?>
<div class="container">				
	<div class="row">
		<div class="big-title text-left col-md-8 ">
			<div class="big-title col-md-12">	
				<?php if($record_num=='section-1') {
				?>
				<h2><span class="accent-color">Section 1 </span>Attracting and Recruiting Young People</h2>
				<?php }elseif ($record_num=='section-2'){?>
				<h2><span class="accent-color">Section 2 </span>Supporting, Guiding and Developing Young People</h2>
				<?php }else {?>
				<h2><span class="accent-color">Section 3 </span>Attracting and Recruiting Young People</h2>
				<?php } ?>
				<hr>
			</div>
			<?php
			$i=1;
			foreach($result as $res)
			{ 
				foreach($res as $x)
				{
					 
			?>
			<div class="row padding-bottom-works accreditation-question">
				<div class="col-md-1">
					<span class="num-i"><?php echo $i++;?></span>
				</div>
				<div class="col-md-11">
				<input type="hidden" value="<?php echo $x->que_body; ?>" name="question[]"> 
					<h4><?php echo $x->que_body; ?></h4>
					<?php 
					$ci->load->helper('accreditation');
					$anss=get_answers($x->que_id);
					foreach($anss as $ans)
					{
						
					?>
					 <div class="row">
						<div class="col-md-1 align-rigt">
						<?php if($x->que_type=='1'){ ?>
							<input type="checkbox" value="<?php print_r($ans->ans_body); ?>" name="<?php echo $x->que_id; ?>[]">
						<?php }else{ ?>
							<input type="radio" value="<?php print_r($ans->ans_body); ?>" name="<?php echo $x->que_id; ?>" required>
						<?php } ?>
						</div>
						<div class="col-md-11">
							<p><?php print_r($ans->ans_body); ?></p>	
						</div>
					</div>
					<?php 
						
					}
					?>
				</div>
			</div>
			<?php 
	 			}
			}
			?>
			<div class="row text-right" style="padding-right: 20px">
			<?php 
			if($record_num=='section-1')
				{
					$m='section-2';
				}
			elseif($record_num=='section-2')
				{
					$m='section-3';
				}
			?>
				<!-- <a href="accreditation/<?php //echo $m;?>" class="btn btn-success">Next Page</a>  -->
				<input type="submit" value="Next Page" class="btn btn-success">
			</div>
		</div>
		<div class="big-title text-center col-md-4" style="border: 1px solid rgb(204, 204, 204);">
			<div class="row service-sidebar">
				<div class="col-md-12 big-title">
					<img src="assets/images/howitworks_organization.png">
					<h2>Organizations we partner with</h2>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</p>
				</div>
			</div>
		</div>		
	</div>
</div>
</form>

<div class="row">
	<div class="big-title text-left col-md-12 works-title-div" style="padding: 13px 58px !important;">
		<div class="col-md-12">
			<h2>Testimonials</h2>
		</div>
		<div class="row" style="padding-top: 45px ! important; padding-bottom: 17px;">
			<div class="col-md-3">
				<p><i class="fa fa-quote-left"></i> &nbsp;&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
				<b style="float:right;">-Mr.Xyz (Abcd ,City)</b>
			</div>
			<div class="col-md-3">
				<p><i class="fa fa-quote-left"></i> &nbsp;&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
				<b style="float:right;">-Mr.Xyz (Abcd ,City)</b>
			</div>
			<div class="col-md-3">
				<p><i class="fa fa-quote-left"></i> &nbsp;&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
				<b style="float:right;">-Mr.Xyz (Abcd ,City)</b>
			</div>
			<div class="col-md-3">
				<p><i class="fa fa-quote-left"></i> &nbsp;&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
				<b style="float:right;">-Mr.Xyz (Abcd ,City)</b>
			</div>	
		</div>
	</div>
</div>
