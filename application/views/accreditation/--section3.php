<?php $this->layout('layouts::default') ?>
<div class="container padding-top-page">
	<div class="row">
		<div class="big-title text-left col-md-12 ">
			<h1>Accreditation<span class="accent-color"></span></h1>
		</div>
	</div>
</div>	
<div class="container">
	<div class="row">
		<div class="big-title text-left col-md-8 ">
			<div class="big-title col-md-12">	
				<h2><span class="accent-color">Section 3 </span>Retaining Young People</h2>
				<hr>
			</div>
			<div class="row padding-bottom-works accreditation-question">
				<div class="col-md-1">
					<span class="num-i">1</span>
				</div>
				<div class="col-md-11">
					<h4>Does the investment you make in Young People make a difference to the current and future performance of your organisation?</h4>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>We don’t know, as we don’t evaluate this. </p>	
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
								<p>Some people understand the impact that Young People make on the organisation but we could not describe this consistently. </p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>Most people would be able to describe the impact that Young People make on the success of the organisation but not all. </p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>Leaders, Managers and Young People are able to describe and give examples of the impact on the success of the organisation as a result of Young People. </p>		
						</div>
					</div>
				</div>
			</div>
			<div class="row padding-bottom-works accreditation-question">
				<div class="col-md-1">
					<span class="num-i">2</span>
				</div>
				<div class="col-md-11">
					<h4>Do you successfully retain Young People?</h4>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>We do not retain many of the Young People we recruit. </p>	
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
								<p>We retain some of the Young People we recruit but many leave because there are no career progression opportunities for them. </p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>We retain many of the Young People we recruit but still feel that more could be done to increase our retention figures. </p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>We have high retention for Young People and the majority of the Young People we recruit progress within the organisation. </p>		
						</div>
					</div>
				</div>
			</div>
			<div class="row padding-bottom-works accreditation-question">
				<div class="col-md-1">
					<span class="num-i">3</span>
				</div>
				<div class="col-md-11">
					<h4>Do you continually review and improve the way in which you develop Young People?</h4>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>We do not review or improve the way we develop Young People. </p>	
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
								<p>We review some of our ways of working in relation to Young People, but nothing changes as a result. </p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>We review our ways of working and do make some changes, but we don’t always consider Young People’s views. </p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1 align-rigt">
							<input type="checkbox" value="">
						</div>
						<div class="col-md-11">
							<p>We take account of Young People’s views and make improvements to our strategies as a result. </p>		
						</div>
					</div>
				</div>
			</div>
			<div class="row text-right">
				<a href="accreditation/section-4" class="btn btn-success">Next Page</a>
			</div>
		</div>
		<div class="big-title text-center col-md-4" style="border: 1px solid rgb(204, 204, 204);">
			<div class="row service-sidebar">
				<div class="col-md-12 big-title">
					<img src="assets/images/howitworks_organization.png">
					<h2>Organizations we partner with</h2>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</p>
				</div>
			</div>
		</div>		
	</div>
</div>
<div class="row">
	<div class="big-title text-left col-md-12 works-title-div" style="padding: 13px 58px !important;">
		<div class="col-md-12">
			<h2>Testimonials</h2>
		</div>
		<div class="row" style="padding-top: 45px ! important; padding-bottom: 17px;">
			<div class="col-md-3">
				<p><i class="fa fa-quote-left"></i> &nbsp;&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
				<b style="float:right;">-Mr.Xyz (Abcd ,City)</b>
			</div>
			<div class="col-md-3">
				<p><i class="fa fa-quote-left"></i> &nbsp;&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
				<b style="float:right;">-Mr.Xyz (Abcd ,City)</b>
			</div>
			<div class="col-md-3">
				<p><i class="fa fa-quote-left"></i> &nbsp;&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
				<b style="float:right;">-Mr.Xyz (Abcd ,City)</b>
			</div>
			<div class="col-md-3">
				<p><i class="fa fa-quote-left"></i> &nbsp;&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
				<b style="float:right;">-Mr.Xyz (Abcd ,City)</b>
			</div>	
		</div>
	</div>
</div>
