<?php $this->layout('layouts::default') ?>
<div class="container padding-top-page">
	<div class="row">
		<div class="big-title text-left col-md-12 ">
			<h1>Accreditation<span class="accent-color"></span></h1>
		</div>
	</div>
</div>	
<div class="container">
	<div class="row">
		<div class="big-title text-left col-md-8 ">
			<div class="big-title col-md-12">	
				<h2><span class="accent-color">Section 4 </span>Company Details</h2>
				<hr>
			</div>
			<div class="row padding-bottom-works accreditation-question">
				<div class="col-md-1">
					<span class="num-i">1</span>
				</div>
				<div class="col-md-11">
					<h4>Does the investment you make in Young People make a difference to the current and future performance of your organisation?</h4>
					<div>
						<select>
							<option label=""> 2 - 9</option>
							<option label=""> 10 - 24</option>
							<option label=""> 25 - 49</option>
							<option label=""> 50 - 99</option>
							<option label=""> 100 - 249</option>
							<option label=""> 250 - 499</option>
							<option label=""> 500 - 999</option>
							<option label=""> 1000 - 2499</option>
							<option label=""> 2500 - 4999</option>
							<option label=""> 5000+</option>
						</select>
					</div>
				</div>
			</div>
			<div class="row padding-bottom-works accreditation-question">
				<div class="col-md-1">
					<span class="num-i">2</span>
				</div>
				<div class="col-md-11">
					<h4>Company size</h4>
					<div>
						<select>
							<option label=""> 2 - 9</option>
							<option label=""> 10 - 24</option>
							<option label=""> 25 - 49</option>
							<option label=""> 50 - 99</option>
							<option label=""> 100 - 249</option>
							<option label=""> 250 - 499</option>
							<option label=""> 500 - 999</option>
							<option label=""> 1000 - 2499</option>
							<option label=""> 2500 - 4999</option>
							<option label=""> 5000+</option>
						</select>
					</div>
				</div>
			</div>
			
			<div class="row text-center">
				<a href="account" class="btn btn-success">Submit</a>
			</div>
		</div>
		<div class="big-title text-center col-md-4" style="border: 1px solid rgb(204, 204, 204);">
			<div class="row service-sidebar">
				<div class="col-md-12 big-title">
					<img src="assets/images/howitworks_organization.png">
					<h2>Organizations we partner with</h2>
					<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages</p>
				</div>
			</div>
		</div>		
	</div>
</div>
<div class="row">
	<div class="big-title text-left col-md-12 works-title-div" style="padding: 13px 58px !important;">
		<div class="col-md-12">
			<h2>Testimonials</h2>
		</div>
		<div class="row" style="padding-top: 45px ! important; padding-bottom: 17px;">
			<div class="col-md-3">
				<p><i class="fa fa-quote-left"></i> &nbsp;&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
				<b style="float:right;">-Mr.Xyz (Abcd ,City)</b>
			</div>
			<div class="col-md-3">
				<p><i class="fa fa-quote-left"></i> &nbsp;&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
				<b style="float:right;">-Mr.Xyz (Abcd ,City)</b>
			</div>
			<div class="col-md-3">
				<p><i class="fa fa-quote-left"></i> &nbsp;&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
				<b style="float:right;">-Mr.Xyz (Abcd ,City)</b>
			</div>
			<div class="col-md-3">
				<p><i class="fa fa-quote-left"></i> &nbsp;&nbsp;Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
				<b style="float:right;">-Mr.Xyz (Abcd ,City)</b>
			</div>	
		</div>
	</div>
</div>
