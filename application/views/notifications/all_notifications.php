<?php $this->layout('layouts::default') ?>
<div class="container padding-top-page">
	<div class="row">
		<div class="big-title text-left col-md-6 ">
			<h1>All <span class="accent-color">Notifications</span></h1>
		</div>
	</div>
</div>
<div class="container">
  <table class="table table-hover">
    <tbody>

	<?php 
		foreach($result as $res)
		{
		?>
		<tr>
        	<td><?php print_r(json_decode($res->data)->data); ?></td>
        	<td><?php echo $res->time; ?></td>
    	</tr>
    	<?php 
		}
	?>
       </tbody>
	</table>
	
</div>