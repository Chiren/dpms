<!doctype html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><html lang="en" class="no-js"> <![endif]-->
<html lang="en">

<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">	
	<base href="<?php echo $base_url; ?>" />
	<?php // Meta data ?>
  <!-- Basic -->
  <title><?=$this->e($title)?> | ExpertsOnCALL</title>

  <!-- Define Charset --> 
  <meta name='author' content='Michael Chan (https://github.com/waifung0207)'>
	<meta name='description' content='CI Bootstrap 3'>
	<?=$this->section('meta')?>
    <link href='<?php echo dist_url('app.min.css'); ?>' rel='stylesheet'>
	<link href='<?php echo dist_url('css/responsive.css'); ?>' rel='stylesheet'>
	<link href='<?php echo dist_url('css/font-awesome.min.css'); ?>' rel='stylesheet'>
	<link href='<?php echo dist_url('css/slicknav.css'); ?>' rel='stylesheet'>
	<link href='<?php echo dist_url('css/animate.css'); ?>' rel='stylesheet'>
	<link href='<?php echo dist_url('css/style.css'); ?>' rel='stylesheet'>
	<!--  <link href='<?php //echo dist_url('css/colors/blue.css'); ?>' rel='stylesheet'>
	<link href='<?php //echo dist_url('css/colors/cyan.css'); ?>' rel='stylesheet'-->
	<link href='<?php echo dist_url('css/colors/green.css'); ?>' rel='stylesheet'>
	<!--link href='<?php //echo dist_url('css/colors/jade.css'); ?>' rel='stylesheet'>-->
	<!-- link href='<?php //echo dist_url('css/colors/orange.css'); ?>' rel='stylesheet'>
	<!--  <link href='<?php //echo dist_url('css/colors/peach.css'); ?>' rel='stylesheet'>
	<link href='<?php //echo dist_url('css/colors/pink.css'); ?>' rel='stylesheet'>
	<link href='<?php //echo dist_url('css/colors/purple.css'); ?>' rel='stylesheet'>
	<link href='<?php //echo dist_url('css/colors/red.css'); ?>' rel='stylesheet'>
	<link href='<?php //echo dist_url('css/colors/sky-blue.css'); ?>' rel='stylesheet'>
	<link href='<?php //echo dist_url('css/colors/yellow.css'); ?>' rel='stylesheet'>-->
	
	<?=$this->section('styles')?>

	<?php // Scripts at page start ?>
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<?=$this->section('scripts_head')?>
</head>
<body class="hold-transition skin-blue sidebar-collapse sidebar-mini">
	<?php // Main content (from inner view, or nested layout) ?>
	<?=$this->section('content')?>

	<?php // Scripts at page end ?>
<script src='<?php echo dist_url('js/jquery.fitvids.js'); ?>'></script>	
<script src='<?php echo dist_url('js/owl.carousel.min.js'); ?>'></script>
<script src='<?php echo dist_url('js/nivo-lightbox.min.js'); ?>'></script>
<script src='<?php echo dist_url('js/jquery.isotope.min.js'); ?>'></script>
<script src='<?php echo dist_url('js/jquery.appear.js'); ?>'></script>
<script src='<?php echo dist_url('js/count-to.js'); ?>'></script>
<script src='<?php echo dist_url('js/jquery.textillate.js'); ?>'></script>
<script src='<?php echo dist_url('js/jquery.lettering.js'); ?>'></script>
<script src='<?php echo dist_url('js/jquery.easypiechart.min.js'); ?>'></script>
<script src='<?php echo dist_url('js/jquery.nicescroll.min.js'); ?>'></script>
<script src='<?php echo dist_url('js/jquery.parallax.js'); ?>'></script>
<script src='<?php echo dist_url('js/jquery.slicknav.js'); ?>'></script>
<script src='<?php echo dist_url('js/mediaelement-and-player.js'); ?>'></script>
<script src='<?php echo dist_url('js/script.js'); ?>'></script>
<script src='<?php echo dist_url('app.min.js'); ?>'></script>  
  
	<?=$this->section('scripts_foot')?>

	<?php // Google Analytics ?>
	<?php $this->insert('partials::ga') ?>
</body>
</html>