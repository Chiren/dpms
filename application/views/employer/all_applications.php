<?php $this->layout('layouts::default');
$ci=&get_instance();
$ci->load->helper('jobs');
?>
<style type="text/css">
.card {
    padding-top: 20px;
    margin: 10px 0 20px 0;
    background-color: rgba(214, 224, 226, 0.2);
    border-top-width: 0;
    border-bottom-width: 2px;
    -webkit-border-radius: 3px;
    -moz-border-radius: 3px;
    border-radius: 3px;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

.card .card-heading {
    padding: 0 20px;
    margin: 0;
}

.card .card-heading.simple {
    font-size: 20px;
    font-weight: 300;
    color: #777;
    border-bottom: 1px solid #e5e5e5;
}

.card .card-heading.image img {
    display: inline-block;
    width: 46px;
    height: 46px;
    margin-right: 15px;
    vertical-align: top;
    border: 0;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    border-radius: 50%;
}

.card .card-heading.image .card-heading-header {
    display: inline-block;
    vertical-align: top;
}

.card .card-heading.image .card-heading-header h3 {
    margin: 0;
    font-size: 14px;
    line-height: 16px;
    color: #262626;
}

.card .card-heading.image .card-heading-header span {
    font-size: 12px;
    color: #999999;
}

.card .card-body {
    padding: 0 20px;
    margin-top: 20px;
}

.card .card-media {
    padding: 0 20px;
    margin: 0 -14px;
}

.card .card-media img {
    max-width: 100%;
    max-height: 100%;
}

.card .card-actions {
    min-height: 30px;
    padding: 0 20px 20px 20px;
    margin: 20px 0 0 0;
}

.card .card-comments {
    padding: 20px;
    margin: 0;
    background-color: #f8f8f8;
}

.card .card-comments .comments-collapse-toggle {
    padding: 0;
    margin: 0 20px 12px 20px;
}

.card .card-comments .comments-collapse-toggle a,
.card .card-comments .comments-collapse-toggle span {
    padding-right: 5px;
    overflow: hidden;
    font-size: 12px;
    color: #999;
    text-overflow: ellipsis;
    white-space: nowrap;
}

.card-comments .media-heading {
    font-size: 13px;
    font-weight: bold;
}

.card.people {
    position: relative;
    display: inline-block;
    width: 170px;
    height: 300px;
    padding-top: 0;
    margin-left: 20px;
    overflow: hidden;
    vertical-align: top;
}

.card.people:first-child {
    margin-left: 0;
}

.card.people .card-top {
    position: absolute;
    top: 0;
    left: 0;
    display: inline-block;
    width: 170px;
    height: 150px;
    background-color: #ffffff;
}

.card.people .card-top.green {
    background-color: #53a93f;
}

.card.people .card-top.blue {
    background-color: #427fed;
}

.card.people .card-info {
    position: absolute;
    top: 150px;
    display: inline-block;
    width: 100%;
    height: 101px;
    overflow: hidden;
    background: #ffffff;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

.card.people .card-info .title {
    display: block;
    margin: 8px 14px 0 14px;
    overflow: hidden;
    font-size: 16px;
    font-weight: bold;
    line-height: 18px;
    color: #404040;
}

.card.people .card-info .desc {
    display: block;
    margin: 8px 14px 0 14px;
    overflow: hidden;
    font-size: 12px;
    line-height: 16px;
    color: #737373;
    text-overflow: ellipsis;
}

.card.people .card-bottom {
    position: absolute;
    bottom: 0;
    left: 0;
    display: inline-block;
    width: 100%;
    padding: 10px 20px;
    line-height: 29px;
    text-align: center;
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

.card.hovercard {
    position: relative;
    padding-top: 0;
    overflow: hidden;
    text-align: center;
    background-color: rgba(214, 224, 226, 0.2);
}

.card.hovercard .cardheader {
    background: url("http://lorempixel.com/850/280/nature/4/");
    background-size: cover;
    height: 135px;
}

.card.hovercard .avatar {
    position: relative;
    top: -50px;
    margin-bottom: -50px;
}

.card.hovercard .avatar img {
    width: 100px;
    height: 100px;
    max-width: 100px;
    max-height: 100px;
    -webkit-border-radius: 50%;
    -moz-border-radius: 50%;
    border-radius: 50%;
    border: 5px solid rgba(255,255,255,0.5);
}

.card.hovercard .info {
    padding: 4px 8px 10px;
}

.card.hovercard .info .title {
    margin-bottom: 4px;
    font-size: 14px;
    line-height: 1;
    color: #262626;
    vertical-align: middle;
}

.card.hovercard .info .desc {
    overflow: hidden;
    font-size: 12px;
    line-height: 20px;
    color: #737373;
    text-overflow: ellipsis;
}

.card.hovercard .bottom {
    padding: 0 20px;
    margin-bottom: 17px;
    margin-top:20px;
    background-color:#F36510;
}

.btn{ border-radius: 0%; /*width:32px;*/ height:32px; line-height:18px;  }

.btn-system.border-btn.btn-gray:hover {
    background-color: rgba(255, 255, 255, 0.2);
    border-radius: 0 !important;
    color: rgb(255, 255, 255);
}
.btn-system.border-btn.btn-gray {
    background-color: rgba(0, 0, 0, 0);
    border: none !important;
    color: rgb(255, 255, 255);
}
.bottom {
    padding-right: 0 !important;
    text-align: right;
}
.btn-system {
    padding: 5px 7px !important;
}
.bottom {
    margin-bottom: 0 !important;
}
.first-link:hover {
    background-color: rgb(51, 51, 51) !important;
    color: rgb(238, 238, 238) !important;
}

</style>
<div class="container padding-top-page">
	<div class="row">
		<div class="big-title text-left col-md-12 ">
			<h1>All <span class="accent-color">Applications</span></h1>
		</div>
	</div>
</div>
<div id="content" style="padding-top: 0px">
    <div class="container">
		<div class="row">
	<?php 
		foreach($result as $res)
		{
		?>
     <?php
            $ci=&get_instance();
			$result1=get_job_data($res->job_id);
			foreach($result1 as $r)
			{
				$job_data=json_decode($r->job_data);
			?>
			<div class="col-lg-3 col-sm-6">
            <div class="card hovercard">
                <div class="cardheader" style="background:rgba(0, 0, 0, 0) url('uploads/<?php echo $job_data->file_name; ?>') repeat scroll 0 0 / cover ">

                </div>
                <div class="avatar">
                    <img alt="" src="uploads/rahil.jpg">
                </div>
                <div class="info">
                    <div class="title">
                        <a href="jobs/single/<?php  echo $r->slug; ?>"><?php echo $job_data->title ?></a>
                    </div>
                    <div class="desc"><i class="fa fa-map-marker"></i>&nbsp; <?php echo $job_data->location; ?></div>
                    <div class="desc"><i class="fa fa-tasks"></i>&nbsp; <?php echo $job_data->type; ?></div>
                    <div class="desc"><i class="fa fa-cubes"></i>&nbsp; <?php $cat=get_category($job_data->category); print_r($cat[0]->category_name); ?></div>
                </div>
                <div class="row">
                	<div class="col-md-4 col-sm-4 col-xs-4 col-lg-4"><i class="fa fa-check-circle" style="color: green"></i><br><?php $app=$res->application; $app1=explode(",",$app); print_r(count($app1)); ?> <br> Total <br> Applications</div>
               		<div class="col-md-4 col-sm-4 col-xs-4 col-lg-4"><i class="fa fa-clock-o" style="color: gold"></i><br><?php $pending=get_pending_application($res->job_id); echo $pending[0]->pending; ?> <br> Pending <br> Applications</div>  
                	<div class="col-md-4 col-sm-4 col-xs-4 col-lg-4"><i class="fa fa-users"></i><br><?php echo $job_data->positions; ?> <br> Pos <br> Avilable</div>
                </div>
                <div class="bottom">
                 	<a class="btn-system btn-mini border-btn btn-gray first-link" href="employer-applications/<?php  echo $r->slug; ?>" style="left: 0px ! important; position: absolute; width: 56%; text-align: left; padding-left: 15px ! important; background-color: rgb(238, 238, 238); color: rgb(51, 51, 51); border-radius: 0px;">All Applications</a>
               		<a class="btn-system btn-mini border-btn btn-gray" href="jobs/<?php  echo $r->slug; ?>">View Job Post</a>
               		
                </div>
            </div>
           </div>
			<?php }?>
			<?php 
		}
		?>
		</div>
	</div>
</div>
