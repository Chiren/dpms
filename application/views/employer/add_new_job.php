<?php $this->layout('layouts::default') ?>
<script src="assets/js/jquery-2.1.4.min.js"></script>
<form action="emp_jobs/add_job_process" method="post" enctype="multipart/form-data">
<div class="container padding-top-page">
	<div class="row">
		<div class="big-title text-left col-md-12 ">
			<h1>Add <span class="accent-color">New Job</span></h1>
		</div>
	</div>
</div>

<div class="container">
		<?php 
		$ci=&get_instance();
		$ci->load->library('session');
		$a=$ci->session->flashdata('error');
		if(!empty($a))  
		{
		?>
	<div class="alert alert-danger">
    	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    	<strong>Error !</strong> <?php print_r($ci->session->flashdata('error')); ?>
  	</div>
		<?php 
		}
		?>
	<div class="form-group col-md-12">
		<input type="text" name="title" class='form-control' placeholder="Job Title" required>
	</div>
	<div class="form-group col-md-12">
		<select id="category" name="category">
			<option value="">Please Select Category</option>
			<script>
				$(function(){
						$.ajax({
							'url':'emp_jobs/get_all_categories',
							'success':function(data){
									$("#category").append(data);		
								},
							});
					});
			</script>
		</select>
	</div>
	<div class="form-group col-md-12" id="c-skills">
		<script>
			$(function(){
					$("#c-skills").hide();
					$("#category").change(function(){
						if($('#category').val()=="")
						{
							$("#c-skills").hide();
						}
						else
						{
							$("#loader-cat").css('display','block');
							$("#c-skills").show();
							$("#skill-data").hide();
								$.ajax({
									'url':'emp_jobs/get_skills/'+$('#category').val(),
									'success':function(data){
										$("#loader-cat").css('display','none'); 
										$("#skill-data").show();
										$("#skill-data").html(data);	
										},
									});
						}	
						
					});
			});
		</script>
		<div class="col-md-12">
			<label>
				Skills
			</label>
			<img src="assets/images/ajax-loader.gif" style="display:none;" id="loader-cat">	
		</div>
		<div class="col-md-12" id="skill-data">
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<input type="number" placeholder="Positions" name="positions" class="form-control">
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
			<input type="date" placeholder="Starting Date  (E.g. 15/11/2015)" name="startdate" class="form-control slimpicker" id="">
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
		<input type="number" placeholder="Duration (In Month)" name="duration" class="form-control">
		</div>
	</div>
	<div class="col-md-6">
		<div class="form-group">
		<input type="text" placeholder="Bonus Skills (Seprate With , )" name="bonus_skills" class="form-control">
		</div>
	</div>
	<div class="big-title text-left col-md-12 ">
			<h2>Project<span class="accent-color"> Details</span></h2>
	</div>
	<div>
		<div class="col-md-6">
			<div class="form-group">
				<input type="text" placeholder="Location" name="location" class="form-control">
			</div>
			<div class="form-group">
				<p>Working Days</p>
				<label><input type="checkbox" name="working_days[]">Sunday</label>
				<label><input type="checkbox" name="working_days[]">Monday</label>
				<label><input type="checkbox" name="working_days[]">Tuesday</label>
				<label><input type="checkbox" name="working_days[]">Wednesday</label>
				<label><input type="checkbox" name="working_days[]">Thursday</label>
				<label><input type="checkbox" name="working_days[]">Friday</label>
				<label><input type="checkbox" name="working_days[]">Saturday</label>
			</div>
			
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<textarea placeholder="Description" name="description" class="form-control" rows="3"></textarea>
			</div>
		</div>
	</div>
	<div class="col-md-12" style=" padding-bottom: 20px;">
		<div class="col-md-6" style="padding-left: 0">
			<span style=" float: left; padding-right: 15px;">Responsibility</span>
			<script>
			$(function(){
				var count = 1;
				var i=0;
				$("#add-res").click(function(){
					$(".res-data").append("<div class='res-"+count+"'><input type='text' name='responsibility[]' class='form-control' style='width: 70%;float: left; margin: 10px 0;'><span class='input-group-btn'> <!-- <button type='button' class='btn btn-danger' style='margin: 10px 0;' id='remove-"+count+"'><i class='fa fa-close'></i></button> --> </span></div>");
					count++;
				});
			});
			
		
			</script>
			<div class="res-container">
				<a id="add-res" class="btn btn-xs btn-success">Add</a>
				<div class="res-data"></div>
			</div>
		</div>
		<div class="col-md-6" style="padding-left: 0">
			<span style=" float: left; padding-right: 15px;">Advantages</span>
			<script>
			$(function(){
				var count = 1;
				var i=0;
				$("#add-adv").click(function(){
					$(".adv-data").append("<div class='adv-"+count+"'><input type='text' name='advantages[]' class='form-control' style='width: 70%;float: left; margin: 10px 0;'><span class='input-group-btn'> <!-- <button type='button' class='btn btn-danger' style='margin: 10px 0;' id='remove-"+count+"'><i class='fa fa-close'></i></button> --> </span></div>");
					count++;
				});
			});
			
		
			</script>
			<div class="adv-container">
				<a id="add-adv" class="btn btn-xs btn-success">Add</a>
				<div class="adv-data"></div>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="col-md-6"  style="padding-left: 0">
			<div class="form-group">
				<input type="text" class="form-control" name="timing" placeholder="Timing (E.g. 10:00 - 19:00)"> 
			</div>
		</div>
		<div class="col-md-6"  style="padding-left: 0">
			<div class="form-group">
				<p>Type</p>
				<label><input type="radio" name="type" value="Full Time">Full Time</label><br>
				<label><input type="radio" name="type" value="Part Time">Part Time</label><br>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="col-md-6"  style="padding-left: 0">
			<div class="form-group">
				<input type="text" class="form-control" name="reward" placeholder="Stayfund or Reward"> 
			</div>
		</div>
		<div class="col-md-6"  style="padding-left: 0">
			<div class="form-group">
				<input type="text" class="form-control" name="capability" placeholder="Minimum capability"> 				
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="col-md-6"  style="padding-left: 0">
			<div class="form-group">
					<label>Upload Cover Image</label><input type="file" class="" name="userfile"> <?php  $b=$ci->session->flashdata('file_error'); if(!empty($b)) { print_r($b); } ?>
			</div>
		</div>
	</div>
	
	<div class="col-md-12">
		<div class="col-md-6"  style="padding-left: 0">
			<div class="form-group">
				<input type="submit" class="btn btn-submit" value="Submit">
			</div>
		</div>
	</div>
	
</div>
</form>