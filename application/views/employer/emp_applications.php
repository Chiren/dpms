<?php $this->layout('layouts::default'); ?>
<?php
$ci=&get_instance();
$ci->load->helper('jobs');
?>
<div class="container padding-top-page">
	<div class="row">
		<div class="big-title text-left col-md-12 ">
			<h1>Applications <span class="accent-color">Of Employer</span></h1>
		</div>
	</div>
</div>
<!-- Tabular -->
<?php 
$approve=$ci->session->flashdata('approve');
if(!empty($approve))
{
?>
<div class="container">
  <div class="alert alert-success">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Success!</strong> <?php print_r($approve); ?>
  </div>
</div>
<?php 
}
?>
<?php 
$error=$ci->session->flashdata('erroe');
if(!empty($error))
{
?>
<div class="container">
  <div class="alert alert-danger">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <strong>Error!</strong> <?php print_r($error); ?>
  </div>
</div>
<?php 
}
?>
<div class="container">
	<table class="table table-hover">
		<thead>
		<tr>
			<th>Application Date</th>
			<th>Job Title</th>
			<th>User Name</th>
			<th>Status</th>
			<th>Action</th>
		</tr>
		</thead>
		<tbody>
		<?php 
		foreach($result as $res)
		{
		?>
		<tr>
			<td><?php echo $res->date; ?></td>
			<td><?php $res1=get_jobs($res->job_id); ?><a href="jobs/<?php echo $res1[0]->slug; ?>"><?php echo $res1[0]->slug; ?></a></td>
			<td><?php $res2=get_users($res->user_id);  print_r($res2[0]->first_name." ".$res2[0]->last_name);?></td>
			<td>
				<?php if($res->status=='pending'){ ?>
					<span class="label label-warning">Pending</span>
				<?php } elseif($res->status=='approved'){?>
					<span class="label label-success">Approved</span>
				<?php } elseif($res->status=='ignored'){?>
					<span class="label label-danger">Ignored</span>
				<?php }?>
			</td>
			<td><a href="emp_jobs/approve_application/<?php echo $res->application_id ?>" class="btn btn-success btn-xs <?php if($res->status=='approved') echo "disabled";?>" onclick="return confirm('Sure you wants to Approve this Application?')">Approve</a> &nbsp;&nbsp;&nbsp;
				<a href="emp_jobs/ignore_application/<?php echo $res->application_id ?>" class="btn btn-danger btn-xs  <?php if($res->status=='ignored') echo "disabled";?>" onclick="return confirm('Sure you wants to Ignore this Application?')">Ignore</a></td>
		</tr>
		<?php 
		}
		?>
		</tbody>
	</table>
	
</div>
