<script src="assets/js/jquery-2.1.4.min.js"></script>
<!-- 
 <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
<div class="container">

	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href=""><?php /*echo $site_name; ?></a>
	</div>

	<div class="navbar-collapse collapse">

		<ul class="nav navbar-nav">
			<?php foreach ($menu as $parent => $parent_params): ?>

				<?php if (empty($parent_params['children'])): ?>

					<?php $active = ($current_uri==$parent_params['url'] || $ctrler==$parent); ?>
					<li <?php if ($active) echo 'class="active"'; ?>>
						<a href='<?php echo $parent_params['url']; ?>'>
							<?php echo $parent_params['name']; ?>
						</a>
					</li>

				<?php else: ?>

					<?php $parent_active = ($ctrler==$parent); ?>
					<li class='dropdown <?php if ($parent_active) echo 'active'; ?>'>
						<a data-toggle='dropdown' class='dropdown-toggle' href='#'>
							<?php echo $parent_params['name']; ?> <span class='caret'></span>
						</a>
						<ul role='menu' class='dropdown-menu'>
							<?php foreach ($parent_params['children'] as $name => $url): ?>
								<li><a href='<?php echo $url; ?>'><?php echo $name; ?></a></li>
							<?php endforeach; ?>
						</ul>
					</li>

				<?php endif; ?>

			<?php endforeach; ?>
		</ul>

		<?php if ( !empty($available_languages) ): ?>
			<ul class="nav navbar-nav navbar-right">
				<li><a onclick="return false;">Current Language: <?php echo $language; ?></a></li>
				<li class="dropdown">
					<a data-toggle='dropdown' class='dropdown-toggle' href='#'>
						<i class="fa fa-globe"></i>
						<span class='caret'></span>
					</a>
					<ul role='menu' class='dropdown-menu'>
						<?php foreach ($available_languages as $abbr => $item): ?>
						<li><a href="language/set/<?php echo $abbr; ?>"><?php echo $item['label']; ?></a></li>
						<?php endforeach; ?>
					</ul>
				</li>
			</ul>
		<?php endif; */?>
		
	</div>

</div>
</nav>
-->
<style type="text/css">
.cont-not .not
{
	margin-left: 10px;
}
</style>
   <!-- Start Header Section -->
    <div class="hidden-header"></div>
    <header class="clearfix">

      <!-- Start Top Bar -->
      <div class="top-bar color-bar">
        <div class="container">
          <div class="row">
            <div class="col-md-7">
			
			
				<!-- Start Social Links 
              <ul class="social-list">
                <li>
                  <a class="facebook itl-tooltip" data-placement="bottom" title="Facebook" href="#"><i class="fa fa-facebook"></i></a>
                </li>
                <li>
                  <a class="twitter itl-tooltip" data-placement="bottom" title="Twitter" href="#"><i class="fa fa-twitter"></i></a>
                </li>
                <li>
                  <a class="google itl-tooltip" data-placement="bottom" title="Google Plus" href="#"><i class="fa fa-google-plus"></i></a>
                </li>
                <li>
                  <a class="dribbble itl-tooltip" data-placement="bottom" title="Dribble" href="#"><i class="fa fa-dribbble"></i></a>
                </li>
                <li>
                  <a class="linkdin itl-tooltip" data-placement="bottom" title="Linkedin" href="#"><i class="fa fa-linkedin"></i></a>
                </li>
                <li>
                  <a class="flickr itl-tooltip" data-placement="bottom" title="Flickr" href="#"><i class="fa fa-flickr"></i></a>
                </li>
                <li>
                  <a class="tumblr itl-tooltip" data-placement="bottom" title="Tumblr" href="#"><i class="fa fa-tumblr"></i></a>
                </li>
                <li>
                  <a class="instgram itl-tooltip" data-placement="bottom" title="Instagram" href="#"><i class="fa fa-instagram"></i></a>
                </li>
                <li>
                  <a class="vimeo itl-tooltip" data-placement="bottom" title="vimeo" href="#"><i class="fa fa-vimeo-square"></i></a>
                </li>
                <li>
                  <a class="skype itl-tooltip" data-placement="bottom" title="Skype" href="#"><i class="fa fa-skype"></i></a>
                </li>
              </ul>
              <!-- Start Contact Info -->
              <!--ul class="contact-details">
                <li><a href="#"><i class="fa fa-map-marker"></i> House-54/A, London, UK</a>
                </li>
                <li><a href="#"><i class="fa fa-envelope-o"></i> info@expertsoncall.com</a>
                </li>
                <li><a href="#"><i class="fa fa-phone"></i> +12 345 678 900</a>
                </li>
              </ul-->
              <!-- End Contact Info -->
            </div>
            <!-- .col-md-6 -->
            <div class="col-md-5">
              <!-- Start Social Links -->
			  
			  <ul class="contact-details">
                
                <li><a href="#"><i class="fa fa-user"></i> Free 1st Consultation.<strong> Try Now.</strong></a>
                </li>
                <li><a href="#"><i class="fa fa-user"></i> Login</a>
                </li>
                <li><a href="#"><i class="fa fa-sign-in"></i> Sign up</a>
                </li>
              </ul>
              <!--ul class="login-list">
                <li>
                  <a class="itl-tooltip" data-placement="bottom" title="Login" href="#">Login</i></a>
                </li>
                <li>
                  <a class="itl-tooltip" data-placement="bottom" title="Signup for a new account" href="#">Sign up</a>
                </li>
                
              </ul-->
              <!-- End Social Links -->
            </div>
            <!-- .col-md-6 -->
          </div>
          <!-- .row -->
        </div>
        <!-- .container -->
      </div>
      <!-- .top-bar -->
      <!-- End Top Bar -->


      <!-- Start  Logo & Naviagtion  -->
      <div class="navbar navbar-default navbar-top">
        <div class="container">
          <div class="navbar-header">
            <!-- Stat Toggle Nav Link For Mobiles -->
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <i class="fa fa-bars"></i>
            </button>
            <!-- End Toggle Nav Link For Mobiles -->
            <a class="navbar-brand" href="index.html">
            
              <img alt="" src="assets/dist/images/tg-logo.png">
            </a>
          </div>
          <div class="navbar-collapse collapse">
            <!-- Stat Search -->
            <!--div class="search-side">
              <a class="show-search"><i class="fa fa-search"></i></a>
              <div class="search-form">
                <form autocomplete="off" role="search" method="get" class="searchform" action="#">
                  <input type="text" value="" name="s" id="s" placeholder="Search the site...">
                </form>
              </div>
            </div-->
            <!-- End Search -->
            <!-- Start Navigation List -->
            <ul class="nav navbar-nav navbar-right">
              <li>
                <a class="active" href="index.html">HOME</a>
                <!--ul class="dropdown">
                  <li><a class="active" href="index.html">Home Main Version</a>
                  </li>
                  <li><a href="index-01.html">Home Version 1</a>
                  </li>
                  <li><a href="index-02.html">Home Version 2</a>
                  </li>
                  <li><a href="index-03.html">Home Version 3</a>
                  </li>
                  <li><a href="index-04.html">Home Version 4</a>
                  </li>
                  <li><a href="index-05.html">Home Version 5</a>
                  </li>
                  <li><a href="index-06.html">Home Version 6</a>
                  </li>
                  <li><a href="index-07.html">Home Version 7</a>
                  </li>
                </ul-->
              </li>
              <li>
                <a href="">ABOUT</a>
                <ul class="dropdown">
                  <li><a href="">About ExpertsOnCall</a>
                  </li>
                  <li><a href="">Features</a> 
                  </li>
                  <li><a href="">Our Vision</a>
                  </li>

                  
                </ul>
              </li>
              <li>
                <a href="#">HOW IT WORKS</a>
                <!--ul class="dropdown">
                  <li><a href="">For Job Seekers / Students</a>
                  </li>
                  <li><a href="">For Companies</a>
                  </li>
                  <li><a href="action-box.html">Action Box</a>
                  </li>
                  <li><a href="testimonials.html">Testimonials</a>
                  </li>
                  <li><a href="latest-posts.html">Latest Posts</a>
                  </li>
                  <li><a href="latest-projects.html">Latest Projects</a>
                  </li>
                  <li><a href="pricing.html">Pricing Tables</a>
                  </li>
                  <li><a href="animated-graphs.html">Animated Graphs</a>
                  </li>
                  <li><a href="accordion-toggles.html">Accordion & Toggles</a>
                  </li>
                </ul-->
              </li>
              <li>
                <a href="#">PRICING</a>
               </li>
              <li>
                <a href="portfolio-3.html">CONTACT</a>
                <!--ul class="dropdown">
                  <li><a href="portfolio-2.html">2 Columns</a>
                  </li>
                  <li><a href="portfolio-3.html">3 Columns</a>
                  </li>
                  <li><a href="portfolio-4.html">4 Columns</a>
                  </li>
                  <li><a href="single-project.html">Single Project</a>
                  </li>
                </ul-->
              </li>
              <!--li>
                <a href="blog.html">Blog</a>
                <ul class="dropdown">
                  <li><a href="blog.html">Blog - right Sidebar</a>
                  </li>
                  <li><a href="blog-left-sidebar.html">Blog - Left Sidebar</a>
                  </li>
                  <li><a href="single-post.html">Blog Single Post</a>
                  </li>
                </ul>
              </li-->
              <!--li><a href="contact.html">Contact</a>
              </li-->
            </ul>
            <!-- End Navigation List -->
          </div>
        </div>

        <!-- Mobile Menu Start -->
        <ul class="wpb-mobile-menu">
          <li>
            <a class="active" href="index.html">Home</a>
            <ul class="dropdown">
              <li><a class="active" href="index.html">Home Main Version</a>
              </li>
              <li><a href="index-01.html">Home Version 1</a>
              </li>
              <li><a href="index-02.html">Home Version 2</a>
              </li>
              <li><a href="index-03.html">Home Version 3</a>
              </li>
              <li><a href="index-04.html">Home Version 4</a>
              </li>
              <li><a href="index-05.html">Home Version 5</a>
              </li>
              <li><a href="index-06.html">Home Version 6</a>
              </li>
              <li><a href="index-07.html">Home Version 7</a>
              </li>
            </ul>
          </li>
          <li>
            <a href="about.html">Pages</a>
            <ul class="dropdown">
              <li><a href="about.html">About</a>
              </li>
              <li><a href="services.html">Services</a>
              </li>
              <li><a href="right-sidebar.html">Right Sidebar</a>
              </li>
              <li><a href="left-sidebar.html">Left Sidebar</a>
              </li>
              <li><a href="404.html">404 Page</a>
              </li>
            </ul>
          </li>
          <li>
            <a href="#">Shortcodes</a>
            <ul class="dropdown">
              <li><a href="tabs.html">Tabs</a>
              </li>
              <li><a href="buttons.html">Buttons</a>
              </li>
              <li><a href="action-box.html">Action Box</a>
              </li>
              <li><a href="testimonials.html">Testimonials</a>
              </li>
              <li><a href="latest-posts.html">Latest Posts</a>
              </li>
              <li><a href="latest-projects.html">Latest Projects</a>
              </li>
              <li><a href="pricing.html">Pricing Tables</a>
              </li>
              <li><a href="animated-graphs.html">Animated Graphs</a>
              </li>
              <li><a href="accordion-toggles.html">Accordion & Toggles</a>
              </li>
            </ul>
          </li>
          <li>
            <a href="portfolio-3.html">Portfolio</a>
            <ul class="dropdown">
              <li><a href="portfolio-2.html">2 Columns</a>
              </li>
              <li><a href="portfolio-3.html">3 Columns</a>
              </li>
              <li><a href="portfolio-4.html">4 Columns</a>
              </li>
              <li><a href="single-project.html">Single Project</a>
              </li>
            </ul>
          </li>
          <li>
            <a href="blog.html">Blog</a>
            <ul class="dropdown">
              <li><a href="blog.html">Blog - right Sidebar</a>
              </li>
              <li><a href="blog-left-sidebar.html">Blog - Left Sidebar</a>
              </li>
              <li><a href="single-post.html">Blog Single Post</a>
              </li>
            </ul>
          </li>
          <li>
            <a href="contact.html">Contact</a>
          </li>
        </ul>
        <!-- Mobile Menu End -->

      </div>
      <!-- End Header Logo & Naviagtion -->

    </header>
    <!-- End Header Section -->
