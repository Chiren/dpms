<?php $this->layout('layouts::default') ?>
<?php //print_r(json_decode($result[0]->job_data));
$data=json_decode($result[0]->job_data);
$ci=&get_instance();
$ci->load->helper('jobs'); 
?>
<style type="text/css">

.padding-company > h4 {
    color: rgb(243, 101, 16);
    font-weight: lighter;
    padding: 15px 0;
}
.padding-company > img
{
	border: 5px solid rgba(255, 255, 255, 0.5);
    border-radius: 50%;
    height: 100px;
    max-height: 100px;
    max-width: 100px;
    width: 100px;
}
.padding-company > label {
    padding: 3px 0 0 0;
}
.blocks-header > label {
    color: rgb(255, 255, 255);
    font-size: 15px;
}
.blocks-header {
    background-color: rgb(243, 101, 16);
    margin: 6px 7px;
    padding: 18px 0;
    text-align: center;
    width: 30%;
    border-radius:10px;
}
.skills li {
    background-color: rgb(243, 101, 16);
    border-radius: 5px;
    color: rgb(255, 255, 255);
    display: inline;
    font-size: 15px;
    margin: 0 6px !important;
    padding: 8px;
}
.skills ul {
    margin-top: 20px;
}
.project p {
    margin-top: 15px;
}
.project label {
    padding-top: 15px;
}
.design-job {
    margin-top: 20px;
}
</style>

<section class="container">
	<div class="row">
		
		<!-- Company Info -->
		<div class="col-md-2" style="background-color: rgba(0, 0, 0, 0.1); border-radius: 10px; padding-bottom: 15px; margin-top: 8%;">
			<div class="col-md-12 padding-company" style="text-align: center;"><h4>Company Name</h4></div>	
			<div class="col-md-12 padding-company" style="text-align: center;"><img src="uploads/rahil.jpg"></div>	
			<div class="col-md-12 padding-company" style="text-align: center;"><i class="fa fa-hospital-o"></i> <label>IT</label></div>	
			<div class="col-md-12 padding-company" style="text-align: center;"><i class="fa fa-flask"></i> <label>Startup</label></div>	
			<div class="col-md-12 padding-company" style="text-align: center;"><i class="fa fa-globe"></i> &nbsp;<a href="#">xemesolutions.com</a></div>	
		</div>
		<!-- Company Info Ends -->
		
		<!-- Job Info -->
		<div class="col-md-10">

			<!-- Job Title -->
			<div class="col-md-12 design-job">
				<div class="big-title text-center">
            		<h1><span class="accent-color"><?php echo $data->title ?></span></h1>
         		 </div>
			</div>
			<!-- Job Title Ends -->

						
			<!-- Cover Image -->
			<div class="col-md-12" style="padding: 0px 4% 4%;;">
				<img src="uploads/<?php echo $data->file_name; ?>" style="width: 100%;">
			</div>
			<!-- Cover Image Ends -->
			
			<!-- Header Blocks -->
			<div class="col-md-12">
				<div class="row" style="padding-left: 4%;">
					<div class="col-md-4 blocks-header">
						<label><i class="fa fa-cubes"></i>&nbsp; Category</label>
					</div>
					<div class="col-md-4 blocks-header">
						<label><i class="fa fa-users"></i>&nbsp; <?php echo $data->positions; ?> Positions</label>
					</div>
					<div class="col-md-4 blocks-header">
						<label><i class="fa fa-calendar"></i>&nbsp; <?php echo $data->duration; ?> Months</label>
					</div>
				</div>
			</div>
			<!-- Header Blocks Ends-->
			
			
			<!-- Required Skills -->
			<div class="col-md-12 skills" style="padding: 4%;">
				<div class="big-title">
            		<h2><i class="fa fa-diamond"></i>&nbsp; Skills<span class="accent-color"> Required</span></h2>
         			<ul>
         			<?php $skills=$data->skills; 
         				foreach($skills as $skill)
         				{
         					$skill=get_skills($skill);
         					foreach($skill as $s)
         					{
         						echo "<li>".$s->skill_name."</li>";
         					}
         				}
         			?>
         			</ul>
         		 </div>
			</div>
			<!-- Required Skills Ends -->
			
			<!-- Project Details -->
			<div class="col-md-12 project" style="padding: 4%;">
				<div class="big-title">
            		<h2><i class="fa fa-paint-brush"></i>&nbsp; Project<span class="accent-color"> Details</span></h2>
         			<div class="col-md-12">
         				<p style="word-wrap: break-word;"><?php echo $data->description ?></p>
         			</div>
         			<hr>
         			<div class="col-md-12">
         				<label><i class="fa fa-map-marker"></i> <b>Location</b> : <?php echo $data->location; ?></label><br>
         				<label style="padding-top: 0px !important"><i class="fa fa-hand-paper-o"></i> <b>Working Days</b> : Monday | Tuesday | Wednesday | Thursday | Friday</label>
         			</div>
         		</div>
         	</div>
         	<!-- Project Details Ends -->
			
			<!-- RESPONSIBILITIES -->
			<div class="col-md-12 project" style="padding: 4%;">
				<div class="big-title">
            		<h2><i class="fa fa-flag-checkered"></i>&nbsp; Project<span class="accent-color"> Responsibilities</span></h2>
         			<?php 
         				$response=$data->responsibility;
         				foreach($response as $res)
         				{
         					echo"<div class='row' style='padding-top: 5px;'><div class='col-md-1'  style='text-align: right;padding-right: 0'><i class='fa fa-check accent-color'></i></div><div class='col-md-11'>'".$res."'</div></div>";
         				} 
         			?>
         		</div>
         	</div>
			<!-- RESPONSIBILITIES Ends -->
			
			<!-- Mid Blocks -->
			<div class="col-md-12">
				<div class="row" style="padding-left: 4%;">
					<div class="col-md-4 blocks-header">
						<label><i class="fa fa-usd"></i>&nbsp; <?php echo $data->reward; ?> Stipend</label>
					</div>
					<div class="col-md-4 blocks-header">
						<label><i class="fa fa-clock-o"></i>&nbsp; <?php echo $data->type; ?> </label>
					</div>
					<div class="col-md-4 blocks-header">
						<label><i class="fa fa-clock-o fa-spin"></i>&nbsp; <?php echo $data->timing; ?></label>
					</div>
				</div>
			</div>
			<!-- Mid Blocks Ends-->
			
			<!-- Advantages  -->
			<div class="col-md-12 project" style="padding: 4%;">
				<div class="big-title">
            		<h2><i class="fa fa-bolt"></i>&nbsp; Project<span class="accent-color"> Advantages</span></h2>
         			<?php 
         				$advantages=$data->advantages;
         				foreach($advantages as $adv)
         				{
         					echo"<div class='row' style='padding-top: 5px;'><div class='col-md-1'  style='text-align: right;padding-right: 0'><i class='fa fa-check accent-color'></i></div><div class='col-md-11'>'".$adv."'</div></div>";
         				} 
         			?>
         		</div>
         	</div>
			<!-- Advantages Ends -->
			
			<!-- Bonus Skills -->
			<div class="col-md-12 skills" style="padding: 4%;">
				<div class="big-title">
            		<h2><i class="fa fa-plus-circle"></i>&nbsp; Bonus<span class="accent-color"> Skills</span></h2>
         			<ul>
         			<?php $bonus_skills=$data->bonus_skills;
         				  $bonus=explode(",",$bonus_skills);
         				  foreach($bonus as $b)
    	     				{
         							echo "<li>".$b."</li>";
         					}
         			?>
         			</ul>
         		 </div>
			</div>
			<!-- Bonus Skills Ends -->

			<!-- Minimum Capability -->			
			<div class="col-md-12 project" style="padding: 4%;">
				<div class="big-title">
            		<h2><i class="fa fa-flag-checkered"></i>&nbsp; Minimum<span class="accent-color"> Capabilities</span></h2>
         			<?php 
         				$capability=$data->capability;
         				$cap=explode(",",$capability);
         				foreach($cap as $c)
         				{
         					echo"<div class='row' style='padding-top: 5px;'><div class='col-md-1'  style='text-align: right;padding-right: 0'><i class='fa fa-check accent-color'></i></div><div class='col-md-11'>'".$c."'</div></div>";
         				}
         			?>
         		</div>
         	</div>
			<!-- Minimum Capability Ends -->
			
			<!-- Apply Button -->
			<div class="col-md-12">
				<a href="apply/<?php echo $result[0]->slug; ?>" class="btn-system btn-large border-btn" style="margin-bottom: 20px;"><i class="fa fa-rocket"></i> &nbsp;Apply Now</a>
			</div>
			<!-- Apply Button Ends -->			
		</div>
		<!-- Job Info Ends -->
	</div>
</section>